﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LabSolution.WebUI2.Models
{
    public class NoteModelForCreate
    {
        [Required]
        [HiddenInput(DisplayValue = false)]
        public Guid NoteId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "Required")]
        [Display(Name = "Notes", ResourceType = typeof(Resources.Resource))]
        public string Description { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "Required")]
        [Display(Name = "Tag", ResourceType = typeof(Resources.Resource))]
        public string Tag { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "Required")]
        [Display(Name = "Owner", ResourceType = typeof(Resources.Resource))]
        public Guid RecordId { get; set; }
    }
}