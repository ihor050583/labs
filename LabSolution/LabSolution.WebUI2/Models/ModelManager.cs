﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
//using LabSolution.AddressBook.Entities;
//using LabSolution.AddressBook.Concrete;
using LabSolution.WebUI2.Configuration;
using System.Xml;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Linq;
using LabSolution.WebUI2.ServiceReference;
using System.Threading.Tasks;

namespace LabSolution.WebUI2.Models
{
    public class ModelManager
    {
        private AddressBookServiceClient manager;

        public ModelManager()
        {
            manager = new AddressBookServiceClient();
        }

        public RepositoryDetails GetCurentRepository()
        {
            RepositoryDetails curentRepository = null;
            RepositoryDetailsDataContract repo = manager.GetCurentRepository();
            if (repo != null)
            {
                curentRepository = new RepositoryDetails();
                curentRepository.Name = repo.Name;
                curentRepository.Path = repo.Path;
                curentRepository.Type = repo.Type;
            }
            return curentRepository;
        }

        public List<RecordModel> Records
        {
            get
            {
                List<RecordModel> objects = new List<RecordModel>();
                foreach (RecordDataContract obj in manager.GetRecords())
                {
                    objects.Add(new RecordModel() { RecordId = obj.RecordId, FirstName = obj.FirstName, SecondName = obj.SecondName, LastChangeTime = obj.LastChangeTime });
                }
                return objects;
            }
        }

        public List<NoteModel> Notes
        {
            get
            {
                List<NoteModel> objects = new List<NoteModel>();
                foreach (NoteDataContract obj in manager.GetNotes())
                {
                    objects.Add(new NoteModel() { Description = obj.Description, NoteId = obj.NoteId, RecordId = obj.RecordId, Tags = obj.Tags });
                }
                return objects;
            }
        }


        public List<PhoneModel> Phones
        {
            get
            {
                List<PhoneModel> objects = new List<PhoneModel>();
                foreach (PhoneDataContract obj in manager.GetPhones())
                {
                    objects.Add(new PhoneModel() { PhoneNumberId = obj.PhoneNumberId, Name = obj.Name, Phone = obj.Phone, RecordId = obj.RecordId });
                }
                return objects;
            }
        }

        public RecordModel Record(Guid id)
        {
            RecordDataContract record = manager.GetRecords().FirstOrDefault(o => o.RecordId == id);
            if (record == null) return null;
            RecordModel model = new RecordModel();
            model.RecordId = record.RecordId;
            model.FirstName = record.FirstName;
            model.SecondName = record.SecondName;
            model.LastChangeTime = record.LastChangeTime;
            return model;
        }

        public bool SaveRecord(RecordModel model, OldRecord oldRecord)
        {
            var rec = manager.GetRecords().FirstOrDefault(o => o.RecordId == model.RecordId);

            if (rec != null && oldRecord != null &&
                (rec.FirstName != oldRecord.OldFirstName || rec.SecondName !=  oldRecord.OldSecondName ))
            {
                return false;
            }
            RecordDataContract record = new RecordDataContract();
            record.RecordId = model.RecordId;
            record.FirstName = model.FirstName;
            record.SecondName = model.SecondName;
            manager.SaveRecord(record);
            return true;
        }

        public void DeleteRecord(Guid id)
        {
            manager.DeleteRecord(id);
        }

        public PhoneModel Phone(Guid id)
        {
            PhoneDataContract phone = manager.GetPhones().FirstOrDefault(o => o.PhoneNumberId == id);
            if (phone == null) return null;
            PhoneModel model = new PhoneModel();
            model.PhoneNumberId = phone.PhoneNumberId;
            model.Name = phone.Name;
            model.Phone = phone.Phone;
            model.RecordId = phone.RecordId;
            return model;
        }

        public bool SavePhone(PhoneModel model, OldPhone oldPhone)
        {
            PhoneDataContract phone = manager.GetPhones().FirstOrDefault(o => o.PhoneNumberId == model.PhoneNumberId);
            RecordDataContract record = manager.GetRecords().FirstOrDefault(o => o.RecordId == model.RecordId);
            if (oldPhone !=null && phone != null && record != null &&
                (phone.Name != oldPhone.OldName || phone.Phone != oldPhone.OldPhoneNamber || phone.RecordId != oldPhone.OldRecordId))
            {
                return false;
            }

            PhoneDataContract newPhone = new PhoneDataContract();
            newPhone.PhoneNumberId = model.PhoneNumberId;
            newPhone.Name = model.Name;
            newPhone.Phone = model.Phone;
            newPhone.RecordId = model.RecordId;
            manager.SavePhone(newPhone);
            return true;
        }

        public void DeletePhone(Guid id)
        {
            manager.DeletePhone(id);
        }

        public NoteModel Note(Guid id)
        {
            NoteDataContract note = manager.GetNotes().FirstOrDefault(o => o.NoteId == id);
            if (note == null) return null;
            NoteModel model = new NoteModel();
            model.NoteId = note.NoteId;
            model.Description = note.Description;
            model.RecordId = note.RecordId;
            model.Tags = note.Tags;
            return model;
            
        }

        public bool SaveNote(NoteModel model, OldNote oldNote)
        {
            RecordDataContract record = manager.GetRecords().FirstOrDefault(o => o.RecordId == model.RecordId);
            NoteDataContract note = manager.GetNotes().FirstOrDefault(o => o.NoteId == model.NoteId);
            bool tagsEqual = false;
            if (note != null)
            {
                tagsEqual = note.Tags.SequenceEqual(model.Tags);
            }
           
            if (oldNote != null && note != null && record != null &&
                (note.Description != oldNote.OldDescription || note.RecordId != oldNote.OldRecordId || !tagsEqual ))
            {
                return false;
            }

            NoteDataContract newNote = new NoteDataContract();
            newNote.NoteId = model.NoteId;
            newNote.Description = model.Description;
            newNote.Tags = model.Tags;
            newNote.RecordId = model.RecordId;
            manager.SaveNote(newNote);
            return true;
        }

        public void CreateNote(NoteModelForCreate model)
        {
            NoteDataContract note = new NoteDataContract();
            note.NoteId = model.NoteId;
            note.Description = model.Description;
            note.RecordId = model.RecordId;
            note.TagsId = new List<Guid>();
            manager.SaveNote(note);

            manager.SaveTagForNote(model.NoteId, model.Tag);
        }

        public void DeleteNote(Guid id)
        {

            manager.DeleteNote(id);
        }

        public bool DeleteTagForNote(Guid notedId, string name)
        {
            return manager.DeleteTagForNote(notedId, name);

        }

        public bool SaveTagForNote(TagModel tag)
        {
            return manager.SaveTagForNote(tag.NoteId, tag.Name);
        }

        public List<RepositoryDetails> GetRepositories()
        {
            List<RepositoryDetails> repositories = new List<RepositoryDetails>();
           
            foreach (RepositoryDataContract item in manager.GetRepositories())
            {
                repositories.Add(new RepositoryDetails() { Name = item.Name, Type = item.Type, Path = item.Path });
            }

            return repositories;
        }

        public bool SwitchRepository(string repository)
        {
            string serverPath = HttpContext.Current.Request.PhysicalApplicationPath;
            RepositoryDetails repo = GetRepositories().FirstOrDefault(o => o.Name == repository);
            RepositoryDetailsDataContract newRepo = null;
            if(repo != null)
            {
                newRepo = new RepositoryDetailsDataContract();
                newRepo.Name = repo.Name;
                newRepo.Path = repo.Path;
                newRepo.Type = repo.Type;            
            }

            return manager.SwitchRepository(serverPath, newRepo);

        }

        

        public void AddRepository(RepositoryDetails model)
        {
            string filePath = HttpContext.Current.Request.PhysicalApplicationPath + model.Path;
             RepositoryDataContract repo = new RepositoryDataContract() { Name = model.Name, Path = model.Path, Type = model.Type };
             manager.AddRepository(repo, filePath);           
        }


        public async Task<List<RecordModel>> AdvancedSearch(SearchModel model)
        {
            
          SearchModelDataContract coreModel = new SearchModelDataContract();
            coreModel.FirstName = model.FirstName;
            coreModel.SecondName = model.SecondName;
            coreModel.LastChangeTime = model.LastChangeTime;
            coreModel.Sign = model.Sign;
            
            List<RecordDataContract> rec = await manager.AdvancedSearchAsync(coreModel);                
            List<RecordModel> objects = new List<RecordModel>();
            foreach (RecordDataContract obj in rec)
            {
                objects.Add(new RecordModel() { RecordId = obj.RecordId, FirstName = obj.FirstName, SecondName = obj.SecondName, LastChangeTime = obj.LastChangeTime });
            }
            return objects;

        }

        public async Task<List<RecordModel>> SimpleSearch(string name)
        {
           List<RecordDataContract> rec = await  manager.SimpleSearchAsync(name);
            List<RecordModel> objects = new List<RecordModel>();
            foreach (RecordDataContract obj in rec)
            {
                objects.Add(new RecordModel() { RecordId = obj.RecordId, FirstName = obj.FirstName, SecondName = obj.SecondName, LastChangeTime = obj.LastChangeTime });
            }
            return objects;

        }


       
    }
}