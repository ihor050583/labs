﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LabSolution.WebUI2.Models
{
    public class Role
    {
        public Guid RoleId { get; set; }
        public string Name { get; set; }
    }
}