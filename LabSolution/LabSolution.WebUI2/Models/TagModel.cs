﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LabSolution.WebUI2.Models
{
    public class TagModel
    {       

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "Required")]
        [Display(Name = "Tag", ResourceType = typeof(Resources.Resource))]
        public string Name { get; set; }

        [Required]
        [HiddenInput(DisplayValue = false)]
        public Guid NoteId { get; set; }
    }
}