﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace LabSolution.WebUI2.Models
{
    public class NoteModelBinder : IModelBinder
    {        

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            // Получаем поставщик значений
            var valueProvider = bindingContext.ValueProvider;
            string[] separators = { ","," "};

            Guid id = (Guid)valueProvider.GetValue("NoteId").ConvertTo(typeof(Guid));
            string description = (string)valueProvider.GetValue("Description").ConvertTo(typeof(string));

            Guid rec = (Guid)valueProvider.GetValue("RecordId").ConvertTo(typeof(Guid));

            string tag = (string)valueProvider.GetValue("Tags").ConvertTo(typeof(string));
            string[] tags = tag.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            return new NoteModel() { NoteId = id, Description = description, RecordId = rec, Tags = tags.ToList() };
        }
    }
}