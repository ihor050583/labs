﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LabSolution.WebUI2.Models
{
    public class PhoneModel
    {
        ModelManager manager;

        public PhoneModel()
        {
            manager = new ModelManager();
        }

        [Required]
        [HiddenInput(DisplayValue = false)]
        public Guid PhoneNumberId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "Required")]
        [Display(Name = "Name", ResourceType = typeof(Resources.Resource))]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "Required")]
        [Display(Name = "Phone", ResourceType = typeof(Resources.Resource))]
        public string Phone { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "Required")]
        [Display(Name = "Owner", ResourceType = typeof(Resources.Resource))]
        public Guid RecordId { get; set; }

        public RecordModel Record
        {
            get
            {
                return manager.Records.FirstOrDefault(o => o.RecordId == RecordId);

            }

        }
    }
}