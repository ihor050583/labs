﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace LabSolution.WebUI2.Models
{
    public class RepositoryDetails
    {
        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "Required")]
        [Display(Name = "Name", ResourceType = typeof(Resources.Resource))]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "Required")]
        [Display(Name = "Type", ResourceType = typeof(Resources.Resource))]
        public string Type { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "Required")]
        [Display(Name = "Path", ResourceType = typeof(Resources.Resource))]
        public string Path { get; set; }
    }
}