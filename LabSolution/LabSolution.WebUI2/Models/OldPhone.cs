﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LabSolution.WebUI2.Models
{
    public class OldPhone
    {
        public string OldName { get; set; }
        public string OldPhoneNamber { get; set; }
        public Guid OldRecordId { get; set; }
    }
}