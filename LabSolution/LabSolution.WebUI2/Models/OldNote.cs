﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LabSolution.WebUI2.Models
{
    public class OldNote
    {
        public string OldDescription { get; set; }
        public Guid OldRecordId { get; set; }
    }
}