﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace LabSolution.WebUI2.Models
{
    public class SearchModel
    {
        [Display(Name = "FirstName", ResourceType = typeof(Resources.Resource))]
        public string FirstName { get; set; }

        [Display(Name = "SecondName", ResourceType = typeof(Resources.Resource))]
        public string SecondName { get; set; }        

        [DataType(DataType.Date)]
        [Display(Name = "Date", ResourceType = typeof(Resources.Resource))]
        public DateTime LastChangeTime { get; set; }

        public string Sign { get; set; }
    }
}