﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LabSolution.WebUI2.Repositories;

namespace LabSolution.WebUI2.Models
{
    public class User
    {
        UsersManager manager;
        public User()
        {
            manager = new UsersManager();
        }
        public Guid UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime CreationDate { get; set; }
        
        public Guid RoleId { get; set; }
        public Role Role 
        {
            get 
            {
                return manager.Roles.Find(o => o.RoleId == RoleId);
            }
           

        }
    }
}