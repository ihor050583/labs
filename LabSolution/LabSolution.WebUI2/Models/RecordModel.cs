﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LabSolution.WebUI2.Models
{
    public class RecordModel
    {
        ModelManager manager;

        public RecordModel()
        {
            manager = new ModelManager();
        }
        [Required]
        [HiddenInput(DisplayValue = false)]
        public Guid RecordId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                  ErrorMessageResourceName = "Required")]
        [Display(Name = "FirstName", ResourceType = typeof(Resources.Resource))]
        public string FirstName
        {
            get;
            set;
        }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                  ErrorMessageResourceName = "Required")]
        [Display(Name = "SecondName", ResourceType = typeof(Resources.Resource))]
        public string SecondName
        {
            get;
            set;
        }

        public DateTime LastChangeTime
        {
            get;
            set;
        }

        public IEnumerable<PhoneModel> PhoneNumbers
        {
            get
            {
                return manager.Phones.Where(o => o.RecordId == RecordId);
            }
        }

        public IEnumerable<NoteModel> Notes
        {
            get
            {
                return manager.Notes.Where(o => o.RecordId == RecordId);
            }
        }

    }
}