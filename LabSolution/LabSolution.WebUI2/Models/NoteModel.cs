﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LabSolution.WebUI2.Models
{
    public class NoteModel
    {
        ModelManager manager;

        public NoteModel()
        {
            manager = new ModelManager();
        }
        [Required]
        [HiddenInput(DisplayValue = false)]
        public Guid NoteId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "Required")]
        [Display(Name = "Notes", ResourceType = typeof(Resources.Resource))]
        public string Description { get; set; }

       
        public List<string> Tags { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.Resource),
                 ErrorMessageResourceName = "Required")]
        [Display(Name = "Owner", ResourceType = typeof(Resources.Resource))]
        public Guid RecordId { get; set; }

        public RecordModel Record
        {
            get
            {
                return manager.Records.FirstOrDefault(o => o.RecordId == RecordId);

            }

        }
    }
}