﻿using System;
using System.Web.Mvc;
using log4net;



namespace LabSolution.WebUI2.Filters
{
    public class LogAttribute : FilterAttribute, IActionFilter
    {
        public static readonly ILog log = LogManager.GetLogger(typeof(Controller));

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            string action = (string)filterContext.RouteData.Values["action"];
            string controller = (string)filterContext.RouteData.Values["controller"];            
            log.Info(String.Format("Конец выполнения метода {0} контроллера {1}", action, controller));
            
        }

        public void OnActionExecuting(ActionExecutingContext filterContext)
        {            
            string action = (string)filterContext.RouteData.Values["action"];            
            string controller = (string)filterContext.RouteData.Values["controller"];           
            log.Info(String.Format("Начало выполнения метода {0} контроллера {1}",action,controller));
            
        }
    }
}