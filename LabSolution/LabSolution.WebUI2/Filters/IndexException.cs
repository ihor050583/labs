﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;

namespace LabSolution.WebUI2.Filters
{
    public class IndexException : FilterAttribute, IExceptionFilter
    {
        public static readonly ILog log = LogManager.GetLogger(typeof(Controller));
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            filterContext.Result = new RedirectResult("/Home/Error");
            filterContext.ExceptionHandled = true;
            string user = filterContext.HttpContext.User.Identity.Name;
            if (String.IsNullOrEmpty(user))
            log.Error(String.Format("Ошибка: {0}, метод: {1}", filterContext.Exception.Message, filterContext.Exception.TargetSite));
            else log.Error(String.Format("Пользователь {0}, Ошибка: {1}, метод: {2}",user, filterContext.Exception.Message, filterContext.Exception.TargetSite));

        }
    }
}