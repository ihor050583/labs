﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace LabSolution.WebUI2.Configuration
{
    [ConfigurationCollection(typeof(RepositoryElement))]
    public class RepositoriesCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new RepositoryElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((RepositoryElement)(element)).Name;
        }

        public RepositoryElement this[int idx]
        {
            get { return (RepositoryElement)BaseGet(idx); }
        }

        public void Add(string name, string type, string path)
        {
            base.BaseAdd(new RepositoryElement() { Name = name, Type = type, Path = path });

        }



    }
}