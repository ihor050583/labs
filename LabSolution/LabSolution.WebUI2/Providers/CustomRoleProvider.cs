﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using LabSolution.WebUI2.Repositories;
using LabSolution.WebUI2.Models;

namespace LabSolution.WebUI2.Providers
{
    public class CustomRoleProvider : RoleProvider
    {
        UsersManager _db;

        public CustomRoleProvider()
        {
            _db = new UsersManager();

        }

        public override string[] GetRolesForUser(string username)
        {
            string[] role = new string[] { };


            
                // Получаем пользователя
            User user = _db.Users.FirstOrDefault(o => String.Compare(o.Name, username, true) == 0);

                if (user != null)
                {
                    // получаем роль

                    Role userRole = _db.Roles.FirstOrDefault(o => o.RoleId == user.RoleId);

                    if (userRole != null)
                    {
                        role = new string[] { userRole.Name };
                    }
                }            

            else            
                role = new string[] { };
            
            
            return role;
        }



        public override void CreateRole(string roleName)
        {
            Role newRole = new Role() { Name = roleName, RoleId = Guid.NewGuid() };

            _db.AddRole(newRole);
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            bool outputResult = false;
            // Находим пользователя            
                
                    // Получаем пользователя
            User user = _db.Users.FirstOrDefault(o => String.Compare(o.Name, username, true) == 0);

                    if (user != null)
                    {
                        // получаем роль
                        Role userRole = _db.Roles.FirstOrDefault(o => o.RoleId == user.RoleId);

                        //сравниваем
                        if (userRole != null && userRole.Name == roleName)
                        {
                            outputResult = true;
                        }
                    }
                
                else                
                    outputResult = false;
               
            
            return outputResult;
        }




        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            throw new NotImplementedException();
        }

        

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }
        

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}