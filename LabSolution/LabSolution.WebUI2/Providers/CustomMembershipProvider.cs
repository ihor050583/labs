﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using LabSolution.WebUI2.Repositories;
using LabSolution.WebUI2.Models;

namespace LabSolution.WebUI2.Providers
{
    public class CustomMembershipProvider : MembershipProvider
    {
        UsersManager _db;
        public CustomMembershipProvider()
        {
            _db = new UsersManager();

        }


        public override bool ValidateUser(string username, string password)
        {
            
            User user = _db.Users.FirstOrDefault(o =>(String.Compare(o.Name , username, true)==0)  && o.Password == password);

            if (user != null)
                return true;
            else
                return false;

        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {



            User user = _db.Users.FirstOrDefault(o => String.Compare(o.Name , username, true)==0);

            if (user != null)
            {

                MembershipUser memberUser = new MembershipUser("MyMembershipProvider", user.Name, null, user.Email, null, null,
                    false, false, user.CreationDate, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue);
                return memberUser;
            }

            else
            {
                return null;
            }

        }


        public MembershipUser CreateUser(string username, string password, string email)
        {
            MembershipUser membershipUser = GetUser(username, false);

            if (membershipUser == null)
            {
                try
                {


                    User user = new User();
                    user.UserId = Guid.NewGuid();
                    user.Name = username;
                    user.Email = email;
                    user.Password = password;
                    user.CreationDate = DateTime.UtcNow;
                    user.RoleId = Guid.Parse("51d7c452-7d0d-4bed-9495-0b9eecec0f83");


                    _db.AddUser(user);

                    membershipUser = GetUser(username, false);
                    return membershipUser;

                }
                catch
                {
                    return null;
                }
            }
            return null;
        }


        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new NotImplementedException();
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }


        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }


        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override bool EnablePasswordReset
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EnablePasswordRetrieval
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }



        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override int MaxInvalidPasswordAttempts
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get { throw new NotImplementedException(); }
        }

        public override int MinRequiredPasswordLength
        {
            get { throw new NotImplementedException(); }
        }

        public override int PasswordAttemptWindow
        {
            get { throw new NotImplementedException(); }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get { throw new NotImplementedException(); }
        }

        public override string PasswordStrengthRegularExpression
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get { throw new NotImplementedException(); }
        }

        public override bool RequiresUniqueEmail
        {
            get { throw new NotImplementedException(); }
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }


    }
}