﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LabSolution.WebUI2.Models;
using LabSolution.AddressBook.Abstract;

namespace LabSolution.WebUI2.Repositories
{
    public class UserRepository: IRepository<User>
    {
        private static List<User> users = new List<User>() 
        { 
            new User() { Name = "Ihor", Password = "ihor", Email="ihor1@tut.by", CreationDate = DateTime.UtcNow, 
                UserId=Guid.NewGuid(), RoleId = Guid.Parse("51d7c452-7d0d-4bed-9495-0b9eecec0f83") },
            new User() { Name = "Admin", Password = "admin", Email="Admin1@tut.by", CreationDate = DateTime.UtcNow, 
                UserId=Guid.NewGuid(), RoleId = Guid.Parse("52d7c452-7d0d-4bed-9495-0b9eecec0f83") }
        };

        public List<User> GetAll()
        {
            return users;
        }

        public void Save(User entity)
        {
                  

                users.Add(entity);
            
            
        }

        public void Delete(Guid id)
        {
            User user = users.FirstOrDefault(o => o.UserId == id);
            if (user != null)

                users.Remove(user);
        }
    }
}