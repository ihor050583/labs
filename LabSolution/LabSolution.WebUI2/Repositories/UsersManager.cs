﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LabSolution.AddressBook.Abstract;
using LabSolution.WebUI2.Models;

namespace LabSolution.WebUI2.Repositories
{
    public class UsersManager
    {
        private IRepository<Role> roles;
        private IRepository<User> users;

        public UsersManager()
        {
            roles = new RoleRepository();
            //users = new UserRepository();
            users = new XmlUserRepository();
        }

        public List<User> Users
        {
            get { return users.GetAll(); }
        }

        public List<Role> Roles
        {
            get { return roles.GetAll(); }
        }

        public void AddUser(User user)
        {
            users.Save(user);
        }

        public void AddRole(Role role)
        {
            roles.Save(role);

        }
    }
}