﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LabSolution.WebUI2.Models;
using System.Configuration;
using LabSolution.WebUI2.Configuration;
using LabSolution.AddressBook.Abstract;
using System.Xml;
using LabSolution.WebUI2.ServiceReference;

namespace LabSolution.WebUI2.Repositories
{
    public class XmlUserRepository : IRepository<User>
    {

        private AddressBookServiceClient manager;
        
        public XmlUserRepository()
        {
            manager = new AddressBookServiceClient();            
        }

        public List<User> GetAll()
        {
            List<User> users = new List<User>();
            foreach(UserDataContract item in manager.GetAllUsers())
            {
                users.Add(new User() { Name = item.Name, Password = item.Password, Email = item.Email, RoleId = item.RoleId});
            }
            return users;
        }

        public void Save(User entity)
        {
            UserDataContract userDataContract = new UserDataContract() 
            {
                Name = entity.Name,
                Password = entity.Password,
                Email = entity.Email,
                RoleId = entity.RoleId
                
            };

            manager.AddUser(userDataContract);
            
        }

        public void Delete(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}