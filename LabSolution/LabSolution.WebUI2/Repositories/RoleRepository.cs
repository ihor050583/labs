﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LabSolution.WebUI2.Models;
using LabSolution.AddressBook.Abstract;

namespace LabSolution.WebUI2.Repositories
{
    public class RoleRepository:IRepository<Role>
    {
        private static List<Role> roles = new List<Role>()
        {
            new Role() { Name = "User", RoleId = Guid.Parse("51d7c452-7d0d-4bed-9495-0b9eecec0f83")},
            new Role() {Name ="Admin", RoleId = Guid.Parse("52d7c452-7d0d-4bed-9495-0b9eecec0f83")}
        };

        public List<Role> GetAll()
        {
            return roles;
        }

        public void Save(Role entity)
        {
            roles.Add(entity);
        }

        public void Delete(Guid id)
        {
            Role user = roles.FirstOrDefault(o => o.RoleId == id);
            if (user != null)

                roles.Remove(user);
        }
    }
}