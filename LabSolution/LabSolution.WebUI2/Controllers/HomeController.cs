﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using LabSolution.WebUI2.Filters;
using LabSolution.WebUI2.Models;

namespace LabSolution.WebUI2.Controllers
{

    public class HomeController : Controller
    {
        private ModelManager manager;

        public HomeController()
        {
            manager = new ModelManager();
        }

        public ActionResult Index()
        {
            return View(manager.Records);
        }

        


        public ActionResult SearchPartial()
        {
            SearchModel model = (SearchModel)Session["SearchModel"];
            if (model != null)
                return View("SearchPartial", model);
            else
                return View("SearchPartial");
        }

        [HttpPost]
        public async Task<ActionResult> AdvancedSearch(SearchModel model)
        {
            Session["SearchModel"] = model;
            var result = await manager.AdvancedSearch(model);
            return View("Index", result );
        }

        [HttpPost]
        public async Task<ActionResult> SimpleSearch(string name)
        {
            var result = await manager.SimpleSearch(name);
            return View("Index", result );
        }

        public ActionResult ChangeCulture(string lang)
        {
            string returnUrl = Request.UrlReferrer.AbsolutePath;
            // Список культур
            List<string> cultures = new List<string>() { "ru", "en", "de" };
            if (!cultures.Contains(lang))
            {
                lang = "ru";
            }
            // Сохраняем выбранную культуру в куки
            HttpCookie cookie = Request.Cookies["lang"];
            if (cookie != null)
                cookie.Value = lang;   // если куки уже установлено, то обновляем значение
            else
            {

                cookie = new HttpCookie("lang");
                cookie.HttpOnly = false;
                cookie.Value = lang;
                cookie.Expires = DateTime.Now.AddYears(1);
            }
            Response.Cookies.Add(cookie);
            return Redirect(returnUrl);
        }


        public ActionResult Error()
        {
            return View();
        }

    }
}
