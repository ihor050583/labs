﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LabSolution.WebUI2.Filters;
using LabSolution.WebUI2.Models;

namespace LabSolution.WebUI2.Controllers
{
    [Authorize(Roles = "Admin")]   
    public class PhoneController : Controller
    {
        private static ModelManager manager = new ModelManager();

        public ActionResult PhoneList()
        {
            return View(manager.Phones);
        }

        [HttpGet]
        public ActionResult EditPhone(Guid id)
        {

            PhoneModel model = manager.Phone(id);
            ViewBag.RecordId = manager.Records;
            return View(model);
        }

        [HttpPost]
        public ActionResult EditPhone(PhoneModel model, OldPhone oldPhone)
        {
            if (ModelState.IsValid)
            {
                bool answer = false;
                RecordModel oldRecord = manager.Record(model.RecordId);
                PhoneModel phone = manager.Phone(model.PhoneNumberId);
                if (oldRecord != null && phone != null)
                {
                    lock (manager) answer = manager.SavePhone(model, oldPhone);
                }
                if (answer)
                return RedirectToAction("PhoneList");
                else
                {
                    ModelState.AddModelError("", "Данные были изменены другим пользователем! Вернитесь обратно к списку, чтобы отредактировать обновленные данные.");
                }
            }

            ViewBag.RecordId = manager.Records;
            return View(model);
        }

        [HttpGet]
        public ActionResult CreatePhone()
        {
            PhoneModel phone = new PhoneModel();
            phone.PhoneNumberId = Guid.NewGuid();
            ViewBag.RecordId = manager.Records;
            return View(phone);
        }

        [HttpPost]
        public ActionResult CreatePhone(PhoneModel model)
        {
            if (ModelState.IsValid)
            {
                bool answer = manager.SavePhone(model, null);
                if (answer)
                return RedirectToAction("PhoneList");
                 else
                {
                    ModelState.AddModelError("", "Ошибка!");
                }
            }

            ViewBag.RecordId = manager.Records;
            return View(model);
        }

        [HttpPost]
        public ActionResult DeletePhone(Guid id)
        {

            manager.DeletePhone(id);
            return RedirectToAction("PhoneList");
        }
    }
}
