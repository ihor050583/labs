﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LabSolution.WebUI2.Models;
using LabSolution.WebUI2.Filters;
using System.Threading;
using System.Globalization;

namespace LabSolution.WebUI2.Controllers
{
    [Authorize(Roles = "Admin")]  
    public class RepositoryController : Controller
    {
        private ModelManager manager;

        public RepositoryController()
        {
            manager = new ModelManager();
        }
        [HttpGet]
        public ActionResult SwitchRepository()
        {
            ViewBag.CurentRepository = manager.GetCurentRepository();
            return View(manager.GetRepositories());
        }

        [HttpPost]
        public ActionResult SwitchRepository(string repository)
        {
            bool switched = manager.SwitchRepository(repository);
            if (switched)                
                ViewBag.Message = Resources.Resource.Switched + repository;
            else ViewBag.Message = Resources.Resource.NotSwitched + repository;
            ViewBag.CurentRepository = manager.GetCurentRepository();          
            return View(manager.GetRepositories());
        }

        [HttpGet]
        public ActionResult AddRepository()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddRepository(RepositoryDetails model)
        {
            manager.AddRepository(model);
            return RedirectToAction("SwitchRepository");
        }
    }
}
