﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LabSolution.WebUI2.Filters;
using LabSolution.WebUI2.Models;

namespace LabSolution.WebUI2.Controllers
{
    [Authorize(Roles = "Admin")]   
    public class NoteController : Controller
    {
        private static ModelManager manager = new ModelManager();

        public ActionResult NoteList()
        {

            return View(manager.Notes);
        }

        [HttpGet]
        public ActionResult EditNote(Guid id)
        {
            ViewBag.RecordId = manager.Records;
            NoteModel model = manager.Note(id);   

            return View(model);
        }

        [HttpPost]
        public ActionResult EditNote(NoteModel model, OldNote oldNote)
        {
            if (ModelState.IsValid)
            {
                bool answer = false;
                RecordModel oldRecord = manager.Record(model.RecordId);
                NoteModel note = manager.Note(model.NoteId);
                if (oldRecord != null && note != null)
                {
                    lock (manager) answer = manager.SaveNote(model, oldNote);
                }

                if (answer)
                return RedirectToAction("NoteList");
                else
                {
                    ModelState.AddModelError("", "Данные были изменены другим пользователем! Вернитесь обратно к списку, чтобы отредактировать обновленные данные.");
                }
            }            

           
            ViewBag.RecordId = manager.Records;
            return View(model);
        }

        [HttpGet]
        public ActionResult CreateNote()
        {
            NoteModelForCreate note = new NoteModelForCreate();
            note.NoteId = Guid.NewGuid();

            ViewBag.RecordId = manager.Records;
            return View(note);
        }

        [HttpPost]
        public ActionResult CreateNote(NoteModelForCreate note)
        {
            if (ModelState.IsValid)
            {
                manager.CreateNote(note);
                return RedirectToAction("NoteList");
            }

            ViewBag.RecordId = manager.Records;
            return View(note);
        }

        [HttpPost]
        public ActionResult DeleteNote(Guid id)
        {
            manager.DeleteNote(id);
            return RedirectToAction("NoteList");
        }

        [HttpPost]
        public ActionResult DeleteTag(Guid id, string tag)
        {
            if (!manager.DeleteTagForNote(id, tag))
            {
                return RedirectToAction("NoteList");
            }
            
            ViewBag.RecordId = manager.Records;
            NoteModel model = manager.Note(id);            
            return View("EditNote", model);
        }

        [HttpGet]
        public ActionResult CreateTag(Guid id)
        {
            TagModel tag = new TagModel();           
            tag.NoteId = id;
            return View(tag);
        }

        [HttpPost]
        public ActionResult CreateTag(TagModel tag)
        {
            if (ModelState.IsValid)
            {
                if (!manager.SaveTagForNote(tag))
                {
                    return RedirectToAction("NoteList");
                }
                
                ViewBag.RecordId = manager.Records;
                NoteModel model = manager.Note(tag.NoteId);
                return View("EditNote", model);
            }
            
            return View(tag);
        }
    }
}
