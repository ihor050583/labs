﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using LabSolution.WebUI2.Models;
using LabSolution.WebUI2.Repositories;
using LabSolution.WebUI2.Providers;
using LabSolution.WebUI2.Filters;


namespace LabSolution.WebUI.Controllers
{
      
    public class AccountController : Controller
    {
        UsersManager manager;

        public AccountController()
        {
            manager = new UsersManager();
        }

       
        public ActionResult Login()
        {
            
            if (HttpContext.User.Identity.IsAuthenticated)
            {                
                return RedirectToAction("Index", "Home");                
            }

            else  return View();
        }

       

        [HttpPost]            
        public ActionResult Login(LabSolution.WebUI2.Models.LoginModel model)
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.UserName, model.Password))
                {                   
                    
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);

                    if (Roles.IsUserInRole(model.UserName, "Admin"))
                    {
                        return RedirectToAction("RecordList", "Record");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Неправильный пароль или логин");
                }
            }
            return View(model);
        }


        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();            
            return RedirectToAction("Index", "Home");
        }

        
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                MembershipUser membershipUser = ((CustomMembershipProvider)Membership.Provider).CreateUser(model.UserName, model.Password,model.Email);

                if (membershipUser != null)
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Ошибка при регистрации! Пользователь с таким именем уже существует.");
                }
            }
            return View(model);
        }
       
       
      

       

       

       
    }
}
