﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LabSolution.WebUI2.Filters;
using LabSolution.WebUI2.Models;
using System.Threading.Tasks;

namespace LabSolution.WebUI2.Controllers
{
    [Authorize(Roles = "Admin")]    
    public class RecordController : Controller
    {
        private static ModelManager manager = new ModelManager();

        
        public ActionResult RecordList()
        {
            return View(manager.Records);
        }

        [HttpGet]
        public ActionResult EditRecord(Guid id)
        {
             
            RecordModel model = manager.Record(id);
            return View(model);
        }

        [HttpPost]
        public  ActionResult EditRecord(RecordModel model, OldRecord oldModel)
        {
           
           
            if (ModelState.IsValid)
            {
                bool answer = false;

                // Заблокировать вызовы метода SaveRecord
                RecordModel oldRecord = manager.Record(model.RecordId);
                if (oldRecord != null)
                {
                    lock (manager) answer = manager.SaveRecord(model, oldModel);
                    
                }
                if (answer)
                    return RedirectToAction("RecordList");
                else
                {
                    ModelState.AddModelError("", "Данные были изменены другим пользователем! Вернитесь обратно к списку, чтобы отредактировать обновленные данные.");
                }
                
            }


            return View(model);
        }

        [HttpGet]
        public ActionResult CreateRecord()
        {
            RecordModel record = new RecordModel();
            record.RecordId = Guid.NewGuid();

            return View(record);
        }

        [HttpPost]
        public ActionResult CreateRecord(RecordModel model)
        {

            if (ModelState.IsValid)
            {
                // Заблокировать вызовы метода SaveRecord
                bool answer = manager.SaveRecord(model, null);
                
                if (answer)
                    return RedirectToAction("RecordList");
                else
                {
                    ModelState.AddModelError("", "Ошибка!");
                }

            }


            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteRecord(Guid id)
        {

            manager.DeleteRecord(id);


            return RedirectToAction("RecordList");
        }

    }
}
