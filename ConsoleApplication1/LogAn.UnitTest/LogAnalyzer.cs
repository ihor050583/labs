﻿
namespace LogAn.UnitTest
{
    using System;

    using NUnit.Framework;

    [TestFixture]
    public class LogAnalyzerTest
    {
        [Test]
        public void IsValidLogFileName_BadExtension_ReturnsFalse()
        {
            LogAnalyzer analyzer = new LogAnalyzer();
            bool result = analyzer.IsValidLogFileName("filewithbadextension.foo");
            Assert.False(result);

        }

        [Test]
        public void
            IsValidLogFileName_GoodExtensionLowercase_ReturnsTrue()
        {
            LogAnalyzer analyzer = new LogAnalyzer();
            bool result = analyzer
                  .IsValidLogFileName("filewithgoodextension.slf");
            Assert.True(result);
        }
        [Test]
        public void IsValidLogFileName_GoodExtensionUppercase_ReturnsTrue()
        {
            LogAnalyzer analyzer = new LogAnalyzer();
            bool result =
                analyzer
                .IsValidLogFileName("filewithgoodextension.SLF");
            Assert.True(result);
        }

        [TestCase("filewithgoodextension.SLF", true)]
        [TestCase("filewithgoodextension.slf", true)]
        [TestCase("filewithbadextension.foo", false)]

        public void IsValidLogFileName_ValidExtensions_ReturnsTrue(string file, bool expected)
        {
            var analyzer = new LogAnalyzer();

            var result = analyzer.IsValidLogFileName(file);

            Assert.AreEqual(expected, result);
        }

        [Test]
        [ExpectedException(typeof(ArgumentException),
          ExpectedMessage = "filename has to be provided")]
        public void IsValidFileName_EmptyFileName_ThrowsException()
        {
            var analyzer = new LogAnalyzer();

           analyzer.IsValidLogFileName(string.Empty);
        }
    }
}
