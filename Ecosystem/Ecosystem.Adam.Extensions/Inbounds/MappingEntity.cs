﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecosystem.Adam.Extensions.Inbounds
{
    class MappingEntity
    {
        public string AdamName { get; set; }
        public ImportMappingType MappingType { get; set; }
        public string Value { get; set; }
        public List<ValueMappingEntity> ValueMapping { get; set; }

        public MappingEntity()
        {
            MappingType = ImportMappingType.Field;
            ValueMapping = new List<ValueMappingEntity>();
        }

        public override string ToString()
        {
            return "ADAM name=" + AdamName + ", Value=" + Value;
        }
    }
}
