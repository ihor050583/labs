﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.JSGenerator;
using Adam.Core.Records;
using Adam.Core.Classifications;
using Adam.Core.Fields;
using System.Text.RegularExpressions;
using Adam.Core.Search;

namespace Ecosystem.Adam.Extensions.Inbounds
{
    class RecordInjector
    {
        private Application App;

        public RecordInjector(Application app)
        {
            this.App = app;
        }

        public static void UpdateRecord(Application app, Record record, List<MappingEntity> metadata, string filePath = null, Guid? classificationId = null)
        {
            IEnumerable<MappingEntity> classifications = from m in metadata
                                                         where m.MappingType == ImportMappingType.Classification
                                                         select m;

            ClassificationHelper classificationHelper = new ClassificationHelper(app);

            foreach (MappingEntity mapEntClassification in classifications)
            {
                Guid? classificationGuid = classificationHelper.GetId(new ClassificationPath(mapEntClassification.Value));

                if (classificationGuid != null && !record.Classifications.Contains((Guid)classificationGuid))
                {
                    record.Classifications.Add((Guid)classificationGuid);
                }
            }

            UpdateFields(metadata, classificationId, record, app);
        }

        private static void UpdateFields(List<MappingEntity> metadata, Guid? classificationId, Record record, Application app)
        {
            IEnumerable<MappingEntity> fields = from m in metadata
                                                where m.MappingType == ImportMappingType.Field
                                                select m;

            foreach (MappingEntity mapEntField in fields)
            {
                if (record.Fields.Contains(mapEntField.AdamName))
                {
                    switch (record.Fields[mapEntField.AdamName].Definition.DataType)
                    {
                        case DataType.ClassificationList:
                            {
                                string[] classificationsSplit = mapEntField.Value.Split(';');

                                foreach (string classification in classificationsSplit)
                                {
                                    ClassificationHelper ch = new ClassificationHelper(app);
                                    Guid? tmpClassificationId = ch.GetId(classification);

                                    if (tmpClassificationId != null)
                                    {
                                        record.Fields.GetField<ClassificationListField>(mapEntField.AdamName).Clear();
                                        record.Fields.GetField<ClassificationListField>(mapEntField.AdamName)
                                              .Items.Add((Guid)tmpClassificationId);
                                    }
                                    else
                                    {
                                        throw new Exception(
                                            "Cannot set the value for the ClassificationList field type, Classification " +
                                            classification + " could not be found by its name.");
                                    }
                                }
                            }
                            break;

                        case DataType.Date:
                            {
                                DateTime date;
                                if (DateTime.TryParse(mapEntField.Value, out date))
                                {
                                    record.Fields.GetField<DateField>(mapEntField.AdamName).SetValue(date);
                                }
                                else
                                {
                                    throw new Exception(
                                        "Cannot set the value for the Date field type, string " +
                                        mapEntField.Value + " can not be converted to the date.");
                                }
                            }
                            break;

                        case DataType.DateTime:
                            {
                                DateTime dateTime;
                                if (DateTime.TryParse(mapEntField.Value, out dateTime))
                                {
                                    record.Fields.GetField<DateTimeField>(mapEntField.AdamName).SetValue(dateTime);
                                }
                                else
                                {
                                    throw new Exception(
                                        "Cannot set the value for the DateTime field type, string " +
                                        mapEntField.Value + " can not be converted to the date&time.");
                                }
                            }
                            break;

                        case DataType.Duration:
                            {
                                TimeSpan timeSpan;
                                if (TimeSpan.TryParse(mapEntField.Value, out timeSpan))
                                {

                                    record.Fields.GetField<DurationField>(mapEntField.AdamName).SetValue(timeSpan);
                                }
                                else
                                {
                                    throw new Exception(
                                        "Cannot set the value for the Duration field type, string " +
                                        mapEntField.Value + " can not be converted to the duration.");

                                }
                            }
                            break;

                        case DataType.DynamicOptionList:
                            {
                                string[] dynaOptionListSplit = mapEntField.Value.Split(';');

                                foreach (string listItem in dynaOptionListSplit)
                                {
                                    record.Fields.GetField<DynamicOptionListField>(mapEntField.AdamName).Clear();
                                    record.Fields.GetField<DynamicOptionListField>(mapEntField.AdamName)
                                          .Items.Add(listItem);
                                }
                            }
                            break;

                        case DataType.LanguageList:
                            {
                                string[] languageListSplit = mapEntField.Value.Split(';');

                                foreach (string listItem in languageListSplit)
                                {
                                    record.Fields.GetField<DynamicOptionListField>(mapEntField.AdamName).Clear();
                                    record.Fields.GetField<DynamicOptionListField>(mapEntField.AdamName)
                                          .Items.Add(listItem);
                                }
                            }
                            break;

                        case DataType.None:
                            {

                            }
                            break;

                        case DataType.Numeric:
                            {
                                Decimal numeric;
                                if (Decimal.TryParse(mapEntField.Value, out numeric))
                                {
                                    record.Fields.GetField<NumericField>(mapEntField.AdamName).SetValue(numeric);
                                }
                                else
                                {
                                    throw new Exception(
                                        "Cannot set the value for the Numeric field type, string " +
                                        mapEntField.Value + " can not be converted to the Numeric.");
                                }
                            }
                            break;

                        case DataType.OptionList:
                            {
                                if (mapEntField.Value == null)
                                    break;

                                var optionListSplit = mapEntField.Value.Split(';');

                                try
                                {
                                    var isMultiple = record.Fields.GetField<OptionListField>(mapEntField.AdamName).Definition.AcceptMultipleOptions;

                                    record.Fields.GetField<OptionListField>(mapEntField.AdamName).Clear();

                                    if (!isMultiple)
                                    {
                                        if (optionListSplit.Length != 0)
                                        {
                                            record.Fields.GetField<OptionListField>(mapEntField.AdamName)
                                                    .Items.Add(optionListSplit[0]);
                                        }
                                    }
                                    else
                                    {
                                        foreach (var listItem in optionListSplit)
                                        {
                                            if (!String.IsNullOrEmpty(listItem))
                                            {
                                                record.Fields.GetField<OptionListField>(mapEntField.AdamName)
                                                    .Items.Add(listItem);
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {

                                }
                            }
                            break;

                        case DataType.RecordLink:
                            {
                            }
                            break;

                        case DataType.RecordList:
                            {
                            }
                            break;

                        case DataType.Text:
                            {
                                if (record.Fields.GetField<TextField>(mapEntField.AdamName).Definition.MultiLine == false)
                                {
                                    record.Fields.GetField<TextField>(mapEntField.AdamName).SetValue(mapEntField.Value);
                                }
                                else
                                {
                                    var separator = "{__#NewLine#__}";

                                    var pattern = string.Empty;

                                    foreach (var c in separator)
                                    {
                                        pattern += string.Format("[{0}]", c);
                                    }

                                    var input = mapEntField.Value.Replace(@"\\", separator);

                                    var strs = Regex.Split(input, pattern, RegexOptions.IgnoreCase);

                                    var res = string.Empty;

                                    for (int i = 0; i < strs.Length; i++)
                                    {
                                        res += strs[i];

                                        if (i != strs.Length - 1)
                                        {
                                            res += Environment.NewLine;
                                        }
                                    }

                                    record.Fields.GetField<TextField>(mapEntField.AdamName).SetValue(res);
                                }
                            }
                            break;

                        case DataType.Time:
                            {
                                TimeSpan time;
                                if (TimeSpan.TryParse(mapEntField.Value, out time))
                                {

                                    record.Fields.GetField<DurationField>(mapEntField.AdamName).SetValue(time);
                                }
                                else
                                {
                                    throw new Exception(
                                        "Cannot set the value for the Time field type, string " +
                                        mapEntField.Value + " can not be converted to the time.");

                                }
                            }
                            break;

                        case DataType.UserGroupList:
                            {
                            }
                            break;

                        case DataType.UserList:
                            {
                            }
                            break;

                    }
                }
            }
        }

        public static void UpdateLatestMaster(string filePath, Record record)
        {
            if (record.Files.LatestMaster != null)
            {
                record.Files.ReplaceFile(filePath, record.Files.LatestMaster.Id);
            }
            else
            {
                record.Files.AddFile(filePath);
            }
        }

        public static void UpdateLatestMaster(string filePath, Record record, FileAddOptions fileAddOptions)
        {
            if (record.Files.LatestMaster != null)
            {
                record.Files.ReplaceFile(filePath, record.Files.LatestMaster.Id, fileAddOptions);
            }
            else
            {
                record.Files.AddFile(filePath, fileAddOptions);
            }
        }

        internal static IList<Guid> GetRecords(string filenName, Guid classificationId, Application app)
        {
            return GetRecords(filenName, new[] { classificationId }, app);
        }

        internal static IList<Guid> GetRecords(string filenName, Guid[] classificationIds, Application app)
        {
            RecordHelper rh = new RecordHelper(app);
            StringBuilder sb = new StringBuilder(String.Format("LatestVersionOfMasterFile.FileName=\"{0}\" ", filenName));

            foreach (Guid classificationId in classificationIds)
            {
                sb.Append(String.Format("AND Classification.Id={0} ", classificationId));
            }

            return rh.GetIds(new SearchExpression(sb.ToString()));
        }

        public static void UpdateFileVersions(Record record, string path, string filename)
        {
            if (record.Files.VersionCount > 1)
            {
                record.Files.AddVersion(path, record.Files.LatestMaster.Id);
            }
        }
    }
}
