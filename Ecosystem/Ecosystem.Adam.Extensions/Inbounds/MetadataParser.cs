﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Ecosystem.Adam.Extensions.Inbounds
{
    class MetadataParser
    {
        public static List<MappingEntity> ParseMetadata(string pathToXml, string filename)
        {
            List<MappingEntity> result = new List<MappingEntity>();

            XDocument doc = XDocument.Load(pathToXml);

            var global = doc.Root.Element("global");
            var file = from e in doc.Root.Elements("file")
                       where e.Attribute("name") != null && e.Attribute("name").Value == filename
                       select e;

            if (file != null)
            {
                var fields = file.Elements("field");
                var classifications = file.Elements("classification");

                foreach (var f in fields)
                {
                    if (f.Attribute("name") == null)
                        continue;

                    MappingEntity me = new MappingEntity();
                    me.AdamName = f.Attribute("name").Value;
                    me.Value = f.Value;
                    me.MappingType = ImportMappingType.Field;
                    result.Add(me);
                }

                foreach (var c in classifications)
                {
                    if (c.Value == "")
                        continue;

                    MappingEntity me = new MappingEntity();
                    me.Value = c.Value;
                    me.MappingType = ImportMappingType.Classification;
                    result.Add(me);
                }
            }

            if (global != null)
            {
                var fields = global.Elements("field");
                var classifications = global.Elements("classification");

                foreach (var f in fields)
                {
                    if (f.Attribute("name") == null)
                        continue;

                    if (result.Where(r => (r.MappingType == ImportMappingType.Field) && (r.AdamName == f.Attribute("name").Value)).Any())
                        continue;


                    MappingEntity me = new MappingEntity();
                    me.AdamName = f.Attribute("name").Value;
                    me.Value = f.Value;
                    me.MappingType = ImportMappingType.Field;
                    result.Add(me);
                }

                foreach (var c in classifications)
                {
                    if (c.Value == "")
                        continue;

                    if (result.Where(r => (r.MappingType == ImportMappingType.Classification) && (r.Value == c.Value)).Any())
                        continue;

                    MappingEntity me = new MappingEntity();
                    me.Value = c.Value;
                    me.MappingType = ImportMappingType.Classification;
                    result.Add(me);
                }
            }

            return result;
        }

        public static List<MappingEntity> ParseTxtMetadata(string pathToTxt)
        {
            List<MappingEntity> result = new List<MappingEntity>();

            StreamReader reader = new StreamReader(pathToTxt, Encoding.Default);
            string line = String.Empty;
            while ((line = reader.ReadLine()) != null)
            {
                line = line.Trim();
                if (!line.StartsWith("#") &&
                    !String.IsNullOrEmpty(line) &&
                    !String.IsNullOrWhiteSpace(line))
                {
                    int index = line.IndexOf('=');
                    if (index > 0)
                    {
                        string key = line.Substring(0, index).Trim();
                        string value = line.Substring(index + 1).Trim();

                        if (!String.IsNullOrEmpty(key) && !String.IsNullOrWhiteSpace(key))
                        {
                            result.Add(new MappingEntity()
                            {
                                AdamName = key,
                                MappingType = GetMappingType(key),
                                Value = value
                            });
                        }
                    }
                }
            }

            return result;
        }

        private static ImportMappingType GetMappingType(string key)
        {
            return key.Equals("Classification", StringComparison.OrdinalIgnoreCase) ?
                ImportMappingType.Classification : ImportMappingType.Field;
        }
    }
}
