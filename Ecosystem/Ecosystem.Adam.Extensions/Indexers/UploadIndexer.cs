﻿using Adam.Core;
using Adam.Core.Indexer;
using Adam.Core.MediaEngines;
using Adam.Core.Records;
using Adam.Tools.LogHandler;
using Ecosystem.Adam.Extensions.Inbounds;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecosystem.Adam.Extensions.Indexers
{
    class UploadIndexer : IndexMaintenanceJob
    {
        private Application app;

        private string metadataDeleteFile = String.Empty;
        private string previewDeleteFile = String.Empty;
        private bool IsFolder = false;

        private List<string> Previews;
        private const string AdditionalFolderPath = @"\AdditionalFiles";

        public UploadIndexer(Application app)
            : base(app)
        {
            this.app = app;

            Previews = new List<string>() { "_preview.png", "_preview.jpg", "_preview.jpeg" };
        }

        protected override void OnPreCatalog(PreCatalogEventArgs e)
        {
            if (!this.Task.ClassifyIn.HasValue)
            {
                throw new Exception(
                    string.Format("UploadIndexer error. Failed to get the classification from the Indexer configuration, possible cause indexer: {0} has incorrect setting in the field 'Classify in'", this.Task.Name)
                    );
            }

            e.Action = CatalogAction.Manual;

            base.OnPreCatalog(e);
        }

        protected override void OnCatalog(CatalogEventArgs e)
        {
            try
            {
                string filename = Path.GetFileNameWithoutExtension(e.Path);
                string directory = Path.GetDirectoryName(e.Path);

                Guid? clnId = GetCln(e.Path);

                List<Record> records = GetRecord(Path.GetFileName(e.Path), clnId.Value);

                foreach (Record record in records)
                {
                    AddCln(record, clnId.Value);
                    AddFile(record, e.Path, filename, GetPreviewPath(e.Path));
                    AddAdditionalFiles(record, filename, directory);

                    List<MappingEntity> enitiesList = GetEnitiesList(e.Path, Path.GetFileName(e.Path), directory);
                    RecordInjector.UpdateRecord(app, record, enitiesList, e.Path, clnId.Value);

                    record.Save();
                }

                ForceDeleteFiles();
            }
            catch (Exception ex)
            {
                throw new Exception(
                   string.Format("UploadIndexer error: {0}. TaskName:{1}", ex.ToString(), this.Task.Name)
                   );
            }

            base.OnCatalog(e);
        }

        private string GetPreviewPath(string filePath)
        {
            var path = Path.GetDirectoryName(filePath) + "\\" + Path.GetFileNameWithoutExtension(filePath);

            previewDeleteFile =
                (from item in Previews
                 where System.IO.File.Exists(path + item)
                 select path + item).FirstOrDefault();

            return previewDeleteFile;
        }

        private Guid? GetCln(string path)
        {
            Guid? clnId = this.Task.ClassifyIn;
            if (this.Task.ClassifyFolders)
            {
                clnId = base.CreateClassificationFromFolder(path);
                if (!clnId.HasValue)
                {
                    clnId = this.Task.ClassifyIn;
                }
            }

            return clnId;
        }

        private void AddCln(Record record, Guid clnId)
        {
            if (!record.Classifications.Contains(clnId))
            {
                record.Classifications.Add(clnId);
            }
        }

        private void AddFile(Record record, string path, string filename, string previewPath)
        {
            if (System.IO.File.Exists(previewPath))
            {
                RecordInjector.UpdateLatestMaster(path, record, GetFileAddOptions(previewPath));
            }
            else
            {
                RecordInjector.UpdateLatestMaster(path, record);
            }

            RecordInjector.UpdateFileVersions(record, path, filename);
        }

        private FileAddOptions GetFileAddOptions(string previewPath)
        {
            var options = new FileAddOptions();

            if (System.IO.File.Exists(previewPath))
            {
                options.Previews.Add(new Preview(previewPath, 1));
            }

            return options;
        }

        private List<Record> GetRecord(string filename, Guid clnId)
        {
            List<Record> records = new List<Record>();

            List<Guid> ids = RecordInjector.GetRecords(filename, clnId, this.App) as List<Guid>;

            foreach (var id in ids)
            {
                Record record = new Record(this.App);
                if (record.TryLoad(id) == TryLoadResult.Success)
                {
                    records.Add(record);
                }
            }

            if (records.Count == 0)
            {
                Record record = new Record(this.App);
                record.AddNew();
                records.Add(record);
            }

            return records;
        }

        private List<MappingEntity> GetEnitiesList(string path, string filename, string directory)
        {
            List<MappingEntity> enitiesList = new List<MappingEntity>();

            string fileToDelete = String.Empty;

            try
            {
                string pathToXml = GetFilePath(directory, filename, "xml");
                if (!string.IsNullOrEmpty(pathToXml) && System.IO.File.Exists(pathToXml))
                {
                    enitiesList = MetadataParser.ParseMetadata(pathToXml, Path.GetDirectoryName(path));
                    fileToDelete = pathToXml;
                }
                else
                {
                    string pathToTxt = GetFilePath(directory, filename, "txt");
                    if (!string.IsNullOrEmpty(pathToTxt) && System.IO.File.Exists(pathToTxt))
                    {
                        enitiesList = MetadataParser.ParseTxtMetadata(pathToTxt);
                        fileToDelete = pathToTxt;
                    }
                }
            }
            catch (Exception ex)
            {
                App.Log(LogSeverity.Error,
                    String.Format("Error reading a metadata file. File path: {0}. Message: {1}", fileToDelete, ex.Message)
                    );
            }

            this.metadataDeleteFile = fileToDelete;

            if (enitiesList.Count < 1)
            {
                enitiesList.Add(
                    new MappingEntity()
                    {
                        AdamName = String.Empty,
                        MappingType = ImportMappingType.Classification,
                        Value = GetDefaultClassification()
                    });
            }

            return enitiesList;
        }

        private bool IsDelete()
        {
            return this.Task.CatalogMode == CatalogMode.Capture &&
                   this.Task.ActionOnSuccess == ActionOnSuccess.DeleteFile;
        }

        private void ForceDeleteFiles()
        {
            DeleteFile(this.metadataDeleteFile, IsDelete() && !this.IsFolder);
            DeleteFile(this.previewDeleteFile, IsDelete());
        }

        private void DeleteFile(string file, bool isDelete)
        {
            try
            {
                if (isDelete && !String.IsNullOrEmpty(file))
                {
                    System.IO.File.Delete(file);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("UploadIndexer DeleteFile error: {0}. FileName:{1}",
                  ex.ToString(), metadataDeleteFile)
                  );
            }
        }

        private string GetDefaultClassification()
        {
            return ((string)this.App.GetSetting("UploadIndexerDefaultClassification")).Trim();
        }

        protected string GetFilePath(string directory, string filename, string ext)
        {
            string file = String.Format("{0}.{1}", filename, ext);
            string folder = String.Format("*folder.{0}", ext);

            this.IsFolder = false;

            IEnumerable<string> list = Directory.EnumerateFiles(directory, file).ToList();
            if (!list.Any())
            {
                list = Directory.EnumerateFiles(directory, folder).ToList();

                this.IsFolder = list.Any();
            }

            return list.FirstOrDefault();
        }

        private void AddAdditionalFiles(Record record, string filename, string directory)
        {
            var afPath = directory + AdditionalFolderPath + "\\" + filename;

            if (Directory.Exists(afPath))
            {
                var files = Directory.GetFiles(afPath);

                foreach (var file in files)
                {
                    record.Files.LatestMaster.AdditionalFiles.Add(file);
                }

                if (IsDelete())
                {
                    foreach (var file in files)
                    {
                        System.IO.File.Delete(file);
                    }
                }
            }
        }
    }
}
