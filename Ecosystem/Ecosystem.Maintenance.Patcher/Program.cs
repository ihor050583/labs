﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Epam.Ecosystem.Maintenance.Patcher.Core;

namespace Epam.Ecosystem.Maintenance.Patcher
{
    class Program
    {
        static void Main(string[] args)
        {

            PatchLogger logger = new PatchLogger();

            logger.LogInfo("Start processing");

            bool isConfigParams = (args.Length == 0);

            ParamsFactory pf = new ParamsFactory();
            Params prms = pf.GetParams(isConfigParams, args, ConfigurationManager.AppSettings);

            List<PatchRunnerResult> results = PatchRunner.Execute(prms);

            ContextReader.Read(logger, results);

            logger.LogInfo("End processing");

        }
    }
}
