﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Epam.Ecosystem.Maintenance.Patcher.Core;

namespace Epam.Ecosystem.Maintenance.Patcher
{
    public class ContextReader
    {
        public static void Read(PatchLogger logger, List<PatchRunnerResult> results) 
        {
            foreach (var result in results)
            {
                if (result.Success)
                {
                    logger.LogInfo(String.Format("Scenario status: Success. MetaDataLocation: {0}, Type: {1}, Name: {2}",
                        result.Context.Scenario.MetaDataLocation, result.Context.Scenario.Type, result.Context.Scenario.Name));
                    logger.LogInfo("-------------------------------------------------------------------------------------------------------");

                    var patchTypes = result.Context.DataList.GroupBy(x => x.ScenarioItem.Type);

                    logger.LogInfo(String.Empty);

                    foreach (var patchType in patchTypes)
                    {
                        var count = GetItemsCount(patchType, result.Context);
                        logger.LogInfo(String.Format("Patch Type: {0}, Items: {1}", patchType.Key, count));

                        foreach (PatchData patchData in patchType)
                        {
                            if (patchData.Patch.Context.PatchResult.ResultType == PatchResultErrorType.Success)
                            {
                                logger.LogInfo(String.Format("\tName: {0}. Status: Success", patchData.ScenarioItem.Name));
                            }
                            else if (patchData.Patch.Context.PatchResult.ResultType == PatchResultErrorType.MetaDataError)
                            {
                                logger.LogError(String.Format("\tName: {0}. Status: Failed. MetaDataError: \n\t[{1}]",
                                    patchData.ScenarioItem.Name, patchData.Patch.Context.PatchResult.Exception.Message));
                            }
                            else if (patchData.Patch.Context.PatchResult.ResultType == PatchResultErrorType.PatchUnexpectedError)
                            {
                                logger.LogError(String.Format("\tName: {0}. Status: Failed. PatchUnexpectedError: \n\t[{1}]",
                                    patchData.ScenarioItem.Name, patchData.Patch.Context.PatchResult.Exception.Message));
                            }

                            LogFailedMetaData(logger, patchData.Patch.Context.FailedMetaData);
                        }

                        logger.LogInfo(String.Empty);
                    }
                }
                else
                {
                    logger.LogError(String.Format("Process status: Failed. Exception: {0}", result.Exception.Message));
                }
            }
        }

        private static int GetItemsCount(IGrouping<string, PatchData> patchType, PatcherContext context)
        {
            int count = 0;
            if (context.Scenario.Type == ScenarioType.XmlToAdam)
            {
                count = (from s in patchType select s).Count();
            }
            else if (context.Scenario.Type == ScenarioType.AdamToXml)
            {
                PatchData patchData = (from s in patchType select s).FirstOrDefault();
                count = patchData.Patch.Context.ItemsCount;
            }
            return count;
        }

        private static List<PatchData> GetTypedList(PatchRunnerResult result, string key)
        {
            return (from s in result.Context.DataList
                    where s.ScenarioItem.Type.Equals(key, StringComparison.OrdinalIgnoreCase)
                    select s).ToList<PatchData>();
        }

        private static void LogFailedMetaData(PatchLogger logger, List<FailedMetaData> list)
        {
            if (list != null && list.Count > 0)
            {
                logger.LogWarning("\tWarnings:");
                        
                foreach (FailedMetaData metaData in list)
                {
                    logger.LogWarning(String.Format("\t\tName: [{0}]. Error: [{1}] Type: [{2}]",
                            metaData.Name, metaData.Error, metaData.Type));

                    if (!String.IsNullOrEmpty(metaData.Message))
                    {
                        logger.LogWarning(String.Format("\t\tMessage: [{0}]", metaData.Message));
                    }

                    logger.LogInfo(String.Empty);
                }
            }
        }
    }
}
