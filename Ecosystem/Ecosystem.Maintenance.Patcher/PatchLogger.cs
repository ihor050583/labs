﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace Epam.Ecosystem.Maintenance.Patcher
{
    public class PatchLogger
    {
         private ILog _logger;

         public PatchLogger()
        {
            log4net.Config.XmlConfigurator.Configure();
            _logger = LogManager.GetLogger("Logger");
        }

        public void LogInfo(string message)
        {
            _logger.Info(message);
        }

        public void LogError(string message)
        {
            _logger.Error(message);
        }

        public void LogWarning(string message)
        {
            _logger.Warn(message);
        }
    }
}
