﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class ConfigValueProvider : IValueProvider
    {
        #region IParamValueProvider Members

        public object Source { get; set; }

        public string GetValue(string key)
        {
            NameValueCollection appSettings = Source as NameValueCollection;
            return appSettings[key];
        }

        #endregion
    }
}
