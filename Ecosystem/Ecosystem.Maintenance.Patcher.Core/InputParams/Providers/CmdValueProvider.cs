﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class CmdValueProvider : IValueProvider
    {
        #region IParamValueProvider Members

        public object Source { get; set; }

        public string GetValue(string key)
        {
            string[] args = Source as string[];
            return GetValueHlp(args, key);
        }

        #endregion

        private string GetValueHlp(string[] args, string key)
        {
            string value = String.Empty;

            for (int i = 0; i < args.Length; i++)
            {
                string paramName = args[i].Substring(0, args[i].IndexOf("="));
                string paramValue = args[i].Substring(args[i].IndexOf("=") + 1, args[i].Length - args[i].IndexOf("=") - 1);

                if (key.Equals(paramName, StringComparison.OrdinalIgnoreCase))
                {
                    value = paramValue;
                    break;
                }
            }

            return value;
        }
    }
}
