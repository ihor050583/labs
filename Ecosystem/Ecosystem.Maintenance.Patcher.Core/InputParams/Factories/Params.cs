﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class Params : FlyweightFactory
    {
        public const string PatcherEnabled = "PatcherEnabled";
        public const string ScenarioPath = "ScenarioPath";
        
        public const string XsdSchemaFolder = "XsdSchemaFolder";
        public const string ScenarioManager = "ScenarioManager";
        

        public const string AdamRegistrationName = "AdamRegistrationName";
        public const string AdamAdminUserName = "AdamAdminUserName";
        public const string AdamAdminPassword = "AdamAdminPassword";


        public const string DebugPatchName = "DebugPatchName";
        public const string DebugScenarioName = "DebugScenarioName";
        public const string EnableDebug = "EnableDebug";

        public const string IsReRunWithWarnings = "IsReRunWithWarnings";
        public const string IsReRunWithErrors = "IsReRunWithErrors";
        

        protected override void InitKeys()
        {
            Keys.Add(PatcherEnabled);
            Keys.Add(ScenarioPath);
            Keys.Add(XsdSchemaFolder);
            Keys.Add(ScenarioManager);
            

            Keys.Add(AdamRegistrationName);
            Keys.Add(AdamAdminUserName);
            Keys.Add(AdamAdminPassword);

            Keys.Add(DebugPatchName);
            Keys.Add(DebugScenarioName);
            Keys.Add(EnableDebug);

            Keys.Add(IsReRunWithWarnings);
            Keys.Add(IsReRunWithErrors);
        }
    }
}
