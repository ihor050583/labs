﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class ParamsFactory 
    {
        public Params GetParams(bool isConfigParams, string[] args, NameValueCollection coll)
        {
            IValueInitializer paramsInitializer = new ValueInitializer();
            paramsInitializer.ValueProvider = InitProvider(isConfigParams, args, coll);

            FlyweightFactory fwf = new Params();
            paramsInitializer.Initialize(ref fwf);

            return (fwf as Params);
        }

        private static IValueProvider InitProvider(bool isConfig, string[] args, NameValueCollection coll)
        {
            IValueProvider valueProvider = null;
            if (isConfig)
            {
                valueProvider = new ConfigValueProvider();
                valueProvider.Source = coll;
            }
            else
            {
                valueProvider = new CmdValueProvider();
                valueProvider.Source = args;
            }
            return valueProvider;
        }
    }
}
