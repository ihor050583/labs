﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adam.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class CompositeRoleName
    {
        private const char CompositeRoleSeparator = '$';
        public RoleType? RoleType { get; set; }
        public string ObjectName { get; set; }

        public static bool IsCompositeRoleName(string roleName)
        {
            return roleName.Split(CompositeRoleSeparator).Length == 2;
        }

        public CompositeRoleName(RoleType roleType, string objectName)
        {
            RoleType = roleType;
            ObjectName = objectName;
        }

        public static bool TryParse(string roleName, out CompositeRoleName compositeRoleName)
        {
            bool retVal = false;

            compositeRoleName = null;
            string[] roleNameParts = roleName.Split(CompositeRoleSeparator);

            if (roleNameParts.Length == 2)
            {
                RoleType roleType;
                if (Enum.TryParse(roleNameParts[0].Replace(".Role", string.Empty), out roleType))
                {
                    compositeRoleName = new CompositeRoleName(roleType, roleNameParts[1]);
                    retVal = true;
                }
            }

            return retVal;
        }

        public string Format(Guid id)
        {
            return string.Format(".Role{0}${1}", RoleType, id);
        }

        public override string ToString()
        {
            return string.Format(".Role{0}${1}", RoleType, ObjectName);
        }
    }
}
