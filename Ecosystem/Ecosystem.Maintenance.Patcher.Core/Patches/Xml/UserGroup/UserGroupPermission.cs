﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adam.Core;
using AManagement = Adam.Core.Management;
using Adam.Core.Settings;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    class UserGroupPermission
    {
        internal static List<FailedMetaData> SetStatus(Guid userGroupId, MetaData.Xml.UserGroup.Permission xPermission, Application app)
        {
            List<FailedMetaData> list = new List<FailedMetaData>();

            RoleSetting aRoleSetting = new RoleSetting(app);
            string name = ResolveCompositeSettingName(xPermission, list, app);

            try
            {
                aRoleSetting.LoadUserGroupValue(name, userGroupId);
                aRoleSetting.Value = xPermission.Status;
                aRoleSetting.Save();
            }
            catch (Exception ex)
            {
                SetError(name, ex.Message, list);
            } 
           
            return list;
        }

        private static string ResolveCompositeSettingName(MetaData.Xml.UserGroup.Permission xPermission, List<FailedMetaData> list, Application app)
        {
            string name = xPermission.Name;
            string identifier = xPermission.Identifier;

            string retVal = name;

            CompositeRoleName compositeRoleName;

            if (CompositeRoleName.TryParse(name, out compositeRoleName))
            {
                switch (compositeRoleName.RoleType)
                {
                    case RoleType.CanAccessDocumentOfFileType:
                        {
                            retVal = SetFileType(name, identifier, list, app, retVal, compositeRoleName);
                        }
                        break;
                    case RoleType.ChangeFieldValuesOfFieldGroup:
                    case RoleType.ReadFieldGroup:
                    case RoleType.ManageExistingFieldGroup:
                        {
                            retVal = SetFieldGroup(name, identifier, list, app, retVal, compositeRoleName);
                        }
                        break;
                    case RoleType.ChangeFieldValuesOfLanguage:
                        {
                            retVal = SetLanguage(name, identifier, list, app, retVal, compositeRoleName);
                        }
                        break;
                    case RoleType.LogOnForSite:
                        {
                            retVal = SetSite(name, identifier, list, app, retVal, compositeRoleName);
                        }
                        break;
                    case RoleType.ReadGroupMembersOfOrganization:
                    case RoleType.ReadGroupProfilesOfOrganization:
                    case RoleType.ManageGroupProfilesOfOrganization:
                    case RoleType.ManageGroupMembersOfOrganization:
                        {
                            retVal = SetOrganization(name, identifier, list, app, retVal, compositeRoleName);
                        }
                        break;
                }
            }
            return retVal;
        }

        private static string SetFileType(string name, string identifier, List<FailedMetaData> list, Application app, string retVal, CompositeRoleName compositeRoleName)
        {
            AManagement.FileType fileType = new AManagement.FileType(app);
            if (fileType.TryLoad(compositeRoleName.ObjectName) == TryLoadResult.Success ||
                (!String.IsNullOrEmpty(identifier) && fileType.TryLoad(identifier) == TryLoadResult.Success))
            {
                retVal = compositeRoleName.Format(fileType.Id);
            }
            else
            {
                SetError(name, String.Format("Can't load file type with name {0}", compositeRoleName.ObjectName), list);
            }
            return retVal;
        }

        private static string SetFieldGroup(string name, string identifier, List<FailedMetaData> list, Application app, string xPermissionName, CompositeRoleName compositeRoleName)
        {
            string retVal = xPermissionName;
            AManagement.FieldGroup fieldGroup = new AManagement.FieldGroup(app);
            if (fieldGroup.TryLoad(compositeRoleName.ObjectName) == TryLoadResult.Success ||
                (!String.IsNullOrEmpty(identifier) && fieldGroup.TryLoad(identifier) == TryLoadResult.Success))
            {
                retVal = compositeRoleName.Format(fieldGroup.Id);
            }
            else
            {
                SetError(name, String.Format("Can't load field group with name {0}", compositeRoleName.ObjectName), list);
            }
            return retVal;
        }

        private static string SetLanguage(string name, string identifier, List<FailedMetaData> list, Application app, string xPermissionName, CompositeRoleName compositeRoleName)
        {
            string retVal = xPermissionName;
            if (!compositeRoleName.ObjectName.Equals("Neutral", StringComparison.OrdinalIgnoreCase))
            {
                var language = new AManagement.Language(app);
                if (language.TryLoad(compositeRoleName.ObjectName) == TryLoadResult.Success ||
                    (!String.IsNullOrEmpty(identifier) && language.TryLoad(identifier) == TryLoadResult.Success))
                {
                    retVal = compositeRoleName.Format(language.Id);
                }
                else
                {
                    SetError(name, String.Format("Can't load language with name {0}", compositeRoleName.ObjectName), list);
                }
            }
            else
            {
                retVal = compositeRoleName.Format(Guid.Empty);
            }
            return retVal;
        }

        private static string SetSite(string name, string identifier, List<FailedMetaData> list, Application app, string xPermissionName, CompositeRoleName compositeRoleName)
        {
            string retVal = xPermissionName;
            var site = new AManagement.Site(app);
            if (site.TryLoad(compositeRoleName.ObjectName) == TryLoadResult.Success ||
                (!String.IsNullOrEmpty(identifier) && site.TryLoad(identifier) == TryLoadResult.Success))
            {
                retVal = compositeRoleName.Format(site.Id);
            }
            else
            {
                SetError(name, String.Format("Can't load site with name {0}", compositeRoleName.ObjectName), list);
            }
            return retVal;
        }

        private static string SetOrganization(string name, string identifier, List<FailedMetaData> list, Application app, string xPermissionName, CompositeRoleName compositeRoleName)
        {
            string retVal = xPermissionName;
            AManagement.Organization organization = new AManagement.Organization(app);
            if (organization.TryLoad(compositeRoleName.ObjectName) == TryLoadResult.Success ||
                (!String.IsNullOrEmpty(identifier) && organization.TryLoad(identifier) == TryLoadResult.Success))
            {
                retVal = compositeRoleName.Format(organization.Id);
            }
            else
            {
                SetError(name, String.Format("Can't load organization with name {0}", compositeRoleName.ObjectName), list);
            }
            return retVal;
        }

        private static void SetError(string name, string message, List<FailedMetaData> list)
        {
            list.Add(new FailedMetaData(name, "UserGroup.Permission.Name", MetaDataErrorType.AdamItemNotFound, message));
        }

        public static string GetIdentifier(string name, Application app)
        {
            string identifier = name;
            CompositeRoleName compositeRoleName;

            if (CompositeRoleName.TryParse(name, out compositeRoleName) && compositeRoleName.RoleType.HasValue)
            {
                switch (compositeRoleName.RoleType)
                {
                    case RoleType.CanAccessDocumentOfFileType:
                        {
                            identifier = GetItemName<AManagement.FileTypeHelper, AManagement.FileType>(compositeRoleName, app);
                        }
                        break;
                    case RoleType.ChangeFieldValuesOfFieldGroup:
                    case RoleType.ReadFieldGroup:
                    case RoleType.ManageExistingFieldGroup:
                        {
                            identifier = GetItemName<AManagement.FieldGroupHelper, AManagement.FieldGroup>(compositeRoleName, app);
                        }
                        break;
                    case RoleType.ChangeFieldValuesOfLanguage:
                        {
                            identifier = GetItemName<AManagement.LanguageHelper, AManagement.Language>(compositeRoleName, app);
                        }
                        break;
                    case RoleType.LogOnForSite:
                        {
                            identifier = GetItemName<AManagement.SiteHelper, AManagement.Site>(compositeRoleName, app);
                        }
                        break;
                    case RoleType.ReadGroupMembersOfOrganization:
                    case RoleType.ReadGroupProfilesOfOrganization:
                    case RoleType.ManageGroupProfilesOfOrganization:
                    case RoleType.ManageGroupMembersOfOrganization:
                        {
                            identifier = GetItemName<AManagement.OrganizationHelper, AManagement.Organization>(compositeRoleName, app);
                        }
                        break;
                }
            }

            return identifier;
        }

        private static string GetItemName<AHelperType, AItemType>(CompositeRoleName compositeRoleName, Application app)
        {
            string identifier = String.Empty;

            dynamic aHelper = (AHelperType)Activator.CreateInstance(typeof(AHelperType), new object[] { app });
            dynamic aItem = (AItemType)Activator.CreateInstance(typeof(AItemType), new object[] { app });

            if (aHelper.Exists(new Guid(compositeRoleName.ObjectName)))
            {
                aItem.Load(new Guid(compositeRoleName.ObjectName));
                identifier = aItem.Name;
            }

            return identifier;
        }
    }
}
