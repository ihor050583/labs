﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Settings;
using AUserGroup = Adam.Core.Management;
using XUserGroup = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.UserGroup;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class UserGroup : XPatch<XUserGroup.UserGroup, AUserGroup.UserGroup>
    {
        protected override void UpdateItem(AUserGroup.UserGroup aUserGroup)
        {
            Guid? aOrganizationId = GetOrganizationId();
            if (aOrganizationId.HasValue)
            {
                this.UpdateName(aUserGroup, this.MetaData);
                UpdateOrganization(aUserGroup, aOrganizationId.Value);
                UpdatePermissions(aUserGroup);
            }
        }

        protected override void PreDeleteItem(AUserGroup.UserGroup aUserGroup)
        {
            RemoveFromMemberships(aUserGroup);
        }

        private void RemoveFromMemberships(AUserGroup.UserGroup aUserGroup)
        {
            AUserGroup.UserGroupHelper ugh = new AUserGroup.UserGroupHelper(this.Context.AdamApplication);
            AUserGroup.UserCollection users = ugh.GetMembers(aUserGroup.Id, new AUserGroup.UserLoadOptions());
            foreach (AUserGroup.User user in users)
            {
                user.Memberships.Remove(aUserGroup.Id);
                user.Save();
            }
        }

        private Guid? GetOrganizationId()
        {
            return GetOrganizationIdHlp(this.MetaData.Organization, "UserGroup.Organization");
        }

        private Guid? GetUpdateOrganizationId()
        {
            return GetOrganizationIdHlp(this.MetaData.UpdateOrganization.Value, "UserGroup.UpdateOrganization");
        }

        private Guid? GetOrganizationIdHlp(string organizationName, string key)
        {
            Guid? id = null;
            AUserGroup.Organization aOrganization = new AUserGroup.Organization(this.Context.AdamApplication);
            if (aOrganization.TryLoad(organizationName) == TryLoadResult.Success)
            {
                id = aOrganization.Id;
            }
            else
            {
                AddErrorCaseToContext(this.MetaData.Name, key, MetaDataErrorType.AdamItemNotFound);
            }
            return id;
        }

        private void UpdateOrganization(AUserGroup.UserGroup aUserGroup, Guid aOrganizationId)
        {
            aUserGroup.OrganizationId = aOrganizationId;
            if (this.MetaData.UpdateOrganization != null)
            {
                Guid? aNewOrganizationId = GetUpdateOrganizationId();
                if (aNewOrganizationId.HasValue)
                {
                    aUserGroup.OrganizationId = aNewOrganizationId.Value;
                }
            }
        }

        private void UpdatePermissions(AUserGroup.UserGroup aUserGroup)
        {
            // cannot work with Permissions until the usergroup exists in adam

            if (this.MetaData.Permissions != null && this.MetaData.Permissions.Count > 0)
            {
                if (aUserGroup.IsNew)
                {
                    this.SaveAdamItem(aUserGroup, aUserGroup.GetType().ToString());
                }

                foreach (XUserGroup.Permission xPermission in this.MetaData.Permissions)
                {
                    List<FailedMetaData> list = UserGroupPermission.SetStatus(aUserGroup.Id, xPermission, this.Context.AdamApplication);
                    AddErrorCaseToContext(list, false);
                }
            }
        }
    }
}
