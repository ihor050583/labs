﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XFieldGroup = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.FieldGroup;
using AManagement = Adam.Core.Management;
using Adam.Core;
using Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class FieldGroup : XPatch<XFieldGroup.FieldGroup, AManagement.FieldGroup>
    {
        protected override void UpdateItem(AManagement.FieldGroup aItem)
        {
            UpdateMembers(aItem);
        }

        private void UpdateMembers(AManagement.FieldGroup aFieldGroup)
        {
            ClearMembers(aFieldGroup);
            FieldDefinitionHelper fdh = new FieldDefinitionHelper(this.Context.AdamApplication);
            foreach (var member in this.MetaData.Members)
            {
                Guid? id = fdh.GetId(member.Name);
                if (id.HasValue)
                {
                    if (!aFieldGroup.Members.Contains(id.Value))
                    {
                        aFieldGroup.Members.Add(id.Value);
                    }
                }
                else
                {
                    AddErrorCaseToContext(member.Name, "FieldGroup.Members.Name", MetaDataErrorType.AdamItemNotFound);
                }
            }
        }

        private void ClearMembers(AManagement.FieldGroup aFieldGroup)
        {
            if (aFieldGroup.Id != AManagement.FieldGroup.AllFieldsId)
            {
                Guid[] ids = aFieldGroup.Members.CopyIdsToArray();
                foreach (Guid id in ids)
                {
                    aFieldGroup.Members.Remove(id);
                }
            }
        }
    }
}
