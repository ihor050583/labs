﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Fields;
using AManagement = Adam.Core.Management;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml;
using AdamCln = Adam.Core.Classifications;
using XClassification = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Classification;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class Classification : Patch<XClassification.Classification>
    {
        protected override void ExecuteHlp()
        {
            ExecItem(this.MetaData);
        }

        private void ExecItem(XClassification.Classification xCln)
        {
            //get Cln and set Cln parent
            AdamCln.Classification adamCln = GetClassification(xCln);
            if (xCln.Action == PatchAction.AddOrUpdate)
            {
                UpdateCln(adamCln, xCln);
                ExecChildren(xCln);
            }
            else if (xCln.Action == PatchAction.Delete)
            {
                DeleteCln(adamCln);
            }
        }

        private void DeleteCln(AdamCln.Classification adamCln)
        {
        }

        private void UpdateCln(AdamCln.Classification adamCln, XClassification.Classification xCln)
        {
            //if (this.IsValid)
            {
                adamCln.Name = xCln.Name;

                UpdateGeneral(adamCln, xCln);

                UpdateLabels(xCln.Labels, adamCln.Labels);
                UpdateRegisteredFieldGroups(adamCln, xCln);
                UpdateAllFields(adamCln, xCln);

                if (adamCln.IsNew)
                {
                    this._isValid = true;
                    this.SaveAdamItem(adamCln, "Classification");
                }

                UpdatePermissions(adamCln, xCln);
                UpdateMapping(adamCln, xCln);

                this._isValid = true;
                this.SaveAdamItem(adamCln, "Classification");
            }
        }

        private void UpdateMapping(AdamCln.Classification adamCln, XClassification.Classification xCln)
        {
            AdamCln.ClassificationHelper clnHlp = new AdamCln.ClassificationHelper(this.Context.AdamApplication);

            foreach (var item in xCln.RegisteredMappings)
            {
                Guid? id = clnHlp.GetId(new AdamCln.ClassificationPath(item.Name));

                if (id.HasValue)
                {
                    bool exists = adamCln.RegisteredMappings.Any(
                            s => ((AdamCln.ClassificationMapping)s).ClassificationId == id.Value
                            );

                    if (item.Action == PatchAction.AddOrUpdate && !exists)
                    {
                        adamCln.RegisteredMappings.Add(id.Value);
                    }
                    else if (item.Action == PatchAction.Delete && exists)
                    {
                        adamCln.RegisteredMappings.Remove(id.Value);
                    }
                }
                else
                {
                    AddErrorCaseToContext(String.Format("{0}\\{1}", xCln.Parent, xCln.Name), 
                        String.Format("Classification.UpdateMapping. Slave: {0}", item.Name), 
                        MetaDataErrorType.AdamItemNotFound, false);
                }
            }
        }

        private AdamCln.Classification GetClassification(XClassification.Classification xCln)
        {
            //parent = namepath of parent of classification, name = name of classification
            AdamCln.Classification adamCln = new AdamCln.Classification(this.Context.AdamApplication);
            TryLoadResult result = adamCln.TryLoad(new AdamCln.ClassificationPath(ClassificationHlp.GetPath(xCln)));

            if (result == TryLoadResult.NotFound)
            {
                if (String.IsNullOrEmpty(xCln.Parent))
                {
                    adamCln.AddNew();
                }
                else
                {
                    Guid parentId = GetParentId(xCln.Parent);
                    if (parentId != Guid.Empty)
                    {
                        adamCln.AddNew(parentId);
                    }
                    else
                    {
                        AddErrorCaseToContext(ClassificationHlp.GetPath(xCln), "Classification", MetaDataErrorType.ParentNotFound);
                    }
                }
            }
            return adamCln;
        }

        private Guid GetParentId(string parent)
        {
            Guid id = Guid.Empty;
            AdamCln.ClassificationHelper adamClnHelper = new AdamCln.ClassificationHelper(this.Context.AdamApplication);
            Guid? idCln = adamClnHelper.GetId(new AdamCln.ClassificationPath(parent));
            if (idCln.HasValue)
            {
                id = idCln.Value;
            }
            return id;
        }

        private static void UpdateGeneral(AdamCln.Classification aCln, XClassification.Classification xCln)
        {
            aCln.Identifier = xCln.Identifier;
            aCln.SortIndex = xCln.SortIndex;
            aCln.SortOrder = xCln.SortOrderForChildren;
        }

        private void UpdateAllFields(AdamCln.Classification adamCln, XClassification.Classification xCln)
        {
            if (xCln.RegisteredFields != null)
            {
                UpdateRegisteredFields(adamCln, xCln);
                UpdateFields(adamCln, xCln);
            }
        }

        private void UpdateRegisteredFields(AdamCln.Classification aCln, XClassification.Classification xCln)
        {
            List<FailedMetaData> list = ClassificationRegisteredFields.UpdateFields(aCln, xCln,
                ClassificationHlp.IsPreClearRegisteredFields(xCln), this.Context.AdamApplication);
            AddErrorCaseToContext(list);
        }

        private void UpdateFields(AdamCln.Classification aCln, XClassification.Classification xCln)
        {
            //if (this.IsValid)
            {
                List<FailedMetaData> list = ClassificationFields.UpdateFields(aCln, xCln, this.Context.AdamApplication);
                AddErrorCaseToContext(list);
            }
        }

        private void UpdateRegisteredFieldGroups(AdamCln.Classification aCln, XClassification.Classification xCln)
        {
            List<FailedMetaData> list = ClassificationRegisteredFieldGroups.UpdateRegisteredFieldGroups(aCln, xCln, 
                ClassificationHlp.IsPreClearRegisteredFieldGroups(xCln), this.Context.AdamApplication);
            AddErrorCaseToContext(list);
        }

        private void UpdatePermissions(AdamCln.Classification aCln, XClassification.Classification xCln)
        {
            List<FailedMetaData> list = ClassificationPermissions.UpdatePermissions(aCln, xCln, this.Context.AdamApplication);
            AddErrorCaseToContext(list);
        }

        private void ExecChildren(XClassification.Classification parentXCln)
        {
            foreach (XClassification.Classification patchCln in parentXCln.Classifications)
            {
                patchCln.Action = parentXCln.Action;
                patchCln.Parent = ClassificationHlp.GetPath(parentXCln);
                ExecItem(patchCln);
            }
        }

    }
}
