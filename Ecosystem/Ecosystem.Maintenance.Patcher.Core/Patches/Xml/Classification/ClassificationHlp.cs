﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdamCln = Adam.Core.Classifications;
using XClassification = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Classification;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class ClassificationHlp
    {
        public static string GetPath(XClassification.Classification patchCln)
        {
            var ClnName = patchCln.Name;
            var backslash = @"\";
            var slash = "/";
            if (ClnName.Contains(slash))
            {
                var parts = ClnName.Split(new string[] { slash }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < parts.Length - 1; i++)
                {
                    if (!parts[i].EndsWith(backslash))
                    {
                        parts[i] += backslash;
                    }
                }
                ClnName = String.Join(slash, parts);
            }
            return String.Format("{0}/{1}", patchCln.Parent.TrimEnd('/').TrimEnd('\\'), ClnName);
            //return String.Format("{0}/{1}", patchCln.Parent.TrimEnd('/').TrimEnd('\\'), patchCln.Name);
        }

        public static bool IsPreClearRegisteredFields(XClassification.Classification xCln)
        {
            return true;
        }

        public static bool IsPreClearRegisteredFieldGroups(XClassification.Classification xCln)
        {
            return true;
        }
    }
}
