﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Management;
using AdamCln = Adam.Core.Classifications;
using XClassification = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Classification;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class ClassificationRegisteredFieldGroups
    {
        public static List<FailedMetaData> UpdateRegisteredFieldGroups(AdamCln.Classification aCln, XClassification.Classification xCln,
            bool isPreClearCollection, Application app)
        {
            List<FailedMetaData> list = new List<FailedMetaData>();
            if (xCln.RegisteredFieldGroups.Count != 0)
            {
                AdamCln.ClassificationRegisteredFieldGroupCollection col = aCln.RegisteredFieldGroups;

                if (isPreClearCollection)
                {
                    CleanFieldGroups(col);
                }

                FieldGroupHelper fgHlp = new FieldGroupHelper(app);

                foreach (MetaData.MetaDataElement group in xCln.RegisteredFieldGroups)
                {
                    Guid? id = fgHlp.GetId(group.Name);
                    if (id.HasValue)
                    {
                        col.Add(id.Value);
                    }
                    else
                    {
                        list.Add(new FailedMetaData(group.Name, "RegisteredFieldGroup.Id", MetaDataErrorType.AdamItemNotFound));
                    }
                }
            }
            return list;
        }

        private static void CleanFieldGroups(AdamCln.ClassificationRegisteredFieldGroupCollection list)
        {
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                list.Remove(list[0].FieldGroupId);
            }
        }
    }
}
