﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Management;
using AdamCln = Adam.Core.Classifications;
using XClassification = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Classification;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class ClassificationPermissions
    {
        public static List<FailedMetaData> UpdatePermissions(AdamCln.Classification aCln, XClassification.Classification xCln, Application app)
        {
            //Classification must exist
            List<FailedMetaData> list = new List<FailedMetaData>();
            if (xCln.Permissions != null)
            {
                AdamCln.ClassificationHelper clnHlp = new AdamCln.ClassificationHelper(app);

                if (xCln.Permissions.BreakSecurityInheritance != null)
                {
                    UpdateBreakSecurityInheritance(
                        clnHlp,
                        xCln.Permissions.BreakSecurityInheritance.ClassificationTreeSecurity,
                        aCln.Id,
                        AdamCln.ClassificationRightTarget.ClassificationTree
                        );

                    UpdateBreakSecurityInheritance(
                        clnHlp,
                        xCln.Permissions.BreakSecurityInheritance.RecordSecurity,
                        aCln.Id,
                        AdamCln.ClassificationRightTarget.Records
                        );
                }

                if (xCln.Permissions.UserGroups.Count != 0)
                {
                    UpdateUserGroups(clnHlp, xCln, aCln.Id, app, list);
                }
            }
            return list;
        }

        private static void UpdateUserGroups(AdamCln.ClassificationHelper clnHlp, XClassification.Classification xCln, 
            Guid aClnId, Application app, List<FailedMetaData> list)
        {
            UserGroupHelper ugHlp = new UserGroupHelper(app);

            foreach (XClassification.UserGroup userGroup in xCln.Permissions.UserGroups)
            {
                if (!userGroup.Name.Equals("Administrators"))
                {
                    Guid? ugId = ugHlp.GetId(userGroup.Name);
                    if (ugId.HasValue)
                    {
                        clnHlp.SetUserGroupRight(
                            aClnId,
                            ugId.Value,
                            AdamCln.ClassificationRightTarget.ClassificationTree,
                            userGroup.ClassificationsType);

                        clnHlp.SetUserGroupRight(
                            aClnId,
                            ugId.Value,
                            AdamCln.ClassificationRightTarget.Records,
                            userGroup.RecordType);
                    }
                    else
                    {
                        list.Add(new FailedMetaData(xCln.Name, "Classification.UserGroup", MetaDataErrorType.AdamItemNotFound,
                            String.Format("UserGroup Name: {0}", userGroup.Name)));
                    }
                }
                else
                {
                    list.Add(new FailedMetaData(xCln.Name, "Classification.Permissions", MetaDataErrorType.InvalidOperation,
                        String.Format("UserGroup Name: {0}", userGroup.Name)));
                }
            }
        }

        private static void UpdateBreakSecurityInheritance(AdamCln.ClassificationHelper clnHlp, bool flagValue, Guid id, AdamCln.ClassificationRightTarget target)
        {
            AdamCln.ClassificationSecurityInheritance value = flagValue == true ?
                AdamCln.ClassificationSecurityInheritance.Break :
                AdamCln.ClassificationSecurityInheritance.Restore;

            clnHlp.SetSecurityInheritance(id, target, value);
        }

    }
}
