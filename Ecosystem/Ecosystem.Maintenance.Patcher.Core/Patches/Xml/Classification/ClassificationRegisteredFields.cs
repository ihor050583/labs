﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Fields;
using AdamCln = Adam.Core.Classifications;
using XClassification = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Classification;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class ClassificationRegisteredFields
    {
        public static List<FailedMetaData> UpdateFields(AdamCln.Classification aCln, XClassification.Classification xCln,
            bool isPreClearCollection, Application app)
        {
            List<FailedMetaData> list = new List<FailedMetaData>();
            if (xCln.RegisteredFields != null)
            {
                AdamCln.ClassificationRegisteredFieldCollection col = aCln.RegisteredFields;

                if (isPreClearCollection)
                {
                    CleanFields(col);
                }

                FieldDefinitionHelper fdh = new FieldDefinitionHelper(app);

                foreach (MetaData.MetaDataElement field in xCln.RegisteredFields.NotClassificationScopeFields)
                {
                    Guid? id = fdh.GetId(field.Name);
                    if (id.HasValue)
                    {
                        col.Add(id.Value);
                    }
                    else
                    {
                        list.Add(new FailedMetaData(field.Name, "RegisteredFields.NotClassificationScopeFields.Id", MetaDataErrorType.AdamItemNotFound));
                    }
                }

                foreach (MetaData.MetaDataElement field in xCln.RegisteredFields.ClassificationScopeFields)
                {
                    Guid? id = fdh.GetId(field.Name);
                    if (id.HasValue)
                    {
                        col.Add(id.Value);
                    }
                    else
                    {
                        list.Add(new FailedMetaData(field.Name, "RegisteredFields.ClassificationScopeFields.Id", MetaDataErrorType.AdamItemNotFound));
                    }
                }
            }
            return list;
        }

        private static void CleanFields(AdamCln.ClassificationRegisteredFieldCollection list)
        {
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                list.Remove(list[0].FieldId);
            }
        }
    }
}
