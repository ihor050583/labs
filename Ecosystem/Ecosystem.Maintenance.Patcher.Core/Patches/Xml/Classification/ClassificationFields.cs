﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Fields;
using Adam.Core.Management;
using AdamCln = Adam.Core.Classifications;
using AField = Adam.Core.Fields;
using XCln = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Classification;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class ClassificationFields
    {
        public static List<FailedMetaData> UpdateFields(AdamCln.Classification adamCln, XCln.Classification xCln, Application app)
        {
            List<FailedMetaData> list = new List<FailedMetaData>();
            if (xCln.RegisteredFields.ClassificationScopeFields.Count != 0)
            {
                adamCln.Save();
                adamCln.Load(adamCln.Id);
                FieldDefinitionHelper fdh = new FieldDefinitionHelper(app);
                foreach (MetaData.MetaDataElement xField in xCln.RegisteredFields.ClassificationScopeFields)
                {
                    Guid? id = fdh.GetId(xField.Name);
                    if (id.HasValue)
                    {
                        FieldContainer fc = adamCln.Fields[id.Value];
                        if (fc != null)
                        {
                            UpdateValue(fc.MyLanguage, xField, app, list);
                        }
                        else
                        {
                            list.Add(new FailedMetaData(xField.Name, "Field.FieldContainer", MetaDataErrorType.AdamItemNotFound, "Warning! Object was saved!"));
                        }
                    }
                    else
                    {
                        list.Add(new FailedMetaData(xField.Name, "Field.Id", MetaDataErrorType.AdamItemNotFound, "Warning! Object was saved!"));
                    }
                }
            }
            return list;
        }

        private static void UpdateValue(AField.Field aField, MetaData.MetaDataElement xField, Application app, List<FailedMetaData> list)
        {
            aField.Clear();
            string value = xField.Value;
            try
            {
                switch (aField.DataType)
                {
                    case DataType.Date:
                        (aField as DateField).SetValue(DateTime.Parse(value));
                        break;
                    case DataType.DateTime:
                        (aField as DateTimeField).SetValue(DateTime.Parse(value));
                        break;
                    case DataType.Duration:
                        (aField as DurationField).SetValue(TimeSpan.Parse(value));
                        break;
                    case DataType.Numeric:
                        (aField as NumericField).SetValue(decimal.Parse(value));
                        break;
                    case DataType.Text:
                        (aField as TextField).SetValue(value);
                        break;
                    case DataType.Time:
                        (aField as TimeField).SetValue(DateTime.Parse(value));
                        break;
                    case DataType.OptionList:
                        UpdateOptionList(aField, xField, app);
                        break;
                    case DataType.UserGroupList:
                        UpdateUserGroupList(aField, value, app);
                        break;
                    case DataType.UserList:
                        UpdateUserList(aField, value, app);
                        break;
                    default:
                        list.Add(new FailedMetaData(xField.Name, "Field.DataType", MetaDataErrorType.AdamItemNotFound));
                        break;
                }
            }
            catch (Exception ex)
            {
                list.Add(new FailedMetaData(xField.Name, "Field.DataType", MetaDataErrorType.XmlInvalidDataFormat, ex.Message));
            }
        }

        private static void UpdateOptionList(AField.Field aField, MetaData.MetaDataElement xField, Application app)
        {
            //verification has already been done
            AField.OptionListFieldDefinition fd = new AField.OptionListFieldDefinition(app);
            fd.Load(xField.Name);
            AField.OptionListItemCollection options = (aField as AField.OptionListField).Items;
            string value = xField.Value.Trim();
            if (!value.Equals(String.Empty))
            {
                AField.OptionListItemDefinition item = fd.Items[xField.Value];
                if (item != null)
                {
                    options.Add(item.Id);
                }
                else
                {
                    throw new Exception(xField.Value);
                }
            }
        }


        private static void UpdateUserGroupList(AField.Field field, string value, Application app)
        {
            string[] parts = value.Split(';');

            if (parts != null && parts.Length > 0)
            {
                UserGroupHelper ugh = new UserGroupHelper(app);
                UserGroupListField item = field as UserGroupListField;

                foreach (string part in parts)
                {
                    string val = part.Trim();
                    if (ugh.Exists(val))
                    {
                        item.Items.Add(ugh.GetId(val).Value);
                    }
                    else
                    {
                        throw new Exception(val);
                    }
                }
            }
        }

        private static void UpdateUserList(AField.Field field, string value, Application app)
        {
            string[] parts = value.Split(';');

            if (parts != null && parts.Length > 0)
            {
                UserHelper uh = new UserHelper(app);
                UserListField item = field as UserListField;

                foreach (string part in parts)
                {
                    string val = part.Trim();
                    if (uh.Exists(val))
                    {
                        item.Items.Add(uh.GetId(val).Value);
                    }
                    else
                    {
                        throw new Exception("Invalid option");
                    }
                }
            }
        }
    }
}
