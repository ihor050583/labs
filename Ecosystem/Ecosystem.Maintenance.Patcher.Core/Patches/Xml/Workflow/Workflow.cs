﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core.Tools;
using Adam.Workflow.Core;
using Adam.Workflow.Core.Client;
using Adam.Workflow.Shared;
using XWorkflow = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Workflow;
using Adam.Core.Search;
using Adam.Core;
using System.Runtime.Serialization;
using Adam.Core.Settings;
using System.Xml;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class Workflow : XPatch<XWorkflow.Workflow, WorkflowDefinition>
    {
        public Workflow()
        {
            TryLoadItemCallback = TryLoadWorkflow;
        }

        private TryLoadResult TryLoadWorkflow(WorkflowDefinition aItem)
        {
            var se = new SearchExpression("Name = ? Status = Released", new object[] { this.MetaData.Name });
            TryLoadResult result = aItem.TryLoad(se);
            return result;
        }

        protected override void UpdateItem(WorkflowDefinition aItem)
        {
            if (IsNew(aItem))
            {
                AddNew(aItem);
            }
            else
            {
                UpdateVersion(aItem);
            }
        }

        private static bool IsNew(WorkflowDefinition aItem)
        {
            return String.IsNullOrEmpty(aItem.Name);
        }

        private void UpdateVersion(WorkflowDefinition aItem)
        {
            bool isCheckedOut = false;

            try
            {
                if (aItem.Status != WorkflowDefinitionConstants.StatusType.CheckedOut)
                {
                    aItem.CheckOut();
                    isCheckedOut = true;
                }

                aItem.Template = GetTemplate();
                aItem.Release();

                aItem.CheckIn();
            }
            catch (Exception ex)
            {
                if (isCheckedOut)
                {
                    aItem.UndoCheckOut();
                }

                AddErrorCaseToContext(this.MetaData.Name, "Workflow.UpdateVersion",
                    MetaDataErrorType.InvalidOperation, ex.Message);
            }
        }

        private void AddNew(WorkflowDefinition aItem)
        {
            if (ValidateWorkflowApplication())
            {
                aItem.Description = this.MetaData.Description;
                aItem.ReleaseDate = GetReleaseDate();
                aItem.WorkflowApplication = this.MetaData.WorkflowApplication;
                aItem.Template = GetTemplate();
            }
        }

        private bool ValidateWorkflowApplication()
        {
            bool retVal = true;

            string name = this.MetaData.WorkflowApplication;

            XmlSetting setting = new XmlSetting(this.Context.AdamApplication);
            setting.LoadSystemValue(".Workflow_Applications");

            XmlDocument value = new XmlDocument();
            value.LoadXml(setting.HasValue ? setting.Value : setting.DefaultValue);
            XmlElement engines = (XmlElement)value.SelectSingleNode("/workflowApplications");

            XmlElement element = engines.SelectSingleNode("./add[@name='" + name + "']") as XmlElement;

            if (element == null)
            {
                retVal = false;
                AddErrorCaseToContext(this.MetaData.Name, "Workflow.WorkflowApplication: " + name,
                    MetaDataErrorType.AdamItemNotFound);
            }

            return retVal;
        }

        private DateTime? GetReleaseDate()
        {
            DateTime? retVal = null;

            try
            {
                retVal = DateTime.Parse(this.MetaData.ReleaseDate);
            }
            catch (Exception)
            {
                //AddErrorCaseToContext(this.MetaData.Name, "Workflow.ReleaseDate: " +
                //    this.MetaData.ReleaseDate, MetaDataErrorType.XmlInvalidDataFormat, ex.Message);

                retVal = DateTime.Now.AddMinutes(5);
            }

            return retVal;
        }

        private WorkflowDefinitionTemplate GetTemplate()
        {
            string value = Extensions.RemoveCDATA(this.MetaData.Template);
            value = value.Replace("&lt;", "<");
            value = value.Replace("&gt;", ">");
            return new WorkflowDefinitionTemplate(System.Text.Encoding.UTF8.GetBytes(value));
        }
    }
}
