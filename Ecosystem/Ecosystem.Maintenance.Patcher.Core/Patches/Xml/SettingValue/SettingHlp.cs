﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Settings;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.SettingValue
{
    public class SettingHlp
    {
        public static dynamic GetRoleSetting(string value)
        {
            Role retVal = Role.None;
            if (!string.IsNullOrEmpty(value))
            {
                Role val;
                if (Enum.TryParse<Role>(value, true, out val))
                {
                    retVal = val;
                }
            }
            return retVal;
        }

        public static dynamic GetNumericSetting(string value)
        {
            int? retVal = null;
            if (!string.IsNullOrEmpty(value))
            {
                int val;
                if (Int32.TryParse(value, out val))
                {
                    retVal = val;
                }
            }
            return retVal;
        }

        public static dynamic GetDateTimeSetting(string value)
        {
            DateTime? retVal = null;
            if (!string.IsNullOrEmpty(value))
            {
                DateTime val;
                if (DateTime.TryParse(value, out val))
                {
                    retVal = val;
                }
            }
            return retVal;
        }

        public static dynamic GetBooleanSetting(string value)
        {
            bool? retVal = null;
            if (!string.IsNullOrEmpty(value))
            {
                bool val;
                if (Boolean.TryParse(value, out val))
                {
                    retVal = val;
                }
            }
            return retVal;
        }

        public static dynamic GetSetting(Application app, string name)
        {
            dynamic retVal = null;

            SettingDefinitionHelper sdh = new SettingDefinitionHelper(app);

            if (sdh.Exists(name))
            {
                var settingDefinition = sdh.GetSettingDefinition(name);

                switch (settingDefinition.DataType)
                {
                    case SettingDataType.Reference:
                        {
                            retVal = new ReferenceSetting(app);
                        }
                        break;
                    case SettingDataType.Boolean:
                        {
                            retVal = new BooleanSetting(app);
                        }
                        break;
                    case SettingDataType.DateTime:
                        {
                            retVal = new DateTimeSetting(app);
                        }
                        break;
                    case SettingDataType.Numeric:
                        {
                            retVal = new NumericSetting(app);
                        }
                        break;
                    case SettingDataType.Role:
                        {
                            retVal = new RoleSetting(app);
                        }
                        break;
                    case SettingDataType.Text:
                        {
                            retVal = new TextSetting(app);
                        }
                        break;
                    case SettingDataType.Xml:
                        {
                            retVal = new XmlSetting(app);
                        }
                        break;
                }
            }

            return retVal;
        }
    }
}
