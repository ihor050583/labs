﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Settings;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.SettingValue;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.SettingValue;
using AManagement = Adam.Core.Management;
using XSettingValue = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.SettingValue;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class SettingValue : Patch<XSettingValue.SettingValue>
    {
        protected delegate void LoadValue(dynamic aSetting, string name, Guid id);

        protected override void ExecuteHlp()
        {
            dynamic aSetting = SettingHlp.GetSetting(this.Context.AdamApplication, this.MetaData.Name);
            if (aSetting == null)
            {
                AddErrorCaseToContext(this.MetaData.Name, "SettingValue", MetaDataErrorType.AdamItemNotFound);
            }
            else if (this.MetaData.Action == Core.MetaData.Xml.PatchAction.AddOrUpdate)
            {
                UpdateByScope(aSetting);
            }
            else if (this.MetaData.Action == Core.MetaData.Xml.PatchAction.Delete)
            {
                this.MetaData.Action = Core.MetaData.Xml.PatchAction.None;
                AddErrorCaseToContext(this.MetaData.Name, "SettingValue.Action.Delete", MetaDataErrorType.InvalidOperation);
            }
        }

        private void UpdateByScope(dynamic aSetting)
        {
            UpdateScopeItemList(aSetting, this.MetaData.ScopeSystem, null);
            UpdateScopeItemList(aSetting, this.MetaData.ScopeSite, new LoadValue(this.LoadSiteValue));
            UpdateScopeItemList(aSetting, this.MetaData.ScopeUser, new LoadValue(this.LoadUserValue));
            UpdateScopeItemList(aSetting, this.MetaData.ScopeUserGroup, new LoadValue(this.LoadUserGroupValue));
        }

        protected void LoadSiteValue(dynamic aSetting, string name, Guid id) { aSetting.LoadSiteValue(name, id); }

        protected void LoadUserValue(dynamic aSetting, string name, Guid id) { aSetting.LoadUserValue(name, id); }

        protected void LoadUserGroupValue(dynamic aSetting, string name, Guid id) { aSetting.LoadUserGroupValue(name, id); }

        private void UpdateScopeItemList(dynamic aSetting, ScopeValue scope, LoadValue LoadValueCallBack)
        {
            foreach (var item in scope.Items)
            {
                if (scope is ScopeSystem)
                {
                    aSetting.LoadSystemValue(this.MetaData.Name);
                }
                else if (scope is ScopeUser)
                {
                    LoadScopeValue<AManagement.User>(aSetting, this.MetaData.Name, item.Name, LoadValueCallBack);
                }
                else if (scope is ScopeUserGroup)
                {
                    LoadScopeValue<AManagement.UserGroup>(aSetting, this.MetaData.Name, item.Name, LoadValueCallBack);
                }
                else if (scope is ScopeSite)
                {
                    LoadScopeValue<AManagement.Site>(aSetting, this.MetaData.Name, item.Name, LoadValueCallBack);
                }

                this.SetValueAndSave(aSetting, GetValue(item));
            }
        }

        private static string GetValue(Core.MetaData.MetaDataElement item)
        {
            string retVal = item.Data.Trim();

            if (item.Action == Core.MetaData.Xml.PatchAction.Delete)
            {
                retVal = null;
            }

            return retVal;
        }

        private void LoadScopeValue<AItemType>(dynamic aSetting, string xSettingName, string xScopeItemName, LoadValue LoadValueCallBack)
        {
            dynamic aItem = (AItemType)Activator.CreateInstance(typeof(AItemType), new object[] { this.Context.AdamApplication });
            if (aItem.TryLoad(xScopeItemName) == TryLoadResult.Success)
            {
                LoadValueCallBack(aSetting, xSettingName, aItem.Id);
            }
            else
            {
                AddErrorCaseToContext(xSettingName, aSetting.GetType().ToString(), MetaDataErrorType.AdamItemNotFound,
                    String.Format("item name: {0}", xScopeItemName));
            }
        }

        private dynamic GetValueData(dynamic setting, string data)
        {
            dynamic retVal = null;

            string value = Extensions.RemoveCDATA(data);

            if (setting is BooleanSetting)
            {
                retVal = SettingHlp.GetBooleanSetting(value);
            }
            else if (setting is DateTimeSetting)
            {
                retVal = SettingHlp.GetDateTimeSetting(value);
            }
            else if (setting is NumericSetting)
            {
                retVal = SettingHlp.GetNumericSetting(value);
            }
            else if (setting is RoleSetting)
            {
                retVal = SettingHlp.GetRoleSetting(value);
            }
            else if (setting is TextSetting || setting is XmlSetting || setting is ReferenceSetting)
            {
                retVal = value;
            }

            return retVal;
        }

        private void SetValueAndSave(dynamic setting, string data)
        {
            setting.Value = GetValueData(setting, data);
            this.SaveAdamItem(setting, "Setting");
        }
    }
}
