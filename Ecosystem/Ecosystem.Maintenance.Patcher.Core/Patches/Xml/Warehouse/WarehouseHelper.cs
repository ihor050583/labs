﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using AWarehouse = Adam.Core.Warehousing;
using XWarehouse = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Warehouse;
using Adam.Core.Fields;
using Adam.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class WarehouseHelper
    {
        public static  Application Application { get; set; }
        public static List<FailedMetaData> AddColumns(XWarehouse.WHTable xTable, AWarehouse.WarehouseTable aTable, AWarehouse.WarehouseSourceTable aSrcTable)
        {
            List<FailedMetaData> list = new List<FailedMetaData>();
            foreach (var xColumn in xTable.Columns)
            {
                List<FailedMetaData> result = null;
                if (xTable.Source ==  AWarehouse.WarehouseSourceTypes.Classification)
                {
                    result = AddColumns<AWarehouse.ClassificationSourceTable>(xColumn, aTable, aSrcTable as AWarehouse.ClassificationSourceTable);
                }
                else if (xTable.Source == AWarehouse.WarehouseSourceTypes.Record)
                {
                    result = AddColumns<AWarehouse.RecordSourceTable>(xColumn, aTable, aSrcTable as AWarehouse.RecordSourceTable);
                }
                else if (xTable.Source == AWarehouse.WarehouseSourceTypes.User)
                {
                    result = AddColumns<AWarehouse.UserSourceTable>(xColumn, aTable, aSrcTable as AWarehouse.UserSourceTable);
                }
                else if (xTable.Source == AWarehouse.WarehouseSourceTypes.UserGroup)
                {
                    result = AddColumns<AWarehouse.UserGroupSourceTable>(xColumn, aTable, aSrcTable as AWarehouse.UserGroupSourceTable);
                }
                else if (xTable.Source == AWarehouse.WarehouseSourceTypes.ApplicationLog)
                {
                    result = AddColumns<AWarehouse.ApplicationLogSourceTable>(xColumn, aTable, aSrcTable as AWarehouse.ApplicationLogSourceTable);
                }
                else if (xTable.Source == AWarehouse.WarehouseSourceTypes.FieldDefinition)
                {
                    result = AddColumns<AWarehouse.FieldDefinitionSourceTable>(xColumn, aTable, aSrcTable as AWarehouse.FieldDefinitionSourceTable);
                }
                else if (xTable.Source == AWarehouse.WarehouseSourceTypes.MaintenanceJobHistory)
                {
                    result = AddColumns<AWarehouse.MaintenanceJobHistorySourceTable>(xColumn, aTable, aSrcTable as AWarehouse.MaintenanceJobHistorySourceTable);
                }
                else if (xTable.Source == AWarehouse.WarehouseSourceTypes.OrderHistory)
                {
                    result = AddColumns<AWarehouse.OrderHistorySourceTable>(xColumn, aTable, aSrcTable as AWarehouse.OrderHistorySourceTable);
                }
                else if (xTable.Source == AWarehouse.WarehouseSourceTypes.File)
                {
                    result = AddColumns<AWarehouse.FileSourceTable>(xColumn, aTable, aSrcTable as AWarehouse.FileSourceTable);
                }
                else if (xTable.Source == AWarehouse.WarehouseSourceTypes.FileType)
                {
                    result = AddColumns<AWarehouse.FileTypeSourceTable>(xColumn, aTable, aSrcTable as AWarehouse.FileTypeSourceTable);
                }
                else if (xTable.Source == AWarehouse.WarehouseSourceTypes.FileVersion)
                {
                    result = AddColumns<AWarehouse.FileVersionSourceTable>(xColumn, aTable, aSrcTable as AWarehouse.FileVersionSourceTable);
                }
                else if (xTable.Source == AWarehouse.WarehouseSourceTypes.Language)
                {
                    result = AddColumns<AWarehouse.LanguageSourceTable>(xColumn, aTable, aSrcTable as AWarehouse.LanguageSourceTable);
                }
                else if (xTable.Source == AWarehouse.WarehouseSourceTypes.Site)
                {
                    result = AddColumns<AWarehouse.SiteSourceTable>(xColumn, aTable, aSrcTable as AWarehouse.SiteSourceTable);
                }
                list.AddRange(result);
            }
            return list;
        }

        private static List<FailedMetaData> AddColumns<T>(MetaData.MetaDataElement xColumn, AWarehouse.WarehouseTable aTable, T aSrcTable)
        {
            List<FailedMetaData> list = new List<FailedMetaData>();

            AWarehouse.WarehouseColumn aColumn = (from s in aTable.Columns
                 where ((AWarehouse.WarehouseColumn)s).ColumnName.Equals(xColumn.Name.Trim(), StringComparison.OrdinalIgnoreCase)
                 select s).FirstOrDefault() as AWarehouse.WarehouseColumn;

            if (aColumn != null && xColumn.Action == MetaData.Xml.PatchAction.Delete)
            {
                aTable.Columns.Remove(aColumn.Id);
            }
            else if (xColumn.Action != MetaData.Xml.PatchAction.Delete)
            {
                aColumn = AddColumnHlp<T>(xColumn, aTable, aSrcTable, list, aColumn);
            }

            return list;
        }

        private static AWarehouse.WarehouseColumn AddColumnHlp<T>(MetaData.MetaDataElement xColumn, AWarehouse.WarehouseTable aTable, 
            T aSrcTable, List<FailedMetaData> list, AWarehouse.WarehouseColumn aColumn)
        {
            if (IsField(xColumn.Data))
            {
                string name = GetFieldName(xColumn.Data);
                //string language = GetFieldLanguage(xColumn.Data);

                FieldDefinitionHelper helper = new FieldDefinitionHelper(Application);

                if (helper.Exists(name))
                {
                    FieldDefinition fd = helper.GetFieldDefinition(name);
                    dynamic t = (T)aSrcTable;

                    if (t.Fields != null)
                    {
                        AWarehouse.WarehouseSourceField value = t.Fields[fd.Id] as AWarehouse.WarehouseSourceField;
                        if (value != null)
                        {
                            aColumn = aTable.Columns.Add(value, xColumn.Name.Trim());
                            aColumn.Indexed = GetIndexed(xColumn);
                        }
                        else
                        {
                            list.Add(new FailedMetaData(xColumn.Name, "Warehouse.Table.Column.Field.Value", MetaDataErrorType.AdamItemNotFound,
                                String.Format("FieldName = '{0}' was not found in table field collection. Check the field scope.", name)));
                        }
                    }
                    else
                    {
                        list.Add(new FailedMetaData(xColumn.Name, "Warehouse.Table.Sourse.Name", MetaDataErrorType.AdamItemNotFound,
                            String.Format("SourseType = '{0}' does not contain a Field collection", typeof(T).ToString())));
                    }
                }
                else 
                {
                    list.Add(new FailedMetaData(xColumn.Name, "Warehouse.Table.Column.Field.Name", MetaDataErrorType.AdamItemNotFound,
                        String.Format("FieldName = '{0}' was not found in Adam", name)));
                }
            }
            else
            {
                AWarehouse.WarehouseSourceColumn value = GetPropValue<AWarehouse.WarehouseSourceColumn>(aSrcTable, xColumn.Data.Trim());
                if (value != null)
                {
                    aColumn = aTable.Columns.Add(value, xColumn.Name.Trim());
                    aColumn.Indexed = GetIndexed(xColumn);
                }
                else
                {
                    list.Add(new FailedMetaData(xColumn.Name.Trim(), "Warehouse.Table.Column.Property.Value", MetaDataErrorType.AdamItemNotFound,
                        String.Format("Property = '{0}' was not found in table property collection.", xColumn.Data.Trim())));
                }
            }
            
            return aColumn;
        }

        private static bool IsField(string data)
        {
            return data.Trim().IndexOf("'") != -1;
        }

        private static string GetFieldName(string data)
        {
            string retVal = String.Empty;

            string[] parts = data.Trim().Split('\'');
            if (parts.Length > 0)
            {
                retVal = parts[1].Trim();
            }

            return retVal;
        }

        private static string GetFieldLanguage(string data)
        {
            string retVal = String.Empty;

            string[] parts = data.Trim().Split('\'');
            if (parts.Length > 0)
            {
                retVal = parts[parts.Length - 1].Trim().TrimStart(new char [] {'('}).TrimEnd(new char[] { ')' });
            }

            return retVal;
        }

        public static Object GetPropValue(Object obj, String name)
        {
            foreach (String part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                PropertyInfo info = type.GetProperty(part, BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly);
                if (info == null) 
                {
                    info = type.GetProperty(part);
                    if (info == null)
                    {
                        return null; 
                    }
                }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }

        public static T GetPropValue<T>(Object obj, String name)
        {
            Object retval = GetPropValue(obj, name);
            if (retval == null) { return default(T); }
            return (T)retval;
        }

        private static bool GetIndexed(MetaData.MetaDataElement xColumn)
        {
            bool result = false;
            if (!String.IsNullOrEmpty(xColumn.Value) && !String.IsNullOrWhiteSpace(xColumn.Value))
            {
                Boolean.TryParse(xColumn.Value.Trim(), out result);
            }
            return result;
        }
    }
}
