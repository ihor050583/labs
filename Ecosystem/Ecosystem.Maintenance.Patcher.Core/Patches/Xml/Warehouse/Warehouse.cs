﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AManagement = Adam.Core.Management;
using Adam.Core.Search;
using AWarehouse = Adam.Core.Warehousing;
using XWarehouse = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Warehouse;
using Adam.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class Warehouse : Patch<XWarehouse.Warehouse>
    {
        protected override void ExecuteHlp()
        {
            if (this.MetaData.Action == Core.MetaData.Xml.PatchAction.AddOrUpdate)
            {
                AWarehouse.WarehouseDatabase warehouseDB = GetWarehouseDatabase();
                if (warehouseDB != null)
                {
                    AWarehouse.WarehouseTableSet aWarehouseTableSet = GetWarehouseTableSet(warehouseDB);
                    if (aWarehouseTableSet != null)
                    {
                        SetTables(aWarehouseTableSet);
                        aWarehouseTableSet.Save();

                        AWarehouse.WarehouseBuilder whBuilder = new AWarehouse.WarehouseBuilder(this.Context.AdamApplication);
                        whBuilder.BuildWarehouse(aWarehouseTableSet, true);
                    }
                }
            }
            else if (this.MetaData.Action == Core.MetaData.Xml.PatchAction.Delete)
            {
                AWarehouse.WarehouseTableSet aWarehouseTableSet = new AWarehouse.WarehouseTableSet(this.Context.AdamApplication);
                SearchExpression se = new SearchExpression(String.Format("Name='{0}'", this.MetaData.Name));

                if (aWarehouseTableSet.TryLoad(se) == TryLoadResult.Success)
                {
                    try
                    {
                        aWarehouseTableSet.Delete();
                    }
                    catch (Exception ex)
                    {
                        AddErrorCaseToContext(aWarehouseTableSet.Name, aWarehouseTableSet.GetType().ToString(), MetaDataErrorType.Unexpected, ex.Message);
                    }
                }
            }
        }

        private void SetTables(AWarehouse.WarehouseTableSet aWarehouseTableSet)
        {
            foreach (var xTable in this.MetaData.Tables)
            {
                AWarehouse.WarehouseSourceTable aSrcTable = GetTable(xTable);

                AWarehouse.WarehouseTable aTable = 
                    (from s in aWarehouseTableSet.Tables
                     where ((AWarehouse.WarehouseTable)s).TableName.Equals(xTable.Name, StringComparison.OrdinalIgnoreCase)
                     select s).FirstOrDefault() as AWarehouse.WarehouseTable;

                if (xTable.Action == Core.MetaData.Xml.PatchAction.Delete && aTable != null)
                {
                    aWarehouseTableSet.Tables.Remove(aTable.Id);
                }
                else if (xTable.Action != Core.MetaData.Xml.PatchAction.Delete && aTable == null)
                {
                    aTable = aWarehouseTableSet.Tables.Add(aSrcTable, xTable.Name);

                    AddColumns(xTable, aSrcTable, aTable);

                    aTable.Filter = GetFilter(xTable.Filter);
                    aTable.CustomScript = GetCustomScript(xTable.CustomScript);
                }
            }
        }

        private void AddColumns(XWarehouse.WHTable xTable, AWarehouse.WarehouseSourceTable aSrcTable, AWarehouse.WarehouseTable aTable)
        {
            WarehouseHelper.Application = this.Context.AdamApplication;
            List<FailedMetaData> list = WarehouseHelper.AddColumns(xTable, aTable, aSrcTable);
            AddErrorCaseToContext(list);
        }

        private SearchExpression GetFilter(string filter)
        {
            SearchExpression retVal = null;
            if (!String.IsNullOrEmpty(filter) && !String.IsNullOrWhiteSpace(filter))
            {
                retVal = new SearchExpression(filter.Trim());
            }
            return retVal;
        }

        private AWarehouse.WarehouseSourceTable GetTable(XWarehouse.WHTable xTable)
        {
            AWarehouse.WarehouseSourceTable aTable = null;

            if (xTable.Source == AWarehouse.WarehouseSourceTypes.Classification)
            {
                aTable = new AWarehouse.ClassificationSourceTable(this.Context.AdamApplication);
            }
            else if (xTable.Source == AWarehouse.WarehouseSourceTypes.Record)
            {
                aTable = new AWarehouse.RecordSourceTable(this.Context.AdamApplication);
            }
            else if (xTable.Source == AWarehouse.WarehouseSourceTypes.User)
            {
                aTable = new AWarehouse.UserSourceTable(this.Context.AdamApplication);
            }
            else if (xTable.Source == AWarehouse.WarehouseSourceTypes.UserGroup)
            {
                aTable = new AWarehouse.UserGroupSourceTable(this.Context.AdamApplication);
            }
            else if (xTable.Source == AWarehouse.WarehouseSourceTypes.ApplicationLog)
            {
                aTable = new AWarehouse.ApplicationLogSourceTable(this.Context.AdamApplication);
            }
            else if (xTable.Source == AWarehouse.WarehouseSourceTypes.FieldDefinition)
            {
                aTable = new AWarehouse.FieldDefinitionSourceTable(this.Context.AdamApplication);
            }
            else if (xTable.Source == AWarehouse.WarehouseSourceTypes.MaintenanceJobHistory)
            {
                aTable = new AWarehouse.MaintenanceJobHistorySourceTable(this.Context.AdamApplication);
            }
            else if (xTable.Source == AWarehouse.WarehouseSourceTypes.OrderHistory)
            {
                aTable = new AWarehouse.OrderHistorySourceTable(this.Context.AdamApplication);
            }
            else if (xTable.Source == AWarehouse.WarehouseSourceTypes.File)
            {
                aTable = new AWarehouse.FileSourceTable(this.Context.AdamApplication);
            }
            else if (xTable.Source == AWarehouse.WarehouseSourceTypes.FileType)
            {
                aTable = new AWarehouse.FileTypeSourceTable(this.Context.AdamApplication);
            }
            else if (xTable.Source == AWarehouse.WarehouseSourceTypes.FileVersion)
            {
                aTable = new AWarehouse.FileVersionSourceTable(this.Context.AdamApplication);
            }
            else if (xTable.Source == AWarehouse.WarehouseSourceTypes.Language)
            {
                aTable = new AWarehouse.LanguageSourceTable(this.Context.AdamApplication);
            }
            else if (xTable.Source == AWarehouse.WarehouseSourceTypes.Site)
            {
                aTable = new AWarehouse.SiteSourceTable(this.Context.AdamApplication);
            }
            
            return aTable;
        }

        private AWarehouse.WarehouseTableSet GetWarehouseTableSet(AWarehouse.WarehouseDatabase warehouseDB)
        {
            AWarehouse.WarehouseTableSet retVal = null;
            SearchExpression se = new SearchExpression(String.Format("Name='{0}'", this.MetaData.Name));

            if (this.MetaData.Action == Core.MetaData.Xml.PatchAction.Delete)
            {
                AWarehouse.WarehouseTableSet ts = new AWarehouse.WarehouseTableSet(this.Context.AdamApplication);
                if (ts.TryLoad(se) == TryLoadResult.Success)
                {
                    ts.Delete();
                }
            }
            else
            {
                AWarehouse.WarehouseTableSet aWarehouseTableSet = new AWarehouse.WarehouseTableSet(this.Context.AdamApplication);
                if (aWarehouseTableSet.TryLoad(se) == TryLoadResult.NotFound)
                {
                    aWarehouseTableSet.AddNew();
                }

                aWarehouseTableSet.Name = GetName();
                aWarehouseTableSet.Warehouse = warehouseDB.Id;
                aWarehouseTableSet.SyncInterval = GetTimeSpan();
                aWarehouseTableSet.CustomScript = GetCustomScript(this.MetaData.CustomScript);
                aWarehouseTableSet.RunAsUser = GetRunAsUser();

                retVal = aWarehouseTableSet;
            }

            return retVal;
        }

        private string GetName()
        {
            string retVal = this.MetaData.Name;
            if (this.MetaData.UpdateName != null)
            {
                retVal = this.MetaData.UpdateName.Value;
            }
            return retVal;
        }

        private TimeSpan GetTimeSpan()
        {
            TimeSpan ts = new TimeSpan(2, 0, 0);
            if (String.IsNullOrEmpty(this.MetaData.SynchronizationInterval) &&
                String.IsNullOrWhiteSpace(this.MetaData.SynchronizationInterval))
            {
                TimeSpan.TryParse(this.MetaData.SynchronizationInterval.Trim(), out ts);
            }
            return ts;
        }

        private Guid? GetRunAsUser()
        {
            Guid? retVal = null;
            if (!String.IsNullOrEmpty(this.MetaData.RunAsUser) && !String.IsNullOrWhiteSpace(this.MetaData.RunAsUser))
            {
                AManagement.UserHelper helper = new AManagement.UserHelper(this.Context.AdamApplication);
                retVal = helper.GetId(this.MetaData.RunAsUser.Trim());
            }
            return retVal;
        }

        private string GetCustomScript(string script)
        {
            string retVal = String.Empty;
            if (!String.IsNullOrEmpty(script) && !String.IsNullOrWhiteSpace(script))
            {
                retVal = Extensions.RemoveCDATA(script.Trim());
            }
            return retVal;
        }

        private AWarehouse.WarehouseDatabase GetWarehouseDatabase()
        {
            AWarehouse.WarehouseDatabase warehouseDB = new AWarehouse.WarehouseDatabase(this.Context.AdamApplication);
            SearchExpression se = new SearchExpression(String.Format("Name='{0}'", this.MetaData.WarehouseDatabase));

            TryLoadResult found = warehouseDB.TryLoad(se);

            if (found == TryLoadResult.NotFound)
            {
                if (this.MetaData.CreateWarehouseDatabase)
                {
                    warehouseDB.AddNew();
                }
                else
                {
                    warehouseDB = null;
                    this.AddErrorCaseToContext(this.MetaData.Name, "Warehouse.WarehouseDatabase.Name", MetaDataErrorType.AdamItemNotFound,
                        "Cannot find Warehouse by name specified and cannot create a new one because CreateWarehouseDatabase param was set to false.");
                }
            }

            UpdateWarehouseDatabase(warehouseDB, found);

            return warehouseDB;
        }

        private void UpdateWarehouseDatabase(AWarehouse.WarehouseDatabase warehouseDB, TryLoadResult found)
        {
            if (warehouseDB != null && warehouseDB.IsNew || found == TryLoadResult.Success)
            {
                warehouseDB.Name = this.MetaData.WarehouseDatabase;
                warehouseDB.ConnectionString = this.MetaData.WarehouseDatabaseConnection.Trim();
                warehouseDB.Save();
            }
        }
    }
}
