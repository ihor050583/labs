﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSettingCategory = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.SettingCategory;
using ASettings = Adam.Core.Settings;
using Adam.Core;
using AManagement = Adam.Core.Management;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class SettingCategory : XPatch<XSettingCategory.SettingCategory, ASettings.SettingCategory>
    {
        protected override void UpdateItem(ASettings.SettingCategory aItem)
        {
            aItem.Name = this.MetaData.Name;
            UpdateLabels(this.MetaData.Labels, aItem.Labels);
        }
    }
}
