﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using AManagement = Adam.Core.Management;
using XSite = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Site;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class Site : XPatch<XSite.Site, AManagement.Site>
    {
        protected override void UpdateItem(AManagement.Site aItem)
        {
        }
    }
}
