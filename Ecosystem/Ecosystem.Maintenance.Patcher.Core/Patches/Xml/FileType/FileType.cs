﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XFileType = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.FileType;
using AManagement = Adam.Core.Management;
using Adam.Core;
using Adam.Core.Fields;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.FileType;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class FileType : XPatch<XFileType.FileType, AManagement.FileType>
    {
        protected override void UpdateItem(AManagement.FileType aItem)
        {
            aItem.Kind = this.MetaData.Kind;

            UpdateMediaEngines(aItem);
            UpdateCatalogActions(aItem);
            UpdateFileInformation(aItem);
            UpdateProcessing(aItem);
            UpdatePreview(aItem);
            UpdateLabels(this.MetaData.Labels, aItem.Labels);
            UpdateRegisteredFields(aItem);
            UpdateRegisteredFieldGroups(aItem);
        }

        private void UpdateRegisteredFields(AManagement.FileType aFileType)
        {
            List<FailedMetaData> list = RegisteredFieldHlp.UpdateFields(aFileType,
                this.MetaData.RegisteredFields, this.Context.AdamApplication);
            AddErrorCaseToContext(list);
        }

        private void UpdateRegisteredFieldGroups(AManagement.FileType aFileType)
        {
            List<FailedMetaData> list = RegisteredFieldGroupHlp.UpdateFieldGroups(aFileType,
                this.MetaData.RegisteredFieldGroups, this.Context.AdamApplication);
            AddErrorCaseToContext(list);
        }

        private void UpdatePreview(AManagement.FileType aFileType)
        {
            aFileType.PreviewRenderWebControls.RemoveAll();
            foreach (var item in this.MetaData.PreviewTypes)
            {
                if (!aFileType.PreviewRenderWebControls.Any(a => a.Name.Equals(item.Name, StringComparison.OrdinalIgnoreCase)))
                {
                    aFileType.PreviewRenderWebControls.Add(new AManagement.RegisteredMediaEngine(item.Name));
                }
            }
        }

        private void UpdateMediaEngines(AManagement.FileType aFileType)
        {
            aFileType.MediaEngines.RemoveAll();
            foreach (var engine in this.MetaData.MediaEngines)
            {
                aFileType.MediaEngines.Add(new AManagement.RegisteredMediaEngine(engine.Name));
            }
        }

        private void UpdateCatalogActions(AManagement.FileType aFileType)
        {
            aFileType.CatalogActions.RemoveAll();
            foreach (var action in this.MetaData.CatalogActions)
            {
                aFileType.CatalogActions.Add(new AManagement.FileTypeAction(action.Name, Convert.ToBoolean(action.Value)));
            }
        }

        private void UpdateFileInformation(AManagement.FileType aFileType)
        {
            aFileType.MacCreator = this.MetaData.FileInformation.MacCreator;
            aFileType.MacType = this.MetaData.FileInformation.MacType;
            aFileType.MimeType = this.MetaData.FileInformation.MimeType;
            aFileType.Extension = this.MetaData.FileInformation.Extension;
        }

        private void UpdateProcessing(AManagement.FileType aFileType)
        {
            aFileType.AllowOrderResizeSource = this.MetaData.Processing.AllowResizeSource;
            aFileType.IsCatalogable = this.MetaData.Processing.IsCatalogable;
            aFileType.KeepDocumentDimensions = this.MetaData.Processing.KeepDocumentDimensions;
            aFileType.PreferredExtension = this.MetaData.Processing.PreferredExtension;
            aFileType.PreferredMacType = this.MetaData.Processing.PreferredMacType;
            aFileType.PreviewRequired = this.MetaData.Processing.PreviewRequired;
            aFileType.SupportsWatermarking = this.MetaData.Processing.SupportsWatermarking;
            aFileType.EngineFormat = this.MetaData.Processing.EngineFormat;
            aFileType.PreviewFormat = this.MetaData.Processing.PreviewFormat;
        }
    }
}
