﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using AManagement = Adam.Core.Management;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.FileType
{
    public class RegisteredFieldGroupHlp
    {
        internal static List<FailedMetaData> UpdateFieldGroups(AManagement.FileType aFileType, 
            List<MetaData.MetaDataElement> list, Application app)
        {
            List<FailedMetaData> retVal = new List<FailedMetaData>();
            RemoveFieldGroups(aFileType);
            AManagement.FieldGroupHelper fgh = new AManagement.FieldGroupHelper(app);
            foreach (var item in list)
            {
                Guid? id = fgh.GetId(item.Name);
                if (id.HasValue)
                {
                    aFileType.RegisteredFieldGroups.Add(id.Value);
                }
                else
                {
                    retVal.Add(new FailedMetaData(item.Name, "RegisteredFieldGroup.Name", MetaDataErrorType.AdamItemNotFound));
                }
            }
            return retVal;
        }

        private static void RemoveFieldGroups(AManagement.FileType aFileType)
        {
            List<Guid> ids = new List<Guid>();
            foreach (AManagement.RegisteredFieldGroup item in aFileType.RegisteredFieldGroups)
            {
                ids.Add(item.FieldGroupId);
            }
            foreach (var id in ids)
            {
                aFileType.RegisteredFieldGroups.Remove(id);
            }
        }
    }
}
