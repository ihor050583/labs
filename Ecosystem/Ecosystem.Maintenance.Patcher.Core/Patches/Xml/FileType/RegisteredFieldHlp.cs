﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Fields;
using AManagement = Adam.Core.Management;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.FileType
{
    public class RegisteredFieldHlp
    {
        internal static List<FailedMetaData> UpdateFields(AManagement.FileType aFileType, 
            List<MetaData.MetaDataElement> list, Application app)
        {
            List<FailedMetaData> retVal = new List<FailedMetaData>();
            RemoveFields(aFileType);

            FieldDefinitionHelper fdh = new FieldDefinitionHelper(app);
            foreach (var item in list)
            {
                Guid? id = fdh.GetId(item.Name);
                if (id.HasValue)
                {
                    FieldDefinition fd = fdh.GetFieldDefinition(id.Value);
                    if (fd.Scope == Scope.FileFloating || fd.Scope == Scope.FileFileTypeDependent)
                    {
                        aFileType.RegisteredFields.Add(id.Value);
                    }
                    else
                    {
                        retVal.Add(new FailedMetaData(item.Name, "RegisteredField.Scope", MetaDataErrorType.InvalidOperation, "Scope is " + fd.Scope.ToString() + ". Scope must be FileFloating or FileFileTypeDependent!"));
                    }
                }
                else
                {
                    retVal.Add(new FailedMetaData(item.Name, "RegisteredField.Name", MetaDataErrorType.AdamItemNotFound));
                }
            }
            return retVal;
        }

        private static void RemoveFields(AManagement.FileType aFileType)
        {
            List<Guid> ids = new List<Guid>();
            foreach (AManagement.RegisteredField item in aFileType.RegisteredFields)
            {
                ids.Add(item.FieldId);
            }
            foreach (var id in ids)
            {
                aFileType.RegisteredFields.Remove(id);
            }
        }
    }
}
