﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XUser = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.User;
using AManagement = Adam.Core.Management;
using Adam.Core;
using System.Text.RegularExpressions;
using Adam.Core.Search;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class User : XPatch<XUser.User, AManagement.User>
    {
        protected override void UpdateItem(AManagement.User aItem)
        {
            UpdateAttributes(aItem);
            SetLocalization(aItem);
            SetAccount(aItem);
            AddMemberships(aItem);
        }

        private void UpdateAttributes(AManagement.User user)
        {
            user.Email = this.MetaData.Email;
            UpdatePassword(user);
        }

        private void UpdatePassword(AManagement.User user)
        {
            if (ValidatePassword(this.MetaData.Password))
            {
                if (user.IsNew)
                {
                    user.Password = this.MetaData.Password;
                }

                if (!user.IsNew && 
                    ValidatePassword(this.MetaData.UpdatePassword) &&
                    !this.MetaData.Password.Equals(this.MetaData.UpdatePassword))
                {
                    AManagement.UserHelper userHelper = new AManagement.UserHelper(this.Context.AdamApplication);
                    userHelper.ChangePassword(user.Id, this.MetaData.Password, this.MetaData.UpdatePassword);
                }
            }
        }

        private void SetLocalization(AManagement.User user)
        {
            user.LanguageId = GetLanguageId(this.MetaData.Localization.Language);
            user.LanguageIdForUI = GetLanguageId(this.MetaData.Localization.LanguageForUI);
        }

        private Guid GetLanguageId(string name)
        {
            Guid result = Guid.Empty;
            var languageHelper = new AManagement.LanguageHelper(this.Context.AdamApplication);
            var id = languageHelper.GetId(name);
            if (id.HasValue)
            {
                result = id.Value;
            }
            else
            {
                AddErrorCaseToContext(name, "User.Language.Name", MetaDataErrorType.AdamItemNotFound);
            }
            return result;
        }

        private void SetAccount(AManagement.User user)
        {
            UpdateExpirationDate(user);
            user.LockedOut = this.MetaData.Account.LockedOut;
            user.StorageQuota = this.MetaData.Account.StorageQuota;
        }

        private void UpdateExpirationDate(AManagement.User user)
        {
            user.ExpirationDateUtc = this.MetaData.Account.ExpirationDate;
        }

        private void AddMemberships(AManagement.User user)
        {
            ClearList(user.Memberships);

            AManagement.UserGroup ug = new AManagement.UserGroup(this.Context.AdamApplication);
            foreach (var xUserGroup in this.MetaData.Membership)
            {
                if (ug.TryLoad(xUserGroup.Name) == TryLoadResult.Success)
                {
                    if (!user.Memberships.Contains(ug.Id))
                    {
                        user.Memberships.Add(ug.Id);
                    }
                }
                else
                {
                    AddErrorCaseToContext(this.MetaData.Name, "User.Membership.UserGroup.Name", MetaDataErrorType.AdamItemNotFound);
                }
            }
        }

        private void ClearList(AManagement.UserMembershipCollection userMembershipCollection)
        {
            AManagement.UserGroupHelper ugh = new AManagement.UserGroupHelper(this.Context.AdamApplication);
            Guid? everyoneId = ugh.GetId("Everyone");
            if (everyoneId.HasValue)
            {
                for (int i = 0; i < userMembershipCollection.Count; i++)
                {
                    AManagement.UserMembership item = userMembershipCollection[0];
                    if (item.UserGroupId != everyoneId.Value)
                    {
                        userMembershipCollection.Remove(item.UserGroupId);
                    }
                }
            }
            else
            {
                AddErrorCaseToContext("Everyone", "UserGroup.Id", MetaDataErrorType.AdamItemNotFound);
            }
        }

        private bool ValidatePassword(string pwd)
        {
            bool retVal = true;

            if (String.IsNullOrEmpty(pwd))
            {
                pwd = String.Empty;
            }

            int minimumPasswordLength = (int)this.Context.AdamApplication.GetSetting(SettingType.MinimumPasswordLength);
            int minimumNumberOfNonAlphanumericCharactersInPassword = (int)this.Context.AdamApplication.GetSetting(SettingType.MinimumNumberOfNonAlphanumericCharactersInPassword);
            
            if (pwd.Length < minimumPasswordLength)
            {
                retVal = false;
                AddErrorCaseToContext(String.Empty, "User.Password.minimumPasswordLength", MetaDataErrorType.XmlInvalidDataFormat);
            }
            
            var pattern = "(.*[^\\w].*){" + minimumNumberOfNonAlphanumericCharactersInPassword.ToString() + ",}";
            if (!Regex.IsMatch(pwd, pattern))
            {
                retVal = false;
                AddErrorCaseToContext(String.Empty, "User.Password.minimumNumberOfNonAlphanumericCharactersInPassword", MetaDataErrorType.XmlInvalidDataFormat);
            }

            return retVal;
        }
    }
}
