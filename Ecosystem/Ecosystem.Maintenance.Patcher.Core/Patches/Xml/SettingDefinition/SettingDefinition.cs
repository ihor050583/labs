﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XSettingDefinition = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.SettingDefinition;
using ASettings = Adam.Core.Settings;
using Adam.Core;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.SettingValue;
using Adam.Core.Search;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class SettingDefinition : Patch<XSettingDefinition.SettingDefinition>
    {
        protected override void ExecuteHlp()
        {
            ASettings.SettingDefinitionHelper helper = new ASettings.SettingDefinitionHelper(this.Context.AdamApplication);
            ASettings.SettingDefinition aSettingDefinition = helper.GetSettingDefinition(this.MetaData.Name);

            if (aSettingDefinition == null)
            {
                aSettingDefinition = GetSettingOfType();
                aSettingDefinition.AddNew();
            }
            if (this.MetaData.Action == Core.MetaData.Xml.PatchAction.AddOrUpdate)
            {
                UpdateItem(aSettingDefinition);
            }
            else if (this.MetaData.Action == Core.MetaData.Xml.PatchAction.Delete)
            {
                DeleteItem(aSettingDefinition);
            }
        }

        private void DeleteItem(ASettings.SettingDefinition aSettingDefinition)
        {
            if (!aSettingDefinition.IsNew)
            {
                try
                {
                    aSettingDefinition.Delete();
                }
                catch (Exception ex)
                {
                    AddErrorCaseToContext(aSettingDefinition.Name, aSettingDefinition.GetType().ToString(), MetaDataErrorType.Unexpected, ex.Message);
                }
            }
        }

        protected void UpdateItem(ASettings.SettingDefinition aItem)
        {
            SetName(aItem);
            SetSchema(aItem);
            SetAccessControls(aItem);
            SetDefaultValue(aItem);
            SetCategory(aItem);
            SaveAdamItem(aItem, "SettingDefinition");
        }

        private void SetName(ASettings.SettingDefinition aSettingDefinition)
        {
            aSettingDefinition.Name = this.MetaData.Name;
        }

        private void SetCategory(ASettings.SettingDefinition aSettingDefinition)
        {
            ASettings.SettingCategory category = new ASettings.SettingCategory(this.Context.AdamApplication);
            if (category.TryLoad(this.MetaData.Category) == TryLoadResult.Success)
            {
                aSettingDefinition.CategoryId = category.Id;
            }
            else
            {
                AddErrorCaseToContext(this.MetaData.Category, "SettingDefinition.Category", MetaDataErrorType.AdamItemNotFound);
            }
        }

        private void SetSchema(ASettings.SettingDefinition aSettingDefinition)
        {
            if (this.MetaData.DataType == ASettings.SettingDataType.Xml && !String.IsNullOrEmpty(this.MetaData.Schema))
            {
                (aSettingDefinition as ASettings.XmlSettingDefinition).Schema = this.MetaData.Schema.Trim();
            }
        }

        private void SetAccessControls(ASettings.SettingDefinition aSettingDefinition)
        {
            if (this.MetaData.AccessControl != null)
            {
                aSettingDefinition.AllowSystemSetting = this.MetaData.AccessControl.AllowSystemSetting;
                aSettingDefinition.AllowUserSetting = this.MetaData.AccessControl.AllowUserSetting;
                aSettingDefinition.AllowAnonymousAccess = this.MetaData.AccessControl.AllowAnonymousAccess;
                aSettingDefinition.AllowSiteSetting = this.MetaData.AccessControl.AllowSiteSetting;
            }
        }

        private void SetDefaultValue(ASettings.SettingDefinition aSettingDefinition)
        {
            dynamic item = aSettingDefinition;
            item.DefaultValue = GetDefaultValue();
        }

        private dynamic GetDefaultValue()
        {
            dynamic retVal = null;

            string value = Extensions.RemoveCDATA(this.MetaData.DefaultValue.Trim());

            if (this.MetaData.DataType == ASettings.SettingDataType.Boolean)
            {
                retVal = SettingHlp.GetBooleanSetting(value);
            }
            else if (this.MetaData.DataType == ASettings.SettingDataType.DateTime)
            {
                retVal = SettingHlp.GetDateTimeSetting(value);
            }
            else if (this.MetaData.DataType == ASettings.SettingDataType.Numeric)
            {
                retVal = SettingHlp.GetNumericSetting(value);
            }
            else if (this.MetaData.DataType == ASettings.SettingDataType.Role)
            {
                retVal = SettingHlp.GetRoleSetting(value);
            }
            else if (this.MetaData.DataType == ASettings.SettingDataType.Xml)
            {
                if (String.IsNullOrEmpty(value))
                {
                    value = "<DefalutValue>1</DefalutValue>";
                }
                retVal = value;
            }
            else
            {
                retVal = value;
            }

            return retVal;
        }

        
        private ASettings.SettingDefinition GetSettingOfType()
        {
            ASettings.SettingDefinition sd = null;

            if (this.MetaData.DataType == ASettings.SettingDataType.Boolean)
            {
                sd = new ASettings.BooleanSettingDefinition(this.Context.AdamApplication);
            }
            else if (this.MetaData.DataType == ASettings.SettingDataType.DateTime)
            {
                sd = new ASettings.DateTimeSettingDefinition(this.Context.AdamApplication);
            }
            else if (this.MetaData.DataType == ASettings.SettingDataType.Numeric)
            {
                sd = new ASettings.NumericSettingDefinition(this.Context.AdamApplication);
            }
            else if (this.MetaData.DataType == ASettings.SettingDataType.Role)
            {
                sd = new ASettings.RoleSettingDefinition(this.Context.AdamApplication);
            }
            else if (this.MetaData.DataType == ASettings.SettingDataType.Text)
            {
                sd = new ASettings.TextSettingDefinition(this.Context.AdamApplication);
            }
            else if (this.MetaData.DataType == ASettings.SettingDataType.Xml)
            {
                sd = new ASettings.XmlSettingDefinition(this.Context.AdamApplication);
            }

            return sd;
        }
        

    }
}
