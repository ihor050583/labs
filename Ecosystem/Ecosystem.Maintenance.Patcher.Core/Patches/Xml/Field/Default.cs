﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Fields;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class Default
    {
        internal static List<FailedMetaData> SetDefaults(FieldDefinition fd, MetaData.Xml.Field.Field xField, Application app)
        {
            List<FailedMetaData> list = new List<FailedMetaData>();

            if (xField.Defaults != null)
            {
                SetResetToDefaultTriggers(fd, xField.Name, xField.Defaults, list);
                SetResetToDefaultFields(fd, xField.Name, xField.Defaults, list, app);
                if (xField.Defaults.DefaultValue != null)
                {
                    fd.DefaultValue = Extensions.RemoveCDATA(xField.Defaults.DefaultValue);
                }
            }

            return list;
        }

        private static void SetResetToDefaultFields(FieldDefinition fd, string name, Defaults xDefaults, List<FailedMetaData> list, Application app)
        {
            if (xDefaults.ResetToDefaultFields != null)
            {
                var fdh = new FieldDefinitionHelper(app);
                foreach (string resetToDefaultField in xDefaults.ResetToDefaultFields)
                {
                    Guid? id = fdh.GetId(resetToDefaultField);
                    if (id.HasValue)
                    {
                        fd.ResetToDefaultFields.Add(id.Value);
                    }
                    else
                    {
                        list.Add(new FailedMetaData(name, "Field.ResetToDefaultFields", MetaDataErrorType.AdamItemNotFound,
                            String.Format("Can't find field definition for field: {0}", resetToDefaultField)));
                    }
                }
            }
        }

        private static void SetResetToDefaultTriggers(FieldDefinition fd, string xName, Defaults xDefaults, List<FailedMetaData> list)
        {
            if (xDefaults.ResetToDefaultFields != null)
            {
                fd.ResetToDefaultTriggers = ResetToDefaultTriggers.OnNewField;

                foreach (var item in xDefaults.ResetToDefaultTriggers)
                {
                    fd.ResetToDefaultTriggers |= item.Name;
                }
            }
        }
    }
}
