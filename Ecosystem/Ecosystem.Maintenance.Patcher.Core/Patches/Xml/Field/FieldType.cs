﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adam.Core;
using AField = Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class FieldType
    {
        public static AField.FieldDefinition GetFieldDefinition(AField.DataType xType, Application app)
        {
            AField.FieldDefinition fd = null;
            if (xType == AField.DataType.ClassificationList)
            {
                fd = new AField.ClassificationListFieldDefinition(app);
            }
            else if (xType == AField.DataType.Date)
            {
                fd = new AField.DateFieldDefinition(app);
            }
            else if (xType == AField.DataType.DateTime)
            {
                fd = new AField.DateTimeFieldDefinition(app);
            }
            else if (xType == AField.DataType.Duration)
            {
                fd = new AField.DurationFieldDefinition(app);
            }
            else if (xType == AField.DataType.DynamicOptionList)
            {
                fd = new AField.DynamicOptionListFieldDefinition(app);
            }
            else if (xType == AField.DataType.LanguageList)
            {
                fd = new AField.LanguageListFieldDefinition(app);
            }
            else if (xType == AField.DataType.Numeric)
            {
                fd = new AField.NumericFieldDefinition(app);
            }
            else if (xType == AField.DataType.OptionList)
            {
                fd = new AField.OptionListFieldDefinition(app);
            }
            else if (xType == AField.DataType.RecordLink)
            {
                fd = new AField.RecordLinkFieldDefinition(app);
            }
            else if (xType == AField.DataType.RecordList)
            {
                fd = new AField.RecordListFieldDefinition(app);
            }
            else if (xType == AField.DataType.Text)
            {
                fd = new AField.TextFieldDefinition(app);
            }
            else if (xType == AField.DataType.Time)
            {
                fd = new AField.TimeFieldDefinition(app);
            }
            else if (xType == AField.DataType.UserGroupList)
            {
                fd = new AField.UserGroupListFieldDefinition(app);
            }
            else if (xType == AField.DataType.UserList)
            {
                fd = new AField.UserListFieldDefinition(app);
            }
            return fd;
        }
    }
}
