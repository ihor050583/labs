﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XField = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field;
using AField = Adam.Core.Fields;
using AManagement = Adam.Core.Management;
using Adam.Core;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class Field : Patch<XField.Field>
    {
        protected override void ExecuteHlp()
        {
            AField.FieldDefinitionHelper helper = new AField.FieldDefinitionHelper(this.Context.AdamApplication);
            AField.FieldDefinition aFieldDefinition = helper.GetFieldDefinition(this.MetaData.Name);

            if (aFieldDefinition == null)
            {
                aFieldDefinition = FieldType.GetFieldDefinition(this.MetaData.DataType, this.Context.AdamApplication);
                aFieldDefinition.AddNew();
            }
            
            if (this.MetaData.Action == Core.MetaData.Xml.PatchAction.AddOrUpdate)
            {
                UpdateItem(aFieldDefinition);
            }
            else if (this.MetaData.Action == Core.MetaData.Xml.PatchAction.Delete)
            {
                DeleteItem(aFieldDefinition);
            }
        }

        private void DeleteItem(AField.FieldDefinition aFieldDefinition)
        {
            if (!aFieldDefinition.IsNew)
            {
                try
                {
                    aFieldDefinition.Delete();
                }
                catch (Exception ex)
                {
                    AddErrorCaseToContext(aFieldDefinition.Name, aFieldDefinition.GetType().ToString(), MetaDataErrorType.Unexpected, ex.Message);
                }
            }
        }

        protected  void UpdateItem(AField.FieldDefinition aItem)
        {
            UpdateAttributes(aItem);
            AddMemberships(aItem);
            SetSortIndex(aItem);

            List<FailedMetaData> list1 = Specific.SetDataTypeSpecific(aItem, this.MetaData, ScenarioType.XmlToAdam, this.Context.AdamApplication);
            List<FailedMetaData> list2 = Default.SetDefaults(aItem, this.MetaData, this.Context.AdamApplication);

            list1.AddRange(list2);

            AddErrorCaseToContext(list1);

            SaveItem(aItem, "FieldDefinition");
        }

        protected void SaveItem(dynamic aItem, string type)
        {
            try
            {
                aItem.Save();
            }
            catch (Exception ex)
            {
                AddErrorCaseToContext(aItem.Name, type, MetaDataErrorType.Unexpected, ex.Message);
            }
        }

        private void UpdateAttributes(AField.FieldDefinition fd)
        {
            fd.Name = this.MetaData.Name;
            fd.IsRequired = this.MetaData.IsRequired;
            fd.Indexed = this.MetaData.IsIndexed;
            fd.FullTextIndexed = this.MetaData.AllowOperatorSearches;
            fd.IsReadOnly = this.MetaData.Advanced.IsReadOnly;
            fd.InheritanceMode = this.MetaData.Behaviour.InheritanceMode;
            fd.LanguageMode = this.MetaData.Behaviour.LanguageMode;
            fd.StorageMode = this.MetaData.Advanced.StorageMode;
            fd.Scope = this.MetaData.Scope;
        }

        private void AddMemberships(AField.FieldDefinition fd)
        {
            AManagement.FieldGroup fg = new AManagement.FieldGroup(this.Context.AdamApplication);

            ClearMemberships(fd);

            foreach (var xFieldGroup in this.MetaData.Membership)
            {
                if (fg.TryLoad(xFieldGroup.Name) == TryLoadResult.Success)
                {
                    if (!fd.Memberships.Contains(fg.Id))
                    {
                        fd.Memberships.Add(fg.Id);
                    }
                }
                else
                {
                    AddErrorCaseToContext(this.MetaData.Name, "Field.Membership.FieldGroup.Name", MetaDataErrorType.AdamItemNotFound,
                        String.Format("FieldGroup: {0}", xFieldGroup.Name), false);
                }
            }
        }

        private void ClearMemberships(AField.FieldDefinition fd)
        {
            Guid [] ids = fd.Memberships.CopyIdsToArray();
            foreach (var id in ids)
            {
                if (id != AManagement.FieldGroup.AllFieldsId)
                {
                    fd.Memberships.Remove(id);
                }
            }
        }

        private void SetSortIndex(AField.FieldDefinition fd)
        {
            if (this.MetaData.SortIndex > 0)
            {
                fd.SortIndex = this.MetaData.SortIndex;
            }
        }
    }
}


