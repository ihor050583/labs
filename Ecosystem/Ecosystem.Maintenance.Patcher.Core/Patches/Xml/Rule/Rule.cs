﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Adam.Core;
using Adam.Core.Settings;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Rule;
using AManagement = Adam.Core.Management;
using ARule = Adam.Core.Rules;
using XRule = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Rule;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class Rule : XPatch<XRule.Rule, ARule.Rule>
    {
        protected override void UpdateItem(ARule.Rule aItem)
        {
            aItem.Target = this.MetaData.Target;
            aItem.Enabled = this.MetaData.Enabled;

            UpdateConditions(aItem);
            UpdateActions(aItem);
        }

        private void UpdateActions(ARule.Rule aRule)
        {
            if (aRule.Actions.Count > 0)
            {
                aRule.Actions.RemoveAll();
            }
            foreach (var action in this.MetaData.Actions)
            {
                if (action.Type == RuleActionType.Reference)
                {
                    aRule.Actions.Add(new ARule.ReferenceRuleAction(this.Context.AdamApplication)
                        {
                            ManualExecutionTrigger = action.Trigger,
                            Reference = Extensions.RemoveCDATA(action.Reference)
                        });
                }
                else if (action.Type == RuleActionType.RefreshFiles)
                {
                    aRule.Actions.Add(new ARule.RefreshFilesRuleAction(this.Context.AdamApplication));
                }
                else if (action.Type == RuleActionType.Custom)
                {
                    ApplyCustomModule(action.Name, action.Value);
                    Type type = Type.GetType(action.Value);
                    ARule.RuleAction ruleAction = (ARule.RuleAction)Activator.CreateInstance(
                        type, new object[] { this.Context.AdamApplication }
                        );
                    aRule.Actions.Add(ruleAction);
                }
            }
        }

        private void ApplyCustomModule(string name, string value)
        {
            XmlSetting setting = new XmlSetting(this.Context.AdamApplication);
            setting.LoadSystemValue(SettingType.RegisteredRuleActions);
            XDocument documentActions = XDocument.Parse(setting.Value);

            int count = documentActions.Root.Elements().Where(c => c.Attribute("name").Value.Equals(name,
               StringComparison.OrdinalIgnoreCase)).Count();

            if (count != 0)
            {
                documentActions.Root.Elements().Where(c => c.Attribute("name").Value.Equals(name,
                    StringComparison.OrdinalIgnoreCase)).Remove();
            }

            documentActions.Root.Add(
                new XElement("add", new XAttribute("name", name), new XAttribute("type", value))
                );

            setting.Value = documentActions.Root.ToString();
            setting.Save();
        }

        private void UpdateConditions(ARule.Rule aRule)
        {
            aRule.Conditions.RemoveAll();
            foreach (var condition in this.MetaData.Conditions)
            {
                if (condition.Type == ConditionType.Changed)
                {
                    aRule.Conditions.Add(new ARule.ObjectChangedRuleCondition(this.Context.AdamApplication));
                }
                else if (condition.Type == ConditionType.Created)
                {
                    aRule.Conditions.Add(new ARule.ObjectCreatedRuleCondition(this.Context.AdamApplication));
                }
                else if (condition.Type == ConditionType.CreatedChanged)
                {
                    aRule.Conditions.Add(new ARule.ObjectCreatedOrChangedRuleCondition(this.Context.AdamApplication));
                }
                else if (condition.Type == ConditionType.MovieAddWithoutMoviePreview)
                {
                    aRule.Conditions.Add(new ARule.MovieAddedWithoutMoviePreviewRuleCondition(this.Context.AdamApplication)
                    { 
                        MoviePreviewExtension = condition.Value 
                    });
                }
            }
        }
    }
}
