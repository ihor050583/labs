﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using AManagement = Adam.Core.Management;
using XLanguage = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Language;
using System.Globalization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class Language : XPatch<XLanguage.Language, AManagement.Language>
    {
        protected override void UpdateItem(AManagement.Language aItem)
        {
            aItem.EnabledForFields = this.MetaData.EnabledForFields;
            UpdateExcludedKeywords(aItem);
            UpdateCulture(aItem);
        }
        
        private void UpdateExcludedKeywords(AManagement.Language aLanguage)
        {
            if (!String.IsNullOrEmpty(this.MetaData.ExcludedKeywords))
            {
                aLanguage.ExcludedKeywords = Extensions.RemoveCDATA(this.MetaData.ExcludedKeywords);
            }
        }

        private void UpdateCulture(AManagement.Language aLanguage)
        {
            aLanguage.Culture = String.Empty;
            CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);
            foreach (CultureInfo item in cultures)
            {
                //Language.Culture is case-sensitive
                if (this.MetaData.Culture.Equals(item.Name, StringComparison.Ordinal))
                {
                    aLanguage.Culture = this.MetaData.Culture;
                    break;
                }
            }
            if (String.IsNullOrEmpty(aLanguage.Culture))
            {
                AddErrorCaseToContext(this.MetaData.Culture, "Language.Culture", MetaDataErrorType.XmlInvalidDataFormat);
            }
        }
    }
}
