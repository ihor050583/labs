﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XTranslation = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Translation;
using AManagement = Adam.Core.Management;
using Adam.Core;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class Translation : XPatch<XTranslation.Translation, AManagement.Translation>
    {
        public Translation()
        {
            TryLoadItemCallback = TryLoadTranslation;
        }

        protected override void UpdateItem(AManagement.Translation aItem)
        {
            UpdateStudio(aItem);
            UpdateModule(aItem);
            UpdateLanguageValues(aItem);
        }

        private void UpdateModule(AManagement.Translation aItem)
        {
            aItem.Module = this.MetaData.Module;
            if (this.MetaData.UpdateModule != null)
            {
                aItem.Module = this.MetaData.UpdateModule.Value;
            }
        }

        private void UpdateStudio(AManagement.Translation aItem)
        {
            aItem.Studio = this.MetaData.Studio;
            if (this.MetaData.UpdateStudio != null)
            {
                aItem.Module = this.MetaData.UpdateStudio.Value;
            }
        }
        
        private void UpdateLanguageValues(AManagement.Translation aTranslation)
        {
            foreach (XTranslation.TranslationLanguage item in this.MetaData.TranslationLanguages)
            {
                Guid id = GetLanguageId(item.Name);
                if (id == Guid.Empty || !aTranslation.Items.Contains(id))
                {
                    AddErrorCaseToContext(item.Name, "Translation.Language", MetaDataErrorType.AdamItemNotFound);
                }
                else 
                {
                    if (String.IsNullOrEmpty(item.LanguageValue))
                    {
                        aTranslation.Items[id].Value = String.Empty;
                    }
                    else
                    {
                        aTranslation.Items[id].Value = Extensions.RemoveCDATA(item.LanguageValue);
                    }
                }
            }
        }

        private Guid GetLanguageId(string name)
        {
            Guid result = Guid.Empty;
            var languageHelper = new AManagement.LanguageHelper(this.Context.AdamApplication);
            var id = languageHelper.GetId(name);
            if (id.HasValue)
            {
                result = id.Value;
            }
            return result;
        }

        private TryLoadResult TryLoadTranslation(AManagement.Translation aTranslation)
        {
            return aTranslation.TryLoad(this.MetaData.Studio, this.MetaData.Module, this.MetaData.Name);
        }
    }
}
