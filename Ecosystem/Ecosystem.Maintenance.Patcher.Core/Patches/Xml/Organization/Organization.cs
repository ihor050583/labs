﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Search;
using AManagement = Adam.Core.Management;
using XOrganization = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Organization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class Organization : XPatch<XOrganization.Organization, AManagement.Organization>
    {
        protected override void UpdateItem(AManagement.Organization aItem)
        {           
        }
    }
}
