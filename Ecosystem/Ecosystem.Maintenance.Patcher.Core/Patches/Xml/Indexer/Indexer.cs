﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using AIndexer = Adam.Core.Indexer;
using XIndexer = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Indexer;
using AClassifications = Adam.Core.Classifications;
using Adam.Core.Search;
using Adam.Core.Settings;
using System.Xml;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch
{
    public class Indexer : XPatch<XIndexer.Indexer, AIndexer.IndexerTask>
    {
        protected override void UpdateItem(AIndexer.IndexerTask aItem)
        {
            UpdateClassify(aItem);
            UpdateAttributes(aItem);
            UpdateClassification(aItem);
            UpdateFilesAndDirectories(aItem);
            UpdateLogging(aItem);
            UpdateJob(aItem);
            UpdateActions(aItem);
            UpdateImports(aItem);
            UpdateNotification(aItem);
            UpdateCustomEngine(aItem);
        }

        private void UpdateClassify(AIndexer.IndexerTask aItem)
        {
            aItem.ClassifyIn = SetClassificationId("Indexer.Classification.ClassifyIn", this.MetaData.Classification.ClassifyIn);
            aItem.ClassifyFoldersRoot = SetClassificationId("Indexer.Classification.Root", this.MetaData.Classification.Root);
        }

        protected override void PreDeleteItem(AIndexer.IndexerTask aItem)
        {
            if (!aItem.ProcessEngine.Equals("Default", StringComparison.Ordinal))
            {
                DeleteCustomEngine(aItem);
            }
        }

        private void DeleteCustomEngine(AIndexer.IndexerTask aIndexer)
        {
            AIndexer.IndexerTaskHelper indexerHelper = new AIndexer.IndexerTaskHelper(this.Context.AdamApplication);
            SearchExpression searchExpr = new SearchExpression("ProcessEngine = " + aIndexer.ProcessEngine);

            List<Guid> list = indexerHelper.GetIds(searchExpr).ToList();

            if (list.Count == 1)
            {
                XmlSetting setting = new XmlSetting(this.Context.AdamApplication);
                setting.LoadSystemValue(SettingType.RegisteredIndexerProcessEngines);

                XmlDocument value = new XmlDocument();
                value.LoadXml(setting.HasValue ? setting.Value : setting.DefaultValue);
                XmlElement engines = (XmlElement)value.SelectSingleNode("/engines");
                XmlElement element = engines.SelectSingleNode("./add[@name='" + aIndexer.ProcessEngine + "']") as XmlElement;

                if (element != null)
                {
                    engines.RemoveChild(element);
                    setting.Value = value.OuterXml;
                    setting.Save();
                }
            }
        }

        private Guid? SetClassificationId(string errorType, string namePath)
        {
            Guid? clnId = null;
            if (!String.IsNullOrEmpty(namePath))
            {
                AClassifications.ClassificationHelper clnHelper = new AClassifications.ClassificationHelper(this.Context.AdamApplication);
                Guid? result = clnHelper.GetId(new AClassifications.ClassificationPath(namePath));
                if (result.HasValue)
                {
                    clnId = result;
                }
                else
                {
                    AddErrorCaseToContext(this.MetaData.Name, errorType, MetaDataErrorType.AdamItemNotFound,
                        String.Format("value provided was not found: {0}", namePath));
                }
            }
            return clnId;
        }

        private void UpdateCustomEngine(AIndexer.IndexerTask aIndexer)
        {
            if (!this.MetaData.ProcessEngine.Equals("Default", StringComparison.Ordinal))
            {
                XmlSetting setting = new XmlSetting(this.Context.AdamApplication);
                setting.LoadSystemValue(SettingType.RegisteredIndexerProcessEngines);

                XmlDocument value = new XmlDocument();
                value.LoadXml(setting.HasValue ? setting.Value : setting.DefaultValue);
                XmlElement engines = (XmlElement)value.SelectSingleNode("/engines");

                string name = this.MetaData.CustomEngine.UniqueName;
                string type = string.Format("{0}, {1}", this.MetaData.CustomEngine.TypeFullName, this.MetaData.CustomEngine.AssemblyName);
                XmlElement element = engines.SelectSingleNode("./add[@name='" + name + "']") as XmlElement;

                if (element == null)
                {
                    element = value.CreateElement("add");
                    element.SetAttribute("name", name);
                    element.SetAttribute("type", type);
                    engines.AppendChild(element);
                }
                else
                {
                    element.SetAttribute("name", name);
                    element.SetAttribute("type", type);
                }
                setting.Value = value.OuterXml;
                setting.Save();
            }
        }

        private void UpdateAttributes(AIndexer.IndexerTask aIndexer)
        {
            aIndexer.Name = this.MetaData.Name;
            aIndexer.Path = this.MetaData.Path;
            aIndexer.Enabled = this.MetaData.Enabled;
            aIndexer.ScanEngine = this.MetaData.ScanEngine;
            aIndexer.ProcessEngine = this.MetaData.ProcessEngine;
            aIndexer.CatalogMode = this.MetaData.CatalogMode;
        }

        private void UpdateClassification(AIndexer.IndexerTask aIndexer)
        {
            aIndexer.ClassifyFolders = this.MetaData.Classification.ClassifySubfolders;
            if (!String.IsNullOrEmpty(this.MetaData.Classification.IgnorePath))
            {
                aIndexer.ClassifyFoldersIgnorePath = this.MetaData.Classification.IgnorePath;
            }
            aIndexer.ClassifyFoldersDepth = this.MetaData.Classification.MaximumDepth;
        }

        private void UpdateFilesAndDirectories(AIndexer.IndexerTask aIndexer)
        {
            aIndexer.CatalogHiddenFiles = this.MetaData.FilesAndDirectories.CatalogHiddenFiles;
            aIndexer.CatalogHiddenDirectories = this.MetaData.FilesAndDirectories.CatalogHiddenDirectories;
            aIndexer.ClassifyFoldersDepth = this.MetaData.FilesAndDirectories.DeleteFoldersFromDepth;
            aIndexer.FileExtensionRestriction = this.MetaData.FilesAndDirectories.FileExtensionRestriction;
            aIndexer.FileTypeRestriction = this.MetaData.FilesAndDirectories.FileTypeRestriction;

            UpdateFailedFilesFolder(aIndexer);
            SetFileSelectionPatterns(aIndexer);
        }

        private void UpdateFailedFilesFolder(AIndexer.IndexerTask aIndexer)
        {
            if (!String.IsNullOrEmpty(this.MetaData.FilesAndDirectories.FailedFilesFolder))
            {
                aIndexer.FailedFilesFolder = this.MetaData.FilesAndDirectories.FailedFilesFolder;
            }
        }

        private void SetFileSelectionPatterns(AIndexer.IndexerTask aIndexer)
        {
            aIndexer.FileSelectionPatterns.RemoveAll();
            if (this.MetaData.FilesAndDirectories.FileSelectionPatterns != null)
            {
                foreach (XIndexer.FileSelectionPattern item in this.MetaData.FilesAndDirectories.FileSelectionPatterns.Patterns)
                {
                    //XIndexer.FileSelectionPattern.Expression can be null or String.Empty
                    aIndexer.FileSelectionPatterns.Add(item.Mode, item.Expression);
                }
            }
        }

        private void UpdateLogging(AIndexer.IndexerTask aIndexer)
        {
            aIndexer.TargetLogSeverity = this.MetaData.Logging.LogSeverity;
        }

        private void UpdateJob(AIndexer.IndexerTask aIndexer)
        {
            aIndexer.JobPriority = this.MetaData.Job.Priority;
            aIndexer.JobThreshold = this.MetaData.Job.Threshold;
            aIndexer.MaximumNumberOfRetries = this.MetaData.Job.MaximumNumberOfRetries;
            aIndexer.JobThresholdTimeout = TimeSpan.Parse(this.MetaData.Job.ThresholdTimeout);
            aIndexer.MinimumRetryWaitTime = TimeSpan.Parse(this.MetaData.Job.MinimumRetryWaitTime);
        }

        private void UpdateActions(AIndexer.IndexerTask aIndexer)
        {
            aIndexer.ActionOnSuccess = this.MetaData.Actions.OnSuccessAction;
            aIndexer.DuplicateAction = this.MetaData.Actions.DuplicateAction;
            aIndexer.FieldValidation = this.MetaData.Actions.FieldValidation;
        }

        private void UpdateImports(AIndexer.IndexerTask aIndexer)
        {
            aIndexer.AllowMissingImports = this.MetaData.Imports.AllowMissingImports;
            aIndexer.MaximumNumberOfRetries = this.MetaData.Imports.MaximumRetriesWhenImportsAreMissing;
            aIndexer.ImportRestriction = this.MetaData.Imports.ImportRestriction;
        }

        private void UpdateNotification(AIndexer.IndexerTask aIndexer)
        {
            //this.MetaData.Notification.Agents can be null or String.Empty
            //this.MetaData.Notification.EmailSettings can be null or String.Empty
            aIndexer.NotificationAgents = Extensions.RemoveCDATA(this.MetaData.Notification.Agents);
            aIndexer.EmailNotificationSettings = Extensions.RemoveCDATA(this.MetaData.Notification.EmailSettings);
        }

        private bool ValidateScanEngine()
        {
            bool retVal = true;

            XmlSetting setting = new XmlSetting(this.Context.AdamApplication);
            setting.LoadSystemValue(SettingType.RegisteredIndexerScanEngines);

            XmlDocument value = new XmlDocument();
            value.LoadXml(setting.HasValue ? setting.Value : setting.DefaultValue);
            XmlElement engines = (XmlElement)value.SelectSingleNode("/engines");

            string name = this.MetaData.ScanEngine;
            XmlElement element = engines.SelectSingleNode("./add[@name='" + name + "']") as XmlElement;

            if (element == null)
            {
                retVal = false;
                AddErrorCaseToContext(this.MetaData.Name, "SettingType.RegisteredIndexerScanEngines." + name, MetaDataErrorType.AdamItemNotFound);
            }

            return retVal;
        }

        private bool ValidateProcessEngine()
        {
            bool retVal = true;

            XmlSetting setting = new XmlSetting(this.Context.AdamApplication);
            setting.LoadSystemValue(SettingType.RegisteredIndexerProcessEngines);

            XmlDocument value = new XmlDocument();
            value.LoadXml(setting.HasValue ? setting.Value : setting.DefaultValue);
            XmlElement engines = (XmlElement)value.SelectSingleNode("/engines");

            string name = this.MetaData.ProcessEngine;
            XmlElement element = engines.SelectSingleNode("./add[@name='" + name + "']") as XmlElement;
            
            if (element == null)
            {
                retVal = false;
                AddErrorCaseToContext(this.MetaData.Name, "SettingType.RegisteredIndexerProcessEngines." + name, MetaDataErrorType.AdamItemNotFound);
            }

            return retVal;
        }

        protected override void Validate()
        {
            ValidateScanEngine();
            //ValidateProcessEngine();
        }

        /*
       
        private void ValidateXml()
        {
            //validate attributes
            if(String.IsNullOrEmpty(this.MetaData.Name))
            {
                AddErrorCaseToContext(String.Empty, "Indexer.Name", MetaDataErrorType.XmlValueIsEmpty);
            }
            if (String.IsNullOrEmpty(this.MetaData.Path))
            {
                AddErrorCaseToContext(String.Empty, "Indexer.Path", MetaDataErrorType.XmlValueIsEmpty);
            }
            //validate Classification
            if (this.MetaData.Classification.MaximumDepth < 0)
            {
                AddErrorCaseToContext(String.Empty, "Indexer.Classification.MaximumDepth", MetaDataErrorType.XmlInvalidDataFormat);
            }
            //validate FilesAndDirectories
            if (this.MetaData.FilesAndDirectories.DeleteFoldersFromDepth < 0)
            {
                AddErrorCaseToContext(String.Empty, "Indexer.FilesAndDirectories.DeleteFoldersFromDepth", MetaDataErrorType.XmlInvalidDataFormat);
            }
            //validate Job
            if (this.MetaData.Job.Priority <= 0)
            {
                AddErrorCaseToContext(String.Empty, "Indexer.Job.Priority", MetaDataErrorType.XmlInvalidDataFormat);
            }
            if (this.MetaData.Job.Threshold <= 0)
            {
                AddErrorCaseToContext(String.Empty, "Indexer.Job.Threshold", MetaDataErrorType.XmlInvalidDataFormat);
            }
            if (this.MetaData.Job.MaximumNumberOfRetries < 0)
            {
                AddErrorCaseToContext(String.Empty, "Indexer.Job.MaximumNumberOfRetries", MetaDataErrorType.XmlInvalidDataFormat);
            }
            if (this.MetaData.Job.MaximumNumberOfRetries < 0)
            {
                AddErrorCaseToContext(String.Empty, "Indexer.Job.MaximumNumberOfRetries", MetaDataErrorType.XmlInvalidDataFormat);
            }
            if (this.MetaData.Job.MaximumNumberOfRetries < 0)
            {
                AddErrorCaseToContext(String.Empty, "Indexer.Job.MaximumNumberOfRetries", MetaDataErrorType.XmlInvalidDataFormat);
            }
            TimeSpan timeSpan = new TimeSpan();
            if (!TimeSpan.TryParse(this.MetaData.Job.ThresholdTimeout, out timeSpan) || timeSpan <= new TimeSpan())
            {
                AddErrorCaseToContext(String.Empty, "Indexer.Job.ThresholdTimeout", MetaDataErrorType.XmlInvalidDataFormat);
            }
            if (!TimeSpan.TryParse(this.MetaData.Job.MinimumRetryWaitTime, out timeSpan) || timeSpan <= new TimeSpan())
            {
                AddErrorCaseToContext(String.Empty, "Indexer.Job.MinimumRetryWaitTime", MetaDataErrorType.XmlInvalidDataFormat);
            }
            //validate Imports
            if (this.MetaData.Imports.MaximumRetriesWhenImportsAreMissing < 0)
            {
                AddErrorCaseToContext(String.Empty, "Indexer.Imports.MaximumRetriesWhenImportsAreMissing", MetaDataErrorType.XmlInvalidDataFormat);
            }
            //validate ScanEngine
            ValidateScanEngine();
            //validate CustomEngine
            if (!this.MetaData.ProcessEngine.Equals("Default", StringComparison.Ordinal) && this.MetaData.CustomEngine == null)
            {
                AddErrorCaseToContext(String.Empty, "Indexer.CustomEngine", MetaDataErrorType.XmlValueIsEmpty);
            }
            if (!this.MetaData.ProcessEngine.Equals("Default", StringComparison.Ordinal) && this.MetaData.CustomEngine != null)
            {
                if (String.IsNullOrEmpty(this.MetaData.CustomEngine.AssemblyName))
                {
                    AddErrorCaseToContext(String.Empty, "Indexer.CustomEngine.AssemblyName", MetaDataErrorType.XmlInvalidDataFormat);
                }
                if (String.IsNullOrEmpty(this.MetaData.CustomEngine.TypeFullName))
                {
                    AddErrorCaseToContext(String.Empty, "Indexer.CustomEngine.TypeFullName", MetaDataErrorType.XmlInvalidDataFormat);
                }
                if (String.IsNullOrEmpty(this.MetaData.CustomEngine.UniqueName))
                {
                    AddErrorCaseToContext(String.Empty, "Indexer.CustomEngine.UniqueName", MetaDataErrorType.XmlInvalidDataFormat);
                }
                if (!this.MetaData.CustomEngine.UniqueName.Equals(this.MetaData.ProcessEngine))
                {
                    AddErrorCaseToContext(String.Empty, "Indexer.CustomEngine.UniqueName", MetaDataErrorType.XmlInvalidDataFormat);
                }
            }
        }
*/

    }
}
