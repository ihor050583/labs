﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Fields;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField
{
    public class Specific
    {
        public static List<FailedMetaData> SetDataTypeSpecific(FieldDefinition fd, MetaData.Xml.Field.Field xField, ScenarioType type, Application app)
        {
            List<FailedMetaData> list = new List<FailedMetaData>();
            TypeSpecific typeSpecific = GetTypeSpecificHelper(fd, xField, app, GetDataType(fd, xField, type));
            if (typeSpecific != null)
            {
                string xml = String.Empty;
                list = typeSpecific.Execute(type, ref xml);

                if (type == ScenarioType.AdamToXml)
                {
                    xField.DataTypeSpecific = xml;
                }
            }
            else
            {
                list.Add(new FailedMetaData(xField.Name, "Field.DataTypeSpecific", MetaDataErrorType.XmlUnknownType));
            }
            return list;
        }

        private static DataType GetDataType(FieldDefinition fd, MetaData.Xml.Field.Field xField, ScenarioType type)
        {
            return (type == ScenarioType.AdamToXml ? fd.DataType : xField.DataType);
        }

        private static TypeSpecific GetTypeSpecificHelper(FieldDefinition fd, MetaData.Xml.Field.Field xField, Application app, DataType dt)
        {
            TypeSpecific typeSpecific = null;
            string dts = xField.DataTypeSpecific == null ? String.Empty : Extensions.RemoveCDATA(xField.DataTypeSpecific);

            if (dt == DataType.ClassificationList)
            {
                ClassificationList item = String.IsNullOrEmpty(dts) ? new ClassificationList() : 
                    Extensions.XmlToObject<ClassificationList>(dts);

                typeSpecific = new ClassificationListHlp(fd, item, xField.Name, app);
            }
            else if (dt == DataType.Date)
            {
                Date item = String.IsNullOrEmpty(dts) ? new Date() :
                    Extensions.XmlToObject<Date>(dts);

                typeSpecific = new DateHlp(fd, item, xField.Name, app);
            }
            else if (dt == DataType.DateTime)
            {
                DateWithTime item = String.IsNullOrEmpty(dts) ? new DateWithTime() :
                    Extensions.XmlToObject<DateWithTime>(dts);

                typeSpecific = new DateTimeHlp(fd, item, xField.Name, app);
            }
            else if (dt == DataType.Duration)
            {
                Duration item = String.IsNullOrEmpty(dts) ? new Duration() :
                   Extensions.XmlToObject<Duration>(dts);

                typeSpecific = new DurationHlp(fd, item, xField.Name, app);
            }
            else if (dt == DataType.DynamicOptionList)
            {
                DynamicOptionList item = String.IsNullOrEmpty(dts) ? new DynamicOptionList() :
                   Extensions.XmlToObject<DynamicOptionList>(dts);

                typeSpecific = new DynamicOptionListHlp(fd, item, xField.Name, app);
            }
            else if (dt == DataType.LanguageList)
            {
                LanguageList item = String.IsNullOrEmpty(dts) ? new LanguageList() :
                  Extensions.XmlToObject<LanguageList>(dts);

                typeSpecific = new LanguageListHlp(fd, item, xField.Name, app);
            }
            else if (dt == DataType.Numeric)
            {
                Numeric item = String.IsNullOrEmpty(dts) ? new Numeric() :
                  Extensions.XmlToObject<Numeric>(dts);

                typeSpecific = new NumericHlp(fd, item, xField.Name, app);
            }
            else if (dt == DataType.OptionList)
            {
                OptionList item = String.IsNullOrEmpty(dts) ? new OptionList() :
                  Extensions.XmlToObject<OptionList>(dts);

                typeSpecific = new OptionListHlp(fd, item, xField.Name, app);
            }
            else if (dt == DataType.RecordLink)
            {
                RecordLink item = String.IsNullOrEmpty(dts) ? new RecordLink() :
                  Extensions.XmlToObject<RecordLink>(dts);

                typeSpecific = new RecordLinkHlp(fd, item, xField.Name, app);
            }
            else if (dt == DataType.RecordList)
            {
                RecordList item = String.IsNullOrEmpty(dts) ? new RecordList() :
                  Extensions.XmlToObject<RecordList>(dts);

                typeSpecific = new RecordListHlp(fd, item, xField.Name, app);
            }
            else if (dt == DataType.Text)
            {
                Text item = String.IsNullOrEmpty(dts) ? new Text() :
                    Extensions.XmlToObject<Text>(dts);

                typeSpecific = new TextHlp(fd, item, xField.Name, app);
            }
            else if (dt == DataType.Time)
            {
                Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field.Time item =
                    String.IsNullOrEmpty(dts) ? new Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field.Time() :
                Extensions.XmlToObject<Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field.Time>(dts);

                typeSpecific = new TimeHlp(fd, item, xField.Name, app);
            }
            else if (dt == DataType.UserGroupList)
            {
                UserGroupList item = String.IsNullOrEmpty(dts) ? new UserGroupList() :
                    Extensions.XmlToObject<UserGroupList>(dts);

                typeSpecific = new UserGroupListHlp(fd, item, xField.Name, app);
            }
            else if (dt == DataType.UserList)
            {
                UserList item = String.IsNullOrEmpty(dts) ? new UserList() :
                   Extensions.XmlToObject<UserList>(dts);

                typeSpecific = new UserListHlp(fd, item, xField.Name, app);
            }

            return typeSpecific;
        }

    }
}
