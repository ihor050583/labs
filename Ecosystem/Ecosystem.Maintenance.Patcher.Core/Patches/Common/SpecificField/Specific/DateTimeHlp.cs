﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adam.Core;
using Adam.Core.Fields;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField
{
    public class DateTimeHlp: TypeSpecific
    {
        public DateTimeHlp(FieldDefinition fd, MetaDataElement value, string xName, Application app) :
            base(fd, value, xName, app)
        {

        }

        protected override void ExecuteSpecificXml(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.DateWithTime;
            var fd = this.FieldDefinition as DateTimeFieldDefinition;
            if (ValidateAttributes(xValue, list))
            {
                if (!String.IsNullOrEmpty(xValue.DatePattern))
                {
                    fd.DatePattern = xValue.DatePattern;
                }
                if (!String.IsNullOrEmpty(xValue.DateTimePattern))
                {
                    fd.DateTimePattern = xValue.DateTimePattern;
                }
                fd.UseUtc = xValue.UseUtc;
            }
        }
        private bool ValidateAttributes(MetaData.Xml.Field.DateWithTime xValue, List<FailedMetaData> list)
        {
            bool isValid = true;
            //isValid = true if both are determined or both are not determined
            //otherwise isValid = false
            if (String.IsNullOrEmpty(xValue.DatePattern) && !String.IsNullOrEmpty(xValue.DateTimePattern))
            {
                isValid = false;
                list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.DateTime.DatePattern", MetaDataErrorType.XmlValueIsEmpty));
            }
            if (!String.IsNullOrEmpty(xValue.DatePattern) && String.IsNullOrEmpty(xValue.DateTimePattern))
            {
                isValid = false;
                list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.DateTime.DateTimePattern", MetaDataErrorType.XmlValueIsEmpty));
            }
            return isValid;
        }

        protected override void ExecuteSpecificAdam(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.DateWithTime;
            var fd = this.FieldDefinition as DateTimeFieldDefinition;
            xValue.DatePattern = fd.DatePattern;
            xValue.DateTimePattern = fd.DateTimePattern;
            xValue.UseUtc = fd.UseUtc;
        }
    }
}
