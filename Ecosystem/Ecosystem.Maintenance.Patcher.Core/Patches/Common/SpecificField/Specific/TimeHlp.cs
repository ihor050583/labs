﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adam.Core;
using Adam.Core.Fields;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField
{
    public class TimeHlp : TypeSpecific
    {
        public TimeHlp(FieldDefinition fd, MetaDataElement value, string xName, Application app) :
            base(fd, value, xName, app)
        {

        }

        protected override void ExecuteSpecificXml(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.Time;
            var fd = this.FieldDefinition as TimeFieldDefinition;
            if (!String.IsNullOrEmpty(xValue.TimePattern))
            {
                fd.TimePattern = xValue.TimePattern;
            }            
        }

        protected override void ExecuteSpecificAdam(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.Time;
            var fd = this.FieldDefinition as TimeFieldDefinition;
            xValue.TimePattern = fd.TimePattern;
        }
    }
}
