﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adam.Core;
using Adam.Core.Fields;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField
{
    public class DynamicOptionListHlp : TypeSpecific
    {
        public DynamicOptionListHlp(FieldDefinition fd, MetaDataElement value, string xName, Application app) :
            base(fd, value, xName, app)
        {

        }

        protected override void ExecuteSpecificXml(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.DynamicOptionList;
            var fd = this.FieldDefinition as DynamicOptionListFieldDefinition;

            if (xValue.Action == MetaData.Xml.PatchAction.AddOrUpdate)
            {
                if (ValidateAttributes(xValue, list))
                {
                    fd.ListSource = xValue.ListSource;
                }
            }
        }
        private bool ValidateAttributes(MetaData.Xml.Field.DynamicOptionList xValue, List<FailedMetaData> list)
        {
            bool isValid = true;
            if (String.IsNullOrEmpty(xValue.ListSource))
            {
                isValid = false;
                list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.DynamicOptionList.ListSource", MetaDataErrorType.XmlValueIsEmpty));
            }
            return isValid;
        }

        protected override void ExecuteSpecificAdam(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.DynamicOptionList;
            var fd = this.FieldDefinition as DynamicOptionListFieldDefinition;
            xValue.ListSource = fd.ListSource;
        }
    }
}
