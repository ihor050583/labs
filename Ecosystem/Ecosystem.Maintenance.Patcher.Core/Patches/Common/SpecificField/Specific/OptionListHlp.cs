﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adam.Core;
using AFields = Adam.Core.Fields;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField
{
    public class OptionListHlp : TypeSpecific
    {
        public OptionListHlp(AFields.FieldDefinition fd, MetaDataElement value, string xName, Application app) :
            base(fd, value, xName, app)
        {

        }

        protected override void ExecuteSpecificXml(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.OptionList;
            var fd = this.FieldDefinition as AFields.OptionListFieldDefinition;

            if (xValue.Action == MetaData.Xml.PatchAction.AddOrUpdate)
            {
                UpdateOptionList(list, xValue, fd);
            }
        }

        private void UpdateOptionList(List<FailedMetaData> list, MetaData.Xml.Field.OptionList xValue, AFields.OptionListFieldDefinition fd)
        {
            fd.AcceptMultipleOptions = xValue.AcceptMultipleOptions;
            //fd.SortOrder = Label by default
            //xValue.SortOrder = Label by default, if user forgot to determine xValue.SortOrder
            fd.SortOrder = xValue.SortOrder;
            ClearList(fd.Items);
            //fd.Options.Count=0 by default
            //xValue.Options.Count=0 by default
            //Each option should have a unique label within the language of this label
            if (xValue.Items != null && xValue.Items.Count != 0)
            {
                foreach (MetaData.Xml.Field.OptionListItemDefinition item in xValue.Items)
                {
                    if (!String.IsNullOrEmpty(item.Name))
                    {
                        fd.Items.Add(item.Name);
                        AFields.OptionListItemDefinition itemDefinition = fd.Items[item.Name];
                        itemDefinition.Tag = item.Tag;
                        //fill in Labels
                        if (item.Labels.Count != 0)
                        {
                            foreach (var label in item.Labels)
                            {
                                if (label.LanguageId != Guid.Empty)
                                {
                                    if (itemDefinition.Labels.Contains(label.LanguageId))
                                    {
                                        if (!String.IsNullOrEmpty(label.Value))
                                        {
                                            itemDefinition.Labels[label.LanguageId].Value = label.Value;
                                        }
                                    }
                                    else
                                    {
                                        list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.OptionList.Items.Labels[int LanguageId]", MetaDataErrorType.AdamItemNotFound));
                                    }
                                }
                                else
                                {
                                    list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.LabelItem.LanguageId", MetaDataErrorType.XmlValueIsEmpty));
                                }
                            }
                        }
                        else
                        {
                            list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.OptionList.Items", MetaDataErrorType.XmlValueIsEmpty));
                        }
                    }
                    else
                    {
                        list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.OptionList.Items", MetaDataErrorType.XmlValueIsEmpty));
                    }
                }
            }
            else
            {
                list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.OptionList.Items", MetaDataErrorType.XmlValueIsEmpty));
            }
        }

        private void ClearList(AFields.OptionListItemDefinitionCollection optionListItemDefinitionCollection)
        {
            int count = optionListItemDefinitionCollection.Count;
            for (int i = 0; i < count; i++)
            {
                optionListItemDefinitionCollection.RemoveAt(0);
            }
        }

        protected override void ExecuteSpecificAdam(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.OptionList;
            var fd = this.FieldDefinition as AFields.OptionListFieldDefinition;
            xValue.AcceptMultipleOptions = fd.AcceptMultipleOptions;
            xValue.SortOrder = fd.SortOrder;
            //OptionList must have at least 1 Option
            if (fd.Items != null)
            {
                xValue.Items = new List<MetaData.Xml.Field.OptionListItemDefinition>();
                foreach (AFields.OptionListItemDefinition item in fd.Items)
                {
                    if (item.Name != "")
                    {
                        MetaData.Xml.Field.OptionListItemDefinition itemDefinition = new MetaData.Xml.Field.OptionListItemDefinition();
                        itemDefinition.Name = item.Name;
                        //itemDefinition.Id = item.Id;
                        //if Labels for Option were not determined in UI, they would be filled by Name automatically
                        //if Option was added but Name for Option wasn't determined in UI, it would have Id and Labels would be filled by Name="" automatically,
                        //so Name and Labels would be empty strings
                        if (item.Labels != null)
                        {
                            itemDefinition.Labels = new List<MetaData.Xml.Field.LabelItem>();
                            foreach (LabelItem label in item.Labels)
                            {
                                MetaData.Xml.Field.LabelItem labelItem = new MetaData.Xml.Field.LabelItem();
                                labelItem.LanguageId = label.LanguageId;
                                labelItem.Value = label.Value;
                                itemDefinition.Labels.Add(labelItem);
                            }
                        }
                        else
                        {
                            list.Add(new FailedMetaData(fd.Name, "Field.TypeSpecific.OptionList.Items.Labels", MetaDataErrorType.AdamItemNotFound));
                        }
                        //if Tag wasn't defined in UI, then Tag=null
                        itemDefinition.Tag = item.Tag;
                        xValue.Items.Add(itemDefinition);
                    }
                }
                //there is no Options filled right, i.e. Name!=""
                if (xValue.Items.Count == 0)
                {
                    list.Add(new FailedMetaData(fd.Name, "Field.TypeSpecific.OptionList.Items", MetaDataErrorType.AdamItemNotFound));
                }
            }
            else 
            {
                list.Add(new FailedMetaData(fd.Name, "Field.TypeSpecific.OptionList.Items", MetaDataErrorType.AdamItemNotFound));
            }
        }
    }
}
