﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adam.Core;
using Adam.Core.Fields;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField
{
    public class RecordListHlp : TypeSpecific
    {
        public RecordListHlp(FieldDefinition fd, MetaDataElement value, string xName, Application app) :
            base(fd, value, xName, app)
        {

        }

        protected override void ExecuteSpecificXml(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.RecordList;
            var fd = this.FieldDefinition as RecordListFieldDefinition;
            if (!String.IsNullOrEmpty(xValue.SummaryField))
            {
                var summaryFD = new FieldDefinitionHelper(this.App).GetFieldDefinition(xValue.SummaryField);
                if (summaryFD != null)
                {
                    fd.SummaryField = summaryFD.Id;
                }
                else 
                {
                    list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.SummaryField", MetaDataErrorType.XmlInvalidDataFormat));
                }
            }
        }

        protected override void ExecuteSpecificAdam(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.RecordList;
            var fd = this.FieldDefinition as RecordListFieldDefinition;
            Guid? gSummaryField = fd.SummaryField;
            if (gSummaryField.HasValue)
            {
                var summaryFD = new FieldDefinitionHelper(this.App).GetFieldDefinition(gSummaryField.Value);
                if (summaryFD == null)
                {
                    list.Add(new FailedMetaData(fd.Name, "Field.TypeSpecific.SummaryField", MetaDataErrorType.AdamItemNotFound));
                }
                else
                {
                    xValue.SummaryField = summaryFD.Name;
                }
            }            
        }
    }
}
