﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adam.Core;
using Adam.Core.Fields;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField;
using Adam.Core.Classifications;
using Adam.Core.Search;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField
{
    public class RecordLinkHlp : TypeSpecific
    {
        public RecordLinkHlp(FieldDefinition fd, MetaDataElement value, string xName, Application app) :
            base(fd, value, xName, app)
        {

        }

        protected override void ExecuteSpecificXml(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.RecordLink;
            var fd = this.FieldDefinition as RecordLinkFieldDefinition;

            if (fd.ParentClassifications != null)
            {
                fd.ParentClassifications.Clear();

                if (xValue.ParentClassifications != null)
                {
                    foreach (var name_path in xValue.ParentClassifications)
                    {
                        Guid classificationId = SetClassificationId("Field.TypeSpecific.ParentClassifications.Item", name_path, list);
                        fd.ParentClassifications.Add(classificationId);
                    }
                }
            }

            if (fd.ChildClassifications != null)
            {
                fd.ChildClassifications.Clear();

                if (xValue.ChildClassifications != null)
                {
                    foreach (var name_path in xValue.ChildClassifications)
                    {
                        Guid classificationId = SetClassificationId("Field.TypeSpecific.ChildClassifications.Item", name_path, list);
                        fd.ChildClassifications.Add(classificationId);
                    }
                }
            }

            fd.LinkType = xValue.LinkType;
            if (!String.IsNullOrEmpty(xValue.ChildSearchExpression))
            {
                fd.ChildSearchExpression = xValue.ChildSearchExpression;
            }
            if (!String.IsNullOrEmpty(xValue.ParentSearchExpression))
            {
                fd.ParentSearchExpression = xValue.ParentSearchExpression;
            }
            if (!String.IsNullOrEmpty(xValue.SummaryField))
            {
                var summaryFD = new FieldDefinitionHelper(this.App).GetFieldDefinition(xValue.SummaryField);
                if (summaryFD != null)
                {
                    fd.SummaryField = summaryFD.Id;
                }
                else
                {
                    list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.SummaryField", MetaDataErrorType.XmlInvalidDataFormat));
                }
            }
            fd.SummaryImage = xValue.SummaryImage;
          
        }

        private Guid SetClassificationId(string errorType, string name_path, List<FailedMetaData> list)
        {
            Guid classificationId = Guid.Empty;
            if (!String.IsNullOrEmpty(name_path))
            {
                ClassificationHelper classificationHelper = new ClassificationHelper(this.App);
                ClassificationPath namePath = new ClassificationPath(name_path);
                Guid? result = classificationHelper.GetId(namePath);
                if (result.HasValue)
                {
                    classificationId = result.Value;
                }
                else
                {
                    list.Add(new FailedMetaData(this.XName, errorType, MetaDataErrorType.XmlInvalidDataFormat));
                }
            }
            else
            {
                list.Add(new FailedMetaData(this.XName, errorType, MetaDataErrorType.XmlValueIsEmpty));
            }
            return classificationId;
        }

        private string SetClassificationName(string name, string errorType, Guid classificationId, List<FailedMetaData> list)
        {
            string name_path = String.Empty;
            ClassificationHelper classificationHelper = new ClassificationHelper(this.App);
            ClassificationPath namePath = classificationHelper.GetNamePath(classificationId);
            if (namePath != null)
            {
                name_path = namePath.ToString();
            }
            else
            {
                list.Add(new FailedMetaData(name, errorType, MetaDataErrorType.AdamItemNotFound));
            }
            return name_path;
        }

        protected override void ExecuteSpecificAdam(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.RecordLink;
            var fd = this.FieldDefinition as RecordLinkFieldDefinition;
            xValue.LinkType = fd.LinkType;
            xValue.ParentSearchExpression = fd.ParentSearchExpression;
            xValue.ChildSearchExpression = fd.ChildSearchExpression;
            Guid? gSummaryField = fd.SummaryField;
            if (gSummaryField.HasValue)
            {
                var summaryFD = new FieldDefinitionHelper(this.App).GetFieldDefinition(gSummaryField.Value);
                if (summaryFD == null)
                {
                    list.Add(new FailedMetaData(fd.Name, "Field.TypeSpecific.SummaryField", MetaDataErrorType.AdamItemNotFound));
                }
                else
                {
                    xValue.SummaryField = summaryFD.Name;
                }
            }
            xValue.SummaryImage = fd.SummaryImage;
            if (fd.ParentClassifications != null)
            {
                xValue.ParentClassifications = new List<string>();
                foreach (var id in fd.ParentClassifications)
                {
                    string name_path = SetClassificationName(fd.Name, "Field.TypeSpecific.ParentClassifications", id, list);
                    xValue.ParentClassifications.Add(name_path);                    
                }
            }
            if (fd.ChildClassifications != null)
            {
                xValue.ChildClassifications = new List<string>();
                foreach (var id in fd.ChildClassifications)
                {
                    string name_path = SetClassificationName(fd.Name, "Field.TypeSpecific.ChildClassifications", id, list);
                    xValue.ChildClassifications.Add(name_path);
                }
            }
        }
    }
}
