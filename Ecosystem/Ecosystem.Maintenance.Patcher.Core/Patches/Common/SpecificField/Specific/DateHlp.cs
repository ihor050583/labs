﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adam.Core;
using Adam.Core.Fields;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField
{
    public class DateHlp : TypeSpecific
    {
        public DateHlp(FieldDefinition fd, MetaDataElement value, string xName, Application app) :
            base(fd, value, xName, app)
        {

        }

        protected override void ExecuteSpecificXml(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.Date;
            var fd = this.FieldDefinition as DateFieldDefinition;
            if (!String.IsNullOrEmpty(xValue.DatePattern))
            {
                fd.DatePattern = xValue.DatePattern;
            }
            if (!String.IsNullOrEmpty(xValue.YearMonthPattern))
            {
                fd.YearMonthPattern = xValue.YearMonthPattern;
            }
        }

        protected override void ExecuteSpecificAdam(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.Date;
            var fd = this.FieldDefinition as DateFieldDefinition;
            xValue.DatePattern = fd.DatePattern;
            xValue.YearMonthPattern = fd.YearMonthPattern;
        }
    }
}
