﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adam.Core;
using Adam.Core.Fields;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField;
using System.Text.RegularExpressions;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField
{
    public class NumericHlp : TypeSpecific
    {
        public NumericHlp(FieldDefinition fd, MetaDataElement value, string xName, Application app) :
            base(fd, value, xName, app)
        {

        }

        protected override void ExecuteSpecificXml(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.Numeric;
            var fd = this.FieldDefinition as NumericFieldDefinition;
            if (ValidateAttrirbutes(xValue, list))
            {
                fd.Accuracy = xValue.Accuracy;
                if (!String.IsNullOrEmpty(xValue.Range))
                {
                    fd.Range = xValue.Range;
                }
            }
        }

        private bool ValidateAttrirbutes(MetaData.Xml.Field.Numeric xValue, List<FailedMetaData> list)
        {
            bool isValid = true;
            if (xValue.Accuracy < 0)
            {           
                isValid = false;
                list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.Numeric.Accuracy", MetaDataErrorType.XmlInvalidDataFormat));
            }
            if (!String.IsNullOrEmpty(xValue.Range))
            {
                var pattern = "[^-0-9(),]+";
                if (Regex.IsMatch(xValue.Range, pattern))
                {
                    isValid = false;
                    list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.Numeric.Range", MetaDataErrorType.XmlInvalidDataFormat));
                }
            }
            return isValid;
        }

        protected override void ExecuteSpecificAdam(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.Numeric;
            var fd = this.FieldDefinition as NumericFieldDefinition;
            xValue.Accuracy = fd.Accuracy;
            xValue.Range = fd.Range;
        }
    }
}
