﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adam.Core;
using Adam.Core.Fields;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField
{
    public class TextHlp : TypeSpecific
    {
        public TextHlp(FieldDefinition fd, MetaDataElement value, string xName, Application app) :
            base(fd, value, xName, app)
        {

        }

        protected override void ExecuteSpecificXml(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.Text;
            var fd = this.FieldDefinition as TextFieldDefinition;
            if (ValidateAttrirbutes(xValue, list))
            {
                fd.References = xValue.References;
                if (!String.IsNullOrEmpty(xValue.RegularExpression))
                {
                    fd.RegularExpression = xValue.RegularExpression;
                }
                //fd.MinLength = 0 by default
                fd.MinLength = xValue.MinLength;
                //fd.MaxLength = 3800 by default
                fd.MaxLength = xValue.MaxLength;
                //fd.ContentType = Text by default
                fd.ContentType = xValue.ContentType;
                fd.MultiLine = xValue.MultiLine;
            }
        }

        private bool ValidateAttrirbutes(MetaData.Xml.Field.Text xValue, List<FailedMetaData> list)
        {
            bool isValid = true;
            isValid = ValidateContentType(xValue.ContentType, list);
            if (xValue.MinLength < 0)
            {
                isValid = false;
                list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.Text.MinLength", MetaDataErrorType.XmlInvalidDataFormat));
            }
            if (xValue.MaxLength < 0)
            {
                isValid = false;
                list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.Text.MaxLength", MetaDataErrorType.XmlInvalidDataFormat));
            }
            return isValid;
        }

        private bool ValidateContentType(TextFieldContentType textFieldContentType, List<FailedMetaData> list)
        {
            bool isValid = true;
            switch (textFieldContentType)
            {
                case TextFieldContentType.Text: 
                case TextFieldContentType.Html:
                    break;
                default:
                    isValid = false;
                    list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific.ContentType", MetaDataErrorType.XmlInvalidDataFormat));
                    break;
            }
            return isValid;
        }

        protected override void ExecuteSpecificAdam(List<FailedMetaData> list)
        {
            var xValue = this.XValue as MetaData.Xml.Field.Text;
            var fd = this.FieldDefinition as TextFieldDefinition;
            xValue.References = fd.References;
            xValue.RegularExpression = fd.RegularExpression;
            xValue.MinLength = fd.MinLength;
            xValue.MaxLength = fd.MaxLength;
            xValue.ContentType = fd.ContentType;
            xValue.MultiLine = fd.MultiLine;
        }
    }
}
