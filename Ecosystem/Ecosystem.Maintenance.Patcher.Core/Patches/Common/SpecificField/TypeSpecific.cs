﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Fields;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField
{
    public abstract class TypeSpecific
    {
        protected FieldDefinition FieldDefinition { get; set; }
        protected MetaDataElement XValue { get; set; }
        protected string XName { get; set; }
        protected Application App { get; set; }

        public TypeSpecific(FieldDefinition fd, MetaDataElement value, string xName, Application app)
        {
            this.FieldDefinition = fd;
            this.XValue = value;
            this.XName = xName;
            this.App = app;
        }

        public List<FailedMetaData> Execute(ScenarioType type, ref string xml)
        {
            List<FailedMetaData> list = new List<FailedMetaData>();
            if (type == ScenarioType.XmlToAdam)
            {
                if (XValue == null)
                {
                    list.Add(new FailedMetaData(this.XName, "Field.TypeSpecific", MetaDataErrorType.XmlValueIsEmpty));
                }
                else
                {
                    ExecuteSpecificXml(list);
                }
            }
            else if (type == ScenarioType.AdamToXml)
            {
                ExecuteSpecificAdam(list);
            }
            if (this.XValue != null)
            {
                try
                {
                  xml = Extensions.ObjectToXml(this.XValue);
                }
                catch (Exception)
                {
                    
                }
            }
            return list;
        }

        protected abstract void ExecuteSpecificXml(List<FailedMetaData> list);
        protected abstract void ExecuteSpecificAdam(List<FailedMetaData> list);
    }
}
