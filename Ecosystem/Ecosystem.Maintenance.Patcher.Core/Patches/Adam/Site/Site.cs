﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASite = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.Site;
using XSite = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Site;
using AManagement = Adam.Core.Management;
using Adam.Core.Search;
using Adam.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class Site : APatch<ASite.Site, XSite.Site, AManagement.Site, AManagement.SiteHelper>
    {
        protected override XSite.Site GetItem(AManagement.Site aItem, AManagement.SiteHelper aHelper)
        {
            XSite.Site xSite = new XSite.Site();
            return xSite;
        }

    }
}
