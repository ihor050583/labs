﻿using Adam.Core;
using Adam.Core.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ALanguage = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.Language;
using XLanguage = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Language;
using AManagement = Adam.Core.Management;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class Language : APatch<ALanguage.Language, XLanguage.Language, AManagement.Language, AManagement.LanguageHelper>
    {
        protected override XLanguage.Language GetItem(AManagement.Language aItem, AManagement.LanguageHelper aHelper)
        {
            XLanguage.Language xLanguage = new XLanguage.Language();
            UpdateAttributes(aItem, xLanguage);
            SetExcludedKeywords(aItem, xLanguage);
            return xLanguage;
        }

        private void UpdateAttributes(AManagement.Language aLanguage, XLanguage.Language xLanguage)
        {
            xLanguage.Culture = aLanguage.Culture;
            xLanguage.EnabledForFields = aLanguage.EnabledForFields;
        }

        private void SetExcludedKeywords(AManagement.Language aLanguage, XLanguage.Language xLanguage)
        {
            if (!String.IsNullOrEmpty(aLanguage.ExcludedKeywords))
            {
                xLanguage.ExcludedKeywords = Extensions.AddCDATA(aLanguage.ExcludedKeywords);
            }
        }
    }
}
