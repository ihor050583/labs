﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOrganization = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.Organization;
using XOrganization = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Organization;
using AManagement = Adam.Core.Management;
using Adam.Core.Search;
using Adam.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class Organization : APatch<AOrganization.Organization, XOrganization.Organization, AManagement.Organization, AManagement.OrganizationHelper>
    {
        protected override XOrganization.Organization GetItem(AManagement.Organization aItem, AManagement.OrganizationHelper aHelper)
        {
            XOrganization.Organization xOrganization = new XOrganization.Organization();
            return xOrganization;
        }
    }
}
