﻿using Adam.Workflow.Core.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XWorkflow = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Workflow;
using AWorkflow = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.Workflow;
using Adam.Core.Search;


namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class Workflow : Patch<AWorkflow.Workflow>
    {
        protected override void ExecuteHlp()
        {
            WorkflowDefinitionHelper helper = new WorkflowDefinitionHelper(this.Context.AdamApplication);
            List<WorkflowDefinition> list = helper.GetDefinitions(new SearchExpression(this.MetaData.SearchExpression));

            foreach (WorkflowDefinition aItem in list)
            {
                XWorkflow.Workflow xItem = GetItem(aItem);
                base.SaveMetaData<XWorkflow.Workflow>(xItem);
            }
        }

        protected XWorkflow.Workflow GetItem(WorkflowDefinition aItem)
        {
            XWorkflow.Workflow xWorkflow = new XWorkflow.Workflow();

            xWorkflow.Name = aItem.Name;
            xWorkflow.Description = aItem.Description;
            xWorkflow.ReleaseDate = GetReleaseDate(aItem.ReleaseDate);
            xWorkflow.Template = GetTemplate(aItem.Template);

            return xWorkflow;
        }

        private string GetTemplate(WorkflowDefinitionTemplate workflowDefinitionTemplate)
        { 
            //Extensions.AddCDATA()
            return Encoding.UTF8.GetString(workflowDefinitionTemplate.Content);
        }

        private string GetReleaseDate(DateTime? date)
        {
            string retVal = String.Empty;

            if (date.HasValue)
            {
                //10/3/2014 1:55:31 PM
                retVal = date.Value.ToString("G");
            }

            return retVal;
        }
    }
}
