﻿using Adam.Core;
using Adam.Core.Search;
using Adam.Workflow.Core.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam
{
    public class WorkflowDefinitionHelper
    {
        private Application _app;

        public WorkflowDefinitionHelper(Application app)
        {
            _app = app;
        }

        public List<WorkflowDefinition> GetDefinitions(SearchExpression se)
        {
            List<WorkflowDefinition> retVal = new List<WorkflowDefinition>();

            WorkflowDefinitionCollection definitions = new WorkflowDefinitionCollection(_app);
            definitions.Load(se);

            foreach (WorkflowDefinition definition in definitions)
            {
                retVal.Add(definition);
            }

            return retVal;
        }
    }
}
