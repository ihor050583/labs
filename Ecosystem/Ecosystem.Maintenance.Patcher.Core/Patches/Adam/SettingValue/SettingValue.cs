﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AManagement = Adam.Core.Management;
using Adam.Core.Search;
using ASettings = Adam.Core.Settings;
using ASettingValue = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.SettingValue;
using XSettingValue = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.SettingValue;
using Adam.Core;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class SettingValue : APatch<ASettingValue.SettingValue, XSettingValue.SettingValue,
        ASettings.SettingDefinition, ASettings.SettingDefinitionHelper>
    {
        public SettingValue()
        {
            this.InitializeItemCallBack = InitializeDefinition;
        }

        protected ASettings.SettingDefinition InitializeDefinition(ASettings.SettingDefinitionHelper aHelper, Guid id)
        {
            return aHelper.GetSettingDefinition(id);
        }

        protected override XSettingValue.SettingValue GetItem(ASettings.SettingDefinition aItem, ASettings.SettingDefinitionHelper aHelper)
        {
            XSettingValue.SettingValue xItem = new XSettingValue.SettingValue();

            dynamic setting = GetSetting(aItem);

            if (setting != null)
            {
                xItem.Name = aItem.Name;
                UpdateByScope(xItem, setting);
            }
            
            return xItem;
        }

        private void UpdateByScope(XSettingValue.SettingValue xItem, dynamic aItem)
        {
            UpdateSystem(xItem, aItem);
            UpdateSite(xItem, aItem);
            UpdateUser(xItem, aItem);
            UpdateUserGroup(xItem, aItem);
        }

        private void UpdateUserGroup(XSettingValue.SettingValue xItem, dynamic aItem)
        {
            xItem.ScopeUserGroup = new XSettingValue.ScopeUserGroup();
            xItem.ScopeUserGroup.Items = new List<Core.MetaData.MetaDataElement>();

            AManagement.UserGroupHelper helper = new AManagement.UserGroupHelper(this.Context.AdamApplication);
            List<Guid> ids = helper.GetIds(new SearchExpression("*")).ToList();
            
            foreach (Guid id in ids)
            {
                try
                {
                    aItem.LoadUserGroupValue(xItem.Name, id);
                    if (aItem.Value != null)
                    {
                        AManagement.UserGroup aUserGroup = new AManagement.UserGroup(this.Context.AdamApplication);
                        if (aUserGroup.TryLoad(id) == TryLoadResult.Success)
                        {
                            xItem.ScopeUserGroup.Items.Add(new MetaDataElement() 
                            { 
                                Action = Core.MetaData.Xml.PatchAction.AddOrUpdate,
                                Name = aUserGroup.Name, 
                                Data = aItem.Value.ToString() 
                            });
                        }
                    }
                }
                catch (Exception)
                {
                    //AddErrorCaseToContext(xItem.Name, "SettingValue", MetaDataErrorType.AdamItemNotFound, ex.Message);
                }
            }
        }

        private void UpdateSite(XSettingValue.SettingValue xItem, dynamic aItem)
        {
            xItem.ScopeSite = new XSettingValue.ScopeSite();
            xItem.ScopeSite.Items = new List<Core.MetaData.MetaDataElement>();

            AManagement.SiteHelper helper = new AManagement.SiteHelper(this.Context.AdamApplication);
            List<Guid> ids = helper.GetIds(new SearchExpression("*")).ToList();

            foreach (Guid id in ids)
            {
                try
                {
                    aItem.LoadSiteValue(xItem.Name, id);
                    if (aItem.Value != null)
                    {
                        AManagement.Site aSite = new AManagement.Site(this.Context.AdamApplication);
                        if (aSite.TryLoad(id) == TryLoadResult.Success)
                        {
                            xItem.ScopeSite.Items.Add(new MetaDataElement() 
                            { 
                                Action = Core.MetaData.Xml.PatchAction.AddOrUpdate,
                                Name = aSite.Name, 
                                Data = aItem.Value.ToString() 
                            });
                        }
                    }
                }
                catch (Exception)
                {
                    //AddErrorCaseToContext(xItem.Name, "SettingValue", MetaDataErrorType.AdamItemNotFound, ex.Message);
                }
            }
        }

        private void UpdateUser(XSettingValue.SettingValue xItem, dynamic aItem)
        {
            xItem.ScopeUser = new XSettingValue.ScopeUser();
            xItem.ScopeUser.Items = new List<Core.MetaData.MetaDataElement>();

            AManagement.UserHelper helper = new AManagement.UserHelper(this.Context.AdamApplication);
            List<Guid> ids = helper.GetIds(new SearchExpression("*")).ToList();

            foreach (Guid id in ids)
            {
                try
                {
                    aItem.LoadUserValue(xItem.Name, id);
                    if (aItem.Value != null)
                    {
                        AManagement.User aUser = new AManagement.User(this.Context.AdamApplication);
                        if (aUser.TryLoad(id) == TryLoadResult.Success)
                        {
                            xItem.ScopeUser.Items.Add(new MetaDataElement() 
                            { 
                                Action = Core.MetaData.Xml.PatchAction.AddOrUpdate,
                                Name = aUser.Name, 
                                Data = aItem.Value.ToString() 
                            });
                        }
                    }
                }
                catch (Exception)
                {
                    //AddErrorCaseToContext(xItem.Name, "SettingValue", MetaDataErrorType.AdamItemNotFound, ex.Message);
                }
            }
        }

        private void UpdateSystem(XSettingValue.SettingValue xItem, dynamic aItem)
        {
            xItem.ScopeSystem = new XSettingValue.ScopeSystem();
            xItem.ScopeSystem.Items = new List<Core.MetaData.MetaDataElement>();

            try
            {
                aItem.LoadSystemValue(xItem.Name);
                if (aItem.Value != null)
                {
                    xItem.ScopeSystem.Items.Add(new MetaDataElement() 
                        { 
                            Action = Core.MetaData.Xml.PatchAction.AddOrUpdate,
                            Data = aItem.Value.ToString() 
                        });
                }
            }
            catch (Exception)
            {
                //AddErrorCaseToContext(xItem.Name, "SettingValue", MetaDataErrorType.AdamItemNotFound, ex.Message);
            }
        }

        public dynamic GetSetting(ASettings.SettingDefinition aItem)
        {
            dynamic retVal = null;

            switch (aItem.DataType)
            {
                case ASettings.SettingDataType.Reference:
                    {
                        retVal = new ASettings.ReferenceSetting(this.Context.AdamApplication);
                    }
                    break;
                case ASettings.SettingDataType.Boolean:
                    {
                        retVal = new ASettings.BooleanSetting(this.Context.AdamApplication);
                    }
                    break;
                case ASettings.SettingDataType.DateTime:
                    {
                        retVal = new ASettings.DateTimeSetting(this.Context.AdamApplication);
                    }
                    break;
                case ASettings.SettingDataType.Numeric:
                    {
                        retVal = new ASettings.NumericSetting(this.Context.AdamApplication);
                    }
                    break;
                case ASettings.SettingDataType.Role:
                    {
                        retVal = new ASettings.RoleSetting(this.Context.AdamApplication);
                    }
                    break;
                case ASettings.SettingDataType.Text:
                    {
                        retVal = new ASettings.TextSetting(this.Context.AdamApplication);
                    }
                    break;
                case ASettings.SettingDataType.Xml:
                    {
                        retVal = new ASettings.XmlSetting(this.Context.AdamApplication);
                    }
                    break;
            }

            return retVal;
        }
    }
}
