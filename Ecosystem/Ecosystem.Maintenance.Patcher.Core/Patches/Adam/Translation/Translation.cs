﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ATranslation = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.Translation;
using XTranslation = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Translation;
using AManagement = Adam.Core.Management;
using Adam.Core.Search;
using Adam.Core;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class Translation : APatch<ATranslation.Translation, XTranslation.Translation, AManagement.Translation, AManagement.TranslationHelper>
    {
        protected override XTranslation.Translation GetItem(AManagement.Translation aItem, AManagement.TranslationHelper aHelper)
        {
            XTranslation.Translation xTranslation = new XTranslation.Translation();

            xTranslation.Studio = aItem.Studio;
            xTranslation.Module = aItem.Module;
            UpdateTranslationLanguages(aItem, xTranslation);

            return xTranslation;
        }

        private void UpdateTranslationLanguages(AManagement.Translation aTranslation, XTranslation.Translation xTranslation)
        {
            xTranslation.TranslationLanguages = new List<XTranslation.TranslationLanguage>();

            foreach (AManagement.TranslationItem aItem in aTranslation.Items)
            {
                XTranslation.TranslationLanguage xLanguage = new XTranslation.TranslationLanguage();
                SetLanguageName(aTranslation.Name, aItem.LanguageId, xLanguage);
                SetLanguageValue(aItem, xLanguage);

                if (!String.IsNullOrEmpty(xLanguage.Name))
                {
                    xTranslation.TranslationLanguages.Add(xLanguage);
                }
            }
        }

        private void SetLanguageName(string name, Guid id, XTranslation.TranslationLanguage xLanguage)
        {
            var aLanguage = new AManagement.Language(this.Context.AdamApplication);
            if (aLanguage.TryLoad(id) == TryLoadResult.Success)
            {
                xLanguage.Name = aLanguage.Name;
            }
        }

        private void SetLanguageValue(AManagement.TranslationItem aItem, XTranslation.TranslationLanguage xLanguage)
        {
            if (!String.IsNullOrEmpty(aItem.Value))
            {
                xLanguage.LanguageValue = Extensions.AddCDATA(aItem.Value);
            }
        }

    }
}
