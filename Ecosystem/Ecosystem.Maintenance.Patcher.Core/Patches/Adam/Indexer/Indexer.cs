﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AIndexer = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.Indexer;
using XIndexer = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Indexer;
using AdamIndexer = Adam.Core.Indexer;
using AClassifications = Adam.Core.Classifications;
using Adam.Core.Search;
using Adam.Core;
using Adam.Core.Classifications;
using Adam.Core.Settings;
using System.Xml;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class Indexer : APatch<AIndexer.Indexer, XIndexer.Indexer, AdamIndexer.IndexerTask, AdamIndexer.IndexerTaskHelper>
    {
        protected override XIndexer.Indexer GetItem(AdamIndexer.IndexerTask aItem, AdamIndexer.IndexerTaskHelper aHelper)
        {
            XIndexer.Indexer xItem = new XIndexer.Indexer();

            DefaultInit(xItem);

            UpdateAttributes(aItem, xItem);
            UpdateClassification(aItem, xItem);
            UpdateFilesAndDirectories(aItem, xItem);
            UpdateLogging(aItem, xItem);
            UpdateJob(aItem, xItem);
            UpdateActions(aItem, xItem);
            UpdateImports(aItem, xItem);
            UpdateNotification(aItem, xItem);
            UpdateCustomeEngine(aItem, xItem);

            return xItem;
        }

        private static void DefaultInit(XIndexer.Indexer xItem)
        {
            xItem.Classification = new XIndexer.ClassificationInfo();
            xItem.FilesAndDirectories = new XIndexer.FilesAndDirectories();
            xItem.FilesAndDirectories.FileSelectionPatterns = new XIndexer.FileSelectionPatterns();
            xItem.FilesAndDirectories.FileSelectionPatterns.Patterns = new List<XIndexer.FileSelectionPattern>();
            xItem.Logging = new XIndexer.Logging();
            xItem.Job = new XIndexer.Job();
            xItem.Actions = new XIndexer.Actions();
            xItem.Imports = new XIndexer.Imports();
            xItem.Notification = new XIndexer.Notification();
            xItem.CustomEngine = new XIndexer.CustomEngine();
        }

        
        private void UpdateAttributes(AdamIndexer.IndexerTask aIndexer, XIndexer.Indexer xIndexer)
        {
            xIndexer.Path = aIndexer.Path;
            xIndexer.Enabled = aIndexer.Enabled;
            xIndexer.ScanEngine = aIndexer.ScanEngine;
            xIndexer.ProcessEngine = aIndexer.ProcessEngine;
            xIndexer.CatalogMode = aIndexer.CatalogMode;
        }

        private void UpdateClassification(AdamIndexer.IndexerTask aIndexer, XIndexer.Indexer xIndexer)
        {
            xIndexer.Classification.ClassifySubfolders = aIndexer.ClassifyFolders;
            xIndexer.Classification.IgnorePath = aIndexer.ClassifyFoldersIgnorePath;
            xIndexer.Classification.MaximumDepth = aIndexer.ClassifyFoldersDepth;
            if (aIndexer.ClassifyIn.HasValue)
            {
                xIndexer.Classification.ClassifyIn = SetClassificationName(aIndexer.Name ,"Indexer.ClassifyIn", aIndexer.ClassifyIn.Value);
            }
            if (aIndexer.ClassifyFoldersRoot.HasValue)
            {
                xIndexer.Classification.Root = SetClassificationName(aIndexer.Name, "Indexer.ClassifyFoldersRoot", aIndexer.ClassifyFoldersRoot.Value);
            }

        }

        private string SetClassificationName(string name, string errorType, Guid classificationId)
        {
            string name_path = String.Empty;
            ClassificationHelper classificationHelper = new ClassificationHelper(this.Context.AdamApplication);
            ClassificationPath namePath = classificationHelper.GetNamePath(classificationId);
            if (namePath != null)
            {
                name_path = namePath.ToString();
            }
            else
            {
                AddErrorCaseToContext(name, errorType, MetaDataErrorType.AdamItemNotFound);
            }
            return name_path;
        }

        private void UpdateFilesAndDirectories(AdamIndexer.IndexerTask aIndexer, XIndexer.Indexer xIndexer)
        {
            xIndexer.FilesAndDirectories.CatalogHiddenFiles = aIndexer.CatalogHiddenFiles;
            xIndexer.FilesAndDirectories.CatalogHiddenDirectories = aIndexer.CatalogHiddenDirectories;
            xIndexer.FilesAndDirectories.DeleteFoldersFromDepth = aIndexer.ClassifyFoldersDepth;
            xIndexer.FilesAndDirectories.FileExtensionRestriction = aIndexer.FileExtensionRestriction;
            xIndexer.FilesAndDirectories.FileTypeRestriction = aIndexer.FileTypeRestriction;
            xIndexer.FilesAndDirectories.FailedFilesFolder = aIndexer.FailedFilesFolder;
            SetFileSelectionPatterns(aIndexer, xIndexer);
        }

        private void SetFileSelectionPatterns(AdamIndexer.IndexerTask aIndexer, XIndexer.Indexer xIndexer)
        {
            if (aIndexer.FileSelectionPatterns.Count != 0)
            {
                foreach (AdamIndexer.FileSelectionPattern item in aIndexer.FileSelectionPatterns)
                {
                    xIndexer.FilesAndDirectories.FileSelectionPatterns.Patterns.Add(new XIndexer.FileSelectionPattern { Mode = item.Mode, Expression = item.Expression });
                }
            }
        }

        private void UpdateLogging(AdamIndexer.IndexerTask aIndexer, XIndexer.Indexer xIndexer)
        {
            xIndexer.Logging.LogSeverity = aIndexer.TargetLogSeverity;
        }

        private void UpdateJob(AdamIndexer.IndexerTask aIndexer, XIndexer.Indexer xIndexer)
        {
            xIndexer.Job.Priority = aIndexer.JobPriority;
            xIndexer.Job.Threshold = aIndexer.JobThreshold;
            aIndexer.MaximumNumberOfRetries = xIndexer.Job.MaximumNumberOfRetries;
            xIndexer.Job.ThresholdTimeout = aIndexer.JobThresholdTimeout.ToString();
            xIndexer.Job.MinimumRetryWaitTime = aIndexer.MinimumRetryWaitTime.ToString();
        }

        private void UpdateActions(AdamIndexer.IndexerTask aIndexer, XIndexer.Indexer xIndexer)
        {
            xIndexer.Actions.OnSuccessAction = aIndexer.ActionOnSuccess;
            xIndexer.Actions.DuplicateAction = aIndexer.DuplicateAction;
            xIndexer.Actions.FieldValidation = aIndexer.FieldValidation;
        }

        private void UpdateImports(AdamIndexer.IndexerTask aIndexer, XIndexer.Indexer xIndexer)
        {
            xIndexer.Imports.AllowMissingImports = aIndexer.AllowMissingImports;
            xIndexer.Imports.MaximumRetriesWhenImportsAreMissing = aIndexer.MaximumNumberOfRetries;
            xIndexer.Imports.ImportRestriction = aIndexer.ImportRestriction;
        }

        private void UpdateNotification(AdamIndexer.IndexerTask aIndexer, XIndexer.Indexer xIndexer)
        {
            xIndexer.Notification.Agents = Extensions.AddCDATA(aIndexer.NotificationAgents);
            xIndexer.Notification.EmailSettings = Extensions.AddCDATA(aIndexer.EmailNotificationSettings);
        }

        private void UpdateCustomeEngine(AdamIndexer.IndexerTask aIndexer, XIndexer.Indexer xIndexer)
        {
            if (!aIndexer.ProcessEngine.Equals("Default", StringComparison.Ordinal))
            {
                XmlSetting setting = new XmlSetting(this.Context.AdamApplication);
                setting.LoadSystemValue(SettingType.RegisteredIndexerProcessEngines);

                XmlDocument value = new XmlDocument();
                value.LoadXml(setting.HasValue ? setting.Value : setting.DefaultValue);
                XmlElement engines = (XmlElement)value.SelectSingleNode("/engines");

                string name = aIndexer.ProcessEngine;
                string stringJoinSeparator = ", ";
                string[] stringSeparators = new string[] { ",", " " };
                XmlElement element = engines.SelectSingleNode("./add[@name='" + name + "']") as XmlElement;

                if (element == null)
                {
                    AddErrorCaseToContext(aIndexer.Name, "SettingType.RegisteredIndexerProcessEngines." + name, MetaDataErrorType.AdamItemNotFound);
                }
                else
                {
                    string[] result=element.GetAttribute("type").Split(stringSeparators,StringSplitOptions.RemoveEmptyEntries);
                    if (result.Count() < 2)
                    {
                        AddErrorCaseToContext(aIndexer.Name, "SettingType.RegisteredIndexerProcessEngines." + name + ".Type", MetaDataErrorType.Unexpected);
                    }
                    else
                    {
                        xIndexer.CustomEngine.UniqueName = aIndexer.ProcessEngine;
                        xIndexer.CustomEngine.TypeFullName = result[0];
                        xIndexer.CustomEngine.AssemblyName = String.Join(stringJoinSeparator, result, 1, result.Count() - 1);
                    }
                }
            }
        }
        
    }
}
