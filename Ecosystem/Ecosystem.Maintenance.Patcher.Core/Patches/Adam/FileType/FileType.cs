﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AManagement = Adam.Core.Management;
using AFileType = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.FileType;
using XFileType = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.FileType;
using Adam.Core.Search;
using Adam.Core;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using AField = Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class FileType : APatch<AFileType.FileType, XFileType.FileType, AManagement.FileType, AManagement.FileTypeHelper>
    {
        protected override XFileType.FileType GetItem(AManagement.FileType aItem, AManagement.FileTypeHelper aHelper)
        {
            XFileType.FileType xItem = new XFileType.FileType();

            xItem.Kind = aItem.Kind;

            UpdateMediaEngines(xItem, aItem);
            UpdateCatalogActions(xItem, aItem);
            UpdateFileInformation(xItem, aItem);
            UpdateProcessing(xItem, aItem);
            UpdatePreview(xItem, aItem);
            UpdateLabels(xItem, aItem);
            UpdateRegisteredFields(xItem, aItem);
            UpdateRegisteredFieldGroups(xItem, aItem);

            return xItem;
        }

        private void UpdatePreview(XFileType.FileType xFileType, AManagement.FileType aFileType)
        {
            xFileType.PreviewTypes = new List<MetaData.MetaDataElement>();
            foreach (AManagement.RegisteredMediaEngine item in aFileType.PreviewRenderWebControls)
            {
                xFileType.PreviewTypes.Add(new MetaDataElement() { Name = item.Name });
            }
        }

        private void UpdateRegisteredFieldGroups(XFileType.FileType xFileType, AManagement.FileType aFileType)
        {
            xFileType.RegisteredFieldGroups = new List<MetaDataElement>();
            AManagement.FieldGroup aFieldGroup = new AManagement.FieldGroup(this.Context.AdamApplication);
            foreach (AManagement.RegisteredFieldGroup item in aFileType.RegisteredFieldGroups)
            {
                if (aFieldGroup.TryLoad(item.FieldGroupId) == TryLoadResult.Success)
                {
                    xFileType.RegisteredFieldGroups.Add(new MetaDataElement() { Name = aFieldGroup.Name });
                }
            }
        }

        private void UpdateRegisteredFields(XFileType.FileType xFileType, AManagement.FileType aFileType)
        {
            xFileType.RegisteredFields = new List<MetaDataElement>();
            AField.FieldDefinitionHelper fdh = new AField.FieldDefinitionHelper(this.Context.AdamApplication);
            foreach (AManagement.RegisteredField item in aFileType.RegisteredFields)
            {
                AField.FieldDefinition fd = fdh.GetFieldDefinition(item.FieldId);
                xFileType.RegisteredFields.Add(new MetaDataElement() { Name = fd.Name });
            }
        }

        private void UpdateProcessing(XFileType.FileType xFileType, AManagement.FileType aFileType)
        {
            xFileType.Processing = new XFileType.Processing();
            xFileType.Processing.AllowResizeSource = aFileType.AllowOrderResizeSource;
            xFileType.Processing.IsCatalogable = aFileType.IsCatalogable;
            xFileType.Processing.KeepDocumentDimensions = aFileType.KeepDocumentDimensions;
            xFileType.Processing.PreferredExtension = aFileType.PreferredExtension;
            xFileType.Processing.PreferredMacType = aFileType.PreferredMacType;
            xFileType.Processing.PreviewRequired = aFileType.PreviewRequired;
            xFileType.Processing.SupportsWatermarking = aFileType.SupportsWatermarking;
            xFileType.Processing.EngineFormat = aFileType.EngineFormat;
            xFileType.Processing.PreviewFormat = aFileType.PreviewFormat;
        }

        private void UpdateFileInformation(XFileType.FileType xFileType, AManagement.FileType aFileType)
        {
            xFileType.FileInformation = new XFileType.FileInformation();
            xFileType.FileInformation.MacCreator = aFileType.MacCreator;
            xFileType.FileInformation.MacType = aFileType.MacType;
            xFileType.FileInformation.MimeType = aFileType.MimeType;
            xFileType.FileInformation.Extension = aFileType.Extension;
        }

        private void UpdateCatalogActions(XFileType.FileType xFileType, AManagement.FileType aFileType)
        {
            xFileType.CatalogActions = new List<MetaDataElement>();
            foreach (AManagement.FileTypeAction item in aFileType.CatalogActions)
            {
                xFileType.CatalogActions.Add(new MetaDataElement() { Name = item.Name, Value = item.IsCritical.ToString().ToLower() });
            }
        }

        private void UpdateMediaEngines(XFileType.FileType xFileType, AManagement.FileType aFileType)
        {
            xFileType.MediaEngines = new List<MetaDataElement>();
            foreach (AManagement.RegisteredMediaEngine item in aFileType.MediaEngines)
            {
                xFileType.MediaEngines.Add(new MetaDataElement() { Name = item.Name });
            }
        }
    }
}
