﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASettingDefinition = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.SettingDefinition;
using XSettingDefinition = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.SettingDefinition;
using ASettings = Adam.Core.Settings;
using Adam.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class SettingDefinition : APatch<ASettingDefinition.SettingDefinition, XSettingDefinition.SettingDefinition,
        ASettings.SettingDefinition, ASettings.SettingDefinitionHelper>
    {
        public SettingDefinition()
        {
            this.InitializeItemCallBack = InitializeDefinition;
        }

        protected ASettings.SettingDefinition InitializeDefinition(ASettings.SettingDefinitionHelper aHelper, Guid id)
        {
            return aHelper.GetSettingDefinition(id);
        }

        protected override XSettingDefinition.SettingDefinition GetItem(ASettings.SettingDefinition aItem, ASettings.SettingDefinitionHelper aHelper)
        {
            XSettingDefinition.SettingDefinition xItem = new XSettingDefinition.SettingDefinition();

            UpdateAttributes(xItem, aItem);
            UpdateDefaultValue(xItem, aItem);
            UpdateSchema(xItem, aItem);
            UpdateAccessControl(xItem, aItem);
            
            return xItem;
        }

        private void UpdateAccessControl(XSettingDefinition.SettingDefinition xItem, ASettings.SettingDefinition aItem)
        {
            xItem.AccessControl = new XSettingDefinition.AccessControl();
            xItem.AccessControl.AllowSystemSetting = aItem.AllowSystemSetting;
            xItem.AccessControl.AllowUserSetting = aItem.AllowUserSetting;
            xItem.AccessControl.AllowAnonymousAccess = aItem.AllowAnonymousAccess;
            xItem.AccessControl.AllowSiteSetting = aItem.AllowSiteSetting;
        }

        private void UpdateSchema(XSettingDefinition.SettingDefinition xItem, ASettings.SettingDefinition aItem)
        {
            if (aItem.DataType == ASettings.SettingDataType.Xml)
            {
                xItem.Schema = (aItem as ASettings.XmlSettingDefinition).Schema;
            }
        }

        private void UpdateDefaultValue(XSettingDefinition.SettingDefinition xItem, ASettings.SettingDefinition aItem)
        {
            dynamic value = GetSettingValue(aItem);
            if (value != null)
            {
                xItem.DefaultValue = value.ToString();
            }
        }

        private void UpdateAttributes(XSettingDefinition.SettingDefinition xItem, ASettings.SettingDefinition aItem)
        {
            xItem.DataType = aItem.DataType;
            xItem.Category = GetCategoryName(aItem.CategoryId);
        }

        private string GetCategoryName(Guid id)
        {
            string retVal = String.Empty;
            ASettings.SettingCategory category = new ASettings.SettingCategory(this.Context.AdamApplication);
            if (category.TryLoad(id) == TryLoadResult.Success)
            {
                retVal = category.Name;
            }
            return retVal;
        }

        private dynamic GetSettingValue(ASettings.SettingDefinition aItem)
        {
            dynamic retVal = null;

            if (aItem.DataType == ASettings.SettingDataType.Boolean)
            {
                retVal = (aItem as ASettings.BooleanSettingDefinition).DefaultValue;
            }
            else if (aItem.DataType == ASettings.SettingDataType.DateTime)
            {
                retVal = (aItem as ASettings.DateTimeSettingDefinition).DefaultValue;
            }
            else if (aItem.DataType == ASettings.SettingDataType.Numeric)
            {
                retVal = (aItem as ASettings.NumericSettingDefinition).DefaultValue;
            }
            else if (aItem.DataType == ASettings.SettingDataType.Role)
            {
                retVal = (aItem as ASettings.RoleSettingDefinition).DefaultValue;
            }
            else if (aItem.DataType == ASettings.SettingDataType.Text)
            {
                retVal = (aItem as ASettings.TextSettingDefinition).DefaultValue;
            }
            else if (aItem.DataType == ASettings.SettingDataType.Xml)
            {
                retVal = (aItem as ASettings.XmlSettingDefinition).DefaultValue;
            }
            else if (aItem.DataType == ASettings.SettingDataType.Reference)
            {
                retVal = (aItem as ASettings.ReferenceSettingDefinition).DefaultValue;
            }

            return retVal;
        }
    }
}
