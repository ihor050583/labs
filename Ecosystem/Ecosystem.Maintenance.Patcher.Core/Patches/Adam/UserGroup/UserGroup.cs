﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AManagement = Adam.Core.Management;
using Adam.Core.Search;
using Adam.Core;
using XUserGroup = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.UserGroup;
using AUserGroup = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.UserGroup;
using ASettings = Adam.Core.Settings;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class UserGroup : APatch<AUserGroup.UserGroup, XUserGroup.UserGroup, AManagement.UserGroup, AManagement.UserGroupHelper>
    {
        protected override XUserGroup.UserGroup GetItem(AManagement.UserGroup aItem, AManagement.UserGroupHelper aHelper)
        {
            XUserGroup.UserGroup xUserGroup = new XUserGroup.UserGroup();
            xUserGroup.Organization = GetOrganizationName(aItem);
            xUserGroup.Permissions = GetPermissions(aItem);
            return xUserGroup;
        }

        private List<XUserGroup.Permission> GetPermissions(AManagement.UserGroup aItem)
        {
            List<XUserGroup.Permission> list = new List<XUserGroup.Permission>();

            ASettings.RoleSetting aRoleSetting = new ASettings.RoleSetting(this.Context.AdamApplication);

            ASettings.SettingDefinitionHelper sdh = new ASettings.SettingDefinitionHelper(this.Context.AdamApplication);

            List<Guid> ids = sdh.GetIds(new SearchExpression("*")).ToList();
            foreach (Guid id in ids)
            {
                ASettings.SettingDefinition aSettingDefinition = sdh.GetSettingDefinition(id);

                if (aSettingDefinition.DataType == ASettings.SettingDataType.Role)
                {
                    aRoleSetting.LoadUserGroupValue(aSettingDefinition.Name, aItem.Id);
                    string value = aRoleSetting.Value.ToString();

                    XUserGroup.Permission xPermission = new XUserGroup.Permission() 
                    {
                        Name = aSettingDefinition.Name,
                        Status = aRoleSetting.Value,
                        Identifier = GetIdentifier(aSettingDefinition.Name)
                    };

                    //if (!String.IsNullOrEmpty(xPermission.Identifier))
                    {
                        list.Add(xPermission);
                    }
                }
                
            }

            return list;
        }

        private string GetIdentifier(string name)
        {
            return Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch.UserGroupPermission.GetIdentifier(name, this.Context.AdamApplication);
        }

        private string GetOrganizationName(AManagement.UserGroup aItem)
        {
            string retVal = String.Empty;
            AManagement.Organization organization = new AManagement.Organization(this.Context.AdamApplication);
            if (organization.TryLoad(aItem.OrganizationId) == TryLoadResult.Success)
            {
                retVal = organization.Name;
            }
            else
            {
                AddErrorCaseToContext(aItem.Name, "UserGroup.OrganizationId", MetaDataErrorType.AdamItemNotFound,
                    String.Format("OrganizationId: {0}.", aItem.OrganizationId.ToString()));
            }
            return retVal;
        }
    }
}
