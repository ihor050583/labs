﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdamCln = Adam.Core.Classifications;
using Adam.Core;
using AClassification = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.Classification;
using XClassification = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Classification;
using AManagement = Adam.Core.Management;
using AField = Adam.Core.Fields;
using Adam.Core.Search;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class Classification : Patch<AClassification.Classification>
    {
        protected override void ExecuteHlp()
        {
            AdamCln.ClassificationHelper clnHlp = new AdamCln.ClassificationHelper(this.Context.AdamApplication);
            Guid? id = clnHlp.GetId(new AdamCln.ClassificationPath(this.MetaData.Path));

            if (id.HasValue)
            {
                AdamCln.Classification cln = new AdamCln.Classification(this.Context.AdamApplication);
                if (cln.TryLoad(id.Value) == TryLoadResult.Success)
                {
                    XClassification.Classification xParentCln = GetXClassification(cln);
                    AdamCln.ClassificationCollection childIds = GetClassificationCollection(clnHlp, id);
                    UpdateChilds(id.Value, childIds, xParentCln);

                    base.SaveMetaData<XClassification.Classification>(xParentCln);
                }
                else
                {
                    AddErrorCaseToContext(this.MetaData.Path, "Classification", MetaDataErrorType.AdamItemNotFound);
                }
            }
            else
            {
                AddErrorCaseToContext(this.MetaData.Path, "Classification.Id", MetaDataErrorType.AdamItemNotFound);
            }
        }

        private AdamCln.ClassificationCollection GetClassificationCollection(AdamCln.ClassificationHelper clnHlp, Guid? id)
        {
            IList<Guid> ids = clnHlp.GetChildIds(id, true);
            AdamCln.ClassificationCollection childIds = new AdamCln.ClassificationCollection(this.Context.AdamApplication);
            childIds.Load(ids);
            return childIds;
        }

        private void UpdateChilds(Guid parentId, AdamCln.ClassificationCollection childIds, XClassification.Classification xParentCln)
        {
            var result = from s in childIds
                         where ((AdamCln.Classification)s).ParentId == parentId
                         orderby ((AdamCln.Classification)s).SortIndex
                         select s;

            if (result != null)
            {
                xParentCln.Classifications = new List<XClassification.Classification>();

                foreach (AdamCln.Classification cln in result)
                {
                    XClassification.Classification xChildCln = GetXClassification(cln);
                    xParentCln.Classifications.Add(xChildCln);

                    UpdateChilds(cln.Id, childIds, xChildCln);
                }
            }
        }

        private XClassification.Classification GetXClassification(AdamCln.Classification aCln)
        {
            XClassification.Classification xCln = new XClassification.Classification();

            xCln.Action = Core.MetaData.Xml.PatchAction.AddOrUpdate;

            UpdateGeneral(aCln, xCln);

            UpdateName(aCln, xCln);
            UpdateLabels(aCln, xCln);
            UpdateAllFields(aCln, xCln);
            UpdateFieldGroups(aCln, xCln);
            UpdatePermissions(aCln, xCln);
            UpdateMapping(aCln, xCln);

            return xCln;
        }

        private void UpdateMapping(AdamCln.Classification aCln, XClassification.Classification xCln)
        {
            xCln.RegisteredMappings = new List<MetaData.MetaDataElement>();
            foreach (AdamCln.ClassificationMapping item in aCln.RegisteredMappings)
            {
                xCln.RegisteredMappings.Add(GetSlave(item));
            }
        }

        private MetaData.MetaDataElement GetSlave(AdamCln.ClassificationMapping item)
        {
            AdamCln.ClassificationHelper clnHlp = new AdamCln.ClassificationHelper(this.Context.AdamApplication);
            
            MetaData.MetaDataElement retVal = new MetaData.MetaDataElement()
            {
                Name = clnHlp.GetNamePath(item.ClassificationId).ToString(),
                Action = Core.MetaData.Xml.PatchAction.AddOrUpdate
            };

            return retVal;
        }

        private static void UpdateGeneral(AdamCln.Classification aCln, XClassification.Classification xCln)
        {
            xCln.Identifier = aCln.Identifier;
            xCln.SortIndex = aCln.SortIndex;
            xCln.SortOrderForChildren = aCln.SortOrder;
        }

        private void UpdateName(AdamCln.Classification aCln, XClassification.Classification xCln)
        {
            xCln.Name = aCln.Name;
            xCln.Parent = aCln.GetNamePath().ToString().Replace(xCln.Name.Replace("/", @"\/"), String.Empty).TrimEnd('/').TrimEnd('\\');
            //xCln.Parent = aCln.GetNamePath().ToString().Replace(xCln.Name, String.Empty).TrimEnd('/').TrimEnd('\\');
        }

        private void UpdateLabels(AdamCln.Classification aCln, XClassification.Classification xCln)
        {
            xCln.Labels = new List<MetaData.MetaDataElement>();
            AManagement.Language lang = new AManagement.Language(this.Context.AdamApplication);

            foreach (LabelItem aLabel in aCln.Labels)
            {
                if (lang.TryLoad(aLabel.LanguageId) == TryLoadResult.Success)
                {
                    MetaData.MetaDataElement xLabel = new MetaData.MetaDataElement()
                    {
                        Name = lang.Name,
                        Value = aLabel.Value
                    };
                    xCln.Labels.Add(xLabel);
                }
                else
                {
                    AddErrorCaseToContext(aLabel.LanguageId.ToString(), "Language", MetaDataErrorType.AdamItemNotFound);
                }
            }
        }

        private void UpdateAllFields(AdamCln.Classification aCln, XClassification.Classification xCln)
        {
            xCln.RegisteredFields = new XClassification.RegisteredFields();
            xCln.RegisteredFields.ClassificationScopeFields = new List<MetaData.MetaDataElement>();
            xCln.RegisteredFields.NotClassificationScopeFields = new List<MetaData.MetaDataElement>();

            UpdateRegisteredFields(aCln, xCln);
            UpdateFields(aCln, xCln);
        }

        private void UpdateRegisteredFields(AdamCln.Classification aCln, XClassification.Classification xCln)
        {
            AField.FieldDefinitionHelper fdHlp = new AField.FieldDefinitionHelper(this.Context.AdamApplication);
            foreach (AManagement.RegisteredField aRF in aCln.RegisteredFields)
            {
                AField.FieldDefinition aField = fdHlp.GetFieldDefinition(aRF.FieldId);
                if (aField != null)
                {
                    if (aField.ScopeCategory != AField.ScopeCategory.Classification)
                    {
                        MetaData.MetaDataElement xField = new MetaData.MetaDataElement()
                        {
                            Name = aField.Name
                        };
                        xCln.RegisteredFields.NotClassificationScopeFields.Add(xField);
                    }
                }
                else
                {
                    AddErrorCaseToContext(aRF.FieldId.ToString(), "FieldDefinition", MetaDataErrorType.AdamItemNotFound);
                }
            }
        }

        private void UpdateFields(AdamCln.Classification aCln, XClassification.Classification xCln)
        {            
            foreach (AField.FieldContainer aField in aCln.Fields)
            {
                MetaData.MetaDataElement xField = new MetaData.MetaDataElement()
                {
                    Name = aField.Definition.Name,
                    Value = String.Empty
                };
                if (aField.HasValue)
                {
                    UpdateValue(aCln.GetNamePath().ToString(), aField.MyLanguage, xField);
                }
                xCln.RegisteredFields.ClassificationScopeFields.Add(xField);
            }
        }

        private void UpdateValue(string name, AField.Field aField, MetaData.MetaDataElement xField)
        {
            try
            {
                switch (aField.DataType)
                {
                    case AField.DataType.Date:
                        xField.Value = (aField as AField.DateField).Value.ToString();
                        break;
                    case AField.DataType.DateTime:
                        xField.Value = (aField as AField.DateTimeField).ValueUtc.ToString();
                        break;
                    case AField.DataType.Duration:
                        xField.Value = (aField as AField.DurationField).Value.ToString();
                        break;
                    case AField.DataType.Numeric:
                        xField.Value = (aField as AField.NumericField).Value.ToString();
                        break;
                    case AField.DataType.Text:
                        xField.Value = (aField as AField.TextField).Value.ToString();
                        break;
                    case AField.DataType.Time:
                        xField.Value = (aField as AField.TimeField).Value.ToString();
                        break;
                    case AField.DataType.OptionList:
                        UpdateOptionList(name, aField, xField);
                        break;
                    case AField.DataType.UserGroupList:
                        UpdateUserGroupList(name, aField, xField);
                        break;
                    case AField.DataType.UserList:
                        UpdateUserList(name, aField, xField);
                        break;
                    default:
                        AddErrorCaseToContext(name, "Field.DataType."+aField.DataType.ToString(), MetaDataErrorType.AdamItemNotFound);
                        break;
                }
            }
            catch (Exception ex)
            {
                AddErrorCaseToContext(name, "Field", MetaDataErrorType.Unexpected, ex.Message);
            }
        }

        private void UpdateUserList(string name, AField.Field aField, MetaData.MetaDataElement xField)
        {
            AField.UserListItemCollection userItems = (aField as AField.UserListField).Items;
            if (userItems.Count != 0)
            {
                string separator = "; ";
                List<Guid> ids = new List<Guid>();
                List<string> names = new List<string>();
                foreach (AField.UserListItem userItem in userItems)
                {
                    ids.Add(userItem.UserId);
                }
                AManagement.UserCollection users = new AManagement.UserCollection(this.Context.AdamApplication);
                users.Load(ids);
                foreach (AManagement.User user in users)
                {
                    if (user != null)
                    {
                        names.Add(user.Name);
                    }
                    else
                    {
                        AddErrorCaseToContext(name, "User", MetaDataErrorType.AdamItemNotFound);
                    }
                }
                xField.Value = String.Join(separator, names);
            }
        }

        private void UpdateUserGroupList(string name, AField.Field aField, MetaData.MetaDataElement xField)
        {
            AField.UserGroupListItemCollection userGroupItems = (aField as AField.UserGroupListField).Items;
            if (userGroupItems.Count != 0)
            {
                string separator = "; ";                
                List<Guid> ids = new List<Guid>();
                List<string> names = new List<string>();
                foreach (AField.UserGroupListItem userGroupItem in userGroupItems)
                {
                    ids.Add(userGroupItem.UserGroupId);
                }
                AManagement.UserGroupCollection userGroups = new AManagement.UserGroupCollection(this.Context.AdamApplication);
                userGroups.Load(ids);
                if (ids.Count != userGroups.Count)
                {
                    AddErrorCaseToContext(name, "UserGroup", MetaDataErrorType.AdamItemNotFound);
                }
                else
                {
                    foreach (AManagement.UserGroup userGroup in userGroups)
                    {
                        names.Add(userGroup.Name);
                    }
                    xField.Value = String.Join(separator, names);
                }
            }
        }

        private void UpdateOptionList(string name, AField.Field aField, MetaData.MetaDataElement xField)
        {
            AField.OptionListItemCollection options = (aField as AField.OptionListField).Items;
            Guid id = options[0].OptionId;
            AField.OptionListFieldDefinition fd = new AField.OptionListFieldDefinition(this.Context.AdamApplication);
            if (fd.TryLoad(aField.DefinitionId) == TryLoadResult.Success)
            {
                xField.Value = fd.Items[id].Name;
            }
            else
            {
                AddErrorCaseToContext(name, "FieldDefinition", MetaDataErrorType.AdamItemNotFound);
            }
        }

        private void UpdateFieldGroups(AdamCln.Classification aCln, XClassification.Classification xCln)
        {
            AManagement.FieldGroupHelper fgHlp = new AManagement.FieldGroupHelper(this.Context.AdamApplication);
            AManagement.FieldGroup aFieldGroup = new AManagement.FieldGroup(this.Context.AdamApplication);
            xCln.RegisteredFieldGroups = new List<MetaData.MetaDataElement>();

            foreach (AManagement.RegisteredFieldGroup aRFG in aCln.RegisteredFieldGroups)
            {
                if (aFieldGroup.TryLoad(aRFG.FieldGroupId) == TryLoadResult.Success)
                {
                    MetaData.MetaDataElement xFieldGroup = new MetaData.MetaDataElement()
                    {
                        Name = aFieldGroup.Name
                    };
                    xCln.RegisteredFieldGroups.Add(xFieldGroup);
                }
                else
                {
                    AddErrorCaseToContext(aRFG.FieldGroupId.ToString(), "FieldGroup", MetaDataErrorType.AdamItemNotFound);
                }
            }
        }

        private void UpdatePermissions(AdamCln.Classification aCln, XClassification.Classification xCln)
        {
            AdamCln.ClassificationHelper clnHlp = new AdamCln.ClassificationHelper(this.Context.AdamApplication);
            AManagement.UserGroup aUserGroup = new AManagement.UserGroup(this.Context.AdamApplication);

            xCln.Permissions = new XClassification.Permissions();
            UpdateBreakSecurityInheritance(aCln, xCln, clnHlp);
            UpdateUserGroups(aCln, xCln, clnHlp, aUserGroup);
        }

        private void UpdateBreakSecurityInheritance(AdamCln.Classification aCln, XClassification.Classification xCln, AdamCln.ClassificationHelper clnHlp)
        {
            AdamCln.ClassificationSecurityInheritance recSec = clnHlp.GetSecurityInheritance(aCln.Id, AdamCln.ClassificationRightTarget.Records);
            AdamCln.ClassificationSecurityInheritance clnSec = clnHlp.GetSecurityInheritance(aCln.Id, AdamCln.ClassificationRightTarget.ClassificationTree);

            xCln.Permissions.BreakSecurityInheritance = new XClassification.BreakSecurityInheritance()
            {
                ClassificationTreeSecurity = (clnSec == AdamCln.ClassificationSecurityInheritance.Break ? true : false),
                RecordSecurity = (recSec == AdamCln.ClassificationSecurityInheritance.Break ? true : false),
            };
        }

        private void UpdateUserGroups(AdamCln.Classification aCln, XClassification.Classification xCln, AdamCln.ClassificationHelper clnHlp, AManagement.UserGroup aUserGroup)
        {
            xCln.Permissions.UserGroups = new List<XClassification.UserGroup>();

            List<Guid> ugIds = GetUserGroupIds(clnHlp, aCln.Id);
            foreach (Guid uId in ugIds)
            {
                if (aUserGroup.TryLoad(uId) == TryLoadResult.Success)
                {
                    XClassification.UserGroup xUserGroup = new XClassification.UserGroup()
                    {
                        Name = aUserGroup.Name,
                        ClassificationsType = GetAccessType(clnHlp, aCln.Id, uId, AdamCln.ClassificationRightTarget.ClassificationTree),
                        RecordType = GetAccessType(clnHlp, aCln.Id, uId, AdamCln.ClassificationRightTarget.Records)
                    };
                    xCln.Permissions.UserGroups.Add(xUserGroup);
                }
                else
                {
                    AddErrorCaseToContext(uId.ToString(), "UserGroup", MetaDataErrorType.AdamItemNotFound);
                }
            }
        }

        private AdamCln.ClassificationRight GetAccessType(AdamCln.ClassificationHelper clnHlp, Guid clnId, Guid uId, AdamCln.ClassificationRightTarget target)
        {
            AdamCln.ClassificationAccess access = clnHlp.GetUserGroupAccess(clnId, uId, target, true);
            return access.Right;
        }

        private List<Guid> GetUserGroupIds(AdamCln.ClassificationHelper clnHlp, Guid aClnId)
        {
            AManagement.UserGroupHelper userGroupHelper = new AManagement.UserGroupHelper(this.Context.AdamApplication);
            List<Guid> ids = userGroupHelper.GetIds(new SearchExpression("Name <> Administrators")).ToList();
            return ids;
        }

    }
}
