﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core.Search;
using XWarehouse = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Warehouse;
using AWarehouse = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.Warehouse;
using AWarehousing = Adam.Core.Warehousing;
using AManagement = Adam.Core.Management;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class Warehouse : APatch<AWarehouse.Warehouse, XWarehouse.Warehouse,
        AWarehousing.WarehouseTableSet, AWarehousing.WarehouseTableSetHelper>
    {
        protected override XWarehouse.Warehouse GetItem(AWarehousing.WarehouseTableSet aItem, AWarehousing.WarehouseTableSetHelper aHelper)
        {
            XWarehouse.Warehouse xWarehouse = new XWarehouse.Warehouse();

            AWarehousing.WarehouseDatabase warehouseDB = GetWarehouse(aItem.Warehouse);

            xWarehouse.WarehouseDatabase = warehouseDB.Name;
            xWarehouse.WarehouseDatabaseConnection = warehouseDB.ConnectionString.Trim();
            xWarehouse.CreateWarehouseDatabase = true;

            xWarehouse.SynchronizationInterval = SetSyncInterval(aItem.SyncInterval);
            xWarehouse.CustomScript = GetCustomScript(aItem.CustomScript);
            xWarehouse.RunAsUser = GetRunAsUser(aItem.RunAsUser);

            SetTables(xWarehouse, aItem);

            return xWarehouse;
        }
        
        private AWarehousing.WarehouseDatabase GetWarehouse(Guid guid)
        {
            AWarehousing.WarehouseDatabase warehouseDB = new AWarehousing.WarehouseDatabase(this.Context.AdamApplication);
            warehouseDB.Load(guid);
            return warehouseDB;
        }

        private void SetTables(XWarehouse.Warehouse xWarehouse, AWarehousing.WarehouseTableSet aItem)
        {
            xWarehouse.Tables = new List<XWarehouse.WHTable>();

            foreach (AWarehousing.WarehouseTable table in aItem.Tables)
            {
                XWarehouse.WHTable xTab = new XWarehouse.WHTable()
                {
                    Action = Core.MetaData.Xml.PatchAction.AddOrUpdate,
                    CustomScript = GetCustomScript(table.CustomScript),
                    Filter = GetFilter(table.Filter),
                    Name = table.TableName,
                    Source = table.SourceType,
                    Columns = AddColumns(table)
                };
                xWarehouse.Tables.Add(xTab);
            }
        }

        private List<MetaData.MetaDataElement> AddColumns(AWarehousing.WarehouseTable table)
        {
            List<MetaData.MetaDataElement> columns = new List<MetaData.MetaDataElement>();

            foreach (AWarehousing.WarehouseColumn column in table.Columns)
            {
                columns.Add(new MetaDataElement() 
                { 
                    Action = Core.MetaData.Xml.PatchAction.AddOrUpdate,
                    Value = column.Indexed.ToString(),
                    Name = column.ColumnName,
                    Data = GetData(column),
                });
            }

            return columns;
        }

        private string GetData(AWarehousing.WarehouseColumn column)
        {
            string retVal = String.Empty;
            if (column.Path.Equals("fieldid", StringComparison.OrdinalIgnoreCase) && column.FieldId.HasValue)
            {
                FieldDefinitionHelper helper = new FieldDefinitionHelper(this.Context.AdamApplication);
                FieldDefinition fd = helper.GetFieldDefinition(column.FieldId.Value);
                retVal = String.Format("'{0}'", fd.Name);
            }
            else 
            {
                retVal = column.Path.Replace('|', '.');
            }
            //return Extensions.AddCDATA(retVal);
            return retVal;
        }

        private string SetSyncInterval(TimeSpan timeSpan)
        {
            string retVal = String.Empty;
            if (timeSpan != null)
            {
                //retVal = String.Format("{0}:{1}:{2}", timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
                retVal = timeSpan.ToString("c");
            }
            return retVal;
        }

        private string GetRunAsUser(Guid? id)
        {
            string retVal = String.Empty;
            if (id.HasValue)
            {
                AManagement.UserHelper userHelper = new AManagement.UserHelper(this.Context.AdamApplication);
                if (userHelper.Exists(id.Value))
                {
                    AManagement.User user = new AManagement.User(this.Context.AdamApplication);
                    user.Load(id.Value);
                    retVal = user.Name;
                }
            }
            return retVal;
        }

        private string GetCustomScript(string value)
        {
            string retVal = String.Empty;
            if (!String.IsNullOrEmpty(value))
            {
                //string value = Extensions.RemoveCDATA(value.Trim());
                retVal = value.Trim();
            }
            return retVal;
        }

        private string GetFilter(SearchExpression value)
        {
            string retVal = String.Empty;
            if (value != null && !String.IsNullOrEmpty(value.ToString()))
            {
                //string value = Extensions.RemoveCDATA(value.Trim());
                retVal = value.ToString().Trim();
            }
            return retVal;
        }
    }
}
