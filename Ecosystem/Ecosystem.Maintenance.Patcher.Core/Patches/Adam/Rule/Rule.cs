﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ARule = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.Rule;
using XRule = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Rule;
using AdamRule = Adam.Core.Rules;
using Adam.Core;
using Adam.Core.Search;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class Rule : APatch<ARule.Rule, XRule.Rule, AdamRule.Rule, AdamRule.RuleHelper>
    {
        protected override XRule.Rule GetItem(AdamRule.Rule aItem, AdamRule.RuleHelper aHelper)
        {
            XRule.Rule xRule = new XRule.Rule();
            xRule.Conditions = new List<XRule.Condition>();
            xRule.Actions = new List<XRule.RuleActions>();
            UpdateAttributes(aItem, xRule);
            return xRule;
        }

        private void UpdateAttributes(AdamRule.Rule aRule, XRule.Rule xRule)
        {
            xRule.Target = aRule.Target;
            xRule.Enabled = aRule.Enabled;

            UpdateConditions(aRule, xRule);
            UpdateActions(aRule, xRule);
        }

        private void UpdateActions(AdamRule.Rule aRule, XRule.Rule xRule)
        {
            foreach (AdamRule.RuleAction action in aRule.Actions)
            {
                if (action is AdamRule.ReferenceRuleAction)
                {
                    AdamRule.ReferenceRuleAction referenceAction = ((AdamRule.ReferenceRuleAction)action);
                    XRule.RuleActions ruleAction = new XRule.RuleActions();
                    ruleAction.Type = XRule.RuleActionType.Reference;
                    ruleAction.Trigger = referenceAction.ManualExecutionTrigger;
                    ruleAction.Reference = Extensions.AddCDATA(referenceAction.Reference);
                    xRule.Actions.Add(ruleAction);
                }
                else if (action is AdamRule.RefreshFilesRuleAction)
                {
                    XRule.RuleActions ruleAction = new XRule.RuleActions() { Type =  XRule.RuleActionType.RefreshFiles };
                    xRule.Actions.Add(ruleAction);
                }
                else //RuleActionType.Custom
                {

                }
            }
        }

        private void UpdateConditions(AdamRule.Rule aRule, XRule.Rule xRule)
        {
            foreach (AdamRule.RuleCondition condition in aRule.Conditions)
            {
                if (condition is AdamRule.ObjectCreatedRuleCondition)
                {
                    xRule.Conditions.Add(new XRule.Condition { Type = XRule.ConditionType.Created });
                }
                else if (condition is AdamRule.ObjectChangedRuleCondition)
                {
                    xRule.Conditions.Add(new XRule.Condition { Type = XRule.ConditionType.Changed });
                }
                else if (condition is AdamRule.ObjectCreatedOrChangedRuleCondition)
                {
                    xRule.Conditions.Add(new XRule.Condition { Type = XRule.ConditionType.CreatedChanged });
                }
                else if (condition is AdamRule.MovieAddedWithoutMoviePreviewRuleCondition)
                {
                    xRule.Conditions.Add(new XRule.Condition 
                    { 
                        Type = XRule.ConditionType.MovieAddWithoutMoviePreview, 
                        Value =  (condition as AdamRule.MovieAddedWithoutMoviePreviewRuleCondition).MoviePreviewExtension
                    });
                }
            }
        }

    }
}
