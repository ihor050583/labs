﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using AManagement = Adam.Core.Management;
using Adam.Core.Search;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Field;
using Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Common.SpecificField;
using AFieldFieldDefinition = Adam.Core.Fields;
using XField = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field;
using AField = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.Field;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class Field : APatch<AField.Field, XField.Field, AFieldFieldDefinition.FieldDefinition, AFieldFieldDefinition.FieldDefinitionHelper>
    {
        public Field()
        {
            this.InitializeItemCallBack = InitializeFieldDefinition;
        }

        protected AFieldFieldDefinition.FieldDefinition InitializeFieldDefinition(AFieldFieldDefinition.FieldDefinitionHelper aHelper, Guid id)
        {
            return aHelper.GetFieldDefinition(id);
        }

        protected override XField.Field GetItem(AFieldFieldDefinition.FieldDefinition aItem, AFieldFieldDefinition.FieldDefinitionHelper aHelper)
        {
            XField.Field xField = new XField.Field();

            UpdateAttributes(aItem, xField);
            UpdateBehaviour(aItem, xField);
            UpdateAdvanced(aItem, xField);
            UpdateMemberships(aItem, xField);
            Defaults.SetDefaults(aItem, xField, this.Context.AdamApplication);
            UpdateTypeSpecific(aItem, xField);

            return xField;
        }

        private void UpdateTypeSpecific(AFieldFieldDefinition.FieldDefinition fd, XField.Field xField)
        {
            List<FailedMetaData> list = Specific.SetDataTypeSpecific(fd, xField, ScenarioType.AdamToXml, this.Context.AdamApplication);
            AddErrorCaseToContext(list);
        }

        private void UpdateAttributes(AFieldFieldDefinition.FieldDefinition fd, XField.Field xField)
        {
            xField.IsRequired = fd.IsRequired;
            xField.IsIndexed = fd.Indexed;
            xField.AllowOperatorSearches = fd.FullTextIndexed;
            xField.Scope = fd.Scope;
            xField.SortIndex = fd.SortIndex;
            xField.DataType = fd.DataType;
        }

        private static void UpdateAdvanced(AFieldFieldDefinition.FieldDefinition fd, XField.Field xField)
        {
            xField.Advanced = new XField.Advanced();
            xField.Advanced.StorageMode = fd.StorageMode;
            xField.Advanced.IsReadOnly = fd.IsReadOnly;
        }

        private static void UpdateBehaviour(AFieldFieldDefinition.FieldDefinition fd, XField.Field xField)
        {
            xField.Behaviour = new XField.Behaviour();
            xField.Behaviour.InheritanceMode = fd.InheritanceMode;
            xField.Behaviour.LanguageMode = fd.LanguageMode;
        }

        private void UpdateMemberships(AFieldFieldDefinition.FieldDefinition fd, XField.Field xField)
        {
            xField.Membership = new List<Core.MetaData.MetaDataElement>();
            AManagement.FieldGroup fg = new AManagement.FieldGroup(this.Context.AdamApplication);
            foreach (AFieldFieldDefinition.FieldDefinitionMembership item in fd.Memberships)
            {
                if (fg.TryLoad(item.FieldGroupId) == TryLoadResult.Success)
                {
                    xField.Membership.Add(new MetaDataElement() { Name = fg.Name });
                }
            }
        }

    }
}
