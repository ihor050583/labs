﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XField = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field;
using AField = Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Field
{
    public class Defaults
    {
        internal static void SetDefaults(global::Adam.Core.Fields.FieldDefinition fd, MetaData.Xml.Field.Field xField, global::Adam.Core.Application application)
        {
            xField.Defaults = new XField.Defaults();
            SetDefaultValue(fd, xField);
            SetTriggers(fd, xField);
            SetFields(fd, xField, application);
        }

        private static void SetFields(AField.FieldDefinition fd, XField.Field xField, global::Adam.Core.Application app)
        {
            xField.Defaults.ResetToDefaultFields = new List<string>();
            AField.FieldDefinitionHelper fdh = new AField.FieldDefinitionHelper(app);

            foreach (Guid id in fd.ResetToDefaultFields)
	        {
                xField.Defaults.ResetToDefaultFields.Add(
                    fdh.GetFieldDefinition(id).Name
                    );
	        }
        }

        private static void SetTriggers(global::Adam.Core.Fields.FieldDefinition fd, MetaData.Xml.Field.Field xField)
        {
            xField.Defaults.ResetToDefaultTriggers = new List<XField.Trigger>();
            List<AField.ResetToDefaultTriggers> list = GetTriggerList();
            foreach (var item in list)
            {
                if (fd.HasResetToDefaultTrigger(item))
                {
                    xField.Defaults.ResetToDefaultTriggers.Add(new MetaData.Xml.Field.Trigger() { Name = item });
                }
            }
        }

        private static List<AField.ResetToDefaultTriggers> GetTriggerList()
        {
            List<AField.ResetToDefaultTriggers> list = new List<AField.ResetToDefaultTriggers>();

            list.Add(AField.ResetToDefaultTriggers.None);
            list.Add(AField.ResetToDefaultTriggers.OnAnyChange);
            list.Add(AField.ResetToDefaultTriggers.OnAnyFileChange);
            list.Add(AField.ResetToDefaultTriggers.OnCurrentFileChange);
            list.Add(AField.ResetToDefaultTriggers.OnFieldChange);
            list.Add(AField.ResetToDefaultTriggers.OnFieldDefinitionChanged);
            list.Add(AField.ResetToDefaultTriggers.OnLoad);
            list.Add(AField.ResetToDefaultTriggers.OnMasterFileChange);
            list.Add(AField.ResetToDefaultTriggers.OnNewField);
            list.Add(AField.ResetToDefaultTriggers.OnReclassifyRecord);
            list.Add(AField.ResetToDefaultTriggers.OnSave);

            return list;
        } 

        private static void SetDefaultValue(global::Adam.Core.Fields.FieldDefinition fd, MetaData.Xml.Field.Field xField)
        {
            if (!String.IsNullOrEmpty(fd.DefaultValue))
            {
                xField.Defaults.DefaultValue = fd.DefaultValue;
            }
        }
    }
}
