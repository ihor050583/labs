﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASettings = Adam.Core.Settings;
using ASettingCategory = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.SettingCategory;
using XSettingCategory = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.SettingCategory;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using AManagement = Adam.Core.Management;
using Adam.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class SettingCategory : APatch<ASettingCategory.SettingCategory, XSettingCategory.SettingCategory, 
        ASettings.SettingCategory, ASettings.SettingCategoryHelper>
    {
        protected override XSettingCategory.SettingCategory GetItem(ASettings.SettingCategory aItem, ASettings.SettingCategoryHelper aHelper)
        {
            XSettingCategory.SettingCategory xItem = new XSettingCategory.SettingCategory();
            UpdateLabels(xItem, aItem);
            return xItem;
        }
    }
}
