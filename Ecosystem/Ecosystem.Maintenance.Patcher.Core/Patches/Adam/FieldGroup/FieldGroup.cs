﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AManagement = Adam.Core.Management;
using Adam.Core.Search;
using AFieldGroup = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.FieldGroup;
using XFieldGroup = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.FieldGroup;
using Adam.Core;
using AField = Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class FieldGroup : APatch<AFieldGroup.FieldGroup, XFieldGroup.FieldGroup, AManagement.FieldGroup, AManagement.FieldGroupHelper>
    {
        protected override XFieldGroup.FieldGroup GetItem(AManagement.FieldGroup aItem, AManagement.FieldGroupHelper aHelper)
        {
            XFieldGroup.FieldGroup xFieldGroup = new XFieldGroup.FieldGroup();
            xFieldGroup.Members = new List<MetaData.MetaDataElement>();
            AField.FieldDefinitionHelper fdh = new AField.FieldDefinitionHelper(this.Context.AdamApplication);
            GetMembers(aItem, xFieldGroup, fdh);
            return xFieldGroup;
        }

        private static void GetMembers(AManagement.FieldGroup aFieldGroup, XFieldGroup.FieldGroup xFieldGroup, AField.FieldDefinitionHelper fdh)
        {
            foreach (AManagement.FieldGroupMember member in aFieldGroup.Members)
            {
                AField.FieldDefinition fd = fdh.GetFieldDefinition(member.FieldId);
                if (fd != null)
                {
                    xFieldGroup.Members.Add(new MetaData.MetaDataElement() { Name = fd.Name });
                }
            }
        }
    }
}
