﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AUser = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.User;
using XUser = Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.User;
using AManagement = Adam.Core.Management;
using Adam.Core;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using Adam.Core.Search;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch
{
    public class User : APatch<AUser.User, XUser.User, AManagement.User, AManagement.UserHelper>
    {
        protected override XUser.User GetItem(AManagement.User aItem, AManagement.UserHelper aHelper)
        {
            XUser.User xUser = new XUser.User();

            xUser.Localization = new XUser.Localization();
            xUser.Account = new XUser.Account();
            xUser.Membership = new List<Core.MetaData.MetaDataElement>();

            UpdateAttributes(aItem, xUser);
            SetLocalization(aItem, xUser);
            SetAccount(aItem, xUser);
            AddMemberships(aItem, xUser);

            return xUser;
        }

        private void UpdateAttributes(AManagement.User user, XUser.User xUser)
        {
            xUser.Email = user.Email;
        }

        private void SetLocalization(AManagement.User user, XUser.User xUser)
        {
            xUser.Localization.Language = GetLanguageName(user.LanguageId, user.Name, "User.LanguageId");
            xUser.Localization.LanguageForUI = GetLanguageName(user.LanguageIdForUI, user.Name, "User.LanguageIdForUI");
        }

        private string GetLanguageName(Guid id, string name, string type)
        {
            string result = String.Empty;
            var language = new AManagement.Language(this.Context.AdamApplication);
            if (language.TryLoad(id) == TryLoadResult.Success)
            {
                result = language.Name;
            }
            else
            {
                AddErrorCaseToContext(name, type, MetaDataErrorType.AdamItemNotFound);
            }
            return result;
        }

        private void SetAccount(AManagement.User user, XUser.User xUser)
        {
            if (user.ExpirationDateUtc.HasValue)
            {
                xUser.Account.ExpirationDate = user.ExpirationDateUtc.Value;
            }
            xUser.Account.LockedOut = user.LockedOut;
            xUser.Account.StorageQuota = user.StorageQuota < 0 ? 0 : user.StorageQuota;
        }


        private void AddMemberships(AManagement.User user, XUser.User xUser)
        {
            AManagement.UserGroup ug = new AManagement.UserGroup(this.Context.AdamApplication);
            foreach (AManagement.UserMembership item in user.Memberships)
            {
                if (ug.TryLoad(item.UserGroupId) == TryLoadResult.Success)
                {
                    xUser.Membership.Add(new MetaDataElement() { Name = ug.Name });
                }
            }
        }
        
    }
}
