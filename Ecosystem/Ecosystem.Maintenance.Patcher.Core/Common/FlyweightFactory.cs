﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public abstract class FlyweightFactory
    {
        public delegate string ValueDelegate(string key);

        protected List<string> Keys { get; set; }
        protected Dictionary<string, string> Values { get; set; }

        public FlyweightFactory()
        {
            this.Keys = new List<string>();
            this.Values = new Dictionary<string, string>();

            InitKeys();
            InitValues();
        }

        internal void SetValue(ValueDelegate getValue)
        {
            foreach (string key in Keys)
            {
                this.Values[key] = getValue(key);
            }
        }

        internal void SetValue(string key, string value)
        {
            this.Values[key] = value;
        }

        public T GetValue<T>(string key)
        {
            string value = String.Empty;
            Values.TryGetValue(key, out value);
            return (T)Convert.ChangeType(value, typeof(T)); 
        }

        protected void InitValues()
        {
            foreach (string key in Keys)
            {
                this.Values[key] = String.Empty;
            }
        }

        abstract protected void InitKeys();
    }
}
