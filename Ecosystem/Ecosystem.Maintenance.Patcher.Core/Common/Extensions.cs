﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public static class Extensions
    {
        public static object GetDataFromFile<T>(string path, Type type)
        {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(path);

            try
            {
                if (Assembly.GetAssembly(type).GetManifestResourceStream(path) == null)
                {
                    throw new Exception("Cannot get resource: " + path + ".");
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error during load of " + path + ", inner message: " + e.Message);
            }

            try
            {
                var serializer = new XmlSerializer(typeof(T));

                return serializer.Deserialize(stream);
            }
            catch (Exception e)
            {
                throw new Exception("Error during processing " + path + ",\n inner mesasge: " + e.Message);

            }
        }

        public static string ObjectToXml(object obj)
        {
            string retVal = String.Empty;
            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            using (StringWriter outStream = new StringWriter())
            { 
                serializer.Serialize(outStream, obj);
                retVal = outStream.ToString();
            }
            return retVal;
        }

        public static T XmlToObject<T>(string value)
        {
            object retVal;
            using (StringReader sr = new StringReader(value))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                retVal = serializer.Deserialize(sr);
            }
            return (T)retVal;
        }

        public static T DeserializeFrom<T>(string path)
        {
            T retVal;
            var xs = new XmlSerializer(typeof(T));
            using (TextReader reader = new StreamReader(path))
            {
                retVal = (T)xs.Deserialize(reader);
            }
            return retVal;
        }

        public static string ExecutingDirectoryName()
        {
            string location = Assembly.GetExecutingAssembly().Location;
            string directory = Path.GetDirectoryName(location);
            return directory;
        }

        public static void SaveToFile<Type>(string directory, string key, Type obj)
        {
            string dir = GetDirectory(directory);

            key = StripInvalidSymbols(key);

            string filePath = String.Format("{0}\\{1}.xml", dir, key);

            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(String.Empty, String.Empty);

            XmlSerializer serializer = new XmlSerializer(typeof(Type));
            TextWriter tw = new StreamWriter(filePath);
            serializer.Serialize(tw, obj, ns);

            tw.Close();
        }

        private static string StripInvalidSymbols(string filePath)
        {
            filePath = filePath.Replace(":", "");
            return filePath;
        }

        private static string GetDirectory(string directory)
        {
            string filePath = Extensions.ExecutingDirectoryName();
            filePath = Path.Combine(filePath, directory);

            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            return filePath;
        }

        public static string AddCDATA(string value)
        {
            if (value != null)
            {
                value = String.Format("<![CDATA[{0}]]>",value);
            }

            return value;
        }

        public static string RemoveCDATA(string value)
        {
            string retVal = value;

            if (!String.IsNullOrEmpty(retVal))
            {
                retVal = retVal.Trim();

                int firstInd = retVal.IndexOf("<![CDATA[");
                int lastInd = retVal.LastIndexOf("]]>");

                if (firstInd == 0 && lastInd != -1)
                {
                    retVal = retVal.Substring(("<![CDATA[").Length);
                    retVal = retVal.Substring(0, retVal.Length - ("]]>").Length);
                }
            }

            //value = value.Replace("<![CDATA[", string.Empty);
            //value = value.Replace("]]>", string.Empty);

            return retVal;
        }


        internal static List<string> GetDirectoryXmlContent(string path, string key)
        {
            string dir = Path.Combine(path, key);
            var ext = new List<string> { ".xml" };
            return Directory.GetFiles(dir, "*.*", SearchOption.TopDirectoryOnly).Where(
                s => ext.Any(e => s.EndsWith(e))
                ).ToList();
        }

        internal static bool XmlSchemaValidate(string xmlFile, string xsdFile, ref string outMessage)
        {
            bool retVal = true;
            string message = String.Empty;

            
            XmlSchemaSet schemas = new XmlSchemaSet();
            schemas.Add(null, xsdFile);

            XDocument custOrdDoc = XDocument.Load(xmlFile);
            
            custOrdDoc.Validate(schemas, (o, e) =>
            {
                message += e.Message;
                retVal = false;
            }, true);

            outMessage = message;
            
/*
            XmlDocument inDoc = new XmlDocument();
            inDoc.Load(xmlFile);
            XmlReader schemaReader = null;
            XmlSchemaSet set = new XmlSchemaSet();
            schemaReader = XmlReader.Create(xsdFile);
            XmlSchema schema = XmlSchema.Read(schemaReader, null);
            set.Add(schema);
            inDoc.Schemas = set;
            inDoc.Validate((o, e) =>
            {
                message += e.Message;
                retVal = false;
            }, inDoc);

            string msg = inDoc.ToString();
*/
            
            return retVal;
        }
    }

}
