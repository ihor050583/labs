﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class ValueInitializer : IValueInitializer
    {

        private string GetParamValue(string key)
        {
            return ValueProvider.GetValue(key);
        }

        #region IValueInitializer Members

        public void Initialize(ref FlyweightFactory fwf)
        {
            fwf.SetValue(this.GetParamValue);
        }

        public IValueProvider ValueProvider { get; set; }

        #endregion

    }
}
