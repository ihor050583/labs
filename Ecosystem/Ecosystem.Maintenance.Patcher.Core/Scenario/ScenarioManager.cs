﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Adam.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    [Serializable]
    public class ScenarioManager
    {
        [XmlAttribute]
        public string MetaDataLocation { get; set; }
        [XmlElement("Scenario")]
        public List<ScenarioManagerItem> ScenarioItems{ get; set; }
        [XmlAttribute]
        public string AdamSettingName { get; set; }

        public void UpdateFromXml(object xml)
        {
            if (xml != null)
            {
                ScenarioManager scenarioManager = ScenarioFactory.ScenarioManager(xml);
                List<ScenarioManagerItem> notFound = new List<ScenarioManagerItem>();

                foreach (var adamItem in scenarioManager.ScenarioItems)
                {
                    bool found = false;
                    foreach (var xmlItem in this.ScenarioItems)
                    {
                        if (xmlItem.Name.Equals(adamItem.Name, StringComparison.OrdinalIgnoreCase))
                        {
                            found = true;
                            if (!String.IsNullOrEmpty(adamItem.LastExecuted))
                            {
                                xmlItem.LastExecuted = adamItem.LastExecuted;
                            }
                            if(adamItem.RunInfo != null)
                            {
                                xmlItem.RunInfo = adamItem.RunInfo;
                            }
                            break;
                        }
                    }
                    adamItem.Enabled = found;
                    if (!found)
                    {
                        notFound.Add(adamItem);
                    }
                }

                this.ScenarioItems.AddRange(notFound);
            }
        }

        internal void SaveResult(ScenarioManagerItem item, PatchRunnerResult result)
        {
            item.LastExecuted = DateTime.Now.ToString("F");

            if (item.RunInfo == null)
            {
                item.RunInfo = new List<ScenarioManagerRunInfo>();
            }

            int count = result.Context.DataList.Count;
            bool error = (from s in result.Context.DataList
                          where s.Patch.Context.PatchResult.ResultType == PatchResultErrorType.MetaDataError ||
                                s.Patch.Context.PatchResult.ResultType == PatchResultErrorType.PatchUnexpectedError
                          select s).Any();
            bool warning = (from s in result.Context.DataList
                            where s.Patch.Context.FailedMetaData.Count > 0
                            select s).Any();

            item.RunInfo.Add(new ScenarioManagerRunInfo
            {
                Success = result.Success,
                Count = count,
                Error = error,
                Warning = warning,
                Executed = item.LastExecuted
            });
        }

        internal void UpdateAdamSetting(Application app)
        {
            Adam.Core.Settings.XmlSettingDefinition xml = new Adam.Core.Settings.XmlSettingDefinition(app);

            if (xml.TryLoad(this.AdamSettingName) == TryLoadResult.NotFound)
            {
                xml.AddNew();
                xml.Name = this.AdamSettingName;
                xml.AllowAnonymousAccess = false;
                xml.AllowSiteSetting = false;
                xml.AllowSystemSetting = true;
                xml.AllowUserSetting = false;
                xml.CategoryId = Adam.Core.Settings.SettingCategory.SystemId;
                xml.DefaultValue = Extensions.ObjectToXml(this);
                xml.Save();
            }
            else
            {
                Adam.Core.Settings.XmlSetting setting = new Adam.Core.Settings.XmlSetting(app);
                setting.LoadSystemValue(this.AdamSettingName);
                setting.Value = Extensions.ObjectToXml(this);
                setting.Save();
            }
        }
    }
}
