﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    [Serializable]
    public class ScenarioManagerRunInfo
    {
        [XmlAttribute]
        public string Executed { get; set; }
        [XmlAttribute]
        public int Count { get; set; }
        [XmlAttribute]
        public bool Success { get; set; }
        [XmlAttribute]
        public bool Error { get; set; }
        [XmlAttribute]
        public bool Warning { get; set; }
    }
}