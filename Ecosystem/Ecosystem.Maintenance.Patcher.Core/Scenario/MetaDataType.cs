﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    [Serializable]
    public class MetaDataType
    {
        [XmlElement("MetaData")]
        public List<ScenarioItem> Items { get; set; }
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public bool UseNameAsSourceDirectory { get; set; }
    }
}
