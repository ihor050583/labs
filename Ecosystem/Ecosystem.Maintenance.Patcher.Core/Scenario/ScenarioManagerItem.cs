﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    [Serializable]
    public class ScenarioManagerItem
    {
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public bool RunOnce { get; set; }
        [XmlAttribute]
        public string LastExecuted { get; set; }
        [XmlElement("Scenario")]
        public List<ScenarioManagerRunInfo> RunInfo { get; set; }
        [XmlIgnore]
        public bool Enabled { get; set; }

        public ScenarioManagerItem()
        {
            Enabled = true;
        }


        internal bool IsReRunWithWarnings(bool enabled)
        {
            return enabled ? (from s in RunInfo where !s.Success || s.Warning select s).Any() : false;
        }

        internal bool IsReRunWithErrors(bool enabled)
        {
            return enabled ? (from s in RunInfo where !s.Success || s.Error select s).Any() : false;
        }

        internal bool IsFirstRun()
        {
            return this.RunOnce && String.IsNullOrEmpty(this.LastExecuted);
        }
    }
}
