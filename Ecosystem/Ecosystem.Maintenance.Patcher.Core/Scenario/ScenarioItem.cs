﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    [Serializable]
    public class ScenarioItem
    {
        [XmlAttribute]
        public int Id { get; set; }
        [XmlAttribute]
        public string Type { get; set; }
        [XmlAttribute]
        public string Name { get; set; }
        [XmlIgnore]
        public string MetaDataFileLocation { get; set; }
        [XmlIgnore]
        public string XsdSchemaFileLocation { get; set; }
    }
}
