﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    [Serializable]
    public class Scenario
    {
        [XmlAttribute]
        public ScenarioType Type { get; set; }
        [XmlAttribute]
        public string MetaDataLocation { get; set; }
        [XmlElement("MetaDataType")]
        public List<MetaDataType> MetaDataTypes { get; set; }
        [XmlIgnore]
        public string Name { get; set; }
        [XmlIgnore]
        public bool Debug { get; set; }
        [XmlIgnore]
        public string DebugPatchName { get; set; }

        public Scenario()
        {
            Debug = false;
            DebugPatchName = String.Empty;
        }
    }
}
