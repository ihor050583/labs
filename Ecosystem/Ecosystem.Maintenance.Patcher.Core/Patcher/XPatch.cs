﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public abstract class XPatch<MetaDataType, AItemType> : Patch<MetaDataType>
    {
        protected delegate Adam.Core.TryLoadResult TryLoadItem(AItemType aItem);
        protected TryLoadItem TryLoadItemCallback = null;

        protected abstract void UpdateItem(AItemType aItem);
        protected virtual void PreDeleteItem(AItemType aItem) { }
        protected virtual void Validate() { }


        private AItemType GetItem()
        {
            dynamic xItem = this.MetaData;
            dynamic aItem = (AItemType)Activator.CreateInstance(typeof(AItemType), new object[] { this.Context.AdamApplication });

            Adam.Core.TryLoadResult result = Adam.Core.TryLoadResult.NotFound;

            if (TryLoadItemCallback != null)
            {
                result = TryLoadItemCallback(aItem);
            }
            else
            {
                result = aItem.TryLoad(xItem.Name);
            }

            if (result == Adam.Core.TryLoadResult.NotFound)
            {
                aItem.AddNew();
            }

            return aItem;
        }

        protected override void ExecuteHlp()
        {
            Validate();
            dynamic xItem = this.MetaData;
            if (xItem.Action == Core.MetaData.Xml.PatchAction.AddOrUpdate)
            {
                if (this.IsValid)
                {
                    AItemType aItem = GetItem();
                    UpdateItem(aItem);
                    UpdateName(aItem, xItem);
                    UpdateTag(aItem, xItem);
                    this.SaveAdamItem(aItem, xItem.GetType().ToString());
                }
            }
            else if (xItem.Action == Core.MetaData.Xml.PatchAction.Delete)
            {
                AItemType aItem = GetItem();
                DeleteItem(aItem);
            }
        }

        private void UpdateTag(AItemType aItem, dynamic xItem)
        {
            try
            {
                dynamic it = aItem;
                it.Tag = Extensions.RemoveCDATA(xItem.Tag);
            }
            catch (Exception)
            {
            }
        }

        protected void UpdateName(dynamic aItem, dynamic xItem)
        {
            aItem.Name = xItem.Name;
            if (xItem.UpdateName != null)
            {
                aItem.Name = xItem.UpdateName.Value;
            }
        }

        private void DeleteItem(AItemType aItem)
        {
            dynamic item = aItem;
            bool isNew = GetIsNew(item);

            if (!isNew)
            {
                try
                {
                    PreDeleteItem(item);
                    item.Delete();
                }
                catch (Exception ex)
                {
                    AddErrorCaseToContext(item.Name, item.GetType().ToString(), MetaDataErrorType.Unexpected, ex.Message);
                }
            }
        }

        private static bool GetIsNew(dynamic item)
        {
            bool isNew = true;

            try
            {
                isNew = item.IsNew;
            }
            catch
            {
                try
                {
                    isNew = item.IsAdded;
                }
                catch
                {
                }
            }

            return isNew;
        }
    }
}
