﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatchData
    {
        public ScenarioItem ScenarioItem { get; protected set; }
        public IPatch Patch { get; protected set; }

        public PatchData(ScenarioItem si, IPatch patch)
        {
            this.ScenarioItem = si;
            this.Patch = patch;
        }
    }
}
