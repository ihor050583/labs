﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public delegate void AddErrorCase(string name, string type, MetaDataErrorType error);

    public abstract class Patch<MetaDataType> : IPatch
    {
        protected bool _isValid = true;
        protected bool IsValid {
            get 
            { 
                return this._isValid; 
            }
            private set 
            {
                if (this._isValid)
                {
                    this._isValid = value;
                }
            } 
        }

        protected MetaDataType MetaData { get; set; }

        public Patch()
        {
            this.IsValid = true;
        }

        #region IPatch Members

        public PatchContext Context { get; set; }

        public void Execute(PatchContext context)
        {
            this.Context = context;

            this.Context.PatchResult.ResultType = PatchResultErrorType.Success;

            try
            {
                Validate(context.ScenarioItem.MetaDataFileLocation, context.ScenarioItem.XsdSchemaFileLocation);
                this.MetaData = GetMetaData(context.ScenarioItem.MetaDataFileLocation);
            }
            catch (Exception ex)
            {
                this.Context.PatchResult.ResultType = PatchResultErrorType.MetaDataError;
                this.Context.PatchResult.Exception = ex;
            }

            if (this.Context.PatchResult.ResultType == PatchResultErrorType.Success)
            {
                try
                {
                    ExecuteHlp();
                }
                catch (Exception ex)
                {
                    this.Context.PatchResult.ResultType = PatchResultErrorType.PatchUnexpectedError;
                    dynamic item = this.MetaData;
                    this.Context.PatchResult.Exception = new Exception(
                       String.Format("Item Name: {0}, Message: {1}", item.Name, ex.ToString()), 
                        ex.InnerException
                        );
                }
            }
        }

        protected MetaDataType GetMetaData(string path)
        {
            return Extensions.DeserializeFrom<MetaDataType>(path);
        }

        #endregion

        protected void AddErrorCaseToContext(string name, string type, MetaDataErrorType error)
        {
            FailedMetaDataBuilder.AddFailedItem(this.Context.FailedMetaData, name, type, error);
            this.IsValid = false;
        }

        protected void AddErrorCaseToContext(string name, string type, MetaDataErrorType error, bool validate)
        {
            FailedMetaDataBuilder.AddFailedItem(this.Context.FailedMetaData, name, type, error);
            if (validate)
            {
                this.IsValid = false;
            }
        }

        protected void AddErrorCaseToContext(string name, string type, MetaDataErrorType error, string message)
        {
            FailedMetaDataBuilder.AddFailedItem(this.Context.FailedMetaData, name, type, error, message);
            this.IsValid = false;
        }

        protected void AddErrorCaseToContext(string name, string type, MetaDataErrorType error, string message, bool validate)
        {
            FailedMetaDataBuilder.AddFailedItem(this.Context.FailedMetaData, name, type, error, message);
            if (validate)
            {
                this.IsValid = false;
            }
        }

        protected void AddErrorCaseToContext(List<FailedMetaData> list)
        {
            FailedMetaDataBuilder.AddFailedItem(this.Context.FailedMetaData, list);
            this.IsValid = (list.Count == 0);
        }

        protected void AddErrorCaseToContext(List<FailedMetaData> list, bool validate)
        {
            FailedMetaDataBuilder.AddFailedItem(this.Context.FailedMetaData, list);
            if (validate)
            {
                this.IsValid = (list.Count == 0);
            }
        }

        protected void SaveMetaData<XType>(dynamic xItem)
        {
            try
            {
                if (this.IsValid)
                {
                    string[] parts = this.MetaData.GetType().ToString().Split('.');
                    string value = parts[parts.Length - 1];
                    dynamic metaData = this.MetaData;

                    //Extensions.SaveToFile<XType>(metaData.MetaDataLocation, value + "_" + xItem.Name.Replace(" ", "_"), xItem);
                    Extensions.SaveToFile<XType>(metaData.MetaDataLocation, value + "_" + xItem.Name.Replace(" ", "_").Replace("/","-"), xItem);
                }
            }
            catch (Exception ex)
            {

                throw new Exception(String.Format("Item Name: {0}, Error Message: {1}", xItem.Name, ex.Message), 
                    ex.InnerException);
            }
            
        }

        protected void SaveAdamItem(dynamic aItem, string type)
        {
            if (this.IsValid)
            {
                try
                {
                    aItem.Save();
                }
                catch (Exception ex)
                {
                    AddErrorCaseToContext(aItem.Name, type, MetaDataErrorType.Unexpected, 
                        String.Format("Item was not saved. {0}", ex.Message));
                }
            }
        }

        protected void Validate(string xmlFile, string xsdFile)
        {
            string outMessage = String.Empty;
            bool result = Extensions.XmlSchemaValidate(xmlFile, xsdFile, ref outMessage);

            if(!result)
            {
                throw new Exception(outMessage);
            }
        }

        protected abstract void ExecuteHlp();

        protected void UpdateLabels(List<MetaData.MetaDataElement> xLabels, Adam.Core.LabelItemCollection aLabels)
        {
            List<FailedMetaData> retVal = new List<FailedMetaData>();

            Adam.Core.Management.LanguageHelper languageHelper = new Adam.Core.Management.LanguageHelper(this.Context.AdamApplication);

            foreach (MetaData.MetaDataElement xLabel in xLabels)
            {
                Guid? id = languageHelper.GetId(xLabel.Name);

                if (id.HasValue)
                {
                    try
                    {
                        aLabels[id.Value].Value = xLabel.Value;
                    }
                    catch (Exception)
                    {
                        this.AddErrorCaseToContext(xLabel.Name, "Label.Language", MetaDataErrorType.AdamItemNotFound, false);
                    }
                }
                else
                {
                    AddErrorCaseToContext(xLabel.Name, "Label.Language", MetaDataErrorType.AdamItemNotFound);
                }
            }
        }
    }
}
