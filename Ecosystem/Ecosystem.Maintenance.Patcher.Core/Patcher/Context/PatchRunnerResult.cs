﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatchRunnerResult
    {
        public PatcherContext Context { get; protected set; }
        public bool Success { get; protected set; }
        public Exception Exception { get; protected set; }

        public PatchRunnerResult(PatcherContext Context)
        {
            this.Context = Context;
            this.Success = true;
        }

        public PatchRunnerResult()
        {
            this.Success = true;
        }

        public void SetException(Exception exception)
        {
            this.Exception = exception;
            this.Success = false;
        }
    }
}
