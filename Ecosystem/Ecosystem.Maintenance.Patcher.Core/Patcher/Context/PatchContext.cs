﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatchContext
    {
        public Params Params { get; set; }
        public PatchResult PatchResult { get; set; }
        public ScenarioItem ScenarioItem { get; set; }
        public Application AdamApplication { get; set; }
        public List<FailedMetaData> FailedMetaData { get; set; }
        public int ItemsCount { get; set; }

        public PatchContext(ScenarioItem si, Params prms, Application app)
        {
            this.PatchResult = new PatchResult();
            this.Params = prms;
            this.ScenarioItem = si;
            this.AdamApplication = app;
            this.FailedMetaData = new List<FailedMetaData>();
        }
    }
}
