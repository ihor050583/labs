﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatcherContext
    {
        public Params Params { get; set; }
        public Scenario Scenario { get; set; }
        public List<PatchData> DataList { get; set; }
        public PatchTypes PatchTypes { get; set; }
        public Application AdamApplication { get; set; }

        public PatcherContext(Params prms, Scenario scenario, PatchTypes patchTypes, Application app)
        {
            this.Params = prms;
            this.Scenario = scenario;
            this.DataList = new List<PatchData>();
            this.PatchTypes = patchTypes;
            this.AdamApplication = app;
        }
    }
}
