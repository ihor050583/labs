﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class FailedMetaData
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public MetaDataErrorType Error { get; set; }
        public string Message { get; set; }

        public FailedMetaData(string name, string type, MetaDataErrorType error)
        {
            this.Name = name;
            this.Type = type;
            this.Error = error;
        }

        public FailedMetaData(string name, string type, MetaDataErrorType error, string message)
        {
            this.Name = name;
            this.Type = type;
            this.Error = error;
            this.Message = message;
        }
    }

    public enum MetaDataErrorType
    {
        None = 0,
        Unexpected,
        AdamItemNotFound,
        ParentNotFound,
        XmlValueIsEmpty,
        XmlInvalidDataFormat,
        XmlUnknownType,
        InvalidOperation
    }
}
