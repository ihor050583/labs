﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatchResult
    {
        public PatchResultErrorType ResultType { get; set; }
        public string Message { get; set; }
        public Exception Exception { get; set; }
    }

    public enum PatchResultErrorType
    { 
        Success = 0,
        MetaDataError,
        PatchUnexpectedError
    }
}
