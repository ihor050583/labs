﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatchRunner
    {
        public static List<PatchRunnerResult> Execute(Params prms)
        {
            PatchRunnerResult result = new PatchRunnerResult();
            List<PatchRunnerResult> results = new List<PatchRunnerResult>();

            try
            {
                Application app = GetApplication(prms);
                ScenarioManager scenarioManager = ScenarioManagerFactory.GetScenarioManager(prms, app);

                bool enableDebug = prms.GetValue<bool>(Params.EnableDebug);
                string debugScenarioName = prms.GetValue<string>(Params.DebugScenarioName);
                string debugPatchName = prms.GetValue<string>(Params.DebugPatchName);

                bool IsReRunWithWarningsEnabled = prms.GetValue<bool>(Params.IsReRunWithWarnings);
                bool IsReRunWithErrorsEnabled = prms.GetValue<bool>(Params.IsReRunWithErrors);

                foreach (var item in scenarioManager.ScenarioItems)
                {
                    if (!item.Enabled) continue;

                    Patcher patcher = null;
                    string scenarioLocation = String.Format("../../{0}/{1}.xml", scenarioManager.MetaDataLocation, item.Name);

                    if (enableDebug)
                    {
                        if (!String.IsNullOrEmpty(debugScenarioName) &&
                            debugScenarioName.Equals(item.Name, StringComparison.OrdinalIgnoreCase))
                        {
                            patcher = PatcherFactory.GetPatcherInDebugMode(prms, scenarioLocation, app, debugPatchName);
                        }
                    }
                    else if (!item.RunOnce || 
                             item.IsFirstRun() ||
                             item.IsReRunWithWarnings(IsReRunWithWarningsEnabled) ||
                             item.IsReRunWithErrors(IsReRunWithErrorsEnabled))
                    {
                        patcher = PatcherFactory.GetPatcher(prms, scenarioLocation, app);
                    }

                    if (patcher != null)
                    {
                        patcher.Execute();

                        result = patcher.GetPatchResult();
                        result.Context.Scenario.Name = item.Name;
                        results.Add(result);

                        scenarioManager.SaveResult(item, result);
                    }
                }

                if (!enableDebug)
                {
                    scenarioManager.UpdateAdamSetting(app);
                }

                LogOffAdam(app);
            }
            catch (Exception ex)
            {
                result.SetException(ex);
                results.Add(result);
            }
            return results;
        }

        private static Application GetApplication(Params prms)
        {
            Application app = new Application();

            LogOnStatus status = app.LogOn(
                prms.GetValue<string>(Params.AdamRegistrationName),
                prms.GetValue<string>(Params.AdamAdminUserName),
                prms.GetValue<string>(Params.AdamAdminPassword)
                );

            if (status != LogOnStatus.LoggedOn)
            {
                throw new UnauthorizedAccessException(
                    String.Format("UnauthorizedAccessException: AdamRegistrationName: {0}, AdamAdminUserName: {1}, AdamAdminPassword: {2}",
                    prms.GetValue<string>(Params.AdamRegistrationName),
                    prms.GetValue<string>(Params.AdamAdminUserName),
                    prms.GetValue<string>(Params.AdamAdminPassword))
                    );
            }

            return app;
        }

        public static void LogOffAdam(Application app)
        {
            if (app != null && app.IsLoggedOn)
            {
                app.LogOff();
            }
        }
    }
}
