﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Adam.Core.Search;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData;
using AManagement = Adam.Core.Management;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public abstract class APatch<AMetaDataType, XMetaDataType, AItemType, AHelper> : Patch<AMetaDataType>
    {
        protected delegate AItemType InitializeItem(AHelper fHelper, Guid id);
        protected InitializeItem InitializeItemCallBack = null;
        protected abstract XMetaDataType GetItem(AItemType aItem, AHelper aHelper);

        protected override void ExecuteHlp()
        {
            dynamic aItem = null;

            if (InitializeItemCallBack == null)
            {
                aItem = (AItemType)Activator.CreateInstance(typeof(AItemType), new object[] { this.Context.AdamApplication });
            }
            
            dynamic aHelper = (AHelper)Activator.CreateInstance(typeof(AHelper), new object[] { this.Context.AdamApplication });

            List<Guid> ids = aHelper.GetIds(new SearchExpression("*"));

            foreach (Guid id in ids)
            {
                if (InitializeItemCallBack == null)
                {
                    if (aItem.TryLoad(id) == TryLoadResult.Success)
                    {
                        XMetaDataType xItem = GetItem(aItem, aHelper);
                        UpdateBaseAttributes(xItem, aItem);
                        base.SaveMetaData<XMetaDataType>(xItem);
                    }
                }
                else
                {
                    aItem = InitializeItemCallBack(aHelper, id);
                    XMetaDataType xItem = GetItem(aItem, aHelper);
                    UpdateBaseAttributes(xItem, aItem);
                    base.SaveMetaData<XMetaDataType>(xItem);
                }
            }

            this.Context.ItemsCount = ids.Count;
        }

        protected void UpdateLabels(XMetaDataType xItem, AItemType aItem)
        {
            dynamic x = xItem;
            dynamic a = aItem;

            x.Labels = new List<MetaDataElement>();
            AManagement.Language aLanguage = new AManagement.Language(this.Context.AdamApplication);
            foreach (LabelItem item in a.Labels)
            {
                if (aLanguage.TryLoad(item.LanguageId) == TryLoadResult.Success)
                {
                    x.Labels.Add(new MetaDataElement() { Name = aLanguage.Name, Value = item.Value });
                }
            }
        }

        protected void UpdateBaseAttributes(dynamic xItem, dynamic aItem)
        {
            xItem.Name = aItem.Name;
            xItem.Action = Core.MetaData.Xml.PatchAction.AddOrUpdate;
            UpdateTag(xItem, aItem);
        }

        private void UpdateTag(dynamic xItem, dynamic aItem)
        {
            try
            {
                xItem.Tag = aItem.Tag;
            }
            catch (Exception)
            {
            }
        }
    }
}
