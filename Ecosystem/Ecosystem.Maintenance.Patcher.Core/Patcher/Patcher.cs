﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class Patcher
    {
        private IPatchCommand Command { get; set; }
        private PatcherContext Context { get; set; }

        public Patcher(PatcherContext context, IPatchCommand provider)
        {
            this.Context = context;
            this.Command = provider;
        }

        public PatchRunnerResult GetPatchResult()
        {
            return new PatchRunnerResult(this.Context);
        }

        internal void Execute()
        {
            CreatePatchData();
            Command.Execute(Context);
        }

        private void CreatePatchData()
        {
            foreach (MetaDataType type in Context.Scenario.MetaDataTypes)
            {
                if (type.UseNameAsSourceDirectory)
                {
                    type.Items = GetMetaDataTypeItems(type);
                }

                foreach (ScenarioItem item in type.Items)
                {
                    if (this.Context.Scenario.Debug &&
                        !this.Context.Scenario.DebugPatchName.Equals(item.Name, StringComparison.OrdinalIgnoreCase))
                        continue;

                    item.Type = type.Name;
                    PatchData data = PatchDataFactory.GetData(Context, item);
                    PatchContextBuilder.AddPatchData(Context, data);
                }
            }
        }

        private List<ScenarioItem> GetMetaDataTypeItems(MetaDataType type)
        {
            List<ScenarioItem> retVal = new List<ScenarioItem>();

            List<string> list = Extensions.GetDirectoryXmlContent(Context.Scenario.MetaDataLocation, type.Name);

            int id = 1;
            foreach (string location in list)
            {
                string name = System.IO.Path.GetFileNameWithoutExtension(location);

                if (this.Context.Scenario.Debug &&
                    !this.Context.Scenario.DebugPatchName.Equals(name, StringComparison.OrdinalIgnoreCase))
                    continue;

                string dir = System.IO.Path.GetDirectoryName(location);
                retVal.Add(new ScenarioItem() { Id = id++, Name = name, MetaDataFileLocation = dir, Type = type.Name });
            }

            return retVal;
        }
    }
}
