﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatchValueProvider : IValueProvider
    {
        public string Format { get; protected set; }
        public PatchValueProvider(string format)
        {
            this.Format = format;
        }

        #region IParamValueProvider Members

        public object Source { get; set; }

        public string GetValue(string key)
        {
            return String.Format(this.Format, key);
        }

        #endregion
    }

}
