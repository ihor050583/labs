﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.Commands
{
    public class PatchCommand : IPatchCommand
    {
        #region IPatchCommand Members

        public void Execute(PatcherContext context)
        {
            foreach (PatchData data in context.DataList)
            {
                data.Patch.Execute(new PatchContext(data.ScenarioItem, context.Params, context.AdamApplication));
            }
        }

        #endregion
    }
}
