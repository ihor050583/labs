﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatchContextBuilder
    {
        internal static void AddPatchData(PatcherContext context, PatchData data)
        {
            context.DataList.Add(data);
        }
    }
}
