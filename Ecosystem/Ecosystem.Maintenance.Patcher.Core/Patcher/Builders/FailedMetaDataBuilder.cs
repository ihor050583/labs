﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class FailedMetaDataBuilder
    {

        internal static void AddFailedItem(List<FailedMetaData> list, string name, string type, MetaDataErrorType error)
        {
            list.Add(new FailedMetaData(name, type, error));
        }

        internal static void AddFailedItem(List<FailedMetaData> list, string name, string type, MetaDataErrorType error, string message)
        {
            list.Add(new FailedMetaData(name, type, error, message));
        }

        internal static void AddFailedItem(List<FailedMetaData> listToStore, List<FailedMetaData> listToAdd)
        {
            if (listToAdd != null && listToAdd.Count > 0)
            {
                listToStore.AddRange(listToAdd);
            }
        }

        internal static void AddFailedItem(List<FailedMetaData> list, FailedMetaData item)
        {
            if (item != null)
            {
                list.Add(item);
            }
        }
    }
}
