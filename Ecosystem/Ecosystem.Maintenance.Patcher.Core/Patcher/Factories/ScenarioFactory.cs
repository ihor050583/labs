﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class ScenarioFactory
    {
        internal static Scenario GetScenario(string path)
        {
            string file = GetPath(path);
            return Extensions.DeserializeFrom<Scenario>(file);
        }

        private static string GetPath(string path)
        {
            return Path.Combine(Extensions.ExecutingDirectoryName(), path);
        }

        internal static ScenarioManager ScenarioManager(string path)
        {
            string file = GetPath(path);
            return Extensions.DeserializeFrom<ScenarioManager>(file);
        }

        internal static ScenarioManager ScenarioManager(object xml)
        {
            return Extensions.XmlToObject<ScenarioManager>(xml as string);
        }
    }
}
