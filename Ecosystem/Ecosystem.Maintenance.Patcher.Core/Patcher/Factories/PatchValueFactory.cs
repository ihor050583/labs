﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatchValueFactory
    {
        public static IValueProvider GetProvider(ScenarioType type)
        {
            IValueProvider provider = new PatchValueProvider("Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Adam.Patch.{0}");
            if (type == ScenarioType.XmlToAdam)
            {
                provider = new PatchValueProvider("Epam.Ecosystem.Maintenance.Patcher.Core.Patches.Xml.Patch.{0}");
            }
            return provider;
        }
    }
}
