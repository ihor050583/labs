﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatchTypes : FlyweightFactory
    {
        public const string Classification = "Classification";
        public const string UserGroup = "UserGroup";
        public const string Field = "Field";
        public const string FieldGroup = "FieldGroup";
        public const string Site = "Site";
        public const string FileType = "FileType";
        public const string Indexer = "Indexer";
        public const string Language = "Language";
        public const string Organization = "Organization";
        public const string Rule = "Rule";
        public const string SettingCategory = "SettingCategory";
        public const string SettingDefinition = "SettingDefinition";
        public const string SettingValue = "SettingValue";
        public const string Translation = "Translation";
        public const string User = "User";
        public const string Warehouse = "Warehouse";
        public const string Workflow = "Workflow";

        protected override void InitKeys()
        {
            Keys.Add(Classification);
            Keys.Add(UserGroup);
            Keys.Add(Field);
            Keys.Add(FieldGroup);
            Keys.Add(Site);
            Keys.Add(FileType);
            Keys.Add(Indexer);
            Keys.Add(Language);
            Keys.Add(Organization);
            Keys.Add(Rule);
            Keys.Add(SettingCategory);
            Keys.Add(SettingDefinition);
            Keys.Add(SettingValue);
            Keys.Add(Translation);
            Keys.Add(User);
            Keys.Add(Warehouse);
            Keys.Add(Workflow);
        }
    }
}
