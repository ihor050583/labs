﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Epam.Ecosystem.Maintenance.Patcher.Core.Commands;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatcherFactory
    {
        public static Patcher GetPatcher(Params prms, string scenarioLocation, Application app)
        {
            PatcherContext context = PatchContextFactory.GetContext(prms, scenarioLocation, app);
            IPatchCommand cmd = new PatchCommand();
            return new Patcher(context, cmd);
        }

        internal static Patcher GetPatcherInDebugMode(Params prms, string scenarioLocation, Application app, string debugPatchName)
        {
            PatcherContext context = PatchContextFactory.GetContext(prms, scenarioLocation, app);

            context.Scenario.Debug = true;
            context.Scenario.DebugPatchName = debugPatchName;

            IPatchCommand cmd = new PatchCommand();
            return new Patcher(context, cmd);
        }
    }
}
