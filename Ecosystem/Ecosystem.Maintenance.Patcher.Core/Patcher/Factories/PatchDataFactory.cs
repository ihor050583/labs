﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatchDataFactory
    {

        public static PatchData GetData(PatcherContext context, ScenarioItem si)
        {
            return new PatchData(si, PatchFactory.GetPatch(context, si));
        }
    }
}
