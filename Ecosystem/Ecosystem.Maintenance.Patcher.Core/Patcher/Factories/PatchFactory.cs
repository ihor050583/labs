﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatchFactory
    {
        internal static IPatch GetPatch(PatcherContext context, ScenarioItem si)
        {
            IPatch patch = null;
            string type = String.Empty;
            try
            {
                type = context.PatchTypes.GetValue<string>(si.Type);
                patch = Activator.CreateInstance(Type.GetType(type)) as IPatch;
                si.MetaDataFileLocation = GetMetaDataFileLocation(context, si);
                si.XsdSchemaFileLocation = GetXsdSchemaFileLocation(context, si);
            }
            catch (Exception ex)
            {
                throw new Exception(
                    String.Format("PatchFactory. Cannot CreateInstance of type {0}. Exception: {1}", type, ex.ToString())
                    );
            }
            return patch;
        }

        private static string GetXsdSchemaFileLocation(PatcherContext context, ScenarioItem si)
        {
            string directory = Path.Combine(Extensions.ExecutingDirectoryName(), context.Params.GetValue<string>(Params.XsdSchemaFolder));
            directory = Path.Combine(directory, context.Scenario.Type == ScenarioType.XmlToAdam ? "Xml" : "Adam");
            return Path.Combine(directory, String.Format("{0}.xsd", si.Type));
        }

        private static string GetMetaDataFileLocation(PatcherContext context, ScenarioItem si)
        {
            string directory = Path.Combine(Extensions.ExecutingDirectoryName(), context.Scenario.MetaDataLocation);
            directory = Path.Combine(directory, si.Type);
            return Path.Combine(directory, String.Format("{0}.xml", si.Name));
        }
    }
}
