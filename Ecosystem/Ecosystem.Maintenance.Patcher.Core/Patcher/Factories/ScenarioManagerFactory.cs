﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class ScenarioManagerFactory
    {
        public static ScenarioManager GetScenarioManager(Params prms, Application app)
        {
            ScenarioManager scenarioManager = null;
            string path = String.Empty;
            try
            {
                path = prms.GetValue<string>(Params.ScenarioManager);
                scenarioManager = ScenarioFactory.ScenarioManager(path);

                Adam.Core.Settings.SettingDefinitionHelper hlp = new Adam.Core.Settings.SettingDefinitionHelper(app);
                if (hlp.Exists(scenarioManager.AdamSettingName))
                {
                    object adamSetting = app.GetSetting(scenarioManager.AdamSettingName);
                    scenarioManager.UpdateFromXml(adamSetting);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(
                    String.Format("Reading ScenarioManager file exception: Path: {0}, Exception: {1}", path, ex.ToString())
                    );
            }

            return scenarioManager;
        }
    }
}
