﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adam.Core;
using Epam.Ecosystem.Maintenance.Patcher.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public class PatchContextFactory
    {
        internal static PatcherContext GetContext(Params prms, string scenarioLocation, Application app)
        {
            //Scenario scenario = GetScenario(prms);
            Scenario scenario = GetScenario(scenarioLocation);
            PatchTypes types = GetPatchTypes(scenario.Type);
            return new PatcherContext(prms, scenario, types, app);
        }

        private static PatchTypes GetPatchTypes(ScenarioType type)
        {
            IValueInitializer vi = new ValueInitializer();
            vi.ValueProvider = PatchValueFactory.GetProvider(type);

            FlyweightFactory fwf = new PatchTypes();
            vi.Initialize(ref fwf);

            return (fwf as PatchTypes);
        }

        private static Scenario GetScenario(string scenarioLocation)
        {
            Scenario scenario = null;
            try
            {
                scenario = ScenarioFactory.GetScenario(scenarioLocation);
            }
            catch (Exception ex)
            {
                throw new Exception(
                    String.Format("Reading Scenario file exception: Path: {0}, Exception: {1}", scenarioLocation, ex.ToString())
                    );
            }

            return scenario;
        }

        private static Scenario GetScenario(Params prms)
        {
            Scenario scenario = null;
            string path = String.Empty;
            try
            {
                path = prms.GetValue<string>(Params.ScenarioPath);
                scenario = ScenarioFactory.GetScenario(path);
            }
            catch (Exception ex)
            {
                throw new Exception(
                    String.Format("Reading Scenario file exception: Path: {0}, Exception: {1}", path, ex.ToString())
                    );
            }

            return scenario;
        }

       
    }
}
