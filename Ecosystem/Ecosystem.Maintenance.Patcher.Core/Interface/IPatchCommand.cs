﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public interface IPatchCommand
    {
        void Execute(PatcherContext context);
    }
}
