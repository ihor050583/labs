﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core
{
    public interface IValueInitializer
    {
        IValueProvider ValueProvider { get; set; }
        void Initialize(ref FlyweightFactory fwf);
    }
}
