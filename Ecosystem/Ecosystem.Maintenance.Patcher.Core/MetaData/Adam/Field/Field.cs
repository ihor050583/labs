﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.Field
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Field : MetaDataElement
    {
        [XmlAttribute]
        public string MetaDataLocation { get; set; }
        [XmlAttribute]
        public Scope Scope { get; set; }
        [XmlAttribute]
        public DataType DataType { get; set; }
    }
}
