﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.Workflow
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Workflow : MetaDataElement
    {
        [XmlAttribute]
        public string MetaDataLocation { get; set; }

        [XmlElement]
        public string SearchExpression { get; set; }

        public Workflow()
        {
            SearchExpression = "Name = *";
        }
        
    }
}
