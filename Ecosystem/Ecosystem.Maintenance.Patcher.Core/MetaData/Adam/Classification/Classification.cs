﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Adam.Classification
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Classification : MetaDataElement
    {
        [XmlAttribute]
        public string Path { get; set; }
        [XmlAttribute]
        public bool Fields { get; set; }
        [XmlAttribute]
        public bool FieldGroups { get; set; }
        [XmlAttribute]
        public bool Labels { get; set; }
        [XmlAttribute]
        public bool Permissions { get; set; }
        [XmlAttribute]
        public int Depth { get; set; }
        [XmlAttribute]
        public string MetaDataLocation { get; set; }
    }
}
