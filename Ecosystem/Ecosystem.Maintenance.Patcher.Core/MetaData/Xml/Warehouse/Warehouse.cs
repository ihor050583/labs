﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Warehouse
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Warehouse : MetaDataElement
    {
        [XmlAttribute]
        public string WarehouseDatabase { get; set; }
        [XmlAttribute]
        public string RunAsUser { get; set; }
        [XmlAttribute]
        public string SynchronizationInterval { get; set; }
        [XmlAttribute]
        public bool CreateWarehouseDatabase { get; set; }
        [XmlElement]
        public string CustomScript { get; set; }
        [XmlArray("Tables"), XmlArrayItem("Table")]
        public List<WHTable> Tables { get; set; }
        [XmlElement]
        public string WarehouseDatabaseConnection { get; set; }
    }
}