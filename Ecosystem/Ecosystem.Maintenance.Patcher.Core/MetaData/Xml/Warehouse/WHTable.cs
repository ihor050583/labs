﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Adam.Core.Warehousing;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Warehouse
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class WHTable : MetaDataElement
    {
        [XmlElement]
        public string CustomScript { get; set; }
        [XmlAttribute]
        public WarehouseSourceTypes Source { get; set; }
        [XmlElement]
        public string Filter { get; set; }
        [XmlArray("Columns"), XmlArrayItem("Column")]
        public List<MetaDataElement> Columns { get; set; }
    }
}