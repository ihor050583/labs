﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.FieldGroup
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class FieldGroup : MetaDataElement
    {
        [XmlArray("Members"), XmlArrayItem("FieldDefinition")]
        public List<MetaDataElement> Members { get; set; }
    }
}
