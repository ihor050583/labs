﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Adam.Core.Settings;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.UserGroup
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Permission : MetaDataElement
    {
        [XmlAttribute]
        public Role Status { get; set; }
        [XmlAttribute]
        public string Identifier { get; set; }
    }
}
