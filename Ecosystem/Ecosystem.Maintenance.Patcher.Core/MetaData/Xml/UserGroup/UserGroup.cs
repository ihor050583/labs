﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.UserGroup
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class UserGroup : MetaDataElement
    {
        
        [XmlAttribute]
        public string Organization { get; set; }
        [XmlArray("Permissions"), XmlArrayItem("Permission")]
        public List<Permission> Permissions { get; set; }
        [XmlElement]
        public MetaDataElement UpdateOrganization { get; set; }
    }
}
