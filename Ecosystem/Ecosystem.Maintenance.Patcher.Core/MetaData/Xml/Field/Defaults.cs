﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Defaults : MetaDataElement
    {
        public string DefaultValue { get; set; }
        [XmlArray("ResetToDefaultTriggers"), XmlArrayItem("Trigger")]
        public List<Trigger> ResetToDefaultTriggers { get; set; }
        [XmlArray("ResetToDefaultFields"), XmlArrayItem("Field")]
        public List<string> ResetToDefaultFields { get; set; }
    }
}
