﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Label : MetaDataElement
    {
        [XmlAttribute]
        public Guid LanguageId { get; set; }
        [XmlAttribute]
        public string Value { get; set; }
    }
}
