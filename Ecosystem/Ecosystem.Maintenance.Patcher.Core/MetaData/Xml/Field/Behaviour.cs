﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Behaviour : MetaDataElement
    {
        [XmlAttribute]
        public InheritanceMode InheritanceMode { get; set; }
        [XmlAttribute]
        public LanguageMode LanguageMode { get; set; }
    }
}
