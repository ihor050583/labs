﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Numeric : MetaDataElement
    {
        [XmlAttribute]
        public decimal Accuracy { get; set; }
        [XmlAttribute]
        public string Range { get; set; }
    }
}
