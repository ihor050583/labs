﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    public enum ListAction
    {
        None = 0,
        Clear = 1,
        AddOrUpdate,
        Remove,
    }
}
