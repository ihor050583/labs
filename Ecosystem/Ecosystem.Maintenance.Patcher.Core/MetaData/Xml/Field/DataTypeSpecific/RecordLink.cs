﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using AFields = Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class RecordLink : MetaDataElement
    {
        [XmlAttribute]
        public AFields.RecordLinkType LinkType { get; set; }
        [XmlAttribute]
        public string ParentSearchExpression { get; set; }
        [XmlAttribute]
        public string ChildSearchExpression { get; set; }
        [XmlAttribute]
        public bool SummaryImage { get; set; }
        [XmlAttribute]
        public string SummaryField { get; set; }

        [XmlArray("ParentClassifications"), XmlArrayItem("Item")]
        public List<string> ParentClassifications { get; set; }
        [XmlArray("ChildClassifications"), XmlArrayItem("Item")]
        public List<string> ChildClassifications { get; set; }

        public RecordLink()
        {
            LinkType = AFields.RecordLinkType.OneParentManyChildren;
        }
    }
}
