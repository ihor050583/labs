﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using AFields = Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class OptionList : MetaDataElement
    {
        [XmlAttribute]
        public bool AcceptMultipleOptions { get; set; }
        [XmlAttribute]

        //[System.ComponentModel.DefaultValueAttribute(AFields.OptionListFieldSortOrder.Label)]
        public AFields.OptionListFieldSortOrder SortOrder { get; set; }

        [XmlElement("OptionListItemDefinition")]
        public List<OptionListItemDefinition> Items { get; set; }

        public OptionList()
        {
            SortOrder = AFields.OptionListFieldSortOrder.Label;
        }
    }

    [Serializable]
    [XmlRoot(IsNullable = true)]
    public class OptionListItemDefinition
    {
        [XmlAttribute]
        public string Name { get; set; }

        [XmlAttribute]
        public string NewName { get; set; }

        //[XmlAttribute]
        //public Guid Id { get; set; }

        //[XmlAttribute]
        //public ListAction ListAction { get; set; }

        [XmlAttribute]
        public string ListAction { get; set; }

        [XmlElement("LabelItem")]
        public List<LabelItem> Labels { get; set; }

        [XmlAttribute]
        public string Tag { get; set; }
    }

    [Serializable]
    [XmlRoot(IsNullable = true)]
    public class LabelItem
    {
        [XmlAttribute]
        public Guid LanguageId { get; set; }

        [XmlAttribute]
        public string Value { get; set; }
    }
}
