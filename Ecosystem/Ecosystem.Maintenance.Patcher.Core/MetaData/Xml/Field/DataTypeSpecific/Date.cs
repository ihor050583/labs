﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Date : MetaDataElement
    {
        [XmlAttribute]
        public string DatePattern { get; set; }
        [XmlAttribute]
        public string YearMonthPattern { get; set; }
    }
}
