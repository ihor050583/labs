﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class RecordLinkType : MetaDataElement
    {
        public const string OneParentOneChild = "OneParentOneChild";
        public const string OneParentManyChildren = "OneParentManyChildren";
        public const string OneParentOrderedChildren = "OneParentOrderedChildren";
        public const string ManyParentsOneChild = "ManyParentsOneChild";
        public const string ManyParentsManyChildren = "ManyParentsManyChildren";
        public const string ManyParentsOrderedChildren = "ManyParentsOrderedChildren";
        public const string NoParentChildRelationship = "NoParentChildRelationship";
    }
}
