﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class OptionListFieldSortOrder : MetaDataElement
    {
        //public const string Name = "Name";
        //public const string SortIndex = "SortIndex";
        //public const string Label = "Label";
    }
}
