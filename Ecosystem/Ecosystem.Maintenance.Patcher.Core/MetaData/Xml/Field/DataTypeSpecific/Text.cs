﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using AFields = Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Text : MetaDataElement
    {        
        [XmlAttribute]
        public AFields.TextFieldReferences References { get; set; }
        [XmlAttribute]
        public string RegularExpression { get; set; }
        [XmlAttribute]
        public int MinLength { get; set; }
        [XmlAttribute]
        public int MaxLength { get; set; }
        [XmlAttribute]
        public AFields.TextFieldContentType ContentType { get; set; }
        [XmlAttribute]
        public bool MultiLine { get; set; }

        public Text()
        {
            ContentType = AFields.TextFieldContentType.Text;
            MultiLine = false;
            MaxLength = 3800;
            MinLength = 0;
        }
    }
}
