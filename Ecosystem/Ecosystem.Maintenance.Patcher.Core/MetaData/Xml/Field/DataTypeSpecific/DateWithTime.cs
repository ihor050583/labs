﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class DateWithTime : MetaDataElement
    {
        [XmlAttribute]
        public string DatePattern { get; set; }
        [XmlAttribute]
        public string DateTimePattern { get; set; }
        [XmlAttribute]
        public bool UseUtc { get; set; }
    }
}
