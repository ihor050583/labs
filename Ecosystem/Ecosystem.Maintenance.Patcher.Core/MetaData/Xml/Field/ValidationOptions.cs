﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    public class Validation
    {
        [XmlAttribute]
        public ValidationTrigger ValidationTrigger { get; set; }

        [XmlElement]
        public string Code { get; set; }

        [XmlAttribute]
        public string ErrorMessage { get; set; }
    }
}
