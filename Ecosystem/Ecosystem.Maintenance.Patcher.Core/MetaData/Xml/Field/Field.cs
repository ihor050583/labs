﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Adam.Core.Fields;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Field
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Field : MetaDataElement
    {
        [XmlAttribute]
        public Scope Scope { get; set; }
        [XmlAttribute]
        public DataType DataType { get; set; }
        [XmlAttribute]
        public bool IsRequired { get; set; }
        [XmlAttribute]
        public bool IsIndexed { get; set; }
        [XmlAttribute]
        public int SortIndex { get; set; }
        [XmlAttribute]
        public bool AllowOperatorSearches { get; set; }
        [XmlElement]
        public Defaults Defaults { get; set; }
        [XmlArray("Membership"), XmlArrayItem("FieldGroup")]
        public List<MetaDataElement> Membership { get; set; }
        [XmlElement]
        public string DataTypeSpecific { get; set; }
        [XmlElement]
        public Behaviour Behaviour { get; set; }
        [XmlElement]
        public Advanced Advanced { get; set; }
        [XmlArray("Labels"), XmlArrayItem("Label")]
        public List<Label> Labels { get; set; } 
        
        [XmlElement]
        public Validation Validation { get; set; }
    }
}
