﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml
{
    public enum PatchAction
    {
        None = 0,
        AddOrUpdate,
        Delete
    }
}
