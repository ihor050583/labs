﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.SettingValue
{
    [Serializable]
    public class ScopeValue
    {
        [XmlElement("Item")]
        public List<MetaDataElement> Items { get; set; }
    }

    [Serializable]
    public class ScopeSystem : ScopeValue { }

    [Serializable]
    public class ScopeSite : ScopeValue { }

    [Serializable]
    public class ScopeUser : ScopeValue { }

    [Serializable]
    public class ScopeUserGroup : ScopeValue { }
}
