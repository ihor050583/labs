﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Adam.Core.Settings;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.SettingValue
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class SettingValue2 : MetaDataElement
    {
        [XmlAttribute]
        public SettingScope Scope { get; set; }
        [XmlElement]
        public string ValueData { get; set; }
    }

    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class SettingValue : MetaDataElement
    {
        [XmlElement("ScopeSystem")]
        public ScopeSystem ScopeSystem { get; set; }

        [XmlElement("ScopeSite")]
        public ScopeSite ScopeSite { get; set; }

        [XmlElement("ScopeUser")]
        public ScopeUser ScopeUser { get; set; }

        [XmlElement("ScopeUserGroup")]
        public ScopeUserGroup ScopeUserGroup { get; set; }
    }
}