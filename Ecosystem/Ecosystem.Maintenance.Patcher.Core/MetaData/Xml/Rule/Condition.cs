﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Rule
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Condition : MetaDataElement
    {
        [XmlAttribute("Type")]
        public ConditionType Type { get; set; }
    }
}
