﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Adam.Core.Rules;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Rule
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class RuleActions : MetaDataElement
    {
        [XmlAttribute("Type")]
        public RuleActionType Type { get; set; }

        [XmlAttribute("Trigger")]
        public RuleExecutionTrigger Trigger { get; set; }

        [XmlElement("Reference")]
        public string Reference { get; set; }
    }
}
