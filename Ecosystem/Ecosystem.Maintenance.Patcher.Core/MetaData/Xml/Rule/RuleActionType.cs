﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Rule
{
    public enum RuleActionType
    {
        Reference = 0,
        Custom,
        RefreshFiles
    }
}
