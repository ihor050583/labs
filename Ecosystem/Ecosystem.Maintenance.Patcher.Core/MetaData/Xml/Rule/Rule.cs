﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Adam.Core.Rules;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Rule
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Rule : MetaDataElement
    {
        [XmlAttribute("Enabled")]
        public bool Enabled { get; set; }

        [XmlAttribute("Target")]
        public RuleTarget Target { get; set; }

        [XmlArray("Conditions"), XmlArrayItem("Condition")]
        public List<Condition> Conditions { get; set; }

        [XmlArray("Actions"), XmlArrayItem("Action")]
        public List<RuleActions> Actions { get; set; }
    }
}
