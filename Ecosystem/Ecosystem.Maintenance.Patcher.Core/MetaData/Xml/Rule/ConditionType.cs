﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Rule
{
    public enum ConditionType
    {
        Created = 0,
        Changed,
        CreatedChanged,
        Deleted,
        LoggedUser,
        ReferenceMatch,
        MovieAddWithoutMoviePreview
    }
}
