﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Indexer
{
    public class Job
    {
        [XmlAttribute]
        public int Priority { get; set; }
        [XmlAttribute]
        public int Threshold { get; set; }
        [XmlAttribute]
        public string ThresholdTimeout { get; set; }
        [XmlAttribute]
        public byte MaximumNumberOfRetries { get; set; }
        [XmlAttribute]
        public string MinimumRetryWaitTime { get; set; }
    }
}
