﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Indexer
{
    public class Notification
    {
        [XmlElement]
        public string Agents { get; set; }
        [XmlElement]
        public string EmailSettings { get; set; }
    }
}
