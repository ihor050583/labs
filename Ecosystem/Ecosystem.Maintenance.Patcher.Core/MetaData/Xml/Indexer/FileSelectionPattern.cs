﻿using Adam.Core.Indexer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Indexer
{
    public class FileSelectionPattern
    {
        [XmlAttribute]
        public FileSelectionPatternMode Mode { get; set; }
        [XmlAttribute]
        public string Expression { get; set; }
    }
}
