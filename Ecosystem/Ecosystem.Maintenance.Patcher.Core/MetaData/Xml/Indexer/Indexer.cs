﻿using Adam.Core.Records;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Indexer
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Indexer : MetaDataElement
    {
        [XmlAttribute]
        public bool Enabled { get; set; }
        [XmlAttribute]
        public string Path { get; set; }
        [XmlAttribute]
        public string ScanEngine { get; set; }
        [XmlAttribute]
        public CatalogMode CatalogMode { get; set; }
        [XmlAttribute]
        public string ProcessEngine { get; set; }
        [XmlElement("Classification")]
        public ClassificationInfo Classification { get; set; }
        [XmlElement("FilesAndDirectories")]
        public FilesAndDirectories FilesAndDirectories { get; set; }
        [XmlElement("Logging")]
        public Logging Logging { get; set; }
        [XmlElement("Job")]
        public Job Job { get; set; }
        [XmlElement("Actions")]
        public Actions Actions { get; set; }
        [XmlElement("Imports")]
        public Imports Imports { get; set; }
        [XmlElement("Notification")]
        public Notification Notification { get; set; }
        [XmlElement("CustomEngine")]
        public CustomEngine CustomEngine { get; set; }
    }
}
