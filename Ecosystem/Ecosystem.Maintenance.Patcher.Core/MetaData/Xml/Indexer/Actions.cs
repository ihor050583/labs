﻿using Adam.Core.Fields;
using Adam.Core.Indexer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Indexer
{
    public class Actions
    {
        [XmlAttribute]
        public ActionOnSuccess OnSuccessAction { get; set; }
        [XmlAttribute]
        public FieldValidation FieldValidation { get; set; }
        [XmlAttribute]
        public DuplicateAction DuplicateAction { get; set; }
    }
}
