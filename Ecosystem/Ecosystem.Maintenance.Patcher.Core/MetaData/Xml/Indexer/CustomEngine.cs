﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Indexer
{
    public class CustomEngine
    {
        [XmlAttribute]
        public string AssemblyName { get; set; }
        [XmlAttribute]
        public string TypeFullName { get; set; }
        [XmlAttribute]
        public string UniqueName { get; set; }
    }
}
