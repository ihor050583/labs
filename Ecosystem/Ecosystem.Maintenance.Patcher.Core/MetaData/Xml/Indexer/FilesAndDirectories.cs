﻿using Adam.Core.Records;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Indexer
{
    public class FilesAndDirectories
    {
        [XmlAttribute]
        public bool CatalogHiddenFiles { get; set; }
        [XmlAttribute]
        public bool CatalogHiddenDirectories { get; set; }
        [XmlAttribute]
        public string FailedFilesFolder { get; set; }
        [XmlAttribute]
        public int DeleteFoldersFromDepth { get; set; }
        [XmlAttribute]
        public FileExtensionRestriction FileExtensionRestriction { get; set; }
        [XmlAttribute]
        public FileTypeRestriction FileTypeRestriction { get; set; }
        [XmlElement]
        public FileSelectionPatterns FileSelectionPatterns { get; set; }
    }
}
