﻿using Adam.Core.Indexer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Indexer
{
    public class Imports
    {
        [XmlAttribute]
        public IndexerImportRestriction ImportRestriction { get; set; }
        [XmlAttribute]
        public byte MaximumRetriesWhenImportsAreMissing { get; set; }
        [XmlAttribute]
        public bool AllowMissingImports { get; set; }
    }
}
