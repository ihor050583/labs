﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Indexer
{
    public class FileSelectionPatterns
    {
        [XmlElement("Pattern")]
        public List<FileSelectionPattern> Patterns { get; set; }
    }
}
