﻿using Adam.Tools.LogHandler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Indexer
{
    public class Logging
    {
        [XmlAttribute]
        public LogSeverity LogSeverity { get; set; }
    }
}
