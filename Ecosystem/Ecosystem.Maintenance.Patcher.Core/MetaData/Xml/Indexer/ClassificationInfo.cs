﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Indexer
{
    public class ClassificationInfo
    {
        [XmlAttribute]
        public string ClassifyIn { get; set; }
        [XmlAttribute]
        public bool ClassifySubfolders { get; set; }
        [XmlAttribute]
        public string Root { get; set; }
        [XmlAttribute]
        public string IgnorePath { get; set; }
        [XmlAttribute]
        public int MaximumDepth { get; set; }
    }
}
