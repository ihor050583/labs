﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.FileType
{
    public class FileInformation
    {
        [XmlAttribute]
        public string Extension { get; set; }
        [XmlAttribute]
        public string MacType { get; set; }
        [XmlAttribute]
        public string MacCreator { get; set; }
        [XmlAttribute]
        public string MimeType { get; set; }
    }
}
