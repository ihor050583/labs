﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Adam.Core;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.FileType
{
    public class Processing
    {
        [XmlAttribute]
        public string EngineFormat { get; set; }
        [XmlAttribute]
        public ImageFormat PreviewFormat { get; set; }
        [XmlAttribute]
        public bool IsCatalogable { get; set; }
        [XmlAttribute]
        public bool PreviewRequired { get; set; }
        [XmlAttribute]
        public bool KeepDocumentDimensions { get; set; }
        [XmlAttribute]
        public bool AllowResizeSource { get; set; }
        [XmlAttribute]
        public bool SupportsWatermarking { get; set; }
        [XmlAttribute]
        public bool PreferredExtension { get; set; }
        [XmlAttribute]
        public bool PreferredMacType { get; set; }
    }
}
