﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.FileType
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class FileType : MetaDataElement
    {
        [XmlAttribute]
        public string Kind { get; set; }

        [XmlElement]
        public FileInformation FileInformation { get; set; }

        [XmlElement]
        public Processing Processing { get; set; }

        [XmlArray("CatalogActions"), XmlArrayItem("CatalogAction")]
        public List<MetaDataElement> CatalogActions { get; set; }

        [XmlArray("MediaEngines"), XmlArrayItem("MediaEngine")]
        public List<MetaDataElement> MediaEngines { get; set; }

        [XmlArray("PreviewTypes"), XmlArrayItem("PreviewType")]
        public List<MetaDataElement> PreviewTypes { get; set; }

        [XmlArray("Labels"), XmlArrayItem("Label")]
        public List<MetaDataElement> Labels { get; set; }

        [XmlArray("RegisteredFields"), XmlArrayItem("Field")]
        public List<MetaDataElement> RegisteredFields { get; set; }

        [XmlArray("RegisteredFieldGroups"), XmlArrayItem("FieldGroup")]
        public List<MetaDataElement> RegisteredFieldGroups { get; set; }
    }
}
