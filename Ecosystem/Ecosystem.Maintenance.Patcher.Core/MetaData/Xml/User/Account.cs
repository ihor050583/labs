﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.User
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Account : MetaDataElement
    {
        [XmlAttribute]
        public DateTime ExpirationDate { get; set; }

        [XmlAttribute]
        public bool LockedOut { get; set; }

        [XmlAttribute]
        public long StorageQuota { get; set; }
    }
}
