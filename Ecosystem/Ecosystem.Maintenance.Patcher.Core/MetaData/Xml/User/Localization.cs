﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.User
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Localization : MetaDataElement
    {
        [XmlAttribute]
        public string Language { get; set; }

        [XmlAttribute]
        public string LanguageForUI { get; set; }
    }
}
