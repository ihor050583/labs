﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.User
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class User : MetaDataElement
    {
        [XmlAttribute]
        public string Password { get; set; }

        [XmlAttribute]
        public string Email { get; set; }

        [XmlElement]
        public Localization Localization { get; set; }

        [XmlElement]
        public Account Account { get; set; }

        [XmlArray("Membership"), XmlArrayItem("UserGroup")]
        public List<MetaDataElement> Membership { get; set; }

        [XmlAttribute]
        public  string UpdatePassword { get; set; }

    }
}
