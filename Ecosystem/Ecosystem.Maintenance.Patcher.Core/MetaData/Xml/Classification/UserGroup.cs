﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Adam.Core.Classifications;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Classification
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class UserGroup : MetaDataElement
    {
        [XmlAttribute]
        public ClassificationRight ClassificationsType { get; set; }
        [XmlAttribute]
        public ClassificationRight RecordType { get; set; } 
    }
}
