﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Classification
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class BreakSecurityInheritance : MetaDataElement
    {
        [XmlAttribute]
        public bool ClassificationTreeSecurity { get; set; }
        [XmlAttribute]
        public bool RecordSecurity { get; set; }
    }
}
