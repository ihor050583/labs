﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Classification
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class RegisteredFields : MetaDataElement
    {
        [XmlArray("ClassificationScopeFields"), XmlArrayItem("ClassificationField")]
        public List<MetaDataElement> ClassificationScopeFields { get; set; }

        [XmlArray("NotClassificationScopeFields"), XmlArrayItem("NotClassificationField")]
        public List<MetaDataElement> NotClassificationScopeFields { get; set; }
    }
}
