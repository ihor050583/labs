﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Classification
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Classification : MetaDataElement
    {
        public Classification()
        {
            Parent = String.Empty;
            SortIndex = 1;
        }

        [XmlAttribute]
        public string Parent { get; set; }

        [XmlElement]
        public MetaDataElement UpdateParent { get; set; }

        [XmlArray("Labels"), XmlArrayItem("Language")]
        public List<MetaDataElement> Labels { get; set; }

        [XmlElement]
        public RegisteredFields RegisteredFields { get; set; }

        [XmlArray("RegisteredFieldGroups"), XmlArrayItem("FieldGroup")]
        public List<MetaDataElement> RegisteredFieldGroups { get; set; }

        [XmlArray("RegisteredMappings"), XmlArrayItem("Slave")]
        public List<MetaDataElement> RegisteredMappings { get; set; }

        [XmlElement]
        public Permissions Permissions { get; set; }

        [XmlElement("Classification")]
        public List<Classification> Classifications { get; set; }

        [XmlAttribute]
        public string Identifier { get; set; }

        [XmlAttribute]
        public int SortIndex { get; set; }

        [XmlAttribute]
        public string SortOrderForChildren { get; set; }

        [XmlAttribute]
        public bool RemoveExistingFieldGroups { get; set; }
    }
}