﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace AdamDemo.Maintenance.Patcher.Core.MetaData.Xml.Classification
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Label : MetaDataElement
    {
        [XmlAttribute]
        public string Language { get; set; }
    }
}
