﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Classification
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Permissions : MetaDataElement
    {
        [XmlElement]
        public  BreakSecurityInheritance BreakSecurityInheritance { get; set; }

        [XmlElement("UserGroup")]
        public List<UserGroup> UserGroups { get; set; }
    }
}
