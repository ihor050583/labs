﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.SettingCategory
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class SettingCategory : MetaDataElement
    {
        [XmlArray("Labels"), XmlArrayItem("Label")]
        public List<MetaDataElement> Labels { get; set; }
    }
}
