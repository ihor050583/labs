﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.SettingDefinition
{
    public class AccessControl
    {
        [XmlAttribute]
        public bool AllowAnonymousAccess { get; set; }
        [XmlAttribute]
        public bool AllowSiteSetting { get; set; }
        [XmlAttribute]
        public bool AllowUserSetting { get; set; }
        [XmlAttribute]
        public bool AllowSystemSetting { get; set; }
    }
}
