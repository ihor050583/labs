﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Adam.Core.Fields;
using Adam.Core.Settings;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.SettingDefinition
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class SettingDefinition : MetaDataElement
    {
        [XmlAttribute]
        public SettingDataType DataType { get; set; }
        [XmlAttribute]
        public string Category { get; set; }
        [XmlElement]
        public string DefaultValue { get; set; }
        [XmlElement]
        public string Schema { get; set; }
        [XmlElement]
        public AccessControl AccessControl { get; set; }
        
    }
}
