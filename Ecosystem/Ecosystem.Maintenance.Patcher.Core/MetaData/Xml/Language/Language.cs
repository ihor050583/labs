﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Language
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Language : MetaDataElement
    {
        [XmlAttribute]
        public string Culture { get; set; }
        [XmlAttribute]
        public bool EnabledForFields { get; set; }
        [XmlElement]
        public string ExcludedKeywords { get; set; }
    }
}
