﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

          
namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Workflow
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Workflow : MetaDataElement
    {
        [XmlElement]
        public string Description { get; set; }
        [XmlAttribute]
        public string ReleaseDate { get; set; }
        [XmlAttribute]
        public string WorkflowApplication { get; set; }
        [XmlElement]
        public string Template { get; set; }
    }
}
