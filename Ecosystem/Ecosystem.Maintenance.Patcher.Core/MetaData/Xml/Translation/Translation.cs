﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml.Translation
{
    [Serializable]
    [XmlInclude(typeof(MetaDataElement))]
    public class Translation : MetaDataElement
    {
        [XmlAttribute]
        public string Studio { get; set; }
        [XmlAttribute]
        public string Module { get; set; }
        [XmlElement]
        public MetaDataElement UpdateStudio { get; set; }
        [XmlElement]
        public MetaDataElement UpdateModule { get; set; }
        [XmlArray("TranslationLanguages"), XmlArrayItem("Language")]
        public List<TranslationLanguage> TranslationLanguages { get; set; }
    }
}
