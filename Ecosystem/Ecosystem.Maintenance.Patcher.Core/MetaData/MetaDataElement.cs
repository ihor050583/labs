﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Epam.Ecosystem.Maintenance.Patcher.Core.MetaData.Xml;

namespace Epam.Ecosystem.Maintenance.Patcher.Core.MetaData
{
    [Serializable]
    public class MetaDataElement
    {
        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string Value { get; set; }
        [XmlAttribute]
        public PatchAction Action { get; set; }
        [XmlElement]
        public MetaDataElement UpdateName { get; set; }
        [XmlElement]
        public string Data { get; set; }

        [XmlElement]
        public string Tag { get; set; }

        public MetaDataElement()
        {
            this.Tag = null;
            this.Data = null;
            this.UpdateName = null;
        }
    }
}
