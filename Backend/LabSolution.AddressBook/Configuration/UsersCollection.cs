﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace LabSolution.AddressBook.Configuration
{
    [ConfigurationCollection(typeof(UserElement))]
    public class UsersCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new UserElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((UserElement)(element)).Name;
        }

        public UserElement this[int idx]
        {
            get { return (UserElement)BaseGet(idx); }
        }

        public void Add(string name, string password,string email, string roleId )
        {
            base.BaseAdd(new UserElement() { Name = name, Password = password, RoleId = roleId, Email = email });


        }
    }
}