﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace LabSolution.AddressBook.Configuration
{
    public class AddressBookConfigSection : ConfigurationSection
    {
        [ConfigurationProperty("Users")]
        public UsersCollection UserItems
        {
            get { return ((UsersCollection)(base["Users"])); }
        }

        [ConfigurationProperty("Repositories")]
        public RepositoriesCollection RepositoryItems
        {
            get { return ((RepositoriesCollection)(base["Repositories"])); }
        }

    }
}