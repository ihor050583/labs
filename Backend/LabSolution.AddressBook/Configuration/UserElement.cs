﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace LabSolution.AddressBook.Configuration
{
    public class UserElement : ConfigurationElement
    {

        [ConfigurationProperty("name", DefaultValue = "", IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return ((string)(base["name"])); }
            set { base["name"] = value; }
        }

        [ConfigurationProperty("password", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string Password
        {
            get { return ((string)(base["password"])); }
            set { base["password"] = value; }
        }

        [ConfigurationProperty("email", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string Email
        {
            get { return ((string)(base["email"])); }
            set { base["email"] = value; }
        }

        [ConfigurationProperty("roleId", DefaultValue = "", IsKey = false, IsRequired = false)]
        public string RoleId
        {
            get { return ((string)(base["roleId"])); }
            set { base["roleId"] = value; }
        }
    }
}