﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using LabSolution.AddressBook.Configuration;
using System.Xml.Linq;
using LabSolution.AddressBook.Concrete;

namespace LabSolution.AddressBook.Infrastructure
{
    public class RepositoryManager
    {
        private System.Configuration.Configuration cfg;
        private AddressBookConfigSection section;

        public RepositoryManager()
        {

            cfg = ConfigurationManager. OpenExeConfiguration(ConfigurationUserLevel.None);          
             section = (AddressBookConfigSection)cfg.Sections["AddressBook"];
            
        }

        public List<Repository> GetRepositories()
        {      
        
            List<Repository> repositories = new List<Repository>();
            
            foreach (RepositoryElement item in section.RepositoryItems)
            {
                repositories.Add(new Repository() { Name = item.Name, Type = item.Type, Path = item.Path });
            }
            return repositories;
        
        }

        public void AddRepository(Repository model,string filePath)
        {
            DataManager manager = new DataManager();
            section.RepositoryItems.Add(model.Name, model.Type, model.Path);

            if (model.Type == "binary")
            {
                manager.Serialize(filePath);
            }

            else if (model.Type == "xml")
            {
                XNamespace ab = "http://www.example.org/addressBook";
                XDocument doc = new XDocument(
                new XDeclaration("1.0", "UTF-8", "yes"),
                new XElement(ab + "addressBook",
                    new XAttribute(XNamespace.Xmlns + "ab", "http://www.example.org/addressBook"),
                    new XElement("Records", ""),
                    new XElement("Tagi", "")
                    )
                );
                doc.Save(filePath);
            }

        }
    }
}
