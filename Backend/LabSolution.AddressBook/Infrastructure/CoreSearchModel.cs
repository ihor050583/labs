﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace LabSolution.AddressBook.Infrastructure
{
    public class CoreSearchModel
    {        
        public string FirstName { get; set; } 
        public string SecondName { get; set; }   
        public DateTime LastChangeTime { get; set; }
        public string Sign { get; set; }
    }
}