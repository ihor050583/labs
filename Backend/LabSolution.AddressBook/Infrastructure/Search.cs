﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LabSolution.AddressBook.LinqProvider;
using LabSolution.AddressBook.Entities;

using System.Xml;
using LabSolution.AddressBook.Concrete;

namespace LabSolution.AddressBook.Infrastructure
{
    public static class Search
    {
        public static List<Record> SimpleSearch(string name, XmlDocument doc, DataManager manager)
        {
            XmlRepositoryForLinq rep;
            AddressBookData<Record> LinqProvider = null;
            if (doc != null)
            {
                rep = new XmlRepositoryForLinq(doc);
                LinqProvider = new AddressBookData<Record>(rep);
            }
            List<Record> rec = null;
            if (!String.IsNullOrEmpty(name))
            {
                Tag tag = manager.Tags.FirstOrDefault(o => String.Compare(o.Name, name, true) == 0);
                Guid id;
                if (tag != null)
                    id = tag.TagId;
                else id = Guid.NewGuid();

                if (doc == null)
                    rec = GetRecordsWithPhonesAndNotes(manager).Where(o =>
                                String.Compare(o.FirstName, name,true) == 0 ||
                                String.Compare(o.SecondName,name,true) == 0 ||
                                o.PhoneNumbers.Where(t => t.Phone == name).Count() != 0 ||
                                o.PhoneNumbers.Where(t =>String.Compare(t.Name,name,true)==0).Count() != 0 ||
                                o.Notes.Where(n => n.Description.IndexOf(name,StringComparison.OrdinalIgnoreCase)>=0).Count() != 0 ||
                                o.Notes.Where(n => n.TagsId.Contains(id)).Count() != 0).ToList();
                else
                {
                    List<Record> firstName = LinqProvider.Where(o => o.FirstName == name).ToList();
                    List<Record> secondName = LinqProvider.Where(o => o.SecondName == name).ToList();
                    List<Record> phones = LinqProvider.Where(o => o.PhoneNumbers.First().Phone == name).ToList();
                    List<Record> phonesName = LinqProvider.Where(o => o.PhoneNumbers.First().Name == name).ToList();
                    List<Record> notes = LinqProvider.Where(o => o.Notes.First().Description.Contains(name)).ToList();
                    List<Record> tags = LinqProvider.Where(o => o.Notes.First().TagsId.Contains(id)).ToList();

                    rec = firstName.Union(secondName).Union(phones).Union(notes).Union(tags).Union(phonesName).ToList();

                }


            }

            else rec = manager.Records;

            return rec;

        }



        public static List<Record> AdvSearch(CoreSearchModel model, XmlDocument doc, DataManager manager)
        {
            XmlRepositoryForLinq rep;
            AddressBookData<Record> LinqProvider = null;
            if (doc != null)
            {
                rep = new XmlRepositoryForLinq(doc);
                LinqProvider = new AddressBookData<Record>(rep);
            }
            List<Record> rec = null;

            //если непустое поле FirstName
            if (!String.IsNullOrEmpty(model.FirstName) && String.IsNullOrEmpty(model.SecondName)           
             && model.LastChangeTime == DateTime.Parse("01.01.0001 0:00:00"))
            {
                if (doc != null)//если xml репозиторий
                    rec = LinqProvider.Where(o => o.FirstName == model.FirstName).ToList();
                else rec = manager.Records.Where(o => String.Compare(o.FirstName , model.FirstName,true) ==0).ToList();
            }
            else //если непустые поля FirstName и SecondName
                if (!String.IsNullOrEmpty(model.FirstName) && !String.IsNullOrEmpty(model.SecondName)
                    && model.LastChangeTime == DateTime.Parse("01.01.0001 0:00:00"))
                {
                    if (doc != null)//если xml репозиторий
                        rec = LinqProvider.Where(o => o.FirstName == model.FirstName && o.SecondName == model.SecondName).ToList();
                    else rec = manager.Records.Where(o => String.Compare(o.FirstName, model.FirstName, true) == 0 && String.Compare(o.SecondName, model.SecondName,true)==0).ToList();
                }
                else //если непустое поле SecondName
                    if (String.IsNullOrEmpty(model.FirstName) && !String.IsNullOrEmpty(model.SecondName)
                       && model.LastChangeTime == DateTime.Parse("01.01.0001 0:00:00"))
                    {
                        if (doc != null)//если xml репозиторий
                            rec = LinqProvider.Where(o => o.SecondName == model.SecondName).ToList();
                        else rec = manager.Records.Where(o => String.Compare(o.SecondName, model.SecondName, true) == 0).ToList();
                    }
                    else //если непустые поля FirstName, SecondName, и LastChangeTime
                        if (!String.IsNullOrEmpty(model.FirstName) && !String.IsNullOrEmpty(model.SecondName)
                            && model.LastChangeTime != DateTime.Parse("01.01.0001 0:00:00"))
                        {
                            if (doc != null)//если xml репозиторий
                            {
                                if (model.Sign == ">")
                                    rec = LinqProvider.Where(o => o.FirstName == model.FirstName &&
                                 o.SecondName == model.SecondName && o.LastChangeTime > model.LastChangeTime).ToList();
                                else if (model.Sign == "<")
                                    rec = LinqProvider.Where(o => o.FirstName == model.FirstName &&
                                 o.SecondName == model.SecondName && o.LastChangeTime < model.LastChangeTime).ToList();
                                else
                                    rec = LinqProvider.Where(o => o.FirstName == model.FirstName &&
                                 o.SecondName == model.SecondName && o.LastChangeTime == model.LastChangeTime).ToList();
                            }
                            else
                            {
                                if (model.Sign == "<")
                                    rec = manager.Records.Where(o => String.Compare(o.FirstName, model.FirstName, true) == 0 &&
                                String.Compare(o.SecondName, model.SecondName, true) == 0 && o.LastChangeTime.Date < model.LastChangeTime.Date).ToList();
                                else if (model.Sign == ">")
                                    rec = manager.Records.Where(o => String.Compare(o.FirstName, model.FirstName, true) == 0 &&
                                 String.Compare(o.SecondName, model.SecondName, true) == 0 && o.LastChangeTime.Date > model.LastChangeTime.Date).ToList();
                                else
                                    rec = manager.Records.Where(o => String.Compare(o.FirstName, model.FirstName, true) == 0 &&
                                String.Compare(o.SecondName, model.SecondName, true) == 0 && o.LastChangeTime.Date == model.LastChangeTime.Date).ToList();
                            }

                        }
                        else //если непустое поле LastChangeTime
                            if (String.IsNullOrEmpty(model.FirstName) && String.IsNullOrEmpty(model.SecondName)
                                &&  model.LastChangeTime != DateTime.Parse("01.01.0001 0:00:00"))
                            {
                                if (doc != null)//если xml репозиторий
                                {
                                    if (model.Sign == ">")
                                        rec = LinqProvider.Where(o => o.LastChangeTime > model.LastChangeTime).ToList();
                                    else if (model.Sign == "<")
                                        rec = LinqProvider.Where(o => o.LastChangeTime < model.LastChangeTime).ToList();
                                    else rec = LinqProvider.Where(o => o.LastChangeTime == model.LastChangeTime).ToList();
                                }
                                else
                                {
                                    if (model.Sign == "<")
                                        rec = manager.Records.Where(o => o.LastChangeTime.Date < model.LastChangeTime.Date).ToList();
                                    else if (model.Sign == ">")
                                        rec = manager.Records.Where(o => o.LastChangeTime.Date > model.LastChangeTime.Date).ToList();
                                    else rec = manager.Records.Where(o => o.LastChangeTime.Date == model.LastChangeTime.Date).ToList();
                                }

                            }
                            else //если непустые поля FirstName и LastChangeTime
                                if (!String.IsNullOrEmpty(model.FirstName) && String.IsNullOrEmpty(model.SecondName)
                                    && model.LastChangeTime != DateTime.Parse("01.01.0001 0:00:00"))
                                {
                                    if (doc != null)//если xml репозиторий
                                    {
                                        if (model.Sign == ">")
                                            rec = LinqProvider.Where(o => o.FirstName == model.FirstName &&
                                  o.LastChangeTime > model.LastChangeTime).ToList();
                                        else if (model.Sign == "<")
                                            rec = LinqProvider.Where(o => o.FirstName == model.FirstName &&
                                                o.LastChangeTime < model.LastChangeTime).ToList();
                                        else rec = LinqProvider.Where(o => o.FirstName == model.FirstName &&
                                            o.LastChangeTime == model.LastChangeTime).ToList();
                                    }
                                    else
                                    {
                                        if (model.Sign == "<")
                                            rec = manager.Records.Where(o => String.Compare(o.FirstName, model.FirstName, true) == 0 &&
                                                o.LastChangeTime.Date < model.LastChangeTime.Date).ToList();
                                        else if (model.Sign == ">")
                                            rec = manager.Records.Where(o => String.Compare(o.FirstName, model.FirstName, true) == 0 &&
                                                o.LastChangeTime.Date > model.LastChangeTime.Date).ToList();
                                        else rec = manager.Records.Where(o => String.Compare(o.FirstName, model.FirstName, true) == 0 &&
                                            o.LastChangeTime.Date == model.LastChangeTime.Date).ToList();
                                    }

                                }
                                else //если непустые поля SecondName, LastChangeTime
                                    if (String.IsNullOrEmpty(model.FirstName) && !String.IsNullOrEmpty(model.SecondName)
                                        && model.LastChangeTime != DateTime.Parse("01.01.0001 0:00:00"))
                                    {
                                        if (doc != null)//если xml репозиторий
                                        {
                                            if (model.Sign == "<")
                                                rec = LinqProvider.Where(o => o.SecondName == model.SecondName &&
                                                    o.LastChangeTime > model.LastChangeTime).ToList();
                                            else if (model.Sign == ">")
                                                rec = LinqProvider.Where(o => o.SecondName == model.SecondName &&
                                                    o.LastChangeTime < model.LastChangeTime).ToList();
                                            else rec = LinqProvider.Where(o => o.SecondName == model.SecondName &&
                                                o.LastChangeTime == model.LastChangeTime).ToList();
                                        }
                                        else
                                        {
                                            if (model.Sign == "<")
                                                rec = manager.Records.Where(o => String.Compare(o.SecondName, model.SecondName, true) == 0 &&
                                                    o.LastChangeTime.Date < model.LastChangeTime.Date).ToList();
                                            else if (model.Sign == ">")
                                                rec = manager.Records.Where(o => String.Compare(o.SecondName, model.SecondName, true) == 0 &&
                                                    o.LastChangeTime.Date > model.LastChangeTime.Date).ToList();
                                            else rec = manager.Records.Where(o => String.Compare(o.SecondName, model.SecondName, true) == 0 &&
                                                o.LastChangeTime.Date == model.LastChangeTime.Date).ToList();
                                        }

                                    }
                            else rec = manager.Records;


            return rec;

        }

        private static List<Record> GetRecordsWithPhonesAndNotes(DataManager manager)
        {
            List<Record> records = new List<Record>();

            foreach (Record rec in manager.Records)
            {
                records.Add(
                new Record()
                {
                    RecordId = rec.RecordId,
                    FirstName = rec.FirstName,
                    SecondName = rec.SecondName,
                    LastChangeTime = rec.LastChangeTime,
                    PhoneNumbers = manager.Phones.Where(o => o.RecordId == rec.RecordId).ToList(),
                    Notes = manager.Notes.Where(o => o.RecordId == rec.RecordId).ToList()
                });
            }
            return records;
        }
    }
}