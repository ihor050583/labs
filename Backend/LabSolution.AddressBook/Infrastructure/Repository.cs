﻿using System;

namespace LabSolution.AddressBook.Infrastructure
{
    
    public class Repository
    {
        
        public string Name { get; set; }
       
        public string Type { get; set; }
      
        public string Path { get; set; }
    }
}
