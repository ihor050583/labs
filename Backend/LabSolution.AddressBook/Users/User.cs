﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace LabSolution.AddressBook.Users
{
    
    public class User
    {
        
       
        public string Name { get; set; }
        
        public string Email { get; set; }
       
        public string Password { get; set; }
       
        
        public Guid RoleId { get; set; }

    }
}