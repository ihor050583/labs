﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using LabSolution.AddressBook.Configuration;
using LabSolution.AddressBook.Abstract;
using System.Xml;
using System.Configuration;

namespace LabSolution.AddressBook.Users
{
    public class CoreUserRepository : IRepository<User>
    {
        
        private System.Configuration.Configuration cfg;
        private AddressBookConfigSection section;
        
        public CoreUserRepository()
        {

            cfg = ConfigurationManager. OpenExeConfiguration(ConfigurationUserLevel.None);          
             section = (AddressBookConfigSection)cfg.Sections["AddressBook"];
            
        }

        public List<User> GetAll()
        {
            List<User> users = new List<User>();
            foreach(UserElement item in section.UserItems)
            {
                users.Add(new User() { Name = item.Name, Password = item.Password, Email = item.Email, RoleId = Guid.Parse(item.RoleId)});
            }
            return users;
        }

        public void Save(User entity)
        {
            section.UserItems.Add(entity.Name, entity.Password, entity.Email, entity.RoleId.ToString());            
        }

        public void Delete(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}