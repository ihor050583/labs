﻿using System;
using System.Collections.Generic;
using LabSolution.AddressBook.Concrete;

namespace LabSolution.AddressBook.Entities
{
    [Serializable]
    public class Record 
    {
        
        public Guid RecordId {get; set;}
        
        public string FirstName
        {
            get;
            set;
        }
        
        public string SecondName
        {
            get;
            set;
        }
        
        public DateTime LastChangeTime
        {
            get;
            set;
        }
       
        public IEnumerable<PhoneNumber> PhoneNumbers
        {
            get;
            set;
        }
       
        public IEnumerable<Note> Notes
        {
            get;
            set;
        }

        public override string ToString()
        {
            return String.Format("{0};{1};{2},{3:s}", RecordId,FirstName,SecondName,LastChangeTime);
        }

      

        public override int GetHashCode()
        {      
            return RecordId.GetHashCode();
        }

        public override bool Equals(Object ob)
        {
            Record r = (Record)ob;
            return (RecordId == r.RecordId);
        }

        
    }
}
