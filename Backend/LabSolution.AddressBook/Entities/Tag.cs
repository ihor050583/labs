﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LabSolution.AddressBook.Entities
{
     [Serializable]
     public class Tag
    {        
         public Guid TagId { get; set; }
         public string Name { get; set; }
    }
}
