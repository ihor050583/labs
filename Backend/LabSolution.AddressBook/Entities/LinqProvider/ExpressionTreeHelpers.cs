﻿using System;
using System.Linq.Expressions;
using LabSolution.AddressBook.Entities;

namespace LabSolution.AddressBook.LinqProvider
{
    internal class ExpressionTreeHelpers
    {
        
        internal static string GetValueFromEqualsExpression(BinaryExpression be)
        {
            string sign;

            switch (be.NodeType)
            {
                case ExpressionType.LessThan:
                    sign = "<";
                    break;
                case ExpressionType.LessThanOrEqual:
                    sign = "<=";
                    break;
                case ExpressionType.GreaterThan:
                    sign = ">";
                    break;
                case ExpressionType.GreaterThanOrEqual:
                    sign = ">=";
                    break;
                default:
                    sign = "=";
                    break;
            }



            if (be.Left.NodeType == ExpressionType.MemberAccess)
            {
                MemberExpression me = (MemberExpression)be.Left;
               
                if (me.Member.DeclaringType == typeof(Record))
                {                    
                    DateTime dateValue;
                    string result = GetValueFromExpression(be.Right);
                    if (DateTime.TryParse(result, out dateValue))
                        return String.Format("[translate(substring-before(@{0},'T'), '-', '') {1} translate(substring-before('{2}','T'), '-', '')]",
                            me.Member.Name, sign, dateValue.ToString("s"));
                    else                       
                        return String.Format("[translate(@{0},'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ','абвгдеёжзийклмнопрстуфхцчшщъыьэюя'){1}translate('{2}','АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ','абвгдеёжзийклмнопрстуфхцчшщъыьэюя')]",
                           me.Member.Name, sign, result);
                }
                else                   
                    return String.Format("[.//{0}/@{1}[translate(.,'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ','абвгдеёжзийклмнопрстуфхцчшщъыьэюя') = translate('{2}','АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ','абвгдеёжзийклмнопрстуфхцчшщъыьэюя')]]",
                        me.Member.DeclaringType.Name, me.Member.Name, GetValueFromExpression(be.Right));

                
            }
            else if (be.Right.NodeType == ExpressionType.MemberAccess)
            {
                MemberExpression me = (MemberExpression)be.Right;

                if (me.Member.DeclaringType == typeof(Record))
                {
                    DateTime dateValue;
                    string result = GetValueFromExpression(be.Left);
                    if (DateTime.TryParse(result, out dateValue))
                        return String.Format("[translate(substring-before(@{0},'T'), '-', '') {1} translate(substring-before('{2}','T'), '-', '')]",
                            me.Member.Name, sign, dateValue.ToString("s"));
                    else                        
                        return String.Format("[translate(@{0},'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ','абвгдеёжзийклмнопрстуфхцчшщъыьэюя'){1}translate('{2}','АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ','абвгдеёжзийклмнопрстуфхцчшщъыьэюя')]",
                           me.Member.Name, sign, result);

                }
                else
                    return String.Format("[.//{0}/@{1}[translate(.,'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ','абвгдеёжзийклмнопрстуфхцчшщъыьэюя') = translate('{2}','АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ','абвгдеёжзийклмнопрстуфхцчшщъыьэюя')]]", 
                        me.Member.DeclaringType.Name, me.Member.Name, GetValueFromExpression(be.Left));
            }


            throw new Exception("There is a bug in this program.");
        }

        internal static string GetValueFromExpression(Expression expression)
        {
            if (expression.NodeType == ExpressionType.Constant)
            {               
                return ((ConstantExpression)expression).Value.ToString();
            }
            else
                throw new InvalidQueryException(
                    String.Format("The expression type {0} is not supported to obtain a value.", expression.NodeType));


        }



    }
}
