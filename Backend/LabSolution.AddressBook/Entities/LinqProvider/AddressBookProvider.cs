﻿using System;
using System.Linq;
using System.Linq.Expressions;
using LabSolution.AddressBook.Concrete;

namespace LabSolution.AddressBook.LinqProvider
{
    public class AddressBookProvider : IQueryProvider
    {
        XmlRepositoryForLinq rep;
        public AddressBookProvider(XmlRepositoryForLinq rep)
        {
            this.rep = rep;
        }
        public IQueryable CreateQuery(Expression expression)
        {
            Type elementType = TypeSystem.GetElementType(expression.Type);
            try
            {
                return (IQueryable)Activator.CreateInstance(typeof(AddressBookData<>).MakeGenericType(elementType), new object[] { this, expression });
            }
            catch (System.Reflection.TargetInvocationException tie)
            {
                throw tie.InnerException;
            }
        }

        // Queryable's collection-returning standard query operators call this method. 
        public IQueryable<TResult> CreateQuery<TResult>(Expression expression)
        {
            return new AddressBookData<TResult>(this, expression);
        }

        public object Execute(Expression expression)
        {
            return AddressBookContext.Execute(expression, false,rep);
        }

        // Queryable's "single value" standard query operators call this method.
        // It is also called from QueryableTerraServerData.GetEnumerator(). 
        public TResult Execute<TResult>(Expression expression)
        {       
            bool IsEnumerable = (typeof(TResult).Name == "IEnumerable`1");
            var tt = (TResult)AddressBookContext.Execute(expression, IsEnumerable,rep);
            return tt;
        }
    }
}
