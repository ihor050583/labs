﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using LabSolution.AddressBook.Entities;

namespace LabSolution.AddressBook.LinqProvider
{
  

    public class XmlRepositoryForLinq
    {           
        private XmlDocument doc;
        XmlNamespaceManager namespaceManager;
        

        public XmlRepositoryForLinq(XmlDocument doc)
        {           
            this.doc = doc;
            namespaceManager = new XmlNamespaceManager(this.doc.NameTable);
            namespaceManager.AddNamespace("ab", "http://www.example.org/addressBook");
                      
        }



        public IQueryable<Record> SelectNodes(string xPath, string order, string name)
        {
            List<Record> records = new List<Record>();
            IOrderedEnumerable<XmlNode> nodes = null;           
            

            if (order == "OrderBy")
            {
                nodes = doc.SelectNodes(xPath,namespaceManager).Cast<XmlNode>().OrderBy(ndNode => ndNode.Attributes[name].Value);
            }
            else
            {
                nodes = doc.SelectNodes(xPath, namespaceManager).Cast<XmlNode>().OrderByDescending(ndNode => ndNode.Attributes[name].Value);

            }


            foreach (XmlNode node in nodes)
            {
                XmlNodeList phones = node.SelectNodes(".//PhoneNumber");
                XmlNodeList notes = node.SelectNodes(".//Note");
                records.Add(new Record()
                {
                    FirstName = node.Attributes["FirstName"].Value,
                    SecondName = node.Attributes["SecondName"].Value,
                    LastChangeTime = Convert.ToDateTime(node.Attributes["LastChangeTime"].Value),
                    RecordId = Guid.Parse(node.Attributes["RecordId"].Value),
                    PhoneNumbers = GetPhones(phones),
                    Notes = GetNotes(notes)
                });
            }

            return records.AsQueryable<Record>();

        }


        public IQueryable<Record> SelectNodes(string xPath)
        {
            List<Record> records = new List<Record>();
            XmlNodeList nodes = doc.SelectNodes(xPath, namespaceManager);
            if (nodes != null)
            {
                
                foreach (XmlNode node in nodes)
                {
                    XmlNodeList phones = node.SelectNodes(".//PhoneNumber");
                    XmlNodeList notes = node.SelectNodes(".//Note");
                    records.Add(new Record()
                    {
                        FirstName = node.Attributes["FirstName"].Value,
                        SecondName = node.Attributes["SecondName"].Value,
                        LastChangeTime = Convert.ToDateTime(node.Attributes["LastChangeTime"].Value),
                        RecordId = Guid.Parse(node.Attributes["RecordId"].Value),
                        PhoneNumbers = GetPhones(phones),
                        Notes = GetNotes(notes)


                    });
                }
                
            }

            return records.AsQueryable<Record>();           

        }

       

        public Record SelectNode(string xPath)
        {

            XmlNode node = doc.SelectSingleNode(xPath, namespaceManager);
            
            if (node != null)
            {
                XmlNodeList phones = node.SelectNodes(".//PhoneNumber");
                XmlNodeList notes = node.SelectNodes(".//Note");

                Record record = new Record()
                {
                    FirstName = node.Attributes["FirstName"].Value,
                    SecondName = node.Attributes["SecondName"].Value,
                    LastChangeTime = Convert.ToDateTime(node.Attributes["LastChangeTime"].Value),
                    RecordId = Guid.Parse(node.Attributes["RecordId"].Value),
                    PhoneNumbers = GetPhones(phones),
                    Notes = GetNotes(notes)
                };
                return record;
            }
            else return null;      

        }

        public int Count(string xPath)
        {

            XmlNodeList nodes = doc.SelectNodes(xPath, namespaceManager);            
            return nodes.Count;
        }

        
        private List<PhoneNumber> GetPhones(XmlNodeList nodes)
        {
            List<PhoneNumber> phones = new List<PhoneNumber>();
            foreach (XmlNode node in nodes)
            {
                phones.Add(new PhoneNumber()
                {
                    PhoneNumberId = Guid.Parse(node.Attributes["PhoneNumberId"].Value),
                    Name = node.Attributes["Name"].Value,
                    Phone = node.Attributes["Phone"].Value,
                    RecordId = Guid.Parse(node.Attributes["RecordId"].Value)
                });
            }
            return phones;
        }

        private List<Note> GetNotes(XmlNodeList nodes)
        {
            List<Note> notes = new List<Note>();
            foreach (XmlNode node in nodes)
            {
                List<Guid> tagsId = GetTagsId(node.SelectNodes(".//TagId"));
                notes.Add(new Note()
                {
                    NoteId = Guid.Parse(node.Attributes["NoteId"].Value),
                    Description = node.Attributes["Description"].Value,
                    RecordId = Guid.Parse(node.Attributes["RecordId"].Value),
                    TagsId= tagsId,
                    Tags = GetTagsName(tagsId)
                });
            }
            return notes;
        }

        private List<Guid> GetTagsId(XmlNodeList nodes)
        {
            List<Guid> tagsId = new List<Guid>();
            foreach (XmlNode node in nodes)
            {
                tagsId.Add(
                
                     Guid.Parse(node.InnerText)
                    
                );
            }
            return tagsId;
        }

        private List<string> GetTagsName(List<Guid> tagsId)
        {
            List<string> tagi = new List<string>();
            List<Tag> _tags = GetAllTegs();
            foreach (Guid id in tagsId)
            {
                Tag tag = _tags.FirstOrDefault(o => o.TagId == id);
                if (tag != null)
                    tagi.Add(tag.Name);
            }
            return tagi;
        }

        private List<Tag> GetAllTegs()
        {
            List<Tag> tags = new List<Tag>();
            XmlNodeList nodes = doc.SelectNodes(".//Tagi/Tag");

            foreach (XmlNode node in nodes)
            {

                tags.Add(new Tag()
                {
                    TagId = Guid.Parse(node.Attributes["TagId"].Value),
                    Name = node.Attributes["Name"].Value
                   
                });
            }
            return tags;
        }
        

        
    }
}
