﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using LabSolution.AddressBook.Entities;

namespace LabSolution.AddressBook.LinqProvider
{
    internal class RecordFinder : ExpressionVisitor
    {
        StringBuilder sb;        
        string memberNameForSort;


        internal string Translate(Expression expression)
        {
            sb = new StringBuilder();
            sb.Append("/ab:addressBook/Records/Record");
            this.Visit(expression);
            return sb.ToString();
        }

        internal string GetMemberNameForSort(Expression expression)
        {
            this.Visit(expression);
            return memberNameForSort;
        }

        protected override Expression VisitBinary(BinaryExpression be)
        {
            if (be.NodeType == ExpressionType.Equal ||
                be.NodeType == ExpressionType.LessThan ||
                be.NodeType == ExpressionType.LessThanOrEqual ||
                be.NodeType == ExpressionType.GreaterThan ||
                be.NodeType == ExpressionType.GreaterThanOrEqual)
            {
                sb.Append(ExpressionTreeHelpers.GetValueFromEqualsExpression(be));
            }

            else if (be.NodeType == ExpressionType.AndAlso)
            {
                if (be.Left.NodeType == ExpressionType.Equal || be.Left.NodeType == ExpressionType.AndAlso ||
                    be.Right.NodeType == ExpressionType.LessThan || be.Right.NodeType == ExpressionType.GreaterThan)
                {
                    BinaryExpression left = be.Left as BinaryExpression;
                    if (left != null)
                        VisitBinary(left);

                }
                else if (be.Left.NodeType == ExpressionType.Call)
                {
                    MethodCallExpression left = be.Left as MethodCallExpression;
                    if (left != null)
                        VisitMethodCall(left);

                }


                if (be.Right.NodeType == ExpressionType.Equal || be.Left.NodeType == ExpressionType.AndAlso ||
                    be.Right.NodeType == ExpressionType.LessThan || be.Right.NodeType == ExpressionType.GreaterThan)
                {
                    BinaryExpression right = (BinaryExpression)be.Right;
                    VisitBinary(right);

                }
                else if (be.Right.NodeType == ExpressionType.Call)
                {
                    MethodCallExpression right = be.Right as MethodCallExpression;
                    if (right != null)
                        VisitMethodCall(right);

                }
            }

            return be;
        }


        protected override Expression VisitConstant(ConstantExpression c)
        {

            Guid tagId;
            if (Guid.TryParse(c.Value.ToString(), out tagId))
                sb.Append(String.Format("[.//TagId='{0}']", tagId.ToString()));
            else  sb.Append(String.Format("[.//@Description [contains(translate(.,'АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ','абвгдеёжзийклмнопрстуфхцчшщъыьэюя'),translate('{0}','АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ','абвгдеёжзийклмнопрстуфхцчшщъыьэюя'))]]", 
                c.Value));
            return c;
        }



        protected override Expression VisitMethodCall(MethodCallExpression expression)
        {
            int earg = expression.Arguments.Count;
            if (earg > 1)
            {
                if (expression.Arguments[0] is MethodCallExpression && expression.Arguments[1] is UnaryExpression)
                {
                    LambdaExpression lambdaExpression = (LambdaExpression)((UnaryExpression)(expression.Arguments[1])).Operand;
                    lambdaExpression = (LambdaExpression)Evaluator.PartialEval(lambdaExpression);
                    if (lambdaExpression.Body.NodeType == ExpressionType.MemberAccess)
                    {
                        MemberExpression me = (MemberExpression)lambdaExpression.Body;
                        memberNameForSort = me.Member.Name;

                    }
                    BinaryExpression be = lambdaExpression.Body as BinaryExpression;
                    if (be != null)
                        VisitBinary(be);

                    VisitMethodCall((MethodCallExpression)expression.Arguments[0]);


                }
                else if (expression.Arguments[0] is ConstantExpression && expression.Arguments[1] is UnaryExpression)
                {
                    LambdaExpression lambdaExpression = (LambdaExpression)((UnaryExpression)(expression.Arguments[1])).Operand;
                    lambdaExpression = (LambdaExpression)Evaluator.PartialEval(lambdaExpression);
                    if (lambdaExpression.Body.NodeType == ExpressionType.MemberAccess)
                    {
                        MemberExpression me = (MemberExpression)lambdaExpression.Body;
                        memberNameForSort = me.Member.Name;

                    }
                    //для метода Contains
                    if (lambdaExpression.Body is MethodCallExpression)
                    {
                        MethodCallExpression me = (MethodCallExpression)lambdaExpression.Body;
                        if (me.Method.Name == "Contains" && me.Arguments[0] is ConstantExpression)
                        {
                            ConstantExpression con = me.Arguments[0] as ConstantExpression;
                            if (con != null)
                                VisitConstant(con);
                        }

                    }
                    BinaryExpression be = lambdaExpression.Body as BinaryExpression;
                    if (be != null)
                        VisitBinary(be);

                }
            }
            else
            {
                if (expression.Arguments[0] is MethodCallExpression)
                {
                    VisitMethodCall((MethodCallExpression)expression.Arguments[0]);

                }

                if (expression.Method.Name == "First")
                    sb.Append("[1]");
                if (expression.Method.Name == "Last")
                    sb.Append("[last()]");
            }

            if (expression.Method.Name == "FirstOrDefault")
                sb.Append("[1]");
            if (expression.Method.Name == "LastOrDefault")
                sb.Append("[last()]");

            return expression;

        }

    }
}
