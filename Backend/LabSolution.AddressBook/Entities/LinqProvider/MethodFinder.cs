﻿using System;
using System.Linq.Expressions;

namespace LabSolution.AddressBook.LinqProvider
{
    internal class MethodFinder : ExpressionVisitor
    {        
        private string methodname;
        private int countArguments;

        public int GetCountArguments(Expression expression)
        {
            Visit(expression);
            return countArguments;
        }

        public string GetMethodName(Expression expression)
        {
            Visit(expression);
            return methodname;
        }

       

        protected override Expression VisitMethodCall(MethodCallExpression expression)
        {

            methodname = expression.Method.Name;           
            countArguments = expression.Arguments.Count;

            return expression;
        }
    }
}
