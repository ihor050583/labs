﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LabSolution.AddressBook.Entities;
using System.Xml;
using LabSolution.AddressBook.Concrete;
using log4net;

namespace LabSolution.AddressBook.LinqProvider
{
    class AddressBookContext
    {
        public static readonly ILog log = LogManager.GetLogger(typeof(AddressBookContext)); 
      
        internal static object Execute(Expression expression, bool IsEnumerable, XmlRepositoryForLinq rep)
        {
            if (!IsQueryOverDataSource(expression))
                throw new InvalidProgramException("No query over the data source was specified.");

            Expression expressionForXpath;//Выражение для xpath 
            MethodFinder methodFinder = new MethodFinder();
            string firstMethodName = methodFinder.GetMethodName(expression);//имя последнего метода
            string secondMethodName; //имя предпоследнего метода, если такой есть
            //проверяем поддерживается ли метод
            bool unsupportedMethod = firstMethodName != "Where" && firstMethodName != "First" && firstMethodName != "FirstOrDefault"
                && firstMethodName != "Count" && firstMethodName != "Last" && firstMethodName != "LastOrDefault"
                && firstMethodName != "OrderBy" && firstMethodName != "OrderByDescending" && firstMethodName != "Contains";

            if (unsupportedMethod)
            {
                MethodCallExpression m = expression as MethodCallExpression;
                expressionForXpath = m.Arguments[0];
                secondMethodName = methodFinder.GetMethodName(m.Arguments[0]);
            }
            else
            {
                expressionForXpath = expression;
                secondMethodName = firstMethodName;
            }

            RecordFinder recordFinder = new RecordFinder();
            string xpath = recordFinder.Translate(expressionForXpath);
            log.Info(String.Format("Xpath='{0}'", xpath));

            IQueryable<Record> records = null; 
            if (secondMethodName == "OrderBy" || secondMethodName == "OrderByDescending")
            {
                string rr = recordFinder.GetMemberNameForSort(expressionForXpath);
                records = rep.SelectNodes(xpath, secondMethodName, rr);
                if (unsupportedMethod)
                {
                    MethodCallExpression m = expression as MethodCallExpression;
                    LambdaExpression lambdaExpression = (LambdaExpression)((UnaryExpression)(m.Arguments[1])).Operand;
                    MethodCallExpression whereCallExpression = Expression.
                        Call(m.Method, records.Expression, lambdaExpression);                     
                    return records.Provider.CreateQuery(whereCallExpression);
                }
                else return records;
            }


            if (IsEnumerable)
            {
                records = rep.SelectNodes(xpath);
                if (unsupportedMethod)
                {
                    MethodCallExpression m = expression as MethodCallExpression;
                    LambdaExpression lambdaExpression = (LambdaExpression)((UnaryExpression)(m.Arguments[1])).Operand;
                    MethodCallExpression whereCallExpression = Expression.
                        Call(m.Method, records.Expression, lambdaExpression);
                    return records.Provider.CreateQuery(whereCallExpression);
                }
                else return records;
            }
            if (secondMethodName == "Count")
                return rep.Count(xpath);
            
            return rep.SelectNode(xpath);
        }


        private static bool IsQueryOverDataSource(Expression expression)
        {           
            return (expression is MethodCallExpression);
        }
    }
}
