﻿using System;
using System.Collections.Generic;

namespace LabSolution.AddressBook.Entities
{
    [Serializable]
    public class Note
    {        
                
        public Guid NoteId { get; set; }
        
        public string Description { get; set; }

        public List<string> Tags { get; set; }

        public List<Guid> TagsId { get; set; }       
        
        public Guid RecordId { get; set; }

        public Record Record { get; set; }
        
    }
}
