﻿using System;

namespace LabSolution.AddressBook.Entities
{
    [Serializable]
    public class PhoneNumber
    {
               
        public Guid PhoneNumberId { get; set; }

        
        public string Name { get; set; }

        
        public string Phone { get; set; }

        
        public Guid RecordId { get; set; }

        public Record Record { get; set; }
        
    }
}
