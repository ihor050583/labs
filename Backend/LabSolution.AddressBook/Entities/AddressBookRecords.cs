﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LabSolution.AddressBook.Entities
{
    public static class AddressBookRecords
    {
        private static List<Record> records = new List<Record>();           

        private static List<PhoneNumber> phoneNumbers = new List<PhoneNumber>();
            

        private static List<Note> notes = new List<Note>();
           

        private static List<Tag> tags = new List<Tag>();
       

       
         


        public static List<Record> Records
        {
            get { return records; }
            set { records = value; }
        }

        public static List<PhoneNumber> Phons 
        {
            get { return phoneNumbers; }
            set { phoneNumbers = value; }
        }
        public static List<Note> Notes 
        {
            get { return notes; }
            set { notes = value; }
        }
        public static List<Tag> Tags
        {
            get { return tags; }
            set { tags = value; }
        }
        
    }
}
