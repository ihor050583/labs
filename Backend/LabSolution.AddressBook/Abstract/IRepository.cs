﻿using System;
using System.Collections.Generic;


namespace LabSolution.AddressBook.Abstract
{
    public interface IRepository<T>
    {
        List<T> GetAll();
        void Save(T entity);
        void Delete(Guid id); 
    }
}
