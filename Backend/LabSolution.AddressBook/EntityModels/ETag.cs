﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LabSolution.AddressBook.EntityModels
{
    public class ETag
    {
        public Guid ETagId { get; set; }
        public string Name { get; set; }
    }
}
