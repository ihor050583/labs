﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LabSolution.AddressBook.EntityModels
{
    public class ENote
    {
        public Guid ENoteId { get; set; }

        public string Description { get; set; }       

        public string TagsId { get; set; }

        public Guid ERecordId { get; set; }

        public ERecord ERecord { get; set; }
    }
}
