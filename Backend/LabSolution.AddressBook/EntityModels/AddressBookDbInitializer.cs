﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace LabSolution.AddressBook.EntityModels
{
    public class AddressBookDbInitializer : DropCreateDatabaseAlways<AddressBookEntities>
    {
        protected override void Seed(AddressBookEntities db)
        {
            db.ERecords.Add(new ERecord { ERecordId = Guid.Parse("91d7c452-7d0d-4bed-9495-0b9eecec0f83"), FirstName = "Дима", SecondName = "Иванов", LastChangeTime = DateTime.UtcNow });
            db.ERecords.Add(new ERecord { ERecordId = Guid.Parse("97d7c452-7d0d-4bed-9495-0b9eecec0f83"), FirstName = "Игорь", SecondName = "Иванов", LastChangeTime = DateTime.UtcNow });

            db.EPhoneNumbers.Add(new EPhoneNumber { EPhoneNumberId = Guid.NewGuid(), Name = "рабочий", Phone = "11111", ERecordId = Guid.Parse("91d7c452-7d0d-4bed-9495-0b9eecec0f83") });
            db.EPhoneNumbers.Add(new EPhoneNumber { EPhoneNumberId = Guid.NewGuid(), Name = "рабочий", Phone = "11111", ERecordId = Guid.Parse("97d7c452-7d0d-4bed-9495-0b9eecec0f83") });

            db.ENotes.Add(new ENote { ENoteId = Guid.NewGuid(), Description = "kkkkk", ERecordId = Guid.Parse("91d7c452-7d0d-4bed-9495-0b9eecec0f83"), TagsId = "db0894cc-212b-4fa3-8f8d-d5f1856815b5" });
            db.ENotes.Add(new ENote { ENoteId = Guid.NewGuid(), Description = "ffff", ERecordId = Guid.Parse("97d7c452-7d0d-4bed-9495-0b9eecec0f83"), TagsId = "7e2156bf-a454-4de6-a076-c7d8af12ec22" });

            db.ETags.Add(new ETag {ETagId= Guid.Parse("db0894cc-212b-4fa3-8f8d-d5f1856815b5"), Name="Минск" });
            db.ETags.Add(new ETag { ETagId = Guid.Parse("7e2156bf-a454-4de6-a076-c7d8af12ec22"), Name = "центр" });

            base.Seed(db);
        }

    }
}
