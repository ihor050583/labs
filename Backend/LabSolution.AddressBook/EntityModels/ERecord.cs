﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace LabSolution.AddressBook.EntityModels
{
    public class ERecord
    {
        [Key]
        public Guid ERecordId { get; set; }

        public string FirstName
        {
            get;
            set;
        }

        public string SecondName
        {
            get;
            set;
        }

        public DateTime LastChangeTime
        {
            get;
            set;
        }

        public virtual List<EPhoneNumber> EPhoneNumbers
        {
            get;
            set;
        }

        public virtual List<ENote> ENotes
        {
            get;
            set;
        }

       



        

        
    }
}
