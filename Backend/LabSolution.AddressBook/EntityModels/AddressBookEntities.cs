﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace LabSolution.AddressBook.EntityModels
{
    public class AddressBookEntities : DbContext
    {
        public AddressBookEntities(string con)
            : base() 
    {
        AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
    } 
        public DbSet<ETag> ETags { get; set; }
        public DbSet<ENote> ENotes { get; set; }
        public DbSet<EPhoneNumber> EPhoneNumbers { get; set; }
        public DbSet<ERecord> ERecords { get; set; }
    }
}
