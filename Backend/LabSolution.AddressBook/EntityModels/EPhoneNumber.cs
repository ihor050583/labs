﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace LabSolution.AddressBook.EntityModels
{
    public class EPhoneNumber
    {
        public Guid EPhoneNumberId { get; set; }

        public string Name { get; set; }

        public string Phone { get; set; }
    
        public Guid ERecordId { get; set; }

        public virtual ERecord ERecord { get; set; }
    }
}
