﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace LabSolution.AddressBook.CoreModels
{
    public class CoreRepositoryDetails
    {       
        public string Name { get; set; }        
        public string Type { get; set; }        
        public string Path { get; set; }
    }
}