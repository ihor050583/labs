﻿using System;
using System.Collections.Generic;
using LabSolution.AddressBook.Entities;

namespace LabSolution.AddressBook.CoreModels
{
    [Serializable]    
    public class SerializeAddressBook
    {        
        public List<Record> Records { get; set; }
        public List<PhoneNumber> Phones { get; set; }
        public List<Note> Notes { get; set; }
        public List<Tag> Tags { get; set; }     
            
    }
}
