﻿using System;
using System.Collections.Generic;
using System.Linq;
using LabSolution.AddressBook.Abstract;
using LabSolution.AddressBook.Entities;

namespace LabSolution.AddressBook.Concrete
{
    public class RecordRepository : IRepository<Record>
    {   
        public List<Record> GetAll()
        {
            return AddressBookRecords.Records;
        }

        public void Save(Record entity)
        {
            Record record = AddressBookRecords.Records.FirstOrDefault(o => o.RecordId == entity.RecordId);
            if (record == null)
            {
                entity.LastChangeTime = DateTime.UtcNow;
                AddressBookRecords.Records.Add(entity);
            }
            else
            {
                record.FirstName = entity.FirstName;
                record.SecondName = entity.SecondName;
                record.LastChangeTime = DateTime.UtcNow;
            }
        }

        public void Delete(Guid id)
        {
            Record record = AddressBookRecords.Records.FirstOrDefault(o => o.RecordId == id); 
            if(record != null)
            AddressBookRecords.Records.Remove(record);
            
        }
    }
}
