﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabSolution.AddressBook.Abstract;
using LabSolution.AddressBook.Entities;
using LabSolution.AddressBook.EntityModels;

namespace LabSolution.AddressBook.Concrete
{
    public class DBTagRepository : IRepository<Tag>
    {
        private AddressBookEntities db;
        public DBTagRepository(string stringConnection)
        {          
           
            db = new AddressBookEntities(stringConnection);
        }

        public List<Tag> GetAll()
        {
            List<Tag> tags = new List<Tag>();
            List<ETag> eTags = db.ETags.ToList();
            foreach (ETag t in eTags)
            {
                tags.Add(new Tag { Name = t.Name, TagId = t.ETagId });
            }
            return tags; //var pp = 4
        }

        public void Save(Tag entity)
        {
            
                ETag n = new ETag() {  ETagId = entity.TagId, Name = entity.Name };
                db.ETags.Add(n);
                db.SaveChanges();
            
            
        }

        public void Delete(Guid id)
        {
            ETag tag = db.ETags.FirstOrDefault(o => o.ETagId == id);
            if (tag != null)
                db.ETags.Remove(tag);
            db.SaveChanges();
        }
    }
}
