﻿using System;
using System.Collections.Generic;
using System.Linq;
using LabSolution.AddressBook.Entities;
using LabSolution.AddressBook.Abstract;
using System.Xml;
using log4net;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using LabSolution.AddressBook.CoreModels;
using LabSolution.AddressBook.Infrastructure;
using System.Data.Entity;
using LabSolution.AddressBook.EntityModels;

namespace LabSolution.AddressBook.Concrete
{
    public class DataManager
    {
        private static CoreRepositoryDetails curentRepository = null;
        private static XmlDocument doc;
        public static readonly ILog log = LogManager.GetLogger(typeof(DataManager)); 
        


        private IRepository<Record> records;
        private IRepository<Note> notes;
        private IRepository<PhoneNumber> phones;
        private IRepository<Tag> tags;        

        public DataManager()
        {
            records = new RecordRepository();
            phones = new PhoneRepository();
            notes = new NoteRepository();
            tags = new TagRepository();
            log4net.Config.XmlConfigurator.Configure();         
        }
        

        public void DeleteRecord(Guid id)
        {
            log.Info(String.Format("Аргумент id='{0}'",id));
            Record record = records.GetAll().FirstOrDefault(o => o.RecordId == id);
            if (record != null)
            {

                record.PhoneNumbers = phones.GetAll().Where(o => o.RecordId == record.RecordId);

                foreach (PhoneNumber phone in record.PhoneNumbers.ToList())
                {
                    DeletePhone(phone.PhoneNumberId);
                }

                record.Notes = notes.GetAll().Where(o => o.RecordId == record.RecordId);
                foreach (Note note in record.Notes.ToList())
                {
                    DeleteNote(note.NoteId);
                }

                records.Delete(id);
            }
            else log.Error(String.Format("record с id='{0}' не найден!",id));
        }

        public void SaveRecord(Record entity)
        {
            log.Info(String.Format("Аргумент id='{0}', firstName='{1}',secondName='{2}',lastChangeTime='{3}'", 
                entity.RecordId, entity.FirstName,  entity.SecondName,entity.LastChangeTime));
            records.Save(entity);
        }



        public List<PhoneNumber> Phones
        {
            get { return phones.GetAll(); }

        }

        public void DeletePhone(Guid id)
        {
            log.Info(String.Format("Аргумент id='{0}'", id));
            PhoneNumber phone = phones.GetAll().FirstOrDefault(o => o.PhoneNumberId == id);
            if (phone != null)
            {
                Record record = records.GetAll().FirstOrDefault(o => o.RecordId == phone.RecordId);
                records.Save(record);
                phones.Delete(id);
            }
            else log.Error(String.Format("phone id='{0}' c не найден!",id));

        }

        public void SavePhone(PhoneNumber entity)
        {
            log.Info(String.Format("Аргумент id='{0}', name='{1}',phone='{2}',RecordId='{3}'",
                entity.PhoneNumberId, entity.Name, entity.Phone,entity.RecordId));
            phones.Save(entity);
            Record record = records.GetAll().FirstOrDefault(o => o.RecordId == entity.RecordId);
            records.Save(record);
        }


        public List<Record> Records
        {
            get { return records.GetAll(); }

        }

        public List<Note> Notes
        {
            get
            {
                List<Note> _notes = notes.GetAll();               
                List<Tag> _tags = tags.GetAll();                
                foreach (Note note in _notes)
                {
                    List<string> tagi = new List<string>();                  
                    foreach (Guid id in note.TagsId)
                    {
                        Tag tag = _tags.FirstOrDefault(o => o.TagId == id);
                        if (tag!= null) 
                         tagi.Add(tag.Name);
                    }
                    note.Tags = tagi;
                                       
                }
                return _notes;
            }
            
        }

        public void DeleteNote(Guid id)
        {
            log.Info(String.Format("Аргумент id='{0}'", id));

            Note note = notes.GetAll().FirstOrDefault(o => o.NoteId == id);
            if (note != null)
            {                
               
                foreach (Guid g in note.TagsId)
                {
                    List<Note> _notes = new List<Note>();
                    foreach (Note _note in Notes)
                    {
                        if (_note.TagsId.Contains(g))
                            _notes.Add(_note);
                    }

                    if(_notes.Count <= 1)
                        tags.Delete(g);

                }
                Record record = records.GetAll().FirstOrDefault(o => o.RecordId == note.RecordId);
                records.Save(record);
                notes.Delete(id);
            }
            else log.Error(String.Format("note c id='{0}' не найден!"));
        }

        public void SaveNote(Note entity)
        {
            log.Info(String.Format("Аргумент id='{0}', description='{1}',RecordId='{2}'", entity.NoteId, entity.Description,
                 entity.RecordId));  
            Record record = records.GetAll().FirstOrDefault(o => o.RecordId == entity.RecordId);
            records.Save(record);
            notes.Save(entity);
        }

        public bool SaveTagForNote(Guid notedId, string name)
        {
            
            log.Info(String.Format("Аргумент notedId='{0}', name='{1}'", notedId, name));
            Note note = notes.GetAll().FirstOrDefault(o => o.NoteId == notedId);
            if (note == null)
            {
                return false;
            }
            Tag tag = tags.GetAll().FirstOrDefault(o => String.Compare(o.Name, name, true) == 0);
            if (tag == null)
            {
                tag = new Tag();
                tag.TagId = Guid.NewGuid();
                tag.Name = name;
                tags.Save(tag);
            }

            note.TagsId.Add(tag.TagId);
            notes.Save(note);
            Record record = records.GetAll().FirstOrDefault(o => o.RecordId == note.RecordId);
            records.Save(record);
            return true;
        }

        public bool DeleteTagForNote(Guid notedId, string name)
        {
            log.Info(String.Format("Аргумент notedId='{0}', name='{1}'", notedId, name));           

            Tag tag = tags.GetAll().FirstOrDefault(o => String.Compare(o.Name, name, true) == 0);
            Note note = notes.GetAll().FirstOrDefault(o => o.NoteId == notedId);
            if (note == null)
            {
                return false;
            }
            if (tag != null)
            {
                
                    List<Note> _notes = new List<Note>();
                    foreach (Note _note in Notes)
                    {
                        if (_note.TagsId.Contains(tag.TagId))
                            _notes.Add(_note);
                    }

                    if (_notes.Count <= 1)
                        tags.Delete(tag.TagId);
               

                note.TagsId.Remove(tag.TagId);
                notes.Save(note);
                Record record = records.GetAll().FirstOrDefault(o => o.RecordId == note.RecordId);
                records.Save(record);
                
            }
            return true;
        }



        public List<Tag> Tags
        {
            get { return tags.GetAll(); }
        }

        public CoreRepositoryDetails GetCurentRepository()
        {
            return curentRepository;
        }

        public bool SwitchRepository(string serverPath, CoreRepositoryDetails newRepo)
        {
            bool switched = false;           

            if (newRepo != null)
            {

                if (curentRepository != null && curentRepository.Type == "binary") 
                    Serialize(serverPath+curentRepository.Path);

                string filePath = serverPath + newRepo.Path;
                if (newRepo.Type == "xml")
                {
                    XmlReaderSettings settings = new XmlReaderSettings();
                    settings.ValidationType = ValidationType.Schema;
                    string xsdFile = serverPath + @"XmlFiles\AddressBook.xsd";
                    settings.Schemas.Add("http://www.example.org/addressBook", xsdFile);

                    XmlReader reader = XmlReader.Create(filePath, settings);
                    doc = new XmlDocument();

                    doc.Load(reader);
                    reader.Close();

                    this.records = new XmlRecordRepository(doc, filePath);
                    this.notes = new XmlNoteRepository(doc, filePath);
                    this.phones = new XmlPhoneRepository(doc, filePath);
                    this.tags = new XmlTagRepository(doc, filePath);

                    switched = true;

                }
                else if (newRepo.Type == "db")
                {
                    Database.SetInitializer(new AddressBookDbInitializer());//инициализация базы данных     
               
                    this.records = new DBRecordRepository(newRepo.Path);
                    this.notes = new DBNoteRepository(newRepo.Path);
                    this.phones = new DBPhoneRepository(newRepo.Path);
                    this.tags = new DBTagRepository(newRepo.Path);

                    switched = true;
                }
                else
                {
                    doc = null;
                    bool fileExists = File.Exists(filePath);
                    long fileByteSize = 0;
                    if (fileExists)
                    {
                        FileInfo someFileInfo = new FileInfo(filePath);
                        fileByteSize = someFileInfo.Length;
                        switched = true;
                    }

                    if (fileByteSize != 0)
                    {
                        FileStream fs = new FileStream(filePath, FileMode.Open);
                        BinaryFormatter formatter = new BinaryFormatter();
                        try
                        {

                            SerializeAddressBook serClass =
                                (SerializeAddressBook)formatter.Deserialize(fs);
                            AddressBookRecords.Records = serClass.Records;
                            AddressBookRecords.Notes = serClass.Notes;
                            AddressBookRecords.Phons = serClass.Phones;
                            AddressBookRecords.Tags = serClass.Tags;
                        }
                        catch
                        {
                            switched = false;
                        }
                        finally
                        {
                            fs.Close();
                        }

                    }

                    this.records = new RecordRepository();
                    this.notes = new NoteRepository();
                    this.phones = new PhoneRepository();
                    this.tags = new TagRepository();

                }

                curentRepository = newRepo;

            }


            return switched;
        }

        public List<Record> AdvancedSearch(CoreSearchModel model)
        {

            List<Record> rec = Search.AdvSearch(model, doc, this);

            return rec;

        }

        public List<Record> SimpleSearch(string name)
        {

            List<Record> rec = Search.SimpleSearch(name, doc, this);            
            return rec;

        }


        public void Serialize(string fullFilePath)
        {            
            BinaryFormatter formatter = new BinaryFormatter();
            SerializeAddressBook serClass = new SerializeAddressBook();
            serClass.Records = AddressBookRecords.Records;
            serClass.Notes = AddressBookRecords.Notes;
            serClass.Phones = AddressBookRecords.Phons;
            serClass.Tags = AddressBookRecords.Tags;

            FileStream fs = new FileStream(fullFilePath, FileMode.Create);
            formatter.Serialize(fs, serClass);
            fs.Close();
        }
          

       
        
    }
}
