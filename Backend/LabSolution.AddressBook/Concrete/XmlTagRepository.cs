﻿using System;
using System.Collections.Generic;
using System.Linq;
using LabSolution.AddressBook.Abstract;
using LabSolution.AddressBook.Entities;
using System.Xml;

namespace LabSolution.AddressBook.Concrete
{
    public class XmlTagRepository : IRepository<Tag>
    {
        private string filePath;      
        private XmlDocument doc;        

        public XmlTagRepository(XmlDocument doc, string filePath)
        {           
            this.doc = doc;            
            this.filePath = filePath;
        }
        public List<Tag> GetAll()
        {
            List<Tag> tags = new List<Tag>();
            XmlNodeList nodes = doc.SelectNodes(".//Tag");
            foreach (XmlNode node in nodes)
            {
               tags.Add(new Tag()
                {
                    TagId = Guid.Parse(node.Attributes["TagId"].Value),
                    Name = node.Attributes["Name"].Value
                });
            }
            return tags;
        }

        public void Save(Tag entity)
        {
            XmlElement newElem = doc.CreateElement("Tag");
            newElem.SetAttribute("TagId", entity.TagId.ToString());
            newElem.SetAttribute("Name", entity.Name);
            XmlNode root = doc.SelectSingleNode(".//Tagi");         
            root.AppendChild(newElem);      
            doc.Save(filePath);
        }

        public void Delete(Guid id)
        {
            XmlNode tag = doc.SelectSingleNode(String.Format(".//Tag[@TagId='{0}']", id));
            XmlNode parentNode = tag.ParentNode;
            parentNode.RemoveChild(tag);
            doc.Save(filePath);
        }
    }
}
