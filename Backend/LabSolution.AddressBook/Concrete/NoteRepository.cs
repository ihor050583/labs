﻿using System;
using System.Collections.Generic;
using System.Linq;
using LabSolution.AddressBook.Entities;
using LabSolution.AddressBook.Abstract;

namespace LabSolution.AddressBook.Concrete
{
    public class NoteRepository : IRepository<Note>
    {   
        public List<Note> GetAll()
        {
            return AddressBookRecords.Notes;
        }

        public void Save(Note entity)
        {
            Note note = AddressBookRecords.Notes.FirstOrDefault(o => o.NoteId == entity.NoteId);
            if (note == null)
                AddressBookRecords.Notes.Add(entity);
            else
            {
                note.Description = entity.Description;
                note.RecordId = entity.RecordId;
                note.Tags = entity.Tags;
            }
        }

        public void Delete(Guid id)
        {
            Note note = AddressBookRecords.Notes.FirstOrDefault(o => o.NoteId == id);   
            if(note !=null)
                AddressBookRecords.Notes.Remove(note);
        }
    }
}
