﻿using System;
using System.Collections.Generic;
using System.Linq;
using LabSolution.AddressBook.Entities;
using LabSolution.AddressBook.Abstract;

namespace LabSolution.AddressBook.Concrete
{
    public class PhoneRepository: IRepository<PhoneNumber>
    {     

        public List<PhoneNumber> GetAll()
        {
            return AddressBookRecords.Phons;
        }

        public void Save(PhoneNumber entity)
        {
            PhoneNumber phoneNumber = AddressBookRecords.Phons.FirstOrDefault(o => o.PhoneNumberId == entity.PhoneNumberId);
            if (phoneNumber == null)
            {
                AddressBookRecords.Phons.Add(entity);
            }
            else
            {
                phoneNumber.Name = entity.Name;
                phoneNumber.Phone = entity.Phone;
                phoneNumber.RecordId = entity.RecordId;                
            }
        }

        public void Delete(Guid id)
        {
            PhoneNumber phone = AddressBookRecords.Phons.FirstOrDefault(o => o.PhoneNumberId == id);
            if (phone != null)
                AddressBookRecords.Phons.Remove(phone);
        }
    }
}
