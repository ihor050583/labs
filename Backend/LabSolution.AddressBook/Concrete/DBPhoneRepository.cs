﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabSolution.AddressBook.Abstract;
using LabSolution.AddressBook.Entities;
using LabSolution.AddressBook.EntityModels;

namespace LabSolution.AddressBook.Concrete
{
    public class DBPhoneRepository : IRepository<PhoneNumber>
    {
        private AddressBookEntities db;
        public DBPhoneRepository(string stringConnection)
        {        
            db = new AddressBookEntities(stringConnection);
        }
        public List<PhoneNumber> GetAll()
        {
            List<PhoneNumber> phones = new List<PhoneNumber>();
            List<EPhoneNumber> ePhones = db.EPhoneNumbers.ToList();
            foreach (EPhoneNumber p in ePhones)
            {
                phones.Add(new PhoneNumber { RecordId = p.ERecordId, Name = p.Name, Phone = p.Phone, PhoneNumberId = p.EPhoneNumberId });
            }
            return phones;
        }

        public void Save(PhoneNumber entity)
        {
            EPhoneNumber phone = db.EPhoneNumbers.FirstOrDefault(o => o.EPhoneNumberId == entity.PhoneNumberId);
            if (phone == null)
            {
                EPhoneNumber p = new EPhoneNumber() { ERecordId = entity.RecordId, EPhoneNumberId = entity.PhoneNumberId, Phone = entity.Phone, Name = entity.Name };
                db.EPhoneNumbers.Add(p);
                db.SaveChanges();
            }
            else
            {
                phone.Name = entity.Name;
                phone.Phone = entity.Phone;
                phone.ERecordId = entity.RecordId;
                db.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            EPhoneNumber phone = db.EPhoneNumbers.FirstOrDefault(o => o.EPhoneNumberId == id);
            if (phone != null)
                db.EPhoneNumbers.Remove(phone);
            db.SaveChanges();
        }
    }
}
