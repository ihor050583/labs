﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LabSolution.AddressBook.Entities;
using LabSolution.AddressBook.Abstract;

namespace LabSolution.AddressBook.Concrete
{
    public class TagRepository: IRepository<Tag>
    {     

        public List<Tag> GetAll()
        {
            return AddressBookRecords.Tags;
        }

        public void Save(Tag entity)
        {
            AddressBookRecords.Tags.Add(entity);            
        }

        public void Delete(Guid id)
        {
            Tag tag = AddressBookRecords.Tags.FirstOrDefault(o => o.TagId == id);
            AddressBookRecords.Tags.Remove(tag);
        }
    }

    
}
