﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabSolution.AddressBook.Abstract;
using LabSolution.AddressBook.Entities;
using LabSolution.AddressBook.EntityModels;

namespace LabSolution.AddressBook.Concrete
{
    public class DBNoteRepository :IRepository<Note>
    {
        private AddressBookEntities db;
        public DBNoteRepository(string stringConnection)
        {          
           
            db = new AddressBookEntities(stringConnection);
        }
        public List<Note> GetAll()
        {
            List<Note> notes = new List<Note>();
            List<ENote> eNotes = db.ENotes.ToList();
            foreach (ENote n in eNotes)
            {
                notes.Add(new Note { RecordId = n.ERecordId, Description = n.Description, NoteId = n.ENoteId, TagsId = guidsFromString(n.TagsId) });
            }
            return notes;
        }

        public void Save(Note entity)
        {
            ENote note = db.ENotes.FirstOrDefault(o => o.ENoteId == entity.NoteId);
            if (note == null)
            {
                ENote n = new ENote() { ENoteId = entity.NoteId, Description = entity.Description, ERecordId = entity.RecordId, TagsId = stringFromGuids(entity.TagsId) };
                db.ENotes.Add(n);
                db.SaveChanges();
            }
            else
            {
                note.Description = entity.Description;
                note.ERecordId = entity.RecordId;
                note.TagsId = stringFromGuids(entity.TagsId);
                db.SaveChanges();
            }
        }

        public void Delete(Guid id)
        {
            ENote note = db.ENotes.FirstOrDefault(o => o.ENoteId == id);
            if (note != null)
                db.ENotes.Remove(note);
            db.SaveChanges();
        }

        private List<Guid> guidsFromString(string str)
        {
            List<Guid> guids = new List<Guid>();
            string[] separators = { ";", " "};
            string[] tagsId = str.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            foreach (string guid in tagsId)
            {
                guids.Add(Guid.Parse(guid));
            }
            return guids;
        }

     /*   private string stringFromGuids(List<String> tags)
        {
            List<ETag> guids = new List<ETag>();
            foreach (string s in tags)
            {
                ETag t = db.ETags.FirstOrDefault(o => String.Compare(o.Name, s, true) == 0);
                if (t != null)
                    guids.Add(t);
            }


            String sep   = ";";
            List<String> tagsId = new List<string>();
            foreach (ETag g in guids)
            {
                tagsId.Add(g.ETagId.ToString());
            }

            string result = String.Join(sep, tagsId.ToArray());
            return result;
        }*/

        private string stringFromGuids(List<Guid> guids)
        {          


            String sep = ";";
            List<String> tagsId = new List<string>();
            foreach (Guid g in guids)
            {
                tagsId.Add(g.ToString());
            }

            string result = String.Join(sep, tagsId.ToArray());
            return result;
        }
        
    }
}
