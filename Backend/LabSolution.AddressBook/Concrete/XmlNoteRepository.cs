﻿using System;
using System.Collections.Generic;
using System.Linq;
using LabSolution.AddressBook.Abstract;
using LabSolution.AddressBook.Entities;
using System.Xml;

namespace LabSolution.AddressBook.Concrete
{
    public class XmlNoteRepository : IRepository<Note>
    {
        private string filePath;        
        private XmlDocument doc;        

        public XmlNoteRepository(XmlDocument doc, string filePath)
        {            
            this.doc = doc;           
            this.filePath = filePath;

        }
        public List<Note> GetAll()
        {
            List<Note> notes = new List<Note>();
            XmlNodeList nodes = null;
            nodes = doc.SelectNodes(".//Note");
            foreach (XmlNode node in nodes)
            {
                List<Guid> tagsId = GetTagsId(node.SelectNodes(".//TagId"));
                notes.Add(new Note()
                {
                    NoteId = Guid.Parse(node.Attributes["NoteId"].Value),
                    Description = node.Attributes["Description"].Value,
                    RecordId = Guid.Parse(node.Attributes["RecordId"].Value),
                    TagsId = tagsId
                });
            }
            return notes;
        }

        

        public void Save(Note entity)
        {
            XmlNode note = doc.SelectSingleNode(String.Format(".//Note[@NoteId='{0}']", entity.NoteId));
            XmlNode root = doc.SelectSingleNode(String.Format(".//Record[@RecordId='{0}']/Notes", entity.RecordId));

            XmlElement newElem = doc.CreateElement("Note");
            newElem.SetAttribute("NoteId", entity.NoteId.ToString());
            newElem.SetAttribute("Description", entity.Description);
            newElem.SetAttribute("RecordId", entity.RecordId.ToString());

            if (note != null && entity.TagsId == null)
            {
                XmlNodeList tagsId = doc.SelectNodes(String.Format(".//Note[@NoteId='{0}']/Tags/TagId", entity.NoteId));
                entity.TagsId = GetTagsId(tagsId);
            }

            XmlElement tags = doc.CreateElement("Tags");
            foreach (Guid g in entity.TagsId)
            {
                XmlElement tagId = doc.CreateElement("TagId");
                tagId.InnerText = g.ToString();
                tags.AppendChild(tagId);
            }

            newElem.AppendChild(tags);

            root.AppendChild(newElem);
            if (note != null)
            {               
                XmlNode parent = note.ParentNode;
                parent.RemoveChild(note);                
            }
            
            doc.Save(filePath);
        }

        public void Delete(Guid id)
        {
            XmlNode note = doc.SelectSingleNode(String.Format(".//Note[@NoteId='{0}']", id));           
            XmlNode parentNode = note.ParentNode;
            parentNode.RemoveChild(note);
            doc.Save(filePath);
        }


        private List<Guid> GetTagsId(XmlNodeList nodes)
        {
            List<Guid> tagsId = new List<Guid>();
            foreach (XmlNode node in nodes)
            {
                tagsId.Add(

                     Guid.Parse(node.InnerText)

                );
            }
            return tagsId;
        }
        
    }
}
