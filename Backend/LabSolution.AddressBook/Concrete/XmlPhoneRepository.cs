﻿using System;
using System.Collections.Generic;
using System.Linq;
using LabSolution.AddressBook.Abstract;
using LabSolution.AddressBook.Entities;
using System.Xml;

namespace LabSolution.AddressBook.Concrete
{
    public class XmlPhoneRepository : IRepository<PhoneNumber>
    {
        private string filePath;       
        private XmlDocument doc;       
        public XmlPhoneRepository(XmlDocument doc, string filePath)
        {            
            this.doc = doc;            
            this.filePath = filePath;
            
        }
        public List<PhoneNumber> GetAll()
        {
            List<PhoneNumber> phoneNumbers = new List<PhoneNumber>();
            XmlNodeList nodes = doc.SelectNodes(".//PhoneNumber");
            foreach (XmlNode node in nodes)
            {
                phoneNumbers.Add(new PhoneNumber()
                {
                    PhoneNumberId = Guid.Parse(node.Attributes["PhoneNumberId"].Value),
                    Name = node.Attributes["Name"].Value,
                    Phone = node.Attributes["Phone"].Value,               
                    RecordId = Guid.Parse(node.Attributes["RecordId"].Value)
                    
                });
            }
            return phoneNumbers;
        }

        public void Save(PhoneNumber entity)
        {
            XmlElement newElem = doc.CreateElement("PhoneNumber");
            newElem.SetAttribute("PhoneNumberId", entity.PhoneNumberId.ToString());            
            newElem.SetAttribute("Name", entity.Name);
            newElem.SetAttribute("Phone", entity.Phone);
            newElem.SetAttribute("RecordId", entity.RecordId.ToString());

            XmlNode phone = doc.SelectSingleNode(String.Format(".//PhoneNumber[@PhoneNumberId='{0}']", entity.PhoneNumberId));
            XmlNode root = doc.SelectSingleNode(String.Format(".//Record[@RecordId='{0}']/Phones", entity.RecordId));

            root.AppendChild(newElem);
            if (phone != null)
            {
                XmlNode parent = phone.ParentNode;
                parent.RemoveChild(phone);  
            }
            
            doc.Save(filePath);
        }

        public void Delete(Guid id)
        {
            XmlNode phone = doc.SelectSingleNode(String.Format(".//PhoneNumber[@PhoneNumberId='{0}']", id));
            XmlNode parentNode = phone.ParentNode;
            parentNode.RemoveChild(phone);
            doc.Save(filePath);
        }
    }
}
