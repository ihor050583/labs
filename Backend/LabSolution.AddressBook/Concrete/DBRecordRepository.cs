﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LabSolution.AddressBook.Abstract;
using LabSolution.AddressBook.Entities;
using LabSolution.AddressBook.EntityModels;

namespace LabSolution.AddressBook.Concrete
{
    public class DBRecordRepository : IRepository<Record>
    {
        
        private AddressBookEntities db;
        public DBRecordRepository(string stringConnection)
        {           
            
            db = new AddressBookEntities(stringConnection);
        }
        public List<Record> GetAll()
        {
            List<Record> records = new List<Record>();
            List<ERecord> eRecords = db.ERecords.ToList();
            foreach(ERecord  rec in eRecords )
            {
                records.Add(new Record { RecordId = rec.ERecordId, FirstName = rec.FirstName, SecondName = rec.SecondName, LastChangeTime = rec.LastChangeTime });
            }
            return records;
        }

        public void Save(Record entity)
        {
             
            ERecord record = db.ERecords.FirstOrDefault(o => o.ERecordId == entity.RecordId);
            if (record == null)
            {
                ERecord rec = new ERecord() { ERecordId = entity.RecordId, FirstName = entity.FirstName, SecondName = entity.SecondName, LastChangeTime = DateTime.UtcNow };
                db.ERecords.Add(rec);
                db.SaveChanges();
            }
            else
            {
                record.FirstName = entity.FirstName;
                record.SecondName = entity.SecondName;
                record.LastChangeTime = DateTime.UtcNow;
                db.SaveChanges();
            }

           
        }

        public void Delete(Guid id)
        {
            ERecord record = db.ERecords.FirstOrDefault(o => o.ERecordId == id);
            if (record != null)
                db.ERecords.Remove(record);
            db.SaveChanges();
        }
        //метод корректирует ошибку SqlProviderServices
        public void FixEfProviderServicesProblem()
        {
            var instance = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }
    }
}
