﻿using System;
using System.Collections.Generic;
using System.Linq;

using LabSolution.AddressBook.Entities;
using System.Xml;
using LabSolution.AddressBook.Abstract;

namespace LabSolution.AddressBook.Concrete
{
    public class XmlRecordRepository : IRepository<Record>
    {
        private string filePath;
        private XmlDocument doc;            
        public XmlRecordRepository(XmlDocument doc, string filePath)
        {            
            this.doc = doc;           
            this.filePath = filePath;
        }
        public List<Record> GetAll()
        {
            List<Record> records = new List<Record>();
            XmlNodeList nodes = doc.SelectNodes(".//Record");
            foreach (XmlNode node in nodes)
            {
                
                records.Add(new Record()
                {
                    RecordId = Guid.Parse(node.Attributes["RecordId"].Value),
                    FirstName = node.Attributes["FirstName"].Value,
                    SecondName = node.Attributes["SecondName"].Value,                    
                    LastChangeTime = DateTime.Parse(node.Attributes["LastChangeTime"].Value)
                });
            }
           
            return records;
        }

        public void Save(Record entity)
        {
            XmlNode record = doc.SelectSingleNode(String.Format(".//Record[@RecordId='{0}']", entity.RecordId));
            XmlNode root = doc.SelectSingleNode(".//Records");
            

            XmlElement newElem = doc.CreateElement("Record");
            newElem.SetAttribute("RecordId", entity.RecordId.ToString());
            newElem.SetAttribute("FirstName", entity.FirstName);
            newElem.SetAttribute("SecondName", entity.SecondName);
            newElem.SetAttribute("LastChangeTime", DateTime.UtcNow.ToString("s"));

            if (record != null)
            {
                
                XmlNodeList phoneNodes = doc.SelectNodes(String.Format(".//Record[@RecordId='{0}']/Phones/PhoneNumber", entity.RecordId));
                entity.PhoneNumbers = GetPhoneNumbers(phoneNodes);
                XmlNodeList noteNodes = doc.SelectNodes(String.Format(".//Record[@RecordId='{0}']/Notes/Note", entity.RecordId));
                entity.Notes = GetNotes(noteNodes);
            }

            XmlElement phones = CreatePhoneNumbers(entity.PhoneNumbers);
            XmlElement notes = CreateNotes(entity.Notes);
            newElem.AppendChild(phones);
            newElem.AppendChild(notes); 
            
           
            if (record == null)
            {           
                root.AppendChild(newElem);
            }
            else
            {
                root.ReplaceChild(newElem, record);             

            }
            doc.Save(filePath);
        }

        public void Delete(Guid id)
        {
            XmlNode record = doc.SelectSingleNode(String.Format(".//Record[@RecordId='{0}']", id));
            XmlNode parentNode = record.ParentNode;
            parentNode.RemoveChild(record);
            doc.Save(filePath);
        }

        private XmlElement CreatePhoneNumbers(IEnumerable<PhoneNumber> phoneNumbers)
        {
            XmlElement phones = doc.CreateElement("Phones");
            if (phoneNumbers != null)
            {
                foreach (PhoneNumber p in phoneNumbers)
                {
                    XmlElement phone = doc.CreateElement("PhoneNumber");
                    phone.SetAttribute("PhoneNumberId", p.PhoneNumberId.ToString());
                    phone.SetAttribute("Name", p.Name);
                    phone.SetAttribute("Phone", p.Phone);
                    phone.SetAttribute("RecordId", p.RecordId.ToString());

                    phones.AppendChild(phone);
                }
            }

            return phones;
        }

        private XmlElement CreateNotes(IEnumerable<Note> notes)
        {
            XmlElement newElem = doc.CreateElement("Notes");
            if (notes != null)
            {
                foreach (Note n in notes)
                {
                    XmlElement note = doc.CreateElement("Note");
                    note.SetAttribute("NoteId", n.NoteId.ToString());
                    note.SetAttribute("Description", n.Description);
                    note.SetAttribute("RecordId", n.RecordId.ToString());
                    XmlElement tags = CreateTags(n.TagsId);
                    note.AppendChild(tags);
                    newElem.AppendChild(note);
                }
            }

            return newElem;
        }

        private XmlElement CreateTags(IEnumerable<Guid> tagsId)
        {
            XmlElement tags = doc.CreateElement("Tags");
            if (tagsId != null)
            {
                foreach (Guid g in tagsId)
                {
                    XmlElement tagId = doc.CreateElement("TagId");
                    tagId.InnerText = g.ToString();
                    tags.AppendChild(tagId);
                }
            }

            return tags;
        }


        private List<PhoneNumber> GetPhoneNumbers(XmlNodeList phoneNodes)
        {            
                List<PhoneNumber> _phoneNumbers = new List<PhoneNumber>();

                foreach (XmlNode node in phoneNodes)
                {
                    _phoneNumbers.Add(new PhoneNumber()
                    {
                        PhoneNumberId = Guid.Parse(node.Attributes["PhoneNumberId"].Value),
                        Name = node.Attributes["Name"].Value,
                        Phone = node.Attributes["Phone"].Value,
                        RecordId = Guid.Parse(node.Attributes["RecordId"].Value)

                    });
                }
                return _phoneNumbers;            
        }

        private List<Note> GetNotes(XmlNodeList noteNodes)
        {
            List<Note> notes = new List<Note>();
            foreach (XmlNode node in noteNodes)
            {
                List<Guid> tagsId = GetTagsId(node.SelectNodes(".//TagId"));
                notes.Add(new Note()
                {
                    NoteId = Guid.Parse(node.Attributes["NoteId"].Value),
                    Description = node.Attributes["Description"].Value,
                    RecordId = Guid.Parse(node.Attributes["RecordId"].Value),
                    TagsId = tagsId
                });
            }
            return notes;
        }

        private List<Guid> GetTagsId(XmlNodeList nodes)
        {
            List<Guid> tagsId = new List<Guid>();
            foreach (XmlNode node in nodes)
            {
                tagsId.Add(

                     Guid.Parse(node.InnerText)

                );
            }
            return tagsId;
        }



        
    }
}
