﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace WcfService
{
    [DataContract]
    public class UserDataContract
    {
       
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string Password { get; set; }                
        [DataMember]
        public Guid RoleId { get; set; }

    }
}