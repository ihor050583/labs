﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WcfService
{
    [DataContract]
    public class PhoneDataContract
    {
        [DataMember]
        public Guid PhoneNumberId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public Guid RecordId { get; set; }

    }
}
