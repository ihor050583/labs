﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WcfService
{
    [DataContract]
    public class TagDataContract
    {
        [DataMember]
        public Guid TagId { get; set; }
        [DataMember]
        public string Name { get; set; }
    }
}
