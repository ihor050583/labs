﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WcfService
{
    [DataContract]
    public class RecordDataContract
    {
        [DataMember]
        public Guid RecordId { get; set; }
        [DataMember]
        public string FirstName
        {
            get;
            set;
        }
        [DataMember]
        public string SecondName
        {
            get;
            set;
        }
        [DataMember]
        public DateTime LastChangeTime
        {
            get;
            set;
        }
    }
}
