﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WcfService
{
    [DataContract]
    public class NoteDataContract
    {
        [DataMember]
        public Guid NoteId { get; set; }
        [DataMember]
        public string Description { get; set; }
        [DataMember]
        public List<Guid> TagsId { get; set; }  
        [DataMember]
        public List<string> Tags { get; set; }       
        [DataMember]
        public Guid RecordId { get; set; }

    }
}
