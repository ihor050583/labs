﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LabSolution.AddressBook.Users;

namespace WcfService.Services
{
    public class UserService : IUserService
    {
        public List<UserDataContract> GetAllUsers()
        {
            CoreUserRepository core = new CoreUserRepository();            
            List<UserDataContract> users = new List<UserDataContract>();

            foreach (User user in core.GetAll())
            {
                users.Add(new UserDataContract() 
                {
                   
                    Name = user.Name,
                    Email = user.Email,
                   
                    Password = user.Password,
                    RoleId = user.RoleId                
                });
            }

            return users;
        }
    }
}
