﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LabSolution.AddressBook.Concrete;
using LabSolution.AddressBook.Entities;
using LabSolution.AddressBook.CoreModels;
using LabSolution.AddressBook.Infrastructure;
using LabSolution.AddressBook.Users;
using WcfService.ServiceModels;
using log4net;

namespace WcfService
{
    public class AddressBookService : IAddressBookService
    {
        private static DataManager manager = new DataManager();          
        public List<RecordDataContract> GetRecords()
        {
            List<RecordDataContract> objects = new List<RecordDataContract>();
            foreach (Record obj in manager.Records)
            {
                objects.Add(new RecordDataContract() { RecordId = obj.RecordId, FirstName = obj.FirstName, SecondName = obj.SecondName, LastChangeTime = obj.LastChangeTime });
            }
            return objects;            
        }

        public void DeleteRecord(Guid id)
        {
            manager.DeleteRecord(id);
        }

        public void SaveRecord(RecordDataContract entity)
        {            
            Record record = new Record() { RecordId = entity.RecordId, FirstName = entity.FirstName,
                                           SecondName = entity.SecondName,
                                           LastChangeTime = entity.LastChangeTime
            };

            manager.SaveRecord(record);
        }

        public List<PhoneDataContract> GetPhones()
        {
            List<PhoneDataContract> objects = new List<PhoneDataContract>();
            foreach (PhoneNumber obj in manager.Phones)
            {
                objects.Add(new PhoneDataContract() { PhoneNumberId = obj.PhoneNumberId, 
                    Name = obj.Name, Phone = obj.Phone, RecordId = obj.RecordId });
            }
            return objects;
        }

        public void DeletePhone(Guid id)
        {
            manager.DeletePhone(id);
        }

        public void SavePhone(PhoneDataContract entity)
        {
            PhoneNumber phone = new PhoneNumber()
            {
                PhoneNumberId = entity.PhoneNumberId,
                Name = entity.Name,
                Phone = entity.Phone,
                RecordId = entity.RecordId
            };
            manager.SavePhone(phone);

        }

        public List<NoteDataContract> GetNotes()
        {
            List<NoteDataContract> objects = new List<NoteDataContract>();
            foreach (Note obj in manager.Notes)
            {
                objects.Add(new NoteDataContract() { Description = obj.Description, NoteId = obj.NoteId, RecordId = obj.RecordId, Tags = obj.Tags});
            }
            return objects;
        }

        public void DeleteNote(Guid id)
        {
            manager.DeleteNote(id);
        }

        public void SaveNote(NoteDataContract entity)
        {
            Note note = new Note();
            note.NoteId = entity.NoteId;
            note.Description = entity.Description;
            note.Tags = entity.Tags;
            note.RecordId = entity.RecordId;
            note.TagsId = entity.TagsId;
            manager.SaveNote(note);
        }

        public bool SaveTagForNote(Guid notedId, string name)
        {
            return manager.SaveTagForNote(notedId, name);
        }

        public bool DeleteTagForNote(Guid notedId, string name)
        {
            return manager.DeleteTagForNote(notedId, name);
        }

        public List<TagDataContract> GetTags()
        {
            List<TagDataContract> objects = new List<TagDataContract>();
            foreach (Tag obj in manager.Tags)
            {
                objects.Add(new TagDataContract() { Name = obj.Name, TagId = obj.TagId });
            }
            return objects;  
        }

        public RepositoryDetailsDataContract GetCurentRepository()
        {
            CoreRepositoryDetails curentRepository = manager.GetCurentRepository();
            RepositoryDetailsDataContract rep = null;
            if (curentRepository != null)
            {
                rep = new RepositoryDetailsDataContract();
                rep.Name = curentRepository.Name;
                rep.Path = curentRepository.Path;
                rep.Type = curentRepository.Type;
            }
            return rep;
        }

        public bool SwitchRepository(string serverPath, RepositoryDetailsDataContract newRepo)
        {
            CoreRepositoryDetails repo = new CoreRepositoryDetails();
            repo.Name = newRepo.Name;
            repo.Path = newRepo.Path;
            repo.Type = newRepo.Type;
            return manager.SwitchRepository(serverPath, repo);
        }

        public List<RecordDataContract> AdvancedSearch(SearchModelDataContract model)
        {
            CoreSearchModel searchModel = new CoreSearchModel();
            searchModel.FirstName = model.FirstName;
            searchModel.SecondName = model.SecondName;
            searchModel.LastChangeTime = model.LastChangeTime;
            searchModel.Sign = model.Sign;

            List<RecordDataContract> objects = new List<RecordDataContract>();
            List<Record> rec = manager.AdvancedSearch(searchModel);
            foreach (Record obj in rec)
            {
                objects.Add(new RecordDataContract() { RecordId = obj.RecordId, FirstName = obj.FirstName, SecondName = obj.SecondName, LastChangeTime = obj.LastChangeTime });
            }
            return objects; 

        }

        public List<RecordDataContract> SimpleSearch(string name)
        {
            List<RecordDataContract> objects = new List<RecordDataContract>();
            List<Record> rec = manager.SimpleSearch(name);
            foreach (Record obj in rec)
            {
                objects.Add(new RecordDataContract() { RecordId = obj.RecordId, FirstName = obj.FirstName, SecondName = obj.SecondName, LastChangeTime = obj.LastChangeTime });
            }
            return objects;     
        }

        public void Serialize(string fullFilePath)
        {
            manager.Serialize(fullFilePath);
        }


        public List<UserDataContract> GetAllUsers()
        {
            CoreUserRepository core = new CoreUserRepository();
            List<UserDataContract> users = new List<UserDataContract>();

            foreach (User user in core.GetAll())
            {
                users.Add(new UserDataContract()
                {                  
                    Name = user.Name,
                    Email = user.Email,                   
                    Password = user.Password,
                    RoleId = user.RoleId
                });
            }

            return users;
        }

        public void AddUser(UserDataContract userDataContract)
        {
            CoreUserRepository core = new CoreUserRepository();
            User user = new User()
            { 
                Name = userDataContract.Name,
                Password = userDataContract.Password,
                Email = userDataContract.Email,
                RoleId = userDataContract.RoleId
            };

            core.Save(user);
        }

        public List<RepositoryDataContract> GetRepositories()
        {
            RepositoryManager manager = new RepositoryManager();

            List<RepositoryDataContract> repositories = new List<RepositoryDataContract>();

            foreach (Repository item in manager.GetRepositories())
            {
                repositories.Add(new RepositoryDataContract() { Name = item.Name, Type = item.Type, Path = item.Path });
            }
            return repositories;
        }

        public void AddRepository(RepositoryDataContract model, string filePath)
        {
            RepositoryManager manager = new RepositoryManager();
            Repository repo = new Repository() { Name = model.Name, Path=model.Path, Type = model.Type};
            manager.AddRepository(repo, filePath);
            
        }
        
    }
}
