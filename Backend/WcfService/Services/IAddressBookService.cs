﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using LabSolution.AddressBook.Users;
using WcfService.ServiceModels;
using LabSolution.AddressBook.Infrastructure;


namespace WcfService
{
    [ServiceContract]
    public interface IAddressBookService
    {
        [OperationContract]
        List<RecordDataContract> GetRecords();
        [OperationContract]
        void DeleteRecord(Guid id);
        [OperationContract]
        void SaveRecord(RecordDataContract entity);


        [OperationContract]
        List<PhoneDataContract> GetPhones();
        [OperationContract]
        void DeletePhone(Guid id);
        [OperationContract]
        void SavePhone(PhoneDataContract entity);

        [OperationContract]
        List<NoteDataContract> GetNotes();
        [OperationContract]
        void DeleteNote(Guid id);
        [OperationContract]
        void SaveNote(NoteDataContract entity);

        [OperationContract]
        bool SaveTagForNote(Guid notedId, string name);
        [OperationContract]
        bool DeleteTagForNote(Guid notedId, string name);

        [OperationContract]
        List<TagDataContract> GetTags();

        [OperationContract]
        RepositoryDetailsDataContract GetCurentRepository();
        [OperationContract]
        bool SwitchRepository(string serverPath, RepositoryDetailsDataContract newRepo);

        [OperationContract]
        List<RecordDataContract> AdvancedSearch(SearchModelDataContract model);
        [OperationContract]
        List<RecordDataContract> SimpleSearch(string name);
        [OperationContract]
        void Serialize(string fullFilePath);

        [OperationContract]
        List<UserDataContract> GetAllUsers();
        [OperationContract]
        void AddUser(UserDataContract user);

        [OperationContract]
        List<RepositoryDataContract> GetRepositories();
        [OperationContract]
        void AddRepository(RepositoryDataContract model, string filePath);
    }
}
