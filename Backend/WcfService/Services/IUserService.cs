﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace WcfService.Services
{
     [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        List<UserDataContract> GetAllUsers();
    }
}
